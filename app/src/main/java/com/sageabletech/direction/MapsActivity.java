package com.sageabletech.direction;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.droidmentor.locationhelper.Constants;
import com.droidmentor.locationhelper.GetLocation;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;
import org.w3c.dom.Text;

import com.sageabletech.R;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.AddTourScreen;
import com.sageabletech.utils.LocalData;
import com.sageabletech.viewtour.ViewTourGridList;
import com.sageabletech.webservicecalls.GPSTracker;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.R.attr.label;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    ArrayList markerPoints = new ArrayList();
    TextView textView4;
    ImageView image,leftdirection;
    String newToDirections="";
    ImageView interchange;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dialog=new ProgressDialog(MapsActivity.this);
        dialog.setMessage("Loading..");
        dialog.show();
        setContentView(R.layout.directions);

        textView4 = (TextView)findViewById(R.id.textView4);
        leftdirection = (ImageView)findViewById(R.id.leftdirection);
        interchange = (ImageView)findViewById(R.id.interchange);



         newToDirections = LocalData.getInstance().getNewToDirections();


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        getHeader();
    }

    public  void doForLeftCont(){
        if(!newToDirections.equalsIgnoreCase("yes")){
            MyTourItemDO favselectedItem = LocalData.getInstance().getFavselectedItem();
            ArrayList<TourItem> subtourleft = favselectedItem.subtour;
            int directionNumber = LocalData.getInstance().getDirectionroutenumber();
            TourItem tourItemdirection = subtourleft.get(directionNumber);
            textView4.setVisibility(View.GONE);
            leftdirection.setVisibility(View.VISIBLE);
            Picasso.with(this)
                    .load(tourItemdirection.ImagePath)
                    .fit()
                    .into(leftdirection, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {


                        }
                    });


        }else{
            textView4.setVisibility(View.GONE);
            leftdirection.setVisibility(View.VISIBLE);

            MyTourItemDO favselectedItem = LocalData.getInstance().getFavselectedItem();
            ArrayList<TourItem> subtourleft = favselectedItem.subtour;
            int directionNumber = LocalData.getInstance().getDirectionroutenumber();
            TourItem tourItemdirection = subtourleft.get(directionNumber);
            textView4.setVisibility(View.GONE);
            leftdirection.setVisibility(View.VISIBLE);
            Picasso.with(this)
                    .load(tourItemdirection.ImagePath)
                    .fit()
                    .into(leftdirection, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {


                        }
                    });
        }
    }
    public void doforRightDirection(){
        int getDirectionRight = LocalData.getInstance().getGetDirectionRight();
        MyTourItemDO favselectedItem = LocalData.getInstance().getFavselectedItem();
        if(favselectedItem!=null) {
            TourItem tourItem = favselectedItem.subtour.get(getDirectionRight);

            Picasso.with(this)
                    .load(tourItem.ImagePath)
                    .fit()
                    .into(image, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {


                        }
                    });
        }

    }

    TextView header;
    ImageView back;
    ImageView submit;

    public void getHeader() {
        header = (TextView) findViewById(R.id.header_text);
        header.setText("");
        back = (ImageView) findViewById(R.id.back);
        back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_back_white));
        submit = (ImageView) findViewById(R.id.submit);

        submit.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_mapview_outline_white));
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyTourItemDO favselectedItem = LocalData.getInstance().getFavselectedItem();
                ArrayList<TourItem> subtour = favselectedItem.subtour;
                int selectedTourNumber = LocalData.getInstance().getGetDirectionRight();
                TourItem tourItem = subtour.get(selectedTourNumber);


                String tourLangitude = tourItem.longitude;
                String tourLatitude = tourItem.latitude;


                if (tourLangitude == null || tourLangitude.equalsIgnoreCase("") || tourLangitude.equalsIgnoreCase("0")) {
                    tourLatitude=favselectedItem.tourLatitude;
                    tourLangitude = favselectedItem.tourLangitude;
                    System.out.println("tourLatitude....innnewr::"+tourLangitude);

                }
//                System.out.println("tourLatitude....innnewr:1:"+leftlatitude+"::"+leftlongitude);
//                System.out.println("directionUrl::"+"http://maps.google.com/maps?saddr="+leftlatitude+","+leftlongitude+"&daddr="+rightlatitude+","+rightlongitude);
//                String substring = directionUrl.substring(0, directionUrl.indexOf("&sensor="));
//                System.out.println("substring::"+substring);


                Intent intent = new Intent(Intent.ACTION_VIEW,
//                        https://maps.googleapis.com/maps/api/directions/json?origin=11.9138598,79.8144722&destination=11.9138598,79.8144722&sensor=false&mode=driving
                        Uri.parse("http://maps.google.com/maps?saddr="+leftlatitude+","+leftlongitude+"&daddr="+rightlatitude+","+rightlongitude));
//                        Uri.parse(directionUrl));
                                startActivity(intent);



//                Uri.Builder builder = new Uri.Builder();
//                builder.scheme("https")
//                        .authority("www.google.com").appendPath("maps").appendPath("dir").appendPath("").appendQueryParameter("api", "1")
//                        .appendQueryParameter("destination", tourLatitude + "," + tourLangitude);
//                String url = builder.build().toString();
//                Log.d("Directions", url);
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                startActivity(i);

            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });

        image = (ImageView)findViewById(R.id.tourImage);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                finish();
                LocalData.getInstance().setFromWhere("map");
                Intent intent=new Intent(MapsActivity.this,ViewTourGridList.class);
                startActivity(intent);

            }
        });
        textView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                finish();
                LocalData.getInstance().setFromWhere("mapleft");
                Intent intent=new Intent(MapsActivity.this,ViewTourGridList.class);
                startActivity(intent);

            }
        });
        leftdirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                finish();
                LocalData.getInstance().setFromWhere("mapleft");

                Intent intent=new Intent(MapsActivity.this,ViewTourGridList.class);
                startActivity(intent);

            }
        });
        doforRightDirection();
        doForLeftCont();
        interchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(!newToDirections.equalsIgnoreCase("yes")) {
                    int right = LocalData.getInstance().getGetDirectionRight();
                    int left = LocalData.getInstance().getDirectionroutenumber();
                    LocalData.getInstance().setGetDirectionRight(left);
                    LocalData.getInstance().setDirectionroutenumber(right);
                    doForLeftCont();
                    doforRightDirection();
                    addDirections();
//                }
            }
        });



    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
//        makeDirections();
       addDirections();


    }
    public String directionUrl="";
    public void addDirections(){
        MyTourItemDO favselectedItem = LocalData.getInstance().getFavselectedItem();
        ArrayList<TourItem> subtour = favselectedItem.subtour;
        int selectedTourNumber = LocalData.getInstance().getSelectedTourNumber();
        TourItem tourItem = subtour.get(selectedTourNumber);
        String tourLangitude = tourItem.longitude;
        String tourLatitude = tourItem.latitude;
        System.out.println("tourLatitude::::"+tourLangitude);

        if (tourLangitude == null || tourLangitude.equalsIgnoreCase("") || tourLangitude.equalsIgnoreCase("0")) {
            tourLangitude = favselectedItem.tourLangitude;
            tourLatitude = favselectedItem.tourLatitude;

        }
        System.out.println("tourLatitude::::"+tourLangitude);

        LatLng latLng = new LatLng(Double.parseDouble(tourLatitude),
                Double.parseDouble(tourLangitude));

        mMap.addMarker(new MarkerOptions().position(latLng).title("Destination"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
        makeDirections();
    }
    double leftlatitude=0;
    double leftlongitude=0;
    double rightlatitude=0;
    double rightlongitude=0;

    LatLngBounds bounds ;
    public void makeDirections() {
        LatLng latLng;
        if (markerPoints.size() > 1) {
            markerPoints.clear();
            mMap.clear();
        }

        MyTourItemDO favselectedItem = LocalData.getInstance().getFavselectedItem();
        ArrayList<TourItem> subtour = favselectedItem.subtour;
        int selectedTourNumber = LocalData.getInstance().getGetDirectionRight();
        TourItem tourItem = subtour.get(selectedTourNumber);


        String tourLangitude = tourItem.longitude;
        String tourLatitude = tourItem.latitude;

        System.out.println("tourLatitude::::"+tourLangitude);

        if (tourLangitude == null || tourLangitude.equalsIgnoreCase("") || tourLangitude.equalsIgnoreCase("0")) {

            tourLangitude = favselectedItem.tourLangitude;
            tourLatitude = favselectedItem.tourLatitude;

        }
        rightlatitude=Double.parseDouble(tourLatitude);
        rightlongitude=Double.parseDouble(tourLangitude);

        latLng = new LatLng(Double.parseDouble(tourLatitude), Double.parseDouble(tourLangitude));
        markerPoints.add(latLng);
        MarkerOptions options = new MarkerOptions();
        options.position(latLng);

//        Bitmap mainTourImage = image.getDrawingCache();
        Bitmap bitMap = getBitMap(tourItem.ImagePath);

        if(bitMap!=null) {
            BitmapDescriptorFactory.fromBitmap(bitMap);
//            Bitmap smallMarker = Bitmap.createScaledBitmap(mainTourImage, 60, 60, false);
            BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitMap);
            options.icon(bitmapDescriptor);
        }else{

            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }





        mMap.addMarker(options);


        if(newToDirections.equalsIgnoreCase("yes")) {
//            GetLocation getLocation=new GetLocation(MapsActivity.this);
//            getLocation.getLocation();
//            double longitude = Constants.getInstance().getLongitude();
//            System.out.println("longitude:new:"+longitude);
            ArrayList<TourItem> subtourleft = favselectedItem.subtour;
            int directionNumber = LocalData.getInstance().getDirectionroutenumber();
            TourItem tourItemdirection = subtourleft.get(directionNumber);

            String tourLangitudedirection = tourItemdirection.longitude;
            String tourLatitudedirection = tourItemdirection.latitude;
            if (tourLangitudedirection == null || tourLangitudedirection.equalsIgnoreCase("") || tourLangitudedirection.equalsIgnoreCase("0")) {

                tourLangitudedirection = favselectedItem.tourLangitude;
                tourLatitudedirection = favselectedItem.tourLatitude;

            }

            leftlatitude=Double.parseDouble(tourLatitudedirection);
            leftlongitude=Double.parseDouble(tourLangitudedirection);


            latLng = new LatLng(Double.parseDouble(tourLatitudedirection), Double.parseDouble(tourLangitudedirection));
            markerPoints.add(latLng);
            MarkerOptions options1 = new MarkerOptions();
            options1.position(latLng);

            Bitmap bitMapDest = getBitMap(tourItemdirection.ImagePath);

            if(bitMapDest!=null) {
                BitmapDescriptorFactory.fromBitmap(bitMapDest);
//            Bitmap smallMarker = Bitmap.createScaledBitmap(mainTourImage, 60, 60, false);
                BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitMapDest);
                options1.icon(bitmapDescriptor);
            }else{

                options1.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            }


            mMap.addMarker(options1);

        }else{

            ArrayList<TourItem> subtourleft = favselectedItem.subtour;
            int directionNumber = LocalData.getInstance().getDirectionroutenumber();
            TourItem tourItemdirection = subtourleft.get(directionNumber);
            System.out.println("sdfsdfdsf elseeee");

            String tourLangitudedirection = tourItemdirection.longitude;
            String tourLatitudedirection = tourItemdirection.latitude;
            System.out.println("tourLatitude::::"+tourLangitudedirection);

            if (tourLangitudedirection == null || tourLangitudedirection.equalsIgnoreCase("") || tourLangitudedirection.equalsIgnoreCase("0")) {

                tourLangitudedirection = favselectedItem.tourLangitude;
                tourLatitudedirection = favselectedItem.tourLatitude;

            }
            leftlatitude=Double.parseDouble(tourLatitudedirection);
            leftlongitude=Double.parseDouble(tourLangitudedirection);
            latLng = new LatLng(Double.parseDouble(tourLatitudedirection), Double.parseDouble(tourLangitudedirection));
            markerPoints.add(latLng);
            MarkerOptions options1 = new MarkerOptions();
            options1.position(latLng);

            Bitmap bitMapDest = getBitMap(tourItemdirection.ImagePath);

            if(bitMapDest!=null) {
                BitmapDescriptorFactory.fromBitmap(bitMapDest);
//            Bitmap smallMarker = Bitmap.createScaledBitmap(mainTourImage, 60, 60, false);
                BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitMapDest);
                options1.icon(bitmapDescriptor);
            }else{

                options1.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            }


            mMap.addMarker(options1);

        }


        // Add new marker to the Google Map Android API V2
//        mMap.addMarker(options);

        // Checks, whether start and end locations are captured
        if (markerPoints.size() >= 2) {
            LatLng origin = (LatLng) markerPoints.get(0);
            LatLng dest = (LatLng) markerPoints.get(1);
            System.out.println("lat long:::="+origin+"::::+"+dest);
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(origin);
            builder.include(dest);
             bounds = builder.build();

            System.out.println("origin::"+origin.latitude);
            System.out.println("dest::"+dest.latitude);

            // Getting URL to the Google Directions API
            String url = getDirectionsUrl(origin, dest);
            directionUrl=url;

            DownloadTask downloadTask = new DownloadTask();

            // Start downloading json data from Google Directions API
            downloadTask.execute(url);
        }


    }
    ProgressDialog dialog;

    private class DownloadTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dialog=new ProgressDialog(MapsActivity.this);
//            dialog.setMessage("please wait..");
//            dialog.show();
        }

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();


            parserTask.execute(result);

        }
    }


    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            System.out.println("result:::"+result.size());
            dialog.dismiss();
            ArrayList points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(Color.BLUE);
                lineOptions.geodesic(true);

            }

// Drawing polyline in the Google Map for the i-th route
            if(lineOptions!=null)
            mMap.addPolyline(lineOptions);
            if(bounds!=null)
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 20));
        }
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    public Bitmap getBitMap(String path){
        Bitmap nextImageBitmap=null;

        System.out.println("sdsdsdsddsdsfdfsgdfgfg");
            try {
                URL url = new URL(path);
                nextImageBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                return  getResizedBitmap(nextImageBitmap,80);
            } catch (IOException e) {
                System.out.println(e);
            }
            return getResizedBitmap(nextImageBitmap,80) ;

    }
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        System.out.println("resizeeee:::"+maxSize);
        if(image!=null) {
            int width = image.getWidth();
            int height = image.getHeight();

            float bitmapRatio = (float) width / (float) height;
            if (bitmapRatio > 1) {
                width = maxSize;
                height = (int) (width / bitmapRatio);
            } else {
                height = maxSize;
                width = (int) (height * bitmapRatio);
            }
            return Bitmap.createScaledBitmap(image, width, height, true);
        }else{
            Drawable drawable = getResources().getDrawable(R.mipmap.app_icon);
             image      = ((BitmapDrawable) drawable).getBitmap();

            int width = image.getWidth();
            int height = image.getHeight();

            float bitmapRatio = (float) width / (float) height;
            if (bitmapRatio > 1) {
                width = maxSize;
                height = (int) (width / bitmapRatio);
            } else {
                height = maxSize;
                width = (int) (height * bitmapRatio);
            }
            return Bitmap.createScaledBitmap(image, width, height, true);
        }

    }
//    public void dropPin(){
//        try {
//            Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                    Uri.parse("geo:" + AppointmentDetailLayout.docLatitude
//                            + "," + AppointmentDetailLayout.docLongitude
//                            + "?q=" + AppointmentDetailLayout.docLatitude
//                            + "," + AppointmentDetailLayout.docLongitude
//                            + "(" + label + ")"));
//            intent.setComponent(new ComponentName(
//                    "com.google.android.apps.maps",
//                    "com.google.android.maps.MapsActivity"));
//            startActivity(intent);
//        } catch (ActivityNotFoundException e) {
//
//            try {
//                startActivity(new Intent(
//                        Intent.ACTION_VIEW,
//                        Uri.parse("market://details?id=com.google.android.apps.maps")));
//            } catch (android.content.ActivityNotFoundException anfe) {
//                startActivity(new Intent(
//                        Intent.ACTION_VIEW,
//                        Uri.parse("http://play.google.com/store/apps/details?id=com.google.android.apps.maps")));
//            }
//
//            e.printStackTrace();
//        }
//    }

    @Override
    public void onBackPressed() {
        LocalData.getInstance().setFromWhere("");
        Intent intent=new Intent(MapsActivity.this,ViewTourGridList.class);
        startActivity(intent);
    }
}
