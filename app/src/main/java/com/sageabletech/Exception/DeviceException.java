package com.sageabletech.Exception;



/**
 * Created by arige on 7/8/2017.
 */

public class DeviceException extends Exception {

    public DeviceException(String message){
        super(message);
    }
}
