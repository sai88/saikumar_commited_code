package com.sageabletech.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by WIN 7 on 4/17/2017.
 */
public class FeedsDO implements Serializable{

    public String tourId = "";
    public String userId = "";
    public String userName = "";
    public String tourTitle = "";
    public String viewCount = "";
    public String tourPhotoPath = "";
    public String favcount = "";

    public String tourSummary = "";
    public String tourLatitude = "";
    public String tourLangitude = "";
    public String commentsCount = "";
    public String tourOrderNumber = "";
    public String userPicPath = "";
    public String tourTime = "";
    public String isfavorite = "";
    public ArrayList<String> catList=new ArrayList<>();
    public ArrayList<String> tagList=new ArrayList<>();


}
