package com.sageabletech.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Uday on 11-07-2017.
 */

public class GetTourDO implements Serializable {
    public String tourId = "";
    public String userId = "";
    public String title = "";
    public String summary = "";
    public String address = "";
    public String area = "";
    public String city = "";
    public String areacode = "";
    public String tourLatitude = "";
    public String tourLangitude = "";
    public String tourPhotoPath = "";
    public String tourPhotoThumb = "";
    public String tourPhotoThumbExt = "";
    public String createdOn = "";
    public String tourPhotoExt = "";
    public String comments = "";
    public String viewCount = "";
    public String draftTourFlag = "";
    public ArrayList<SubTourItemsDO> subList= null;

}
