package com.sageabletech.model;

import java.io.Serializable;

/**
 * Created by Asman-2 on 19-07-2017.
 */
public class CategoryDO implements Serializable {


    public String categorieId="";
    public String categorieName="";
    public String categorieImageLocation="";
    public boolean isSelect = false;

}
