package com.sageabletech.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by WIN 7 on 4/17/2017.
 */
public class AddTourDO implements Serializable{

    public String title = "";
    public String tourIcon = "";
    public String tourSummary = "";
    public String tourLocation = "";
    public String status = "";
    public double lat = 0;
    public double lang = 0;
    public ArrayList<TourItem> tourPhotos = new ArrayList<>();
    public boolean isModified = false;
    public String date;


    public String tourId = "";
}
