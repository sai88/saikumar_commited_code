package com.sageabletech.model;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by WIN 7 on 4/17/2017.
 */
public class TourItem implements Serializable{

    public String ImagePath = "";
    public String audioPath = "";
    public String text = "";
    public String title = "";
    public String summary = "";
    public boolean isModified = false;
    public String photoId = "";
    public String location = "";
    public String tourId = "";
    public String latitude="";
    public  String longitude="";
    public String photLat="";
    public String photLng="";
    public String lat = "";
    public String lang = "";
    public String audioTime="";
    public String photoCaption="";
    public String createdOn="";
    public String tourLangitude="";
    public String tourLatitude="";
    public String audio_duration="";
    public  static String ADD="add";
    public Bitmap bitmap=null;




}
