package com.sageabletech.model;

import java.io.Serializable;

/**
 * Created by Uday on 14-07-2017.
 */

public class SubTourItemsDO implements Serializable {
    public String photoId = "";
    public String photoTitle = "";
    public String photoCaption = "";
    public String photoLocation = "";
    public String photoPath = "";
    public String photoThumbPath = "";
    public String photoAudioPath = "";
    public String audioTime = "";
    public String imageExtension = "";
    public String audioExtension = "";
    public String photolatitude = "";
    public String photolangitude = "";
    public String createdOn = "";
    public String tourOrderNumber = "";


}
