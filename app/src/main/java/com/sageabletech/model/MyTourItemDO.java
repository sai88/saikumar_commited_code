package com.sageabletech.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by WIN 7 on 4/17/2017.
 */
public class MyTourItemDO implements Serializable{

    public String tourId = "";
    public String userId = "";
    public String title = "";
    public String summary = "";
    public String area = "";
    public String city = "";
    public String areacode = "";
    public String tourLatitude = "";
    public String tourLangitude = "";
    public String tourPhotoPath = "";
    public String createdOn = "";
    public String tourPhotoExt = "";
    public String photos = "";
    public String comments = "";
    public String tourRating = "";
    public String viewCount = "";
    public String location = "";
    public String address="";
    public String status="";
    public String unPublishFlag="";



    public ArrayList<TourItem> subtour = new ArrayList<>();
    public ArrayList<String> catArry = new ArrayList<>();
    public ArrayList<String> tagArry = new ArrayList<>();

    public ArrayList<String> catArryId = new ArrayList<>();
    public ArrayList<String> tagArryId = new ArrayList<>();
}
