package com.sageabletech.model;

import java.io.Serializable;

/**
 * Created by Uday on 20-07-2017.
 */

public class EditProfileDO implements Serializable {
    public String email = "";
    public String userId = "";
    public String firstName = "";
    public String lastName = "";
    public String userPic = "";
    public String userLoginId = "";

}
