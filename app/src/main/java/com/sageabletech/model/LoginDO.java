package com.sageabletech.model;

import java.io.Serializable;

/**
 * Created by WIN 7 on 4/12/2017.
 */
public class LoginDO implements Serializable{

    public String userId = "";
    public String emailId = "";
    public String role = "";
    public String firstName = "";
    public String lastName = "";
    public String accessToken = "";
    public String resetPwd = "";
    public String password = "";
    public String userPicPath="";
    public String active = "";
    public String message = "";
    public String location="";
    public String email="";

}
