package com.sageabletech.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.sageabletech.Exception.DeviceException;
import com.sageabletech.R;
import com.sageabletech.fragment.TermsAndConditionsActivity;
import com.sageabletech.shomiround.BaseActivity;
import com.sageabletech.shomiround.MainScreen;
import com.sageabletech.utils.DeviceInfo;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.Urls;
import com.sageabletech.utils.Utils;
import com.sageabletech.webservicecalls.AsyncRemoteCall;
import com.sageabletech.webservicecalls.RequestParams;
import com.sageabletech.webservicecalls.ServiceCalls;

import java.util.regex.Pattern;

/**
 * Created by arige on 7/1/2017.
 */

public class SettingsActivity extends BaseActivity implements AsyncRemoteCall.OnDataListener {

    private TextView changePasswordTV, privacyPolicyTV, feedbackTV,terms_conditions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        TextView passwordTitle=(TextView)findViewById(R.id.passwordTitle);
        TextView privacytitle=(TextView)findViewById(R.id.privacytitle);
        TextView termstitle=(TextView)findViewById(R.id.termstitle);
        passwordTitle.setTypeface(Utils.getHelvaticaBold(SettingsActivity.this));
        privacytitle.setTypeface(Utils.getHelvaticaBold(SettingsActivity.this));
        termstitle.setTypeface(Utils.getHelvaticaBold(SettingsActivity.this));



        init();

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        LocalData.getInstance().setIsFromProfile("yes");
        Intent intent=new Intent(SettingsActivity.this,MainScreen.class);
        startActivity(intent);

    }
TextView tvTitle;
    ImageView right;
    ImageView back;
    public void init(){


        tvTitle=(TextView)findViewById(R.id.header_text);
        tvTitle.setTypeface(Utils.getHelvaticaBold(SettingsActivity.this));

        tvTitle.setText("Settings");
        right = (ImageView) findViewById(R.id.submit);
        right.setVisibility(View.GONE);





        back =(ImageView)findViewById(R.id.back);
        back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_back_white));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalData.getInstance().setIsFromProfile("yes");
                Intent intent=new Intent(SettingsActivity.this,MainScreen.class);
                startActivity(intent);
            }
        });
        changePasswordTV = (TextView) findViewById(R.id.id_change_password);
        changePasswordTV.setTypeface(Utils.getHelvaticaBold(SettingsActivity.this));
        privacyPolicyTV = (TextView) findViewById(R.id.id_privacy_policy);
        privacyPolicyTV.setTypeface(Utils.getHelvaticaBold(SettingsActivity.this));
        terms_conditions = (TextView) findViewById(R.id.terms_conditions);
        terms_conditions.setTypeface(Utils.getHelvaticaBold(SettingsActivity.this));
        feedbackTV = (TextView) findViewById(R.id.id_feedback);
        feedbackTV.setTypeface(Utils.getHelvaticaBold(SettingsActivity.this));


        changePasswordTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //GOTO change password screen
                startActivity(new Intent(SettingsActivity.this, ChangePasswordActivity.class));
            }
        });

        privacyPolicyTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //GOTO privacy policy screen
               /* String accessToken = preferenceUtils.getStringFromPreference("accessToken",null);
                String url = "http://35.154.128.56:8080/privacyPolicy/"+accessToken;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);*/

                privacyPolicyCall();

            }
        });

        terms_conditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //GOTO privacy policy screen
               /* String accessToken = preferenceUtils.getStringFromPreference("accessToken",null);
                String url = "http://35.154.128.56:8080/privacyPolicy/"+accessToken;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);*/

                Intent intent=new Intent(SettingsActivity.this, TermsAndConditionsActivity.class);
                startActivity(intent);

            }
        });
        feedbackTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /* Create the Intent to send feedback*/
                final Intent emailIntent = new Intent(Intent.ACTION_SEND);

                /* Fill it with Data */
                emailIntent.setType("plain/text");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"shomiround@gmail.com"});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback for Shomiround Android");
                emailIntent.putExtra(Intent.EXTRA_TEXT, deviceInfoInMail());

                /* Send it off to the Activity-Chooser */
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            }
        });
    }

    //TODO Get device Information
    private String deviceInfoInMail(){
        String data = new String();
        try {
            data="\n\n\n\n\n\n\n\n\nDevice Info: \n"+"Model : "+DeviceInfo.getDeviceName()+" \n"+ "OS version : "+DeviceInfo.getOSVersionName()+ DeviceInfo.getOSVersionNo()+"\n"+"App version : "+DeviceInfo.getAppVersion();
        } catch (DeviceException e) {
            e.printStackTrace();
        }
        return data;
    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void privacyPolicyCall(){
        showLoader();
        RequestParams requestParams = new RequestParams();


        String accessToken = this.preferenceUtils.getStringFromPreference("accessToken",null);
        String url = Urls.PRIVACY_POLICY+accessToken;
        requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
        requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;
        ServiceCalls serviceCalls = new ServiceCalls(this, "PRIVACY_POLICY");
        serviceCalls.execute(requestParams);
    }

    @Override
    public void onData(Object data, String error, String tag) {
        if("PRIVACY_POLICY".equalsIgnoreCase(tag)){
            if(error == null){
                hideLoader();
                Intent i = new Intent(this,PrivacyPolicy.class);
                i.putExtra("PRIVACY_POLICY",data.toString());
                startActivity(i);
            } else{
                hideLoader();
                Toast.makeText(this,"Temporarily service is not available!",Toast.LENGTH_SHORT).show();
            }
        }


    }


}
