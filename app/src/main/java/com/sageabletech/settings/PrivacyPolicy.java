package com.sageabletech.settings;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.sageabletech.R;
import com.sageabletech.shomiround.BaseActivity;

/**
 * Created by arige on 7/9/2017.
 */

public class PrivacyPolicy extends BaseActivity {

    private TextView tvPrivacyPolicy;


    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.privacy_policy);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back_white);

        tvPrivacyPolicy = (TextView) findViewById(R.id.id_policy);
        tvPrivacyPolicy.setText(""+getIntent().getStringExtra("PRIVACY_POLICY"));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
