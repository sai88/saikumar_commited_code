package com.sageabletech.settings;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.bumptech.glide.Glide;

import com.sageabletech.R;

import com.sageabletech.model.EditProfileDO;
import com.sageabletech.model.LoginDO;
import com.sageabletech.newclassess.FeedSearch;
import com.sageabletech.shomiround.BaseActivity;
import com.sageabletech.shomiround.FinalScreen;
import com.sageabletech.shomiround.MainScreen;
import com.sageabletech.shomiround.MyTourList;
import com.sageabletech.shomiround.TourIconSelection;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;
import com.sageabletech.utils.Utils;
import com.sageabletech.webservicecalls.AsyncRemoteCall;
import com.sageabletech.webservicecalls.RequestParams;
import com.sageabletech.webservicecalls.ServiceCalls;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import io.fabdialogmorph.DialogActivity;

/**
 * Created by arige on 7/4/2017.
 */

public class EditProfile extends BaseActivity implements AsyncRemoteCall.OnDataListener {

    private ImageView ivRight,back;
    private CircleImageView civEditProfile, civProfileEditImage;
    private EditText firstNameEditProfile, lastNameEditProfile;
    private String firstNameString, lastNameString, userPicPathString;
    //    private ProgressDialog progressDialog;
    private String profileImage;
    private EditProfileDO mEditDo;
    private String userId;
    //    private ProgressBar progressBar;
    private String allImages;
    TextView header_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);
        TextView profilepic=(TextView)findViewById(R.id.profilepic);
        TextView userdetails=(TextView)findViewById(R.id.userdetails);
        profilepic.setTypeface(Utils.getHelvaticaBold(EditProfile.this));
        userdetails.setTypeface(Utils.getHelvaticaBold(EditProfile.this));


        ivRight = (ImageView) findViewById(R.id.submit);
        back = (ImageView) findViewById(R.id.back);
        ivRight.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_save_white));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalData.getInstance().setIsFromProfile("yes");
                Intent intent=new Intent(EditProfile.this,MainScreen.class);
                startActivity(intent);
            }
        });
        header_text=(TextView)findViewById(R.id.header_text);
        header_text.setText("Edit profile");
        header_text.setTypeface(Utils.getHelvaticaMedium(EditProfile.this));





        civEditProfile = (CircleImageView) findViewById(R.id.id_edit_profile);
        civEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onImageView();
//                Intent intent = new Intent(EditProfile.this, DialogActivity.class);
//                intent.putExtra("name",profileImage);
//                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(EditProfile.this, civEditProfile,"help");
//                startActivity(intent, options.toBundle());
            }
        });
        civProfileEditImage = (CircleImageView) findViewById(R.id.id_profile_edit_image);
        firstNameEditProfile = (EditText) findViewById(R.id.firstNameEditProfile);
        lastNameEditProfile = (EditText) findViewById(R.id.lastNameEditProfile);

        mEditDo = new EditProfileDO();

//        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        civProfileEditImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(EditProfile.this, TourIconSelection.class).putExtra("From", "EditProfile"), 2003);
            }
        });

        ivRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(EditProfile.this, "Temporarily Service is not available!", Toast.LENGTH_SHORT).show();
//                mEditDo.firstName = firstNameEditProfile.getText().toString();
//                mEditDo.lastName = lastNameEditProfile.getText().toString();
                firstNameString = firstNameEditProfile.getText().toString();
                lastNameString = lastNameEditProfile.getText().toString();
                userId = preferenceUtils.getStringFromPreference("userId", "");

                if (firstNameString != null && !firstNameString.isEmpty()
                        && lastNameString != null && !lastNameString.isEmpty()) {
                    if (profileImage != null && !profileImage.isEmpty()) {
                        submitUserPic();
                    } else {
//                        userPicPathString = "";
                        serviceCallToUpdateProfile();
                    }

                } else {
                    Toast.makeText(EditProfile.this, "Please enter the fields", Toast.LENGTH_SHORT).show();
                }


            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        LocalData.getInstance().setIsFromProfile("yes");
        Intent intent=new Intent(EditProfile.this,MainScreen.class);
        startActivity(intent);

    }

    private void serviceCallToUpdateProfile() {
        showLoader();
        RequestParams requestParams = new RequestParams();
        String accessToken = this.preferenceUtils.getStringFromPreference("accessToken", null);
        String url = Urls.EDIT_PROFILE + accessToken;
        requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
        requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;

        JSONObject loginRequest = new JSONObject();
        try {
            loginRequest.put("email", preferenceUtils.getStringFromPreference("emailId", ""));
            loginRequest.put("userId", userId);
            loginRequest.put("firstName", firstNameString);
            loginRequest.put("lastName", lastNameString);
            loginRequest.put("userPic", mEditDo.userPic);
            loginRequest.put("userLoginId", "");
            requestParams.data = loginRequest.toString();
            ServiceCalls serviceCalls = new ServiceCalls(this, "EDIT_PROFILE");
            serviceCalls.execute(requestParams);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void submitUserPic() {
        AmazonS3 s3 = new AmazonS3Client(new CognitoCachingCredentialsProvider(
                EditProfile.this,
                AppConstants.COGNITO_POOL_ID,
                Regions.fromName(AppConstants.COGNITO_POOL_REGION)));
        TransferUtility transferUtility = new TransferUtility(s3, getApplicationContext());
        System.out.println("mEditDo.userPic:" + mEditDo.userPic);
        if (mEditDo.userPic.contains("file://")) {
            mEditDo.userPic = mEditDo.userPic.replace("file://", "");
        }
        final File tIcon = new File(mEditDo.userPic);
        String iconName[] = mEditDo.userPic.split("/");
        TransferObserver obsIcon = transferUtility.upload(
                AppConstants.BUCKET_NAME,
                userId + "/" + iconName[iconName.length - 1],
                tIcon
        );
//                showLoader();
        obsIcon.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
//                        Toast.makeText(FinalScreen.this,"Uploaded:"+id,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                try {
                    int percentage = (int) (bytesCurrent / bytesTotal * 100);
                    //Display percentage transfered to user
                    Log.e("TAG", "percentage : " + percentage + " " + id);

                    if (percentage == 100) {
//                            mDBCreator.updateTourIcon(tIcon.getAbsolutePath());
                    }
                } catch (ArithmeticException ae) {
                }

            }

            @Override
            public void onError(int id, Exception ex) {
                // do something
//                progressBar.setVisibility(View.GONE);
            }

        });
        mEditDo.userPic = userId + "/" + iconName[iconName.length - 1];
        try {
            serviceCallToUpdateProfile();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();

        profileImage = preferenceUtils.getProfileImageURL("SelectedImage", null);
        if (profileImage == null) {
            LoginDO loginDO = LocalData.getInstance().getLoginDO();
            String userPicPath = loginDO.userPicPath;
            profileImage = userPicPath;
            new PreferenceUtils(this).saveProfileImageURL("SelectedImage", userPicPath);
            new PreferenceUtils(this).saveString("firstName", loginDO.firstName);
            new PreferenceUtils(this).saveString("lastName", loginDO.lastName);

        }

        firstNameEditProfile.setText(preferenceUtils.getStringFromPreference("firstName", ""));
        lastNameEditProfile.setText(preferenceUtils.getStringFromPreference("lastName", ""));


        if (profileImage != null && !profileImage.isEmpty()) {
            Glide.with(this)
                    .load(profileImage)
                    .centerCrop()
                    .into(civEditProfile);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        profileImage = new PreferenceUtils(this).getProfileImageURL("SelectedImage", null);
        mEditDo.userPic = profileImage;

        if (profileImage != null && !profileImage.isEmpty()) {
            Glide.with(this)
                    .load(profileImage)
                    .centerCrop()
                    .into(civEditProfile);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onData(Object data, String error, String tag) {
        hideLoader();
        if (data != null) {
            new PreferenceUtils(this).saveString("firstName", firstNameEditProfile.getText().toString());
            new PreferenceUtils(this).saveString("lastName", lastNameEditProfile.getText().toString());
            Toast.makeText(this, "User profile is saved successfully", Toast.LENGTH_SHORT).show();
            finish();


        } else {
            Toast.makeText(EditProfile.this, "data:" + error.toString(), Toast.LENGTH_SHORT).show();
        }
    }



    public void onImageView() {
        Dialog builder = new Dialog(EditProfile.this,R.style.CustomDialog);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        ImageView imageView = new ImageView(EditProfile.this);
        imageView.setImageBitmap(getBitMap(profileImage));         //set the image in dialog popup
        //below code fullfil the requirement of xml layout file for dialoge popup

        builder.addContentView(imageView, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        builder.show();
    }



    Bitmap nextImageBitmap = null;

    public Bitmap getBitMap(String path) {
        if (nextImageBitmap == null) {
            System.out.println("sdsdsdsddsdsfdfsgdfgfg:::");
            try {
                URL url = new URL(path);
                nextImageBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                return nextImageBitmap;
            } catch (IOException e) {
                System.out.println(e);
            }
            return nextImageBitmap;
        } else {
            return nextImageBitmap;
        }

    }




}