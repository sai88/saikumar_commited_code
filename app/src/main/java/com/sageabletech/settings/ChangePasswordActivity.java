package com.sageabletech.settings;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.sageabletech.R;
import com.sageabletech.shomiround.BaseActivity;
import com.sageabletech.shomiround.Login;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;
import com.sageabletech.webservicecalls.AsyncRemoteCall;
import com.sageabletech.webservicecalls.AsyncRemoteCall.OnDataListener;
import com.sageabletech.webservicecalls.RequestParams;
import com.sageabletech.webservicecalls.ServiceCalls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

/**
 * This class is used to change password.
 * Created by arige on 7/1/2017.
 */

public class ChangePasswordActivity extends BaseActivity implements OnDataListener {

    private EditText etCurrentPwd, etNewPwd, etConfirmPwd;
    private ImageView ivRight;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivRight= (ImageView)toolbar.findViewById(R.id.right);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back_white);

        init();

    }

    private void init(){
        etCurrentPwd = (EditText) findViewById(R.id.id_current_password);
        etNewPwd = (EditText) findViewById(R.id.id_new_password);
        etConfirmPwd = (EditText) findViewById(R.id.id_confirm_password);

        ivRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currentPwdStr = etCurrentPwd.getText().toString();
                String newPwdStr = etNewPwd.getText().toString();
                String confirmPwdStr = etConfirmPwd.getText().toString();
                if(currentPwdStr!=null && !currentPwdStr.isEmpty()  && newPwdStr!=null && !newPwdStr.isEmpty() && confirmPwdStr!=null && !confirmPwdStr.isEmpty()){
                    if(newPwdStr.matches(confirmPwdStr)){
                        changePasswordServiceCall(currentPwdStr,newPwdStr);
                    }else{
                        Toast.makeText(ChangePasswordActivity.this, "New Password & Confirm Password should be same.",Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(ChangePasswordActivity.this, "Enter all input values",Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    private void changePasswordServiceCall(String currentPwd, String newPwd){
        try {
            if (progressDialog == null
                    || (progressDialog != null && !progressDialog.isShowing())) {
                progressDialog = ProgressDialog.show(ChangePasswordActivity.this, "", "processing...");
            }
        } catch (Exception e) {
            progressDialog = null;
        }
        RequestParams requestParams = new RequestParams();
        String accessToken = this.preferenceUtils.getStringFromPreference("accessToken",null);
        String url = Urls.CHANGE_PASSWORD+accessToken;
        requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
        requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;

        JSONObject loginRequest = new JSONObject();
        try {
            loginRequest.put("password",currentPwd);
            loginRequest.put("newPwd",newPwd);
            loginRequest.put("deviceType","mobile");
            loginRequest.put("deviceToken","3BBB848D-79D6-4880-BBF6-DF645756493B");
            requestParams.data = loginRequest.toString();
            ServiceCalls serviceCalls = new ServiceCalls(this, "CHANGE_PWD");
            serviceCalls.execute(requestParams);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onData(Object data, String error, String tag) {
        hideLoader();
        System.out.println("data:::"+error.toString());
        if(error.contains("Password Changed Successfully")){
            PreferenceUtils preferenceUtils = new PreferenceUtils(this);
            preferenceUtils.clearData();
            etCurrentPwd.setText("");
            etNewPwd.setText("");
             etConfirmPwd.setText("");
            Toast.makeText(this, R.string.change_pwd_success_msg, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, Login.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }else {
            Toast.makeText(ChangePasswordActivity.this, "data:"+error.toString(),Toast.LENGTH_SHORT).show();
        }





    }

    public void showLoader() {
        runOnUiThread(new RunShowLoader("Loading..."));
    }

    class RunShowLoader implements Runnable {
        private String strMsg;

        public RunShowLoader(String strMsg) {
            this.strMsg = strMsg;
        }

        @Override
        public void run() {
            try {
                if (progressDialog == null
                        || (progressDialog != null && !progressDialog
                        .isShowing())) {
                    progressDialog = ProgressDialog.show(ChangePasswordActivity.this, "",
                            strMsg);
                }
            } catch (Exception e) {
                progressDialog = null;
            }
        }
    }

    public void hideLoader() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    progressDialog = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
