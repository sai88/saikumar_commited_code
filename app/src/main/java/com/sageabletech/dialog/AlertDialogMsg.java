package com.sageabletech.dialog;

import android.app.AlertDialog;
import android.content.Context;

import com.sageabletech.R;

public class AlertDialogMsg extends AlertDialog.Builder{
	
	Context context;
	public AlertDialogMsg(Context context,String msg) 
	{
		super(context);
		this.setMessage(msg);
		this.setTitle(context.getString(R.string.app_name));
		this.setCancelable(false);

	}

}
