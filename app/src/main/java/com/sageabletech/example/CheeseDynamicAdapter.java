//package com.sageabletech.example;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.CheckBox;
//import android.widget.GridView;
//import android.widget.ImageView;
//
//import com.sageabletech.R;
//import com.sageabletech.dynamicgrid.BaseDynamicGridAdapter;
//import com.sageabletech.model.TourItem;
//import com.sageabletech.newclassess.GridFragmentNew;
//import com.sageabletech.utils.AppConstants;
//import com.sageabletech.utils.PreferenceUtils;
//import com.squareup.picasso.Picasso;
//
//import java.util.ArrayList;
//
//public class CheeseDynamicAdapter extends BaseDynamicGridAdapter {
//    PreferenceUtils preferenceUtils = null;
//    ArrayList<TourItem> imagesLocal;
//
//    public CheeseDynamicAdapter(Context context, ArrayList<TourItem> images, int columnCount,PreferenceUtils preferenceUtils) {
//        super(context, images, columnCount);
//        this.imagesLocal = images;
//
//       this. preferenceUtils = preferenceUtils;
//    }
//
//    @Override
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        CheeseViewHolder holder;
//        if (convertView == null) {
//            convertView = LayoutInflater.from(getContext()).inflate(R.layout.grid_item, null);
//            holder = new CheeseViewHolder(convertView);
//            convertView.setTag(holder);
//        } else {
//            holder = (CheeseViewHolder) convertView.getTag();
//        }
//        convertView.setLayoutParams(new GridView.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2, (AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2));
//        holder.build((TourItem) getItem(position),position);
//        return convertView;
//    }
//
//    private class CheeseViewHolder {
//
//        CheckBox tv;
//        ImageView img, ivClose;
//        String type = "";
//
//
//        private CheeseViewHolder(View view) {
//            img = (ImageView) view.findViewById(R.id.imageView1);
//            tv = (CheckBox) view.findViewById(R.id.checkBox1);
//            ivClose = (ImageView) view.findViewById(R.id.ivClose);
//
//
//        }
//
//        void build(TourItem data, final int position) {
//            img.setScaleType(ImageView.ScaleType.CENTER_CROP);
//
//            System.out.println("Imagesss:::::SAI::" + data.ImagePath);
//
//            String url = data.ImagePath;
//            String userId = preferenceUtils.getStringFromPreference("userId", "");
//
//            if (url.contains(userId) && !url.contains("https://s3.amazonaws.com/")) {
//                url = "https://s3.amazonaws.com/shomiround-production/" + url;
//            }
//            System.out.println("url loaded::" + url);
//            Picasso.with(getContext())
//                    .load(url)
//                    .placeholder(R.color.black)
//                    .resize(AppConstants.DEVICE_DISPLAY_WIDTH / 2, AppConstants.DEVICE_DISPLAY_WIDTH / 2).into(img);
////            Glide.with(getActivity()).load(url).into(holder.img);
//
//
//            ivClose.setVisibility(View.VISIBLE);
//            System.out.println("holder.ivClose" + ivClose.isShown());
//            System.out.println("holder.img" + ivClose.isShown());
//
//
//            ivClose.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    System.out.println("clickeddddd::::::::");
////                    alertDialogFinish("Are you want to delete this photo.", position);
//
//                }
//            });
//        }
//    }
//}