//package com.showmeround.webservicecalls;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.UnsupportedEncodingException;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.apache.http.HttpEntity;
//import org.apache.http.HttpResponse;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.protocol.BasicHttpContext;
//import org.apache.http.protocol.HttpContext;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import android.app.AlertDialog;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.net.ConnectivityManager;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.inputmethod.EditorInfo;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.AdapterView;
//import android.widget.AdapterView.OnItemClickListener;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.pyrus.edify.AppConstant;
//import com.pyrus.edify.BaseActivity;
//import com.pyrus.edify.Globals;
//import com.pyrus.edify.R;
//import com.pyrus.edify.TabGroupActivity;
//import com.pyrus.edify.db.DataBaseHelper;
//import com.pyrus.edify.db.LocalDBUpdater;
//import com.pyrus.edify.db.SyllabusClassDetails;
//import com.pyrus.edify.teacher.TeachersPopUPAdapter;
//
//public class DailyAssignmentNew extends BaseActivity{
//
//	private class LongRunningGetIO extends AsyncTask <Void, Void, String> {
//
//		ProgressDialog dialog;
//		protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
//			InputStream in = entity.getContent();
//			StringBuffer out = new StringBuffer();
//			int n = 1;
//			while (n>0) {
//				byte[] b = new byte[4096];
//				n =  in.read(b);
//				if (n>0) out.append(new String(b, 0, n));
//			}
//			return out.toString();
//		}
//		@Override
//		protected void onPreExecute() {
//			// TODO Auto-generated method stub
//			super.onPreExecute();
//			dialog=new ProgressDialog(getParent());
//			dialog.setMessage("please wait..");
//			dialog.show();
//		}
//
//		@Override
//		protected String doInBackground(Void... params) {
//			HttpClient httpClient = new DefaultHttpClient();
//
//			HttpContext localContext = new BasicHttpContext();
//			HttpPost httpGet = new HttpPost(AppConstant.URL+"/masters/teacherAddHomework");
//			//			httpGet.setHeader("Content-Type", "application/x-www-form-urlencoded");
//
//
//
////
//			httpGet.setHeader("Content-Type", "application/json");
//			JSONObject customerdata = new JSONObject();
//			try {
//
//				String decscrpitionTxt=decscrpition.getText().toString();
//
//				customerdata.put("location_id", globals.getLocid());
//				customerdata.put("user_id", globals.getUserId());
//				customerdata.put("assignment_type", "3");
//				customerdata.put("content_type_id", "2");
//				customerdata.put("school_class_id", classId);
//				customerdata.put("subject_id", subjectId);
//				customerdata.put("section_id", sectionId);
//				customerdata.put("teacher_comments", decscrpitionTxt);
//				customerdata.put("class_subject_map_id", classSectionmapidForSunbmission.get(0));
//
//				httpGet.setEntity((new StringEntity(customerdata.toString())));
//				URL url = httpGet.getURI().toURL();
//				System.out.println("url for home works:::"+url.toString());
//				System.out.println("json data::"+customerdata.toString());
//
//			} catch (JSONException e1) {
//				e1.printStackTrace();
//			} catch (UnsupportedEncodingException e) {
//				e.printStackTrace();
//			} catch (MalformedURLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			String text = null;
//			try {
//				HttpResponse response = httpClient.execute(httpGet,localContext);
//				HttpEntity entity = response.getEntity();
//				text = getASCIIContentFromEntity(entity);
//			} catch (Exception e) {
//				System.out.println(e.getLocalizedMessage());
//				return e.getLocalizedMessage();
//			}
//			return text;
//		}
//
//
//
//		protected void onPostExecute(String results) {
//			dialog.dismiss();
//			System.out.println("results:"+results);
//
//		}
//
//	}
//
//
//
//}
