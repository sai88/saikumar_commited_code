package com.sageabletech.webservicecalls;

import android.app.AlarmManager;
import android.util.Log;

import com.sageabletech.amazon.Util;
import com.sageabletech.model.CategoryDO;
import com.sageabletech.model.EditProfileDO;
import com.sageabletech.model.FeedsDO;
import com.sageabletech.model.GetTourDO;
import com.sageabletech.model.LoginDO;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.SubTourItemsDO;
import com.sageabletech.model.TagsDo;
import com.sageabletech.model.TourItem;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ServiceCalls extends AsyncRemoteCall {

	private String tag;

	public ServiceCalls(OnDataListener context, String tag) {
		super(context);

		this.tag = tag;
	}


	public ServiceCalls(OnDataListener context, String tag, String option) {
		super(context);

		this.tag = tag;
	}


	@Override
	protected void onPostExecute(ResponseData result) {
		// TODO Auto-generated method stub
		Log.e("Resonse******", result.toString());

		if (result.error == null) {

			parse(result);

		} else {

			mDataListener.onData(null, result.error, tag);
		}
		super.onPostExecute(result);
	}

	public void parse(ResponseData result) {

		String status = null;

		try {

			result.data = result.data.toString().replaceAll(":null", ":\"\"");

			//Orders getting direct json array.
			/*if(tag.equalsIgnoreCase("orders")){
				
				if(!result.data.contains("error")){
					JSONObject tempObj = new JSONObject();
					tempObj.put("data", result.data);
					result.data = tempObj.toString();
				}else{
					mDataListener.onData(null, "Error",tag);
					return;
				}
				
			}*/

			JSONObject jsonObjectMain = new JSONObject(result.data);

			try {

				status = jsonObjectMain.getString("error");

			} catch (Exception e) {
				// TODO: handle exception

				status = null;
//				e.printStackTrace();
			}

			if (status == null) {

				if (tag.equalsIgnoreCase("Login")) {

					System.out.println("jsonObjectMain:::"+jsonObjectMain);
						if (!jsonObjectMain.has("message")) {
							LoginDO mLoginDO = new LoginDO();
//							{"userId":235,"emailId":"",
// "role":null,"firstName":"","lastName":"",
// "createdBy":null,"createdOn":null,
// "accessToken":"7ee7bc8c9ea597e2978868223166d7c1f85c633c","resetPwd":false,
// "password":null,"newPwd":null,
// "picPath":"https://graph.facebook.com/139837963422667/picture?type=large","userName":"","active":true} , No error
//                    {"userId":199,"emailId":"kumari@asmansoft.com","role":"","firstName":"sai","lastName":"kumar","createdBy":"","createdOn":"","accessToken":"0ab8cdc5aa2d8c03605bae10a98ab657c587872d","resetPwd":false,"password":"","newPwd":"","picPath":"https:\/\/s3.amazonaws.com\/shomiround\/199\/IMG_Feb232017_115522.jpg","userName":"sai kumar","active":true}
							System.out.println("total logindataa::" + jsonObjectMain);
							mLoginDO.accessToken = jsonObjectMain.optString("accessToken");
							mLoginDO.userId = jsonObjectMain.optString("userId");
//						mLoginDO.message = jsonObjectMain.optString("message");
							mLoginDO.emailId = jsonObjectMain.optString("emailId");
							mLoginDO.userPicPath = jsonObjectMain.optString("picPath");
							mLoginDO.firstName = jsonObjectMain.optString("firstName");

							LocalData.getInstance().setProfilepicpath(mLoginDO.userPicPath);

							mLoginDO.lastName = jsonObjectMain.optString("lastName");

							mDataListener.onData(mLoginDO, null, tag);
						} else {
						mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
						}


				} else if (tag.equalsIgnoreCase("Register")) {

					if (!jsonObjectMain.toString().contains("Failed")) {
						String jsonObject = jsonObjectMain.optString("message");
						mDataListener.onData(jsonObject, null, tag);
					} else {
						mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
					}
				} else if (tag.equalsIgnoreCase("NEWS")) {

					if (jsonObjectMain.toString().contains("success")) {
					System.out.println("news dataaa:::"+jsonObjectMain);
						JSONArray jsondata = jsonObjectMain.optJSONArray("data");
						System.out.println("json data.lenght::"+jsondata.length());

						ArrayList<FeedsDO> mFeeds = new ArrayList<>();
						for (int i = 0; i < jsondata.length(); i++) {
							JSONObject jo = jsondata.getJSONObject(i);
							FeedsDO mfeeds = new FeedsDO();
							mfeeds.tourPhotoPath = jo.optString("tourPhotoPath");
							mfeeds.tourTitle = jo.optString("tourTitle");
							mfeeds.tourSummary = jo.optString("tourSummary");
							mfeeds.tourLatitude = jo.optString("tourLatitude");
							mfeeds.tourLangitude = jo.optString("tourLangitude");
							mfeeds.viewCount = jo.optString("viewCount");
							mfeeds.commentsCount = jo.optString("commentsCount");
							mfeeds.tourOrderNumber = jo.optString("tourOrderNumber");
							mfeeds.tourId = jo.optString("tourId");
							mfeeds.userPicPath = jo.optString("userPicPath");
							mfeeds.userId = jo.optString("userId");

							mfeeds.favcount = jo.optString("favCount");
							mfeeds.tourTime = jo.optString("tourTime");
							String timeAgo = Utils.getTimeAgo(mfeeds.tourTime);
							System.out.println("timeAgo::"+timeAgo);
							mfeeds.tourTime=timeAgo;
							mfeeds.userName = jo.optString("userName");
//							mfeeds.userPicPath = jo.optString("userPicPath");
							mfeeds.isfavorite = jo.optString("isfavourite");

							JSONArray categoriesList = jo.getJSONArray("categoriesList");
							ArrayList<String> catArry=new ArrayList<>();
							for (int cat=0;cat<categoriesList.length();cat++){
								JSONObject catObject = categoriesList.getJSONObject(cat);
								String categorieName = catObject.getString("categorieName");
								catArry.add("#"+categorieName);

							}
							mfeeds.catList=catArry;

							JSONArray tagList = jo.getJSONArray("tagsList");
							ArrayList<String> tagArry=new ArrayList<>();
							for (int tag=0;tag<tagList.length();tag++){
								JSONObject tagObject = tagList.getJSONObject(tag);
								String tagName = tagObject.getString("tagName");
								tagArry.add("#"+tagName);

							}
							mfeeds.tagList=tagArry;


							mFeeds.add(mfeeds);
						}

						mDataListener.onData(mFeeds, null, tag);
					} else {
						mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
					}

				} else if (tag.equalsIgnoreCase("LOGOUT")) {

					if (jsonObjectMain.toString().contains("Successfully logout")) {
						String jsonObject = jsonObjectMain.optString("message");
						mDataListener.onData(jsonObject, null, tag);
					} else {
						mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
					}
				} else if (tag.equalsIgnoreCase("CHANGE_PWD")) {

					if (jsonObjectMain.optString("message").equalsIgnoreCase("Password updated")) {

						mDataListener.onData(jsonObjectMain.optString("message"), null, tag);
					} else {
						mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
					}
				} else if (tag.equalsIgnoreCase("EDIT_PROFILE")) {
					if (jsonObjectMain.optString("message").equalsIgnoreCase("User profile is saved successfully")) {

						JSONObject jo = jsonObjectMain.getJSONObject("data");
						ArrayList<EditProfileDO> mEdit = new ArrayList<>();
						EditProfileDO medit = new EditProfileDO();
						medit.email = jo.getString("email");
						medit.userId = jo.getString("userId");
						medit.firstName = jo.getString("firstName");
						medit.lastName = jo.getString("lastName");
						medit.userPic = jo.getString("userPic");
						medit.userLoginId = jo.getString("userLoginId");
						mEdit.add(medit);
						mDataListener.onData(mEdit, null, tag);
//						mDataListener.onData(jsonObjectMain.optString("message"),null,tag);

					} else {
						mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
					}
				} else if (tag.equalsIgnoreCase("PRIVACY_POLICY")) {

					if (jsonObjectMain.toString().contains("Success")) {
						JSONObject jsonObject = jsonObjectMain.getJSONObject("data");
						mDataListener.onData(jsonObject.opt("privacyPolicyFile"), null, tag);
					} else {
						mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
					}
				} else if (tag.equalsIgnoreCase("listCategories")) {
					if (jsonObjectMain.toString().contains("Success")) {

						ArrayList<CategoryDO> mCategoryDOs = new ArrayList<>();

						System.out.println("jsonObjectMain::" + jsonObjectMain);

						JSONArray jsondata = jsonObjectMain.optJSONArray("data");

						for (int i = 0; i < jsondata.length(); i++) {

							CategoryDO mCategoryDO = new CategoryDO();
							JSONObject jo = jsondata.getJSONObject(i);
							mCategoryDO.categorieId = jo.optString("categorieId");
							mCategoryDO.categorieName = jo.optString("categorieName");
							mCategoryDO.categorieImageLocation = jo.optString("categorieImageLocation");

							mCategoryDOs.add(mCategoryDO);
						}
						mDataListener.onData(mCategoryDOs, null, tag);

						System.out.println("listCategories::" + jsondata);
					} else {
						mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
					}
				} else if (tag.equalsIgnoreCase("listCategoryAndTags")) {
					if (jsonObjectMain.toString().contains("Success")) {

						ArrayList<TagsDo> mTagDos = new ArrayList<>();

						System.out.println("jsonObjectMain::" + jsonObjectMain);
						JSONObject datajsondata = jsonObjectMain.optJSONObject("data");

						JSONArray jsondata = datajsondata.optJSONArray("tags");

						for (int i = 0; i < jsondata.length(); i++) {

							TagsDo mTagsDo = new TagsDo();
							JSONObject jo = jsondata.getJSONObject(i);
							mTagsDo.tagId = jo.optString("tagId");
							mTagsDo.tagName = jo.optString("tagName");

							mTagDos.add(mTagsDo);
						}
						mDataListener.onData(mTagDos, null, tag);

						System.out.println("listCategories::" + jsondata);
					} else {
						mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
					}
				} else if (tag.equalsIgnoreCase("savetag")) {
					if (jsonObjectMain.toString().contains("Success")) {

//						ArrayList<TagsDo> mTagDos = new ArrayList<>();

						System.out.println("jsonObjectMain::" + jsonObjectMain);
						JSONObject datajsondata = jsonObjectMain.optJSONObject("data");

//						JSONArray jsondata = jsonObjectMain.optJSONArray("data");

//						for(int i=0;i<jsondata.length();i++){

						TagsDo mTagsDo = new TagsDo();
//							JSONObject jo = datajsondata.getJSONObject(i);
						mTagsDo.tagId = datajsondata.optString("tagId");
						mTagsDo.tagName = datajsondata.optString("tagName");

//							mTagDos.add(mTagsDo);
//						}
						mDataListener.onData(mTagsDo, null, tag);

//						System.out.println("listCategories::"+jsondata);
					} else {
						mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
					}
				} else if (tag.equalsIgnoreCase("MYTOUR")) {

					if (jsonObjectMain.toString().contains("success")) {
						JSONArray jsondata = jsonObjectMain.optJSONArray("data");

						ArrayList<MyTourItemDO> mFeeds = new ArrayList<>();
						for (int i = 0; i < jsondata.length(); i++) {
							JSONObject jo = jsondata.getJSONObject(i);
							MyTourItemDO mfeeds = new MyTourItemDO();
							mfeeds.tourPhotoPath = jo.optString("tourPhotoPath");
							mfeeds.title = jo.optString("title");
							mfeeds.summary = jo.optString("summary");
							mfeeds.tourLatitude = jo.optString("tourLatitude");
							mfeeds.tourLangitude = jo.optString("tourLangitude");
							mfeeds.viewCount = jo.optString("viewCount");
							mfeeds.comments = jo.optString("comments");
							mfeeds.area = jo.optString("area");
							mfeeds.city = jo.optString("city");
							mfeeds.tourId = jo.optString("tourId");
							mfeeds.tourRating = jo.optString("tourRating");
							mfeeds.userId = jo.optString("userId");
							mfeeds.createdOn = jo.optString("createdOn");

							JSONArray photsArray = jo.optJSONArray("photos");

							for (int j = 0; j < photsArray.length(); j++) {

								JSONObject joj = photsArray.getJSONObject(j);
								TourItem ti = new TourItem();
								ti.ImagePath = joj.optString("photoPath");
								ti.text = joj.optString("photoTitle");
								ti.audioPath = joj.optString("photoAudioPath");
								ti.summary = joj.optString("photoCaption");
								ti.photoId = joj.optString("photoId");
								ti.location = joj.optString("photoLocation");
								ti.latitude = joj.optString("photolatitude");
								ti.location = joj.optString("photoLocation");
								ti.tourId = jo.optString("tourId");

								mfeeds.subtour.add(ti);
							}


							mFeeds.add(mfeeds);
						}

						mDataListener.onData(mFeeds, null, tag);
					} else {
						mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
					}
				} else if (tag.equalsIgnoreCase("FORGET")) {

					if (!jsonObjectMain.toString().contains("Failed")) {
						String jsonObject = jsonObjectMain.optString("message");
						mDataListener.onData(jsonObject, null, tag);
					} else {
						mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
					}
				} else if (tag.equalsIgnoreCase("Create")) {

					if (jsonObjectMain.toString().contains("Tour Created Successfully")) {
						String jsonObject = jsonObjectMain.optString("message");

						JSONObject jData = jsonObjectMain.getJSONObject("data");

						jsonObject = jData.getString("tourId");

						mDataListener.onData(jsonObject, null, tag);
					} else {
						mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
					}
				} else if (tag.equalsIgnoreCase("Update")) {
                    System.out.println("update:::"+jsonObjectMain);
					String jsonObject = jsonObjectMain.getString("message");

                    if (jsonObject.toString().equalsIgnoreCase("Tour updated Successfully")) {

//
						JSONObject jData = jsonObjectMain.getJSONObject("data");

						jsonObject = jData.getString("tourId");

						mDataListener.onData(jsonObject, null, tag);
					} else {
						mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
					}
				}
				else if (tag.equalsIgnoreCase("DELETE")) {

					if (jsonObjectMain.toString().contains("successfully")) {
						String jsonObject = jsonObjectMain.optString("message");
						mDataListener.onData(jsonObject, null, tag);
					} else {
						mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
					}
				} else if (tag.equalsIgnoreCase("GET_TOUR")) {
					if (jsonObjectMain.toString().contains("success")) {

//                        JSONArray jsondataTour = jsonObjectMain.optJSONArray("data");
						JSONObject jo = jsonObjectMain.getJSONObject("data");
						ArrayList<GetTourDO> mTours = new ArrayList<>();
						GetTourDO mtours = new GetTourDO();
						mtours.tourId = jo.getString("tourId");
						mtours.userId = jo.getString("userId");
						mtours.title = jo.getString("title");
						mtours.summary = jo.getString("summary");
						mtours.address = jo.getString("address");
						mtours.area = jo.getString("area");
						mtours.city = jo.getString("city");
						mtours.areacode = jo.getString("areacode");
						mtours.tourLatitude = jo.getString("tourLatitude");
						mtours.tourLangitude = jo.getString("tourLangitude");
						mtours.tourPhotoPath = jo.getString("tourPhotoPath");
						mtours.tourPhotoThumb = jo.getString("tourPhotoThumb");
						mtours.tourPhotoThumbExt = jo.getString("tourPhotoThumbExt");
						mtours.createdOn = Utils.longToDate(jo.getLong("createdOn"));
						mtours.tourPhotoExt = jo.getString("tourPhotoExt");
						mtours.comments = jo.getString("comments");
						mtours.viewCount = jo.getString("viewCount");
						mtours.draftTourFlag = jo.getString("draftTourFlag");
						JSONArray subJsonObject = jo.getJSONArray("photos");
						ArrayList<SubTourItemsDO> subList = new ArrayList<>();

						for (int i = 0; i < subJsonObject.length(); i++) {
							JSONObject c = subJsonObject.getJSONObject(i);
							SubTourItemsDO subItem = new SubTourItemsDO();
							subItem.photoId = c.getString("photoId");
							subItem.photoTitle = c.getString("photoTitle");
							subItem.photoCaption = c.getString("photoCaption");
							subItem.photoLocation = c.getString("photoLocation");
							subItem.photoPath = c.getString("photoPath");
							subItem.photoThumbPath = c.getString("photoThumbPath");
							subItem.photoAudioPath = c.getString("photoAudioPath");
							subItem.audioTime = c.getString("audioTime");
							subItem.imageExtension = c.getString("imageExtension");
							subItem.audioExtension = c.getString("audioExtension");
							subItem.photolatitude = c.getString("photolatitude");
							subItem.photolangitude = c.getString("photolangitude");
							subItem.createdOn = c.getString("createdOn");
							subItem.tourOrderNumber = c.getString("tourOrderNumber");
							subList.add(subItem);
//							subList.addAll(subList);
							Log.d("test ", subItem.photoId);

						}

						mtours.subList = subList;

						mTours.add(mtours);
//
//                        }
						mDataListener.onData(mTours, null, tag);
					}else {
						mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
					}


				} else {
					Log.e("", "error");
					result.error = "Error";
					mDataListener.onData(null, result.error, tag);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}