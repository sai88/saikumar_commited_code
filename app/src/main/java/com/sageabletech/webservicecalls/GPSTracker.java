package com.sageabletech.webservicecalls;





import java.util.List;
import java.util.Locale;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;



	public class GPSTracker extends Service implements LocationListener {

	   private final Context mContext;

	   // flag for GPS status
	   boolean isGPSEnabled = false;

	   // flag for network status
	   boolean isNetworkEnabled = false;

	   // flag for GPS status
	   boolean canGetLocation = false;

	   Location location; // location
	   double latitude; // latitude
	   double longitude; // longitude
	   double speed;

	   // The minimum distance to change Updates in meters
	   private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

	   // The minimum time between updates in milliseconds
	   private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

	   // Declaring a Location Manager
	   protected LocationManager locationManager;

	   public GPSTracker(Context context) {
	       this.mContext = context;
	       getLocation();
	   }

	   public Location getLocation() {
	       try {
	           locationManager = (LocationManager) mContext
	                   .getSystemService(LOCATION_SERVICE);
	           Criteria criteria = new Criteria();
	           criteria.setPowerRequirement(Criteria.POWER_LOW); // Chose your desired power consumption level.
	           criteria.setAccuracy(Criteria.ACCURACY_FINE); // Choose your accuracy requirement.
	           criteria.setSpeedRequired(true); 
	           criteria.setSpeedAccuracy(Criteria.ACCURACY_FINE);// Chose if speed for first location fix is required.
	           criteria.setAltitudeRequired(false); // Choose if you use altitude.
	           criteria.setBearingRequired(false); // Choose if you use bearing.
	           criteria.setCostAllowed(false); // Choose if this provider can waste money :-)
	        

	           // getting GPS status
	           isGPSEnabled = locationManager
	                   .isProviderEnabled(LocationManager.GPS_PROVIDER);
	           
	           

	           // getting network status
	           isNetworkEnabled = locationManager
	                   .isProviderEnabled(LocationManager.NETWORK_PROVIDER);


	           if (!isGPSEnabled && !isNetworkEnabled) {
	               // no network provider is enabled
	           } else {
	               this.canGetLocation = true;
	               if (isNetworkEnabled) {
	                   locationManager.requestLocationUpdates(
	                           LocationManager.NETWORK_PROVIDER,
	                           MIN_TIME_BW_UPDATES,
	                           MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
	                   Log.d("Network", "Network");
	                   if (locationManager != null) {
	                       location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
	                      // locationManager.getBestProvider(criteria, true);
	                       if (location != null) {
	                           latitude = location.getLatitude();
	                           longitude = location.getLongitude();
	                       }
	                   }
	               }
	               // if GPS Enabled get lat/long using GPS Services
	               if (isGPSEnabled) {
	                   if (location == null) {
	                       locationManager.requestLocationUpdates(
	                               LocationManager.GPS_PROVIDER,
	                               MIN_TIME_BW_UPDATES,
	                               MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
	                       Log.d("GPS Enabled", "GPS Enabled");
	                       if (locationManager != null) {
	                           location = locationManager
	                                   .getLastKnownLocation(LocationManager.GPS_PROVIDER);
	                           if (location != null) {
	                               latitude = location.getLatitude();
	                               longitude = location.getLongitude();
	                           }
	                       }
	                   }
	               }
	           }

	       } catch (Exception e) {
	       }

		   
	       return location;
	   }

	   /**
	    * Stop using GPS listener Calling this function will stop using GPS in your
	    * app.
	    * */
	   public void stopUsingGPS() {
	       if (locationManager != null) {
	           locationManager.removeUpdates(GPSTracker.this);
	       }
	   }

	   /**
	    * Function to get latitude
	    * */
	   public double getLatitude() {
	       if (location != null) {
	           latitude = location.getLatitude();
	           
	       }
	       // return latitude
	       return latitude;
	   }

	   /**
	    * Function to get longitude
	    * */
	   public double getLongitude() {
	       if (location != null) {
	           longitude = location.getLongitude();
	       }

	       // return longitude
	       return longitude;
	   }
	   public double getCurrentSpeed(){
	   	 if (location != null) {
	   	 if(location.hasSpeed()){
	   	 
	   	 Log.e("HasSpeed", ""+location.hasSpeed());
	           speed = location.getSpeed();
	   	 }
	       }
	   	 return speed;
	   }

	   /**
	    * Function to check GPS/wifi enabled
	    * 
	    * @return boolean
	    * */
	   public boolean canGetLocation() {
	       return this.canGetLocation;
	   }

	   /**
	    * Function to show settings alert dialog On pressing Settings button will
	    * lauch Settings Options
	    * */
	   public void showSettingsAlert() {
	      /* AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

	       // Setting DialogHelp Title
	       alertDialog.setTitle("GPS is settings");
	       // Setting DialogHelp Message
	       alertDialog
	               .setMessage("GPS is not enabled. Do you want to go to settings menu?");
	       // On pressing Settings button
	       alertDialog.setPositiveButton("Settings",
	               new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int which) {
	                       Intent intent = new Intent(
	                               Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	                       mContext.startActivity(intent);
	                   }
	               });

	       // on pressing cancel button
	       alertDialog.setNegativeButton("Cancel",
	               new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int which) {
	                       dialog.cancel();
	                   }
	               });

	       // Showing Alert Message
	       alertDialog.show();*/
	       
	   /*	final Dialog alertDialog=new Dialog(mContext, R.style.DialogSlideAnim);
		alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
		View layout = inflater.inflate(R.layout.exitalertdailog, null);
		alertDialog.setContentView(layout);
		alertDialog.setCancelable(false);
		TextView title1 = (TextView) layout.findViewById(R.id.showmsg);
		Button settingsId = (Button)layout.findViewById(R.id.yesId);
		Button cancelId = (Button)layout.findViewById(R.id.cancelId);
		alertDialog.show();
		DisplayMetrics metrics = new DisplayMetrics();
		Display mDisplay = ((Activity) mContext).getWindowManager()
				.getDefaultDisplay();
		((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int width = mDisplay.getWidth();
		int Height = mDisplay.getHeight();
		int screen_orientaiton=mContext.getResources().getConfiguration().orientation;
	 
		
		 String userAgent = new WebView(mContext).getSettings().getUserAgentString();
		 if (userAgent.contains("Mobile")) {
		     // It is a Mobile
			 Log.e("Mobile", "It is a Mobile");
			 if(mContext.getResources().getBoolean(R.bool.normalscreen)){
			Log.e("normalscreen bool", "normalscreen bool");	
		    //alertDialog.getWindow().setLayout(200, 235);
		   
		    if(screen_orientaiton==Configuration.ORIENTATION_PORTRAIT)
			{
			   if(width>600)
			   {
				   alertDialog.getWindow().setLayout(480, 350);
			   }else{
				   alertDialog.getWindow().setLayout(350, 300);
			   }
			}else if(screen_orientaiton==Configuration.ORIENTATION_LANDSCAPE)
			  {
				if(Height>600)
				{
				   alertDialog.getWindow().setLayout(480, 350);
				   }else{
				   alertDialog.getWindow().setLayout(350, 300);
				}
			  }
			 }
			 if(mContext.getResources().getBoolean(R.bool.largescreen)){
			Log.e("largescreen bool", "largescreen bool");	
			   alertDialog.getWindow().setLayout(200, 235);
			 }
		 } 
		// Tab
		 else {
			 if(mContext.getResources().getBoolean(R.bool.largescreen)){
			 Log.e("largescreen bool", "largescreen bool");	
			   alertDialog.getWindow().setLayout(180, 245);
			 }
			 if(mContext.getResources().getBoolean(R.bool.seveninchscreen)){
			 Log.e("seveninchscreen bool", "seveninchscreen bool");	
			   alertDialog.getWindow().setLayout(400,270);
			 }
			 if(mContext.getResources().getBoolean(R.bool.teninchscreen)){
			   Log.e("teninchscreen bool", "teninchscreen bool");	
			   alertDialog.getWindow().setLayout(450, 270);
			 }
		     // It is a Tablet
			 Log.e("Tablet", "It is a Tablet");
			// double tabsize = tabletSize();
			 //Log.e("Tablet", "Tablet size"+tabsize);
			 
		 }
	   title1.setText(mContext.getResources().getString(R.string.gpsenable));
	   cancelId.setText(mContext.getResources().getString(R.string.cancel));
	   cancelId.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alertDialog.dismiss();
			}
		});
		settingsId.setText(mContext.getResources().getString(R.string.settings));
		settingsId.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
	            Intent intent = new Intent(
	                    Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	            mContext.startActivity(intent);
	            alertDialog.dismiss();
			}
		});*/
		   
		  /* final Dialog alertDialog=new Dialog(this, R.style.DialogSlideAnim);
			alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			View layout = inflater.inflate(R.layout.exitalertdailog, null);
			alertDialog.setContentView(layout);
			alertDialog.setCancelable(false);
			TextView title1 = (TextView) layout.findViewById(R.id.showmsg);
			Button yesId = (Button)layout.findViewById(R.id.yesId);
			Button cancelId = (Button)layout.findViewById(R.id.cancelId);
			alertDialog.show();
			DisplayMetrics metrics = new DisplayMetrics();
			Display mDisplay = ((Activity) mContext).getWindowManager()
					.getDefaultDisplay();
			((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(metrics);
			int width = mDisplay.getWidth();
			int Height = mDisplay.getHeight();
			int screen_orientaiton=this.getResources().getConfiguration().orientation;
		 
			String userAgent = new WebView(mContext).getSettings().getUserAgentString();
			 if (userAgent.contains("Mobile")) {
			     // It is a Mobile
				 Log.e("Mobile", "It is a Mobile");
				 if(getResources().getBoolean(R.bool.normalscreen)){
				Log.e("normalscreen bool", "normalscreen bool");	
			    //alertDialog.getWindow().setLayout(200, 235);
			   
			    if(screen_orientaiton==Configuration.ORIENTATION_PORTRAIT)
				{
				   if(width>600)
				   {
					   alertDialog.getWindow().setLayout(430, 470);
				   }else{
					   alertDialog.getWindow().setLayout(330, 360);
				   }
				}else if(screen_orientaiton==Configuration.ORIENTATION_LANDSCAPE)
				  {
					if(Height>600)
					{
					   alertDialog.getWindow().setLayout(430, 470);
					   }else{
					   alertDialog.getWindow().setLayout(330, 360);
					}
				  }
				 }
				 if(getResources().getBoolean(R.bool.largescreen)){
				Log.e("largescreen bool", "largescreen bool");	
				   alertDialog.getWindow().setLayout(200, 235);
				 }
			 } 
			 Tab
			 else {
				 if(getResources().getBoolean(R.bool.largescreen)){
				 Log.e("largescreen bool", "largescreen bool");	
				   alertDialog.getWindow().setLayout(180, 245);
				 }
				 if(getResources().getBoolean(R.bool.seveninchscreen)){
				 Log.e("seveninchscreen bool", "seveninchscreen bool");	
				   alertDialog.getWindow().setLayout(400,260);
				 }
				 if(getResources().getBoolean(R.bool.teninchscreen)){
				   Log.e("teninchscreen bool", "teninchscreen bool");	
				   alertDialog.getWindow().setLayout(450, 250);
				 }
			     // It is a Tablet
				 Log.e("Tablet", "It is a Tablet");
				// double tabsize = tabletSize();
				 //Log.e("Tablet", "Tablet size"+tabsize);
				 
			 }
		   title1.setText(this.getResources().getString(R.string.exitmessage));
			cancelId.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					alertDialog.dismiss();
				}
			});
			yesId.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					 Intent intent = new Intent(
	                         Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	                 mContext.startActivity(intent);
					alertDialog.dismiss();
				}
			});  */
		   
		   
		   
		   
		   
		   
		   
		   
	   }

	   @Override
	   public void onLocationChanged(Location location) {
	       float bestAccuracy = -1f;
	       if (location.getAccuracy() != 0.0f
	           && (location.getAccuracy() < bestAccuracy) || bestAccuracy == -1f) {
	               locationManager.removeUpdates(this);
	       }
	       bestAccuracy = location.getAccuracy();
	   }

	   @Override
	   public void onProviderDisabled(String provider) {
	   }

	   @Override
	   public void onProviderEnabled(String provider) {
	   }

	   @Override
	   public void onStatusChanged(String provider, int status, Bundle extras) {
	   }

	   @Override
	   public IBinder onBind(Intent arg0) {
	       return null;
	   }

	   public float getAccurecy()
	   {
	       return location.getAccuracy();
	   }

	   public String getCompleteAddressString(Context context ,double LATITUDE, double LONGITUDE) {
           String strAdd = "";
           Geocoder geocoder = new Geocoder(context, Locale.getDefault());
           try {
               List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
               if (addresses != null&&addresses.size()!=0) {
                   Address returnedAddress = addresses.get(0);
                   StringBuilder strReturnedAddress = new StringBuilder("");

                   for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                       strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                   }
                   strAdd = strReturnedAddress.toString();
//                   Log.e("My Current loction address", "" + strReturnedAddress.toString());
               } else {
                   Log.w("My Current loction address", "No Address returned!");
               }
           } catch (Exception e) {
               e.printStackTrace();
//               Log.w("My Current loction address", "Canont get Address!");
           }
           return strAdd;
       }
}
