package com.sageabletech.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.sageabletech.R;
import com.sageabletech.adapter.FeedsAdapter;
import com.sageabletech.model.CategoryDO;
import com.sageabletech.model.FeedsDO;
import com.sageabletech.model.TagsDo;
import com.sageabletech.newclassess.FeedSearch;
import com.sageabletech.newclassess.FilterSearch;
import com.sageabletech.newclassess.WordUtils;
import com.sageabletech.shomiround.BaseActivity;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.NetworkUtils;
import com.sageabletech.utils.Urls;
import com.sageabletech.utils.Utils;
import com.sageabletech.views.RecyclerTouchListener;
import com.sageabletech.viewtour.AddAsFavourite;
import com.sageabletech.viewtour.GetCategories;
import com.sageabletech.viewtour.GetTourDetails;
import com.sageabletech.viewtour.SaveViewCount;
import com.sageabletech.viewtour.SearchFeeds;
import com.sageabletech.webservicecalls.AsyncRemoteCall;
import com.sageabletech.webservicecalls.AsyncRemoteCall.OnDataListener;
import com.sageabletech.webservicecalls.RequestParams;
import com.sageabletech.webservicecalls.ServiceCalls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class FeedFragment extends Fragment implements OnDataListener {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private RecyclerView recyclerView;
    private FeedsAdapter mOfferAdapter;
    private List<FeedsDO> Offers = new ArrayList<>();
        private SwipeRefreshLayout swipeContainer;
    static String strToken;
        private static boolean swipeRefresh;
//    RecyclerView.LayoutManager mLayoutManager;
    private LinearLayoutManager mLayoutManager;
    private boolean userScrolled = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private int tourOrderNumber=0;
    public static ServiceCalls serviceCalls;

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("resumeeee>>>>>");
        swipeRefresh = true;

        serviceCallToGetData(strToken,0);

    }
//    public static void resumeCall(){
//        swipeRefresh = true;
//        serviceCallToGetDataFav(strToken,0);
//    }


    public FeedFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FeedFragment newInstance(int sectionNumber) {
        FeedFragment fragment = new FeedFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }
    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         rootView = inflater.inflate(R.layout.feeds_fragment, container, false);
        TextView searchError = (TextView)rootView.findViewById(R.id.searchError);
        LocalData.getInstance().setSearchError(searchError);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.respodedList);

        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);

        strToken = ((BaseActivity) getActivity()).preferenceUtils.getStringFromPreference("accessToken", "");
//         serviceCalls = new ServiceCalls(this, "NEWS");
        serviceCallToGetData(strToken,0);
        mOfferAdapter = new FeedsAdapter(getActivity(), Offers,"normal");

        LocalData.getInstance().setFeedsAdapter(mOfferAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                // Here get the child count, item count and visibleitems
                // from layout manager

                visibleItemCount = mLayoutManager.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
//                tourOrderNumber = Integer.parseInt(Offers.get(dx).tourOrderNumber);

                // Now check if userScrolled is true and also check if
                // the item is end then update recycler view and set
                // userScrolled to false
                if (userScrolled
                        && (visibleItemCount + pastVisiblesItems) == totalItemCount) {
                    userScrolled = false;
                    if(totalItemCount==Offers.size()){
                        System.out.println("totalItemCount::"+totalItemCount);
                        serviceCallToGetData(strToken,totalItemCount);
                    }
                }
            }
        });


        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

//        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mOfferAdapter);
        LocalData.getInstance().setFeedFragmentRecycle(recyclerView);


        mOfferAdapter.notifyDataSetChanged();
        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                View map = view.findViewById(R.id.tvItemLocation);
                View fav = view.findViewById(R.id.addFavorite);
                View AddAll = view.findViewById(R.id.AddAll);
                View switch_btn = view.findViewById(R.id.switch_btn);
                System.out.println("switch_btn::"+switch_btn.isPressed());


//                FeedsDO feedItem = Offers.get(position);
//                if (feedItem.tourId != null&&!map.isPressed()&&!fav.isPressed()&&!AddAll.isPressed()&&!switch_btn.isPressed()) {
////                    String tourId = mOffers.get(postion).tourId;
//                    new SaveViewCount(getActivity(), "" + feedItem.tourId).execute();
//
//                    new GetTourDetails(getActivity(), feedItem.tourId).execute();
//
//
////
//                }

            }


            @Override
            public void onLongClick(View view, int position) {
                FeedsDO feedItem = Offers.get(position);
                String userId = feedItem.userId;
                String tourId =feedItem.tourId;
//                new AddAsFavourite(getActivity(),tourId,userId).execute();

//                button.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.favoriteselected));
            }
        }));


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh = true;
                serviceCallToGetData(strToken,0);

            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        getHeader();
        activity=getActivity();
        return rootView;
    }


    TextView header;
    ImageView back;
    ImageView submit;
    public void getHeader(){
        header=(TextView) rootView.findViewById(R.id.header_text);
        header.setText(WordUtils.capitalize("Tour feed"));
        header.setTypeface(Utils.getHelvaticaBold(getActivity()));
        back=(ImageView)rootView.findViewById(R.id.back);
        submit=(ImageView)rootView.findViewById(R.id.submit);
        submit.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_search_outline_white));

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalData.getInstance().setfilterTagServiceData(new ArrayList<TagsDo>());
                LocalData.getInstance().setFilterTags(new ArrayList<TagsDo>());
                ArrayList<CategoryDO> categoryDOs = LocalData.getInstance().getmCategoryDOs();
                           if(categoryDOs.size()==0) {

                    new GetCategories(getActivity(),"feed").execute();
                }else{
                    LocalData.getInstance().setExistingFeeds(mOffers);
                    Intent intent=new Intent(getActivity(), FeedSearch.class);
                    getActivity().startActivity(intent);
                }

            }
        });
//        submit.setVisibility(View.GONE);
        back.setVisibility(View.GONE);







    }

    private static void serviceCallToGetDataFav(String strToken, int tourOrderNumber) {
        if (NetworkUtils.isNetworkConnectionAvailable(LocalData.getInstance().getMainActivity())) {
            prepareMovieDataFav(strToken,tourOrderNumber);
        } else {
            Toast.makeText(LocalData.getInstance().getMainActivity(), "There is No Internet", Toast.LENGTH_LONG).show();
        }
    }
    private  void serviceCallToGetData(String strToken, int tourOrderNumber) {
        if (NetworkUtils.isNetworkConnectionAvailable(LocalData.getInstance().getMainActivity())) {
            prepareMovieData(strToken,tourOrderNumber);
        } else {
            Toast.makeText(LocalData.getInstance().getMainActivity(), "There is No Internet", Toast.LENGTH_LONG).show();
        }
    }
public static Activity activity;
    public  void prepareMovieData(String vendorId, int orderNumber) {

//        ((BaseActivity) getActivity()).showLoader();
        System.out.println("calleddddddddd");
        RequestParams requestParams = new RequestParams();
        String url = Urls.NEWS_FEED + vendorId;
        requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
        requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;
        JSONObject loginRequest = new JSONObject();
        try {
            loginRequest.put("tourEndOrderNumber", orderNumber);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        requestParams.data = loginRequest.toString();

       ServiceCalls serviceCalls = new ServiceCalls(this, "NEWS");
        serviceCalls.execute(requestParams);


    }
    public static void prepareMovieDataFav(String vendorId, int orderNumber) {

//        ((BaseActivity) getActivity()).showLoader();
        System.out.println("calleddddddddd");
        RequestParams requestParams = new RequestParams();
        String url = Urls.NEWS_FEED + vendorId;
        requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
        requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;
        JSONObject loginRequest = new JSONObject();
        try {
            loginRequest.put("tourEndOrderNumber", orderNumber);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        requestParams.data = loginRequest.toString();


        serviceCalls.execute(requestParams);


    }

    ArrayList<FeedsDO> mOffers;
    @Override
    public void onData(Object data, String error, String tag) {

//        ((BaseActivity) getActivity()).hideLoader();
        if (tag.equalsIgnoreCase("NEWS")) {

            if (error == null && data != null) {



               mOffers = (ArrayList<FeedsDO>) data;
                ArrayList<FeedsDO> mOffersClone = new ArrayList<FeedsDO>();

                if (mOffers.size() > 0) {
                    if(swipeRefresh){
                        //Swipe refresher
                        mOfferAdapter.clear();
                        mOfferAdapter.addAll(mOffers);
                        swipeContainer.setRefreshing(false);
                        swipeRefresh = false;
                    }else {
                        boolean addFalg=false;
                        for(int i=0;i<mOffers.size();i++){
                            FeedsDO feedsDO = mOffers.get(i);
                            String tourId = feedsDO.tourId;
                            addFalg=false;
                            for (int j=0;j<Offers.size();j++){
                                FeedsDO feedsDO1 = Offers.get(j);
                                String tourId1 = feedsDO1.tourId;
                                if(tourId1.equalsIgnoreCase(tourId)){
                                    addFalg=true;
                                }
                            }
                            if(!addFalg){
                                Offers.add(feedsDO);
                            }
                        }
//                        Offers.addAll(mOffers);
                        mOfferAdapter.notifyDataSetChanged();
                    }

                } else {
//
                }

            }

        } else {
            if (error == null && data != null) {
                String str = ((BaseActivity) getActivity()).preferenceUtils.getStringFromPreference("vendorId", "");
                prepareMovieData(str,0);
            }
        }


    }
}