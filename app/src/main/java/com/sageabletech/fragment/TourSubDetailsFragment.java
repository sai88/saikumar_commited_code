package com.sageabletech.fragment;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.sageabletech.R;
import com.sageabletech.model.TourItem;

/**
 * Created by Uday on 09-07-2017.
 */

public class TourSubDetailsFragment extends Fragment  {
    private ImageView ivSelectedImageSub;
    private ImageView ivMapTourDetailsSub;
    private ImageView play_pause_mediaIv;
    private ImageView stop_mediaIv;
    private SeekBar seekBar;
    private TextView presentMedia, totalMedia, tvTitle, tvCaption;
    private boolean isPlaying;
    private int playPause=0;
    private LinearLayout llTitleCaption,llAudio;
    private TourItem tourItemsDO;
    private MediaPlayer mp;
    private ProgressBar progressBar;

    public static TourSubDetailsFragment newInstance(TourItem tourItemsDO) {
        TourSubDetailsFragment fragment = new TourSubDetailsFragment();

        Bundle args = new Bundle();
        args.putSerializable("tourItemsDO", tourItemsDO);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sub_tour_selected_details, container, false);
//        tourItemsDO = (TourItem) getArguments().getSerializable("tourItemsDO");
//        //init views
//        progressBar = (ProgressBar)rootView.findViewById(R.id.imageprogressbar);
//        ivSelectedImageSub = (ImageView) rootView.findViewById(R.id.ivSelectedImageSub);
//        tvTitle = (TextView) rootView.findViewById(R.id.tv_title);
//        tvCaption = (TextView)rootView.findViewById(R.id.id_tv_caption);
//        ivMapTourDetailsSub = (ImageView) rootView.findViewById(R.id.ivMapTourDetailsSub);
//        play_pause_mediaIv = (ImageView) rootView.findViewById(R.id.play_pause_mediaIv);
//        stop_mediaIv = (ImageView) rootView.findViewById(R.id.stop_mediaIv);
//        seekBar = (SeekBar) rootView.findViewById(R.id.seekBar);
//        presentMedia = (TextView) rootView.findViewById(R.id.presentMedia);
//        totalMedia = (TextView) rootView.findViewById(R.id.totalMedia);
//        llTitleCaption =  (LinearLayout) rootView.findViewById(R.id.ll_title_caption);
//        llAudio = (LinearLayout) rootView.findViewById(R.id.ll_audio_play);
//
//        //adding listeners to view
//        ivSelectedImageSub.setOnClickListener(this);
//        ivMapTourDetailsSub.setOnClickListener(this);
//        play_pause_mediaIv.setOnClickListener(this);
//        stop_mediaIv.setOnClickListener(this);
//        seekBar.setOnSeekBarChangeListener(this);
//
//        stop_mediaIv.setEnabled(true);
//        play_pause_mediaIv.setEnabled(true);
//
////        prepareAudioFile();
//
//
//        if(tourItemsDO.ImagePath!=null && !tourItemsDO.ImagePath.isEmpty()) {
//            Glide.with(this).load(tourItemsDO.ImagePath)
//                    .listener(new RequestListener<String, GlideDrawable>() {
//                        @Override
//                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                            progressBar.setVisibility(View.GONE);
//                            return false;
//                        }
//
//                        @Override
//                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                            progressBar.setVisibility(View.GONE);
//                            return false;
//                        }
//                    })
////                    .placeholder(R.mipmap.ic_progressing)
//                    .centerCrop()
////                    .override((AppConstants.DEVICE_DISPLAY_WIDTH), (AppConstants.DEVICE_DISPLAY_HEIGHT / 3))
//                    .into(ivSelectedImageSub);
//        }
//        if(tourItemsDO.title!=null && !tourItemsDO.title.isEmpty()){
//            llTitleCaption.setVisibility(View.VISIBLE);
//            tvTitle.setText(""+tourItemsDO.title);
//            if(tourItemsDO.text!=null && !tourItemsDO.text.isEmpty()){
//                tvCaption.setText(""+tourItemsDO.text);
//            }
//        }else {
//            llTitleCaption.setVisibility(View.GONE);
//        }
//
//        if(tourItemsDO.latitude!=null && tourItemsDO.longitude!=null){
//            ivMapTourDetailsSub.setVisibility(View.VISIBLE);
//        }else {
//            ivMapTourDetailsSub.setVisibility(View.GONE);
//        }
//
//        if(tourItemsDO.audioPath!=null && !tourItemsDO.audioPath.isEmpty()) {
//
//            llAudio.setVisibility(View.VISIBLE);
//        }else{
//            llAudio.setVisibility(View.GONE);
//        }

        return rootView;
    }



//    private void prepareAudioFile(){
//        downloadAudioFile();
//
//        if(tourItemsDO.audioPath!=null&& !tourItemsDO.audioPath.isEmpty() && tourItemsDO.audioPath!=null && !tourItemsDO.audioPath.isEmpty()){
//            mp = new MediaPlayer();
//            try {
//                totalMedia.setText("-"+Float.valueOf(tourItemsDO.audioTime));
////                        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
////                        mp.setDataSource(tourItemsDO.photoAudioPath+tourItemsDO.audioExtension);
//                mp.setDataSource(Environment.getExternalStorageDirectory().getPath()+"/Music/WA0003.aac");
//                mp.prepare();
//                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//
//                    @Override
//                    public void onCompletion(MediaPlayer mp) {
//                        // TODO Auto-generated method stub
//                        mp.release();
//                    }
//                });
//
//            } catch (IOException e) {
//                Log.e("Media ", "prepare() failed");
//            }
//        }
//
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.ivMapTourDetailsSub:
//                Intent i =new Intent(getActivity(), MapsActivity.class);
//                i.putExtra("tourLangitude",tourItemsDO.latitude);
//                i.putExtra("tourLatitude",tourItemsDO.longitude);
//                i.putExtra("title",tourItemsDO.title);
//                getActivity().startActivity(i);
//                break;
//            case R.id.play_pause_mediaIv:
//                try {
//
//                    if(mp!=null && mp.isPlaying()){
//                        play_pause_mediaIv.setImageResource(R.mipmap.ic_play_outline_black);
//                        mp.pause();
//                    }else {
//                        play_pause_mediaIv.setImageResource(R.mipmap.ic_pause_outline_black);
//                        mp.start();
//                    }
//
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//                break;
//            case R.id.stop_mediaIv:
//                try{
//                if(mp!=null) {
//                    stopPlaying();
//                    prepareAudioFile();
//                    play_pause_mediaIv.setImageResource(R.mipmap.ic_play_outline_black);
//                }
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//
//                break;
//            default:
//                break;
//
//        }
//
//    }
//
//    private void stopPlaying(){
//        if(mp!=null){
//            mp.stop();
//            mp.release();
//            mp=null;
//        }
//    }
//
//    @Override
//    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//
//    }
//
//    @Override
//    public void onStartTrackingTouch(SeekBar seekBar) {
//
//    }
//
//    @Override
//    public void onStopTrackingTouch(SeekBar seekBar) {
//
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        stopPlaying();
//    }
//
//    private void downloadAudioFile() {
//        AmazonS3 s3 = new AmazonS3Client(new CognitoCachingCredentialsProvider(
//                getActivity(),
//                AppConstants.COGNITO_POOL_ID,
//                Regions.fromName(AppConstants.COGNITO_POOL_REGION)));
//        TransferUtility transferUtility = new TransferUtility(s3, getContext());
//        String iconName[] = tourItemsDO.audioPath.split("/");
//        String path = Environment.getExternalStorageDirectory().getPath()+"/"+ AppConstants.BUCKET_NAME;
//        boolean exists = (new File(path)).exists();
//        if (!exists){
//            new File(path).mkdirs();
//        }
//        final File tIcon = new File(path+"/"+iconName[iconName.length-1]);
//        TransferObserver obsIcon = transferUtility.download(
//                AppConstants.BUCKET_NAME,
//                iconName[4] + "/" + iconName[iconName.length-1],  //TODO change tourid
//                tIcon
//        );
////                showLoader();
//        obsIcon.setTransferListener(new TransferListener() {
//
//            @Override
//            public void onStateChanged(int id, TransferState state) {
////                        Toast.makeText(FinalScreen.this,"Uploaded:"+id,Toast.LENGTH_LONG).show();
//            }
//
//            @Override
//            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
//                try {
//                    int percentage = (int) (bytesCurrent / bytesTotal * 100);
//                    //Display percentage transfered to user
//                    Log.e("TAG", "percentage : " + percentage + " " + id);
//
//                    if (percentage == 100) {
////                            mDBCreator.updateTourIcon(tIcon.getAbsolutePath());
//
//                    }
//                } catch (ArithmeticException ae) {
//                }
//
//            }
//
//            @Override
//            public void onError(int id, Exception ex) {
//                // do something
////                progressBar.setVisibility(View.GONE);
//            }
//
//        });
//
//
//    }
}
