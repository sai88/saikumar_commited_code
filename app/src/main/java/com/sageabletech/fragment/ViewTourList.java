package com.sageabletech.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.sageabletech.R;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.MapsActivity;
import com.sageabletech.shomiround.MyTourDetails;
import com.sageabletech.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

//*Ganesh */
public class ViewTourList extends Fragment{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    public ArrayList<TourItem> tourList = null;

    public ViewTourList() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ViewTourList newInstance(int sectionNumber, Context ctx, ArrayList<TourItem> mSubTourItems) {

        ViewTourList fragment = new ViewTourList();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putSerializable("SubTour", mSubTourItems);
        fragment.setArguments(args);
        return fragment;
    }
     MyTourAdapter mImageAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.preview_fragment, container, false);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.respodedList);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        tourList = (ArrayList<TourItem>) getArguments().getSerializable("SubTour");

        mImageAdapter = new MyTourAdapter(getActivity());
        recyclerView.setAdapter(mImageAdapter);

        return rootView;
    }

    private class MyTourAdapter extends RecyclerView.Adapter<MyTourAdapter.MyViewHolder> {

        private Context mContext;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title,tvItemName,tvItemDesc;
            public ImageView ivImage,ivEdit,ivLocation,ivDelete,ivEye;


            public MyViewHolder(View view) {
                super(view);
                tvItemName = (TextView) view.findViewById(R.id.tvItemName);
                tvItemDesc = (TextView) view.findViewById(R.id.tvDesc);

                ivImage = (ImageView)view.findViewById(R.id.ivImage);
                ivEdit = (ImageView)view.findViewById(R.id.ivEdit);
                ivLocation = (ImageView)view.findViewById(R.id.ivLocation);
                ivEye = (ImageView)view.findViewById(R.id.ivEye);
                ivDelete = (ImageView)view.findViewById(R.id.ivDelete);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(getLayoutPosition() < tourList.size())
                            mContext.startActivity(new Intent(mContext, MyTourDetails.class)
                                    .putExtra("Details",tourList.get(getLayoutPosition())));
                    }
                });

            }
        }

        public void refresh(List<MyTourItemDO> mOffers){

            this.notifyDataSetChanged();
        }


        public MyTourAdapter(Context mContext) {
            this.mContext = mContext;
        }

        @Override
        public MyTourAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.my_grid_itemnew, parent, false);

            RelativeLayout.LayoutParams lm = new RelativeLayout.LayoutParams(AppConstants.DEVICE_DISPLAY_WIDTH,
                    AppConstants.DEVICE_DISPLAY_HEIGHT/5);
            lm.setMargins(5,7,5,7);
            itemView.setLayoutParams(lm);

            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), ViewTourPhotosDetails.class)
                            .putExtra("SubTour",tourList));
                }
            });*/


            return new MyViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {

            if(!tourList.get(position).title.equalsIgnoreCase("No Text"))
                holder.tvItemName.setText(tourList.get(position).title);
            else
                holder.tvItemName.setVisibility(View.GONE);

            if(tourList.get(position).summary.length()>0 && !tourList.get(position).summary.equalsIgnoreCase("No Text")){
                holder.tvItemDesc.setText(tourList.get(position).summary);
            }else{
                holder.tvItemDesc.setVisibility(View.INVISIBLE);
            }

            Glide.with(mContext).load(tourList.get(position).ImagePath)
                    .placeholder(R.mipmap.app_icon)
                    .centerCrop()
                    .override(AppConstants.DEVICE_DISPLAY_WIDTH/3, AppConstants.DEVICE_DISPLAY_HEIGHT/5)
                    .into(holder.ivImage);


            holder.ivLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(new Intent(getActivity(),MapsActivity.class)
                                    .putExtra("tourLangitude",tourList.get(position).lang)
                                    .putExtra("tourLatitude",tourList.get(position).lat)
                                    .putExtra("title",tourList.get(position).title));
                }
            });

            holder.ivEdit.setVisibility(View.INVISIBLE);
            holder.ivEye.setVisibility(View.INVISIBLE);
            holder.ivDelete.setVisibility(View.INVISIBLE);

        }

        @Override
        public int getItemCount() {
            if(tourList == null)
                return 0;

            return tourList.size();
        }
    }

}