package com.sageabletech.fragment;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.sageabletech.R;
import com.sageabletech.adapter.GridItemAdapter;
import com.sageabletech.adapter.ListItemAdapter;
import com.sageabletech.model.FeedsDO;
import com.sageabletech.model.GetTourDO;
import com.sageabletech.utils.PreferenceUtils;

import java.util.ArrayList;

/**
 * Created by Uday on 09-07-2017.
 */

public class TourDetailsFragment extends FragmentActivity {
    private ImageView ivSelectedImage;
    private ImageView ivMapTourDetails;
    private ImageView leftBackSubDetails;
    private ImageView rightSubDetails;
    private TextView tvDateDetails;
    private Button buyTour;
    private GoogleMap gMap;
    private static final String ARG_SECTION_NUMBER = "section_number";
    private MapFragment mapFragment;
    private ListItemAdapter subListAdapter;
    private GridItemAdapter subGridAdapter;
    ArrayList<GetTourDO> mOffers;

    private LinearLayout myTourLl;
    private LinearLayout subList1;
    private LinearLayout subList2;

    //TourSubList

    private ImageView liststyle;
    private ImageView gridstyle;
    private ListView subTourList;
    private GridView subTourGrid;
    private TextView tvTitleTourDetails;

    //TourSubList

    private ImageView ivSelectedImageSub;
    private ImageView ivMapTourDetailsSub;
    private ImageView play_pause_mediaIv;
    private ImageView stop_mediaIv;
    private SeekBar seekBar;
    private TextView presentMedia;
    private TextView totalMedia;
    private boolean isPlaying;
    private int playPause = 0;
    private int nextScreen = 0;
    private MediaPlayer mp;
    private String title1, title2, title3;
    private String mediaPath;
    private PreferenceUtils preferenceUtils;
    private String tourId;
    private FeedsDO feedsDO;
    private ProgressBar progressBar;

   /* public static TourDetailsFragment newInstance(int sectionNumber) {
        TourDetailsFragment fragment = new TourDetailsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tour_detail_layout);
//        preferenceUtils = new PreferenceUtils(this);
//        if (NetworkUtils.isNetworkConnectionAvailable(this)) {
//            String str = preferenceUtils.getStringFromPreference("accessToken", "");
//            feedsDO = (FeedsDO) getIntent().getSerializableExtra("tourId");
//
//            if(feedsDO != null){
//                tourId = feedsDO.tourId;
//                prepareTourData(str, tourId);
//            }
//        } else {
//            Toast.makeText(this, "There is No Internet", Toast.LENGTH_LONG).show();
//        }
////        View rootView = inflater.inflate(R.layout.tour_detail_layout, container, false);
//
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.mapTourDetails);
//        mapFragment.getMapAsync(this);
//      /*  mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapTourDetails);
//        mapFragment.getMapAsync(this);*/
////        getActivity().getSupportFragmentManager();
//        //init the views
//        progressBar  = (ProgressBar) findViewById(R.id.imageprogressbar);
//        ivSelectedImage = (ImageView) findViewById(R.id.ivSelectedImage);
//        ivMapTourDetails = (ImageView) findViewById(R.id.ivMapTourDetails);
//        leftBackSubDetails = (ImageView) findViewById(R.id.leftBackSubDetails);
//        rightSubDetails = (ImageView) findViewById(R.id.rightSubDetails);
//        tvDateDetails = (TextView) findViewById(R.id.tvDateDetails);
//        buyTour = (Button) findViewById(R.id.buyTour);
//        myTourLl = (LinearLayout) findViewById(R.id.myTourLl);
//        subList1 = (LinearLayout) findViewById(R.id.mySubTourLl);
//        subList2 = (LinearLayout) findViewById(R.id.mySubItemLl);
//
//        //listeners to views
//        ivSelectedImage.setOnClickListener(this);
//        ivMapTourDetails.setOnClickListener(this);
//        leftBackSubDetails.setOnClickListener(this);
//        rightSubDetails.setOnClickListener(this);
//        tvDateDetails.setOnClickListener(this);
//        buyTour.setOnClickListener(this);
//
//        //SubList
//        //init views
//        liststyle = (ImageView) findViewById(R.id.liststyle);
//        gridstyle = (ImageView) findViewById(R.id.gridstyle);
//        subTourList = (ListView) findViewById(R.id.subTourList);
//        subTourGrid = (GridView) findViewById(R.id.subTourGrid);
//        tvTitleTourDetails = (TextView) findViewById(R.id.tvTitleTourDetails);
//        tvTitleTourDetails.setText(""+feedsDO.tourTitle);
//
//        //adding listeners to the views
//        liststyle.setOnClickListener(this);
//        gridstyle.setOnClickListener(this);
//        subTourList.setOnItemClickListener(this);
//        subTourGrid.setOnItemClickListener(this);
//        //sublistItem
//        //init views
//        ivSelectedImageSub = (ImageView) findViewById(R.id.ivSelectedImageSub);
//        ivMapTourDetailsSub = (ImageView) findViewById(R.id.ivMapTourDetailsSub);
//        play_pause_mediaIv = (ImageView) findViewById(R.id.play_pause_mediaIv);
//        stop_mediaIv = (ImageView) findViewById(R.id.stop_mediaIv);
//        seekBar = (SeekBar) findViewById(R.id.seekBar);
//        presentMedia = (TextView) findViewById(R.id.presentMedia);
//        totalMedia = (TextView) findViewById(R.id.totalMedia);
//
//
//        //adding listeners to view
//        ivSelectedImageSub.setOnClickListener(this);
//        ivMapTourDetailsSub.setOnClickListener(this);
//        play_pause_mediaIv.setOnClickListener(this);
//        stop_mediaIv.setOnClickListener(this);
//        seekBar.setOnSeekBarChangeListener(this);
//
//

//    }
//    private void prepareTourData(String str, String tourId) {
////        ((BaseActivity) getActivity()).showLoader();
//        RequestParams requestParams = new RequestParams();
//        String url = Urls.GET_TOUR + str + "?tourid="+tourId;
//        requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
//        requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;
//        ServiceCalls serviceCalls = new ServiceCalls(this, "GET_TOUR");
//        serviceCalls.execute(requestParams);
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.ivSelectedImage:
//
//                break;
//            case R.id.ivMapTourDetails:
//                Intent i =new Intent(this, MapsActivity.class);
//                i.putExtra("tourLangitude",mOffers.get(0).tourLangitude);
//                i.putExtra("tourLatitude",mOffers.get(0).tourLatitude);
//                i.putExtra("title",mOffers.get(0).title);
//                startActivity(i);
//                break;
//            case R.id.leftBackSubDetails:
//                Log.d("LEFT", "working");
//                if (nextScreen >= 1) {
//                    rightSubDetails.setVisibility(View.VISIBLE);
//                    nextScreen--;
//
//                    if (nextScreen == 1) {
//                        myTourLl.setVisibility(View.GONE);
//                        subList1.setVisibility(View.VISIBLE);
//                        subList2.setVisibility(View.GONE);
//                        tvTitleTourDetails.setText(title2);
//
//                    } else if (nextScreen == 0) {
//
//                        myTourLl.setVisibility(View.VISIBLE);
//                        subList1.setVisibility(View.GONE);
//                        subList2.setVisibility(View.GONE);
//                        tvTitleTourDetails.setText(title2);
//                    }
//                } else {
//                    onBackPressed();
//                }
//                break;
//            case R.id.rightSubDetails:
//                nextScreen++;
//                if (nextScreen == 1) {
//                    myTourLl.setVisibility(View.GONE);
//                    subList1.setVisibility(View.VISIBLE);
//                    subList2.setVisibility(View.GONE);
//                    tvTitleTourDetails.setText(title2);
//                    rightSubDetails.setVisibility(View.GONE);
//
//                } /*else if (nextScreen == 2) {
//                    rightSubDetails.setVisibility(View.GONE);
//                    myTourLl.setVisibility(View.GONE);
//                    subList1.setVisibility(View.GONE);
//                    subList2.setVisibility(View.VISIBLE);
//                    tvTitleTourDetails.setText(title3);
//                }*/
//                break;
//            case R.id.tvDateDetails:
//
//                break;
//            case R.id.buyTour:
//
//                break;
//            case R.id.ivSelectedImageSub:
//                break;
//            case R.id.ivMapTourDetailsSub:
//                break;
//            case R.id.play_pause_mediaIv:
//                playPause++;
//                if (playPause == 1) {
//                    play_pause_mediaIv.setImageResource(R.mipmap.ic_pause_outline_black);
////                    MediaPlayer mp = new MediaPlayer();
//                    try {
////                        mp.setDataSource(" ");
//                        /*mp.setLooping(true);
//                        mp.prepare();
//                        mp.start();*/
//                        Runnable r = new Runnable() {
//                            public void run() {
//                                try {
//                                    setDataSource(mediaPath);
//                                } catch (IOException e) {
//                                    Log.e("", e.getMessage(), e);
//                                }
//                                try {
//                                    mp.prepare();
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                                Log.v("", "Duration:  ===>" + mp.getDuration());
//                                mp.start();
//                            }
//                        };
//                        new Thread(r).start();
//
//                    } catch (Exception e) {
//                        Log.e("Media ", "prepare() failed");
//                    }
//                } else {
//                    playPause = 0;
//                    mp.pause();
//                    mp = null;
//                    play_pause_mediaIv.setImageResource(R.mipmap.ic_play_outline_black);
//                }
//                break;
//            case R.id.stop_mediaIv:
//                if (mp != null) {
//                    mp.seekTo(0);
//                }
//                break;
//            /*case R.id.left_back:
//                break;*/
//            case R.id.liststyle:
//                liststyle.setImageResource(R.mipmap.ic_list_solid_blue);
//                gridstyle.setImageResource(R.mipmap.ic_grid_solid_gray);
//                subTourList.setVisibility(View.VISIBLE);
//                subTourGrid.setVisibility(View.GONE);
//                break;
//            case R.id.gridstyle:
//                liststyle.setImageResource(R.mipmap.ic_list_solid_gray);
//                gridstyle.setImageResource(R.mipmap.ic_grid_solid_blue);
//                subTourList.setVisibility(View.GONE);
//                subTourGrid.setVisibility(View.VISIBLE);
//                break;
//            default:
//                break;
//        }
//
//    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        gMap = googleMap;
//    }
//
//    @Override
//    public void onData(Object data, String error, String tag) {
////        ((BaseActivity) getActivity()).hideLoader();
//        if (tag.equalsIgnoreCase("GET_TOUR")) {
//
//            if (error == null && data != null) {
//
//                mOffers = (ArrayList<GetTourDO>) data;
//                if (mOffers.size() > 0) {
//                    /*Offers = (List<GetTourDO>) mOffers.clone();
//                    mOfferAdapter.refresh(Offers);*/
//
//                    Glide.with(this).load(mOffers.get(0).tourPhotoPath)
//                            .listener(new RequestListener<String, GlideDrawable>() {
//                                @Override
//                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                                    progressBar.setVisibility(View.GONE);
//                                    return false;
//                                }
//
//                                @Override
//                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                                    progressBar.setVisibility(View.GONE);
//                                    return false;
//                                }
//                            })
////                            .placeholder(R.color.black)
//                            .centerCrop()
////                            .override((AppConstants.DEVICE_DISPLAY_WIDTH), (AppConstants.DEVICE_DISPLAY_HEIGHT / 3))
//                            .into(ivSelectedImage);
//                    tvDateDetails.setText("Created On " + mOffers.get(0).createdOn);
//
//                    for(SubTourItemsDO item : mOffers.get(0).subList){
//                        if(item.photolatitude!=null && !item.photolatitude.equalsIgnoreCase("0") && item.photolangitude!=null && !item.photolangitude.equalsIgnoreCase("0"))
//                        setSubTourLatLongMapMarkers(Double.parseDouble(item.photolatitude),Double.parseDouble(item.photolangitude));
//                    }
//
//                    title1 = mOffers.get(0).title;
//                    title2 = mOffers.get(0).title;
//
//
//                    //For SubList
//                    subListAdapter = new ListItemAdapter(this, mOffers.get(0).subList);
//                    subTourList.setAdapter(subListAdapter);
//                    subTourList.setOnItemClickListener(this);
//
//                    //For SubListItem
//                    subGridAdapter = new GridItemAdapter(this, mOffers.get(0).subList);
//                    subTourGrid.setAdapter(subGridAdapter);
//                    subTourGrid.setOnItemClickListener(this);
//                } else {
//
//                }
//
//            } else {
//                if (error == null && data != null) {
//
//                /*if(((MainActivity)getActivity()) !=null)
//                    if(((MainActivity)getActivity()).dialog !=null)
//                        ((MainActivity)getActivity()).dialog.dismiss();*/
//
//                    String str = preferenceUtils.getStringFromPreference("vendorId", "");
//                    prepareTourData(str, tourId);
//                }
//            }
//
//        }
//    }
//
//    private void setSubTourLatLongMapMarkers(double latitude, double longitude){
//        LatLng latLng = new LatLng(latitude,longitude);
//        gMap.addMarker(new MarkerOptions().position(latLng).title(""+mOffers.get(0).address));
//        gMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//        gMap.animateCamera(CameraUpdateFactory.zoomTo(10));
//    }
//
//    @Override
//    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//
//    }
//
//    @Override
//    public void onStartTrackingTouch(SeekBar seekBar) {
//
//    }
//
//    @Override
//    public void onStopTrackingTouch(SeekBar seekBar) {
//
//    }
//
//    @Override
//    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        SubTourItemsDO vo = (SubTourItemsDO) parent.getItemAtPosition(position);
////           switch (view.getId()) {
////              case R.id.subTourList:
////                addSubItems(vo);
//                  Intent i =new Intent(this,SubTourDetailsViewPager.class);
//                  i.putExtra("subtourList",mOffers.get(0).subList);
//                  i.putExtra("position",position);
//                  startActivity(i);
////                  break;
////          case R.id.subTourGrid:
////              Intent i1 =new Intent(this,SubTourDetailsViewPager.class);
////              i1.putExtra("subtourList",mOffers.get(0).subList);
////              i1.putExtra("position",position);
////              startActivity(i1);
//////                break;
////           default:
////                break;
////       }
//
//    }
//
//    private void addSubItems(SubTourItemsDO vo) {
//
//        totalMedia.setText(vo.audioTime);
//        Log.d("Audio time", vo.audioTime);
//        Glide.with(this).load(vo.photoPath)
//                .placeholder(R.color.black)
//                .centerCrop()
//                .override((AppConstants.DEVICE_DISPLAY_WIDTH), (AppConstants.DEVICE_DISPLAY_HEIGHT / 3))
//                .into(ivSelectedImageSub);
//        title3 = vo.photoTitle;
////        Toast.makeText(getActivity(), "test " + vo.photoTitle, Toast.LENGTH_SHORT).show();
//        try {
//            mp = new MediaPlayer();
//            mp.setOnErrorListener(this);
//            mp.setOnBufferingUpdateListener(this);
//            mp.setOnCompletionListener(this);
//            mp.setOnPreparedListener(this);
//            mp.setAudioStreamType(2);
//            mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
//            mediaPath =vo.photoAudioPath;
////            mp.setDataSource(vo.photoAudioPath);
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//    private void setDataSource(String path) throws IOException {
//        if (!URLUtil.isNetworkUrl(path)) {
//            mp.setDataSource(path);
//        } else {
//            URL url = new URL(path);
//            URLConnection cn = url.openConnection();
//            cn.connect();
//            InputStream stream = cn.getInputStream();
//            if (stream == null)
//                throw new RuntimeException("stream is null");
//            File temp = File.createTempFile("mediaplayertmp", "dat");
//            String tempPath = temp.getAbsolutePath();
//            FileOutputStream out = new FileOutputStream(temp);
//            byte buf[] = new byte[128];
//            do {
//                int numread = stream.read(buf);
//                if (numread <= 0)
//                    break;
//                out.write(buf, 0, numread);
//            } while (true);
//            mp.setDataSource(tempPath);
//            try {
//                stream.close();
//            }
//            catch (IOException ex) {
//                Log.e("", "error: " + ex.getMessage(), ex);
//            }
//        }
//    }
//    @Override
//    public boolean onError(MediaPlayer mp, int what, int extra) {
//        return false;
//    }
//
//    @Override
//    public void onBufferingUpdate(MediaPlayer mp, int percent) {
//
//    }
//
//    @Override
//    public void onCompletion(MediaPlayer mp) {
//
//    }
//
//    @Override
//    public void onPrepared(MediaPlayer mp) {
//
//    }
    }
}

