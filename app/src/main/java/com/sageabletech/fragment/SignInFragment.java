package com.sageabletech.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sageabletech.R;
import com.sageabletech.model.LoginDO;
import com.sageabletech.shomiround.ForgetPasswordScreen;
import com.sageabletech.shomiround.Login;
import com.sageabletech.shomiround.MainScreen;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.Urls;
import com.sageabletech.webservicecalls.AsyncRemoteCall;
import com.sageabletech.webservicecalls.AsyncRemoteCall.OnDataListener;
import com.sageabletech.webservicecalls.RequestParams;
import com.sageabletech.webservicecalls.ServiceCalls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class SignInFragment extends Fragment implements View.OnClickListener,OnDataListener{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    public SignInFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static SignInFragment newInstance(int sectionNumber) {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    private TextView tvSubmit,tvFgt;
    private EditText input_user,input_password;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        System.out.println("Logiiiiiiddnnn");
//            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
//            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));

        tvSubmit = (TextView)rootView.findViewById(R.id.tvSubmit);
        tvFgt = (TextView)rootView.findViewById(R.id.tvFgt);
        input_user = (EditText)rootView.findViewById(R.id.input_user);
        input_password = (EditText)rootView.findViewById(R.id.input_password);
        tvSubmit.setOnClickListener(this);
        tvFgt.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.tvSubmit){
            if(input_user.getText().toString().length() == 0 || input_password.getText().toString().length()==0){
                Toast.makeText(v.getContext(),"Please fill details",Toast.LENGTH_LONG).show();
                return;
            }
        }else{

            startActivity(new Intent(v.getContext(), ForgetPasswordScreen.class));
            return;
        }


        ((Login)getActivity()).showLoader();
        System.out.println("login clickedddd::::"+input_user.getText().toString());
        LocalData.getInstance().setEmailId(input_user.getText().toString());

        RequestParams requestParams = new RequestParams();
        String url = Urls.POST_LOGIN;
        requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
        requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;

        JSONObject loginRequest = new JSONObject();
        try {
            loginRequest.put("deviceType","mobile");
            loginRequest.put("userLoginId","");
            loginRequest.put("emailId",""+input_user.getText().toString().trim());
            loginRequest.put("password",""+input_password.getText().toString().trim());
            loginRequest.put("loginUsing","EMAIL");
            loginRequest.put("deviceToken","3BBB848D-79D6-4880-BBF6-DF645756493B");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        requestParams.data = loginRequest.toString();


//        new MakeLogin(getActivity(),""+input_user.getText().toString().trim(),input_password.getText().toString().trim()).execute();
        ServiceCalls serviceCalls = new ServiceCalls(this, "Login");
        serviceCalls.execute(requestParams);

    }

    @Override
    public void onData(Object data, String error, String tag) {

        ((Login)getActivity()).hideLoader();
        if(error == null){
            LoginDO mLogin = (LoginDO) data;
            ((Login) getActivity()).preferenceUtils.saveString("userId",mLogin.userId);
            ((Login) getActivity()).preferenceUtils.saveString("accessToken",mLogin.accessToken);
            ((Login) getActivity()).preferenceUtils.saveProfileImageURL("SelectedImage",mLogin.userPicPath);
            ((Login) getActivity()).preferenceUtils.saveString("firstName",mLogin.firstName);
            ((Login) getActivity()).preferenceUtils.saveString("lastName",mLogin.lastName);
            startActivity(new Intent(getActivity(), MainScreen.class));
            getActivity().finish();
        }else if(error.contains("Invalid Username")) {
            EditText signupedittext = LocalData.getInstance().getSignupedittext();
            signupedittext.setText(LocalData.getInstance().getEmailId());

            LocalData.getInstance().getLoginadapter().setCurrentItem(1);

        }else{
            Toast.makeText(getActivity(),""+error,Toast.LENGTH_LONG).show();
        }

    }
}