package com.sageabletech.fragment;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sageabletech.R;
import com.sageabletech.model.TourItem;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.Utils;

import java.io.IOException;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class ViewTourDetailsFragment extends Fragment implements View.OnClickListener{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private  static final String ARG_LIST = "list";
    MediaPlayer mediaPlayer = null;
    SeekBar mSeekBar;

    public ViewTourDetailsFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ViewTourDetailsFragment newInstance(int sectionNumber, TourItem mTourItem) {
        ViewTourDetailsFragment fragment = new ViewTourDetailsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putSerializable(ARG_LIST,mTourItem);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.view_tour_image_fragment, container, false);

        final TourItem mTourItem = (TourItem) getArguments().getSerializable(ARG_LIST);

        ImageView ivMap = (ImageView)rootView.findViewById(R.id.ivMap);
        ivMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*startActivity(new Intent(getActivity(),MapsActivity.class)
//                        .putExtra("tourLangitude",mTourItem.tourLangitude)
//                        .putExtra("tourLatitude",mTourItem.tourLatitude)
                        .putExtra("title",mTourItem.title));*/
            }
        });

        TextView tvDetails = (TextView)rootView.findViewById(R.id.tvDetails);

        if(!mTourItem.summary.equalsIgnoreCase("No Text"))
            tvDetails.setText(""+mTourItem.summary);
        else
            tvDetails.setVisibility(View.GONE);

        ImageView ivSelectedImage = (ImageView)rootView.findViewById(R.id.ivSelectedImage);
        ivSelectedImage.setLayoutParams(new FrameLayout.LayoutParams(AppConstants.DEVICE_DISPLAY_WIDTH , AppConstants.DEVICE_DISPLAY_WIDTH/2));

        Glide.with(this)
                .load(mTourItem.ImagePath)
                .placeholder(R.color.black)
                .centerCrop()
                .override(AppConstants.DEVICE_DISPLAY_WIDTH , AppConstants.DEVICE_DISPLAY_WIDTH/2)
                .crossFade()
                .into(ivSelectedImage);

        LinearLayout llAudio = (LinearLayout)rootView.findViewById(R.id.llAudio);

        if(mTourItem.audioPath.length()>0){
            llAudio.setVisibility(View.VISIBLE);

            mSeekBar = (SeekBar)rootView.findViewById(R.id.sbViewTour);
            final ImageView ivPlay = (ImageView)rootView.findViewById(R.id.ivPlay);
            final ImageView ivPause = (ImageView)rootView.findViewById(R.id.ivPause);

            mediaPlayer = new MediaPlayer();
            if(mTourItem.audioPath !=null)
            if(!mTourItem.audioPath.equalsIgnoreCase("")){

                try {
                    mediaPlayer.setDataSource(mTourItem.audioPath);
                    mediaPlayer.prepare();
                    mSeekBar.setProgress(0);
                    mSeekBar.setMax(100);
                } catch (IOException e) {
                    e.printStackTrace();
                }
//                    tvTime.setText(mediaPlayer.getDuration());
            }else{
                Toast.makeText(getActivity(),"File currepted",Toast.LENGTH_LONG).show();
            }

            ivPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mediaPlayer !=null)
                        if(!mediaPlayer.isPlaying()){
                            ivPlay.setEnabled(false);
                            ivPause.setEnabled(true);
                            mediaPlayer.start();
                        }
                    mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {
                            mHandler.removeCallbacks(mUpdateTimeTask);
                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {
                            mHandler.removeCallbacks(mUpdateTimeTask);
                            int totalDuration = mediaPlayer.getDuration();
                            int currentPosition = Utils.progressToTimer(seekBar.getProgress(), totalDuration);

                            // forward or backward to certain seconds
                            mediaPlayer.seekTo(currentPosition);

                            // update timer progress again
                            updateProgressBar();
                        }
                    });
                    updateProgressBar();
                }
            });

            ivPause.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mediaPlayer !=null)
                        if(mediaPlayer.isPlaying()){
                            ivPlay.setEnabled(true);
                            ivPause.setEnabled(false);
                            mediaPlayer.stop();
                        }
                }
            });

        }else{
            llAudio.setVisibility(View.GONE);
        }

        return rootView;
    }
    private Handler mHandler = new Handler();;
    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }
    /**
     * Background Runnable thread
     * */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = mediaPlayer.getDuration();
            long currentDuration = mediaPlayer.getCurrentPosition();

            // Displaying Total Duration time
//            songTotalDurationLabel.setText(""+utils.milliSecondsToTimer(totalDuration));
            // Displaying time completed playing
//            songCurrentDurationLabel.setText(""+utils.milliSecondsToTimer(currentDuration));

            // Updating progress bar
            int progress = (int)(Utils.getProgressPercentage(currentDuration, totalDuration));
            //Log.d("Progress", ""+progress);
            mSeekBar.setProgress(progress);

            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };




    @Override
    public void onClick(View v) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(mediaPlayer !=null)
            if(mediaPlayer.isPlaying())
                mediaPlayer.release();

        if(mHandler !=null)
            mHandler.removeCallbacks(mUpdateTimeTask);

    }

}