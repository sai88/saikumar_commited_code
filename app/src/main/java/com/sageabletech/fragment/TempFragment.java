package com.sageabletech.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.sageabletech.R;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class TempFragment extends Fragment{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    public TempFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static TempFragment newInstance(int sectionNumber) {
        TempFragment fragment = new TempFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    private TextView tvSubmit;
    private EditText input_user,input_password;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.placeholder, container, false);

        return rootView;
    }

}