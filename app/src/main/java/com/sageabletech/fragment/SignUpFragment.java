package com.sageabletech.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sageabletech.R;
import com.sageabletech.shomiround.Login;
import com.sageabletech.shomiround.MainScreen;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.Urls;
import com.sageabletech.webservicecalls.AsyncRemoteCall;
import com.sageabletech.webservicecalls.RequestParams;
import com.sageabletech.webservicecalls.ServiceCalls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class SignUpFragment extends Fragment implements View.OnClickListener,AsyncRemoteCall.OnDataListener {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    public SignUpFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static SignUpFragment newInstance(int sectionNumber) {
        SignUpFragment fragment = new SignUpFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        System.out.println("caledddddddddfdfdgdfgdfgdfg");
        return fragment;
    }

    private TextView tvSubmit,terms_conditions;
    CheckBox check_terms;
    private EditText input_user,input_password,input_confirm_password;

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("on resumeeeeee::::");
        input_user.setText(LocalData.getInstance().getEmailId());
    }

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("on pauseee::::");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_signup, container, false);
        System.out.println("caledddddddd");

        tvSubmit = (TextView)rootView.findViewById(R.id.tvSubmit);
        input_user = (EditText)rootView.findViewById(R.id.input_user);
        LocalData.getInstance().setSignupedittext(input_user);

        check_terms=(CheckBox)rootView.findViewById(R.id.check_terms);
        terms_conditions=(TextView)rootView.findViewById(R.id.terms_conditions);
        input_confirm_password = (EditText)rootView.findViewById(R.id.input_confirm_password);
        input_password = (EditText)rootView.findViewById(R.id.input_password);
        tvSubmit.setOnClickListener(this);
        terms_conditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),TermsAndConditionsActivity.class);
                getActivity().startActivity(intent);
            }
        });

        return rootView;
    }

    @Override
    public void onClick(View v) {

        if(input_user.getText().toString().length() == 0 || input_password.getText().toString().length()==0){
            Toast.makeText(v.getContext(), "Please fill details", Toast.LENGTH_LONG).show();
            return;
        }else if(!input_confirm_password.getText().toString().equalsIgnoreCase(input_password.getText().toString())){
            Toast.makeText(v.getContext(), "Password and Confirm Password should be same", Toast.LENGTH_LONG).show();
            return;
        }else if(!check_terms.isChecked()){
            Toast.makeText(v.getContext(), "Please accept terms and conditions.", Toast.LENGTH_LONG).show();
            return;
        }

        ((Login)getActivity()).showLoader();
        RequestParams requestParams = new RequestParams();
        String url = Urls.POST_SIGNUP;
        requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
        requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;

        JSONObject loginRequest = new JSONObject();
        try {
            loginRequest.put("firstName","");
            loginRequest.put("lastName","");
            loginRequest.put("email",""+input_user.getText().toString().trim());
            loginRequest.put("password",""+input_password.getText().toString().trim());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        requestParams.data = loginRequest.toString();

        ServiceCalls serviceCalls = new ServiceCalls(this, "Register");
        serviceCalls.execute(requestParams);

    }

    @Override
    public void onData(Object data, String error, String tag) {
        ((Login)getActivity()).hideLoader();
        if(error == null){

            if("User is signed in successfully".equalsIgnoreCase(data.toString())){
                startActivity(new Intent(getActivity(), MainScreen.class));
                getActivity().finish();
            }else {
                    Toast.makeText(getActivity(),""+data.toString(),Toast.LENGTH_LONG).show();
                        input_confirm_password.setText("");
                        input_password.setText("");
                        input_user.setText("");
                        ((Login)getActivity()).changeTabToLogin();

            }

        }else{
            Toast.makeText(getActivity(),""+error,Toast.LENGTH_LONG).show();
        }

    }
}