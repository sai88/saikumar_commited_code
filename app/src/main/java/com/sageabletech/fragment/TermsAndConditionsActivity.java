package com.sageabletech.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sageabletech.R;
import com.sageabletech.utils.Urls;

import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by Ramya_Android on 7/31/2017.
 */

public class TermsAndConditionsActivity extends Activity {

    private TextView termsText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_condition_layout);

        termsText = (TextView)findViewById(R.id.termsText);
getHeader();
        new TermsTask().execute();
    }
    TextView header;
    ImageView back;
    ImageView submit;
    public void getHeader(){
        header=(TextView) findViewById(R.id.header_text);
        header.setText("Terms And Conditions");
        back=(ImageView)findViewById(R.id.back);
        submit=(ImageView)findViewById(R.id.submit);
        submit.setVisibility(View.GONE);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();

            }
        });





    }

    class TermsTask extends AsyncTask<Void,Void,String>{

        private String text;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            dialog=new ProgressDialog(TermsAndConditionsActivity.this);
            dialog.setMessage("please wait..");
            dialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {

            String  url = Urls.TERMS;
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            System.out.println("url formed::::" + url);
            HttpPost httpPost = new HttpPost(url);

            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
            try {
                text = getASCIIContentFromEntity(httpClient.execute(httpPost, localContext).getEntity());
            } catch (Exception e) {
                System.out.println(e.getLocalizedMessage());
                return e.getLocalizedMessage();
            }
            return text;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            System.out.println("terms result:"+s);
            if(s != null){
                try{
                    JSONObject object = new JSONObject(s);
                    String msg  = object.getString("message");
                    if(msg != null && msg.equalsIgnoreCase("Success")){
                       JSONObject dataObj = object.getJSONObject("data");
                        if(dataObj != null){
                            String termId = dataObj.getString("termsId");
                            String termfile = dataObj.getString("termsFile");
                            String updatedDate = dataObj.getString("updatedOn");

                            termsText.setText(termfile);
                        }
                    }

                }catch (Exception e){

                }

            }
        }

        public  String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
            InputStream in = entity.getContent();
            StringBuffer out = new StringBuffer();
            int n = 1;
            while (n > 0) {
                byte[] b = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_FORWARD];
                n = in.read(b);
                if (n > 0) {
                    out.append(new String(b, 0, n));
                }
            }
            return out.toString();
        }
    }
}
