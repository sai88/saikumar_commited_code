package com.sageabletech.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sageabletech.R;

/**
 * Created by Uday on 09-07-2017.
 */

public class TourSubListFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {
//    private ImageView left_back;
    private ImageView liststyle;
    private ImageView gridstyle;
    private ListView subTourList;
    private GridView subTourGrid;
    private TextView tvTitleTourDetails;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sub_tour_details, container, false);

        //init views
//        left_back = (ImageView) rootView.findViewById(R.id.left_back);
        liststyle = (ImageView) rootView.findViewById(R.id.liststyle);
        gridstyle = (ImageView) rootView.findViewById(R.id.gridstyle);
        subTourList = (ListView) rootView.findViewById(R.id.subTourList);
        subTourGrid = (GridView) rootView.findViewById(R.id.subTourGrid);
        tvTitleTourDetails = (TextView) rootView.findViewById(R.id.tvTitleTourDetails);

        //adding listeners to the views
//        left_back.setOnClickListener(this);
        liststyle.setOnClickListener(this);
        gridstyle.setOnClickListener(this);
        subTourList.setOnItemClickListener(this);
        subTourGrid.setOnItemClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
          /*  case R.id.left_back:

                break;*/
            case R.id.liststyle:
                liststyle.setImageResource(R.mipmap.ic_list_solid_blue);
                gridstyle.setImageResource(R.mipmap.ic_grid_solid_gray);

                break;
            case R.id.gridstyle:
                liststyle.setImageResource(R.mipmap.ic_list_solid_gray);
                gridstyle.setImageResource(R.mipmap.ic_grid_solid_gray);

                break;
            default:

                break;
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.subTourList:

                break;
            case R.id.subTourGrid:

                break;
            default:
                break;
        }

    }
}
