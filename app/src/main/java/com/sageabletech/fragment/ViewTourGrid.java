package com.sageabletech.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import com.sageabletech.R;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.MapsActivity;
import com.sageabletech.utils.AppConstants;

import java.util.ArrayList;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class ViewTourGrid extends Fragment{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    ArrayList<TourItem> tourList = null;

    public ViewTourGrid() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ViewTourGrid newInstance(int sectionNumber, Context ctx, ArrayList<TourItem> mSubTourItems) {

        ViewTourGrid fragment = new ViewTourGrid();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putSerializable("SubTour", mSubTourItems);
        fragment.setArguments(args);
        return fragment;
    }
     ImageAdapter mImageAdapterGridView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.my_grid_fragment, container, false);

        GridView gallery = (GridView) rootView.findViewById(R.id.galleryGridView);

        tourList = (ArrayList<TourItem>) getArguments().getSerializable("SubTour");

        mImageAdapterGridView = new ImageAdapter(getActivity());
        gallery.setAdapter(mImageAdapterGridView);

        /*gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(getActivity(), ViewTourPhotosDetails.class)
                        .putExtra("SubTour",tourList));
            }
        });*/


        return rootView;
    }

    private class ImageAdapter extends BaseAdapter {

        /** The context. */
        private Activity context;

        /**
         * Instantiates a new image adapter.
         *
         * @param localContext
         *            the local context
         */
        public ImageAdapter(Activity localContext) {
            context = localContext;
        }

        public int getCount() {
            if(tourList == null){
                return 0;
            }
            return tourList.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            final ViewHolder holder;

            if (convertView == null) {

                convertView = LayoutInflater.from(context).inflate(R.layout.mytour_grid_itemnew,null,false);
                holder = new ViewHolder();
                holder.img = (ImageView) convertView.findViewById(R.id.imageView1);
                holder.ivEdit = (ImageView) convertView.findViewById(R.id.ivEdit);
                holder.ivLocation = (ImageView) convertView.findViewById(R.id.ivLocation);
                holder.ivDelete = (ImageView) convertView.findViewById(R.id.ivDelete);
                convertView.setTag(holder);

            } else {
                holder = (ViewHolder)convertView.getTag();
            }

            holder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);
            convertView.setLayoutParams(new GridView.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2, (AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2));

            //Check availablity in Local Db

            Glide.with(context)
                        .load(tourList.get(position).ImagePath)
                        .centerCrop()
                        .placeholder(R.color.black)
                        .thumbnail(0.5f)
                        .crossFade()
                        .into(holder.img);

            holder.ivEdit.setVisibility(View.INVISIBLE);
            holder.ivLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                            startActivity(new Intent(getActivity(),MapsActivity.class)
                                    .putExtra("tourLangitude",tourList.get(position).lang)
                                    .putExtra("tourLatitude",tourList.get(position).lat)
                                    .putExtra("title",tourList.get(position).title));
                }
            });
            holder.ivDelete.setVisibility(View.INVISIBLE);

            return convertView;
        }


        public class ViewHolder{
                ImageView img,ivEdit,ivLocation,ivDelete;
        }

    }

    @Override
    public void onResume() {
        super.onResume();

    }

}