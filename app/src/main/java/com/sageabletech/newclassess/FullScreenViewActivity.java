package com.sageabletech.newclassess;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.sageabletech.R;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.Utils;

import java.util.ArrayList;

/**
 * Created by ASMAN on 07-08-2017.
 */

public class FullScreenViewActivity extends Activity {

    private Utils utils;
    private FullScreenImageAdapter adapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_view);

        viewPager = (ViewPager) findViewById(R.id.pager);

        utils = new Utils();

        Intent i = getIntent();
        int position = i.getIntExtra("position", 1);
        ArrayList<String> arrayList=new ArrayList<>();

//        MyTourItemDO favselectedItem = LocalData.getInstance().getFavselectedItem();
//        ArrayList<TourItem> subtour = favselectedItem.subtour;
//        for (int j=0;j<subtour.size();j++){
//            TourItem tourItem = subtour.get(j);
//            String imagePath1 = tourItem.ImagePath;
//            arrayList.add(imagePath1);
//
//
//        }



        adapter = new FullScreenImageAdapter(FullScreenViewActivity.this, LocalData.getInstance().getDisplayImagesArry());

        viewPager.setAdapter(adapter);

        // displaying selected image first
        viewPager.setCurrentItem(position);
    }

}