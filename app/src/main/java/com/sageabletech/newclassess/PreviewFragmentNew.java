package com.sageabletech.newclassess;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sageabletech.R;
import com.sageabletech.model.FeedsDO;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.views.DividerItemDecoration;
import com.sageabletech.viewtour.FavMainTour;
import com.sageabletech.viewtour.ObjectSerializer;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class PreviewFragmentNew extends Fragment  implements OnMapReadyCallback {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */    MyTourItemDO selectedMyTourItemDO;

    private static final String ARG_SECTION_NUMBER = "section_number";
    private ArrayList<TourItem> images;

    public PreviewFragmentNew() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PreviewFragmentNew newInstance(int sectionNumber, Context ctx) {

        PreviewFragmentNew fragment = new PreviewFragmentNew();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);

        fragment.setArguments(args);
        return fragment;
    }
    PreviewAdapter mImageAdapter;
ImageView ivMapTourDetails;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.preview_fragment, null, false);



        ivMapTourDetails = (ImageView) rootView.findViewById(R.id.ivMapTourDetails);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.mapTourDetails);
        mapFragment.getMapAsync(this);
//        SupportMapFragment mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager()
//                .findFragmentById(R.id.mapTourDetails);
//        mapFragment.getMapAsync(PreviewFragmentNew.this);

        selectedMyTourItemDO = LocalData.getInstance().getSelectedMyTourItemDO();
        images=selectedMyTourItemDO.subtour;

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.respodedList);
        recyclerView.setNestedScrollingEnabled(false);

        mImageAdapter = new PreviewAdapter(getActivity());

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mImageAdapter);
//        try {
//            String serialize = ObjectSerializer.serialize(getAllImagePaths());
//            System.out.println("getAllImagePaths()::"+getAllImagePaths());
//            PreferenceUtils.saveString("update", serialize);
//        }catch (Exception e){
//
//        }

        return rootView;
    }
    public ArrayList getAllImagePaths(){
        ArrayList<String> arryImg=new ArrayList<>();
        MyTourItemDO myTour = this.selectedMyTourItemDO;
        String tourPhotoPath = myTour.tourPhotoPath;
        arryImg.add(tourPhotoPath);
        ArrayList<TourItem> subtour = myTour.subtour;
        for (int i=0;i<subtour.size();i++){
            TourItem tourItem = subtour.get(i);
            String imagePath = tourItem.ImagePath;
            System.out.println("imagePath:::"+imagePath);
            arryImg.add(imagePath);

        }
        return arryImg;

    }
    private GoogleMap googleMap;
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap  = googleMap;
        ArrayList<Marker> mMarkerArray = new ArrayList<Marker>();
//        System.out.println("favselectedItem.tourLatitude::"+favselectedItem.tourLatitude+"::"+favselectedItem.tourLangitude);
        if (!selectedMyTourItemDO.tourLatitude.isEmpty() && !selectedMyTourItemDO.tourLangitude.isEmpty()) {

            LatLng latLng1 = new LatLng(Double.parseDouble(selectedMyTourItemDO.tourLatitude),
                    Double.parseDouble(selectedMyTourItemDO.tourLangitude));
            if (latLng1 != null) {
                Marker markers = googleMap.addMarker(new MarkerOptions().position(latLng1).title("Markers"));
                mMarkerArray.add(markers);
//                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng1));


            }
        }
        ArrayList<TourItem> subtour = selectedMyTourItemDO.subtour;
        System.out.println("subtour array size:::"+subtour.size());
        for (int i=0;i<subtour.size();i++){
            TourItem tourItem = subtour.get(i);
            if(tourItem!=null) {
                String tourLatitude = tourItem.latitude;
                String tourLangitude = tourItem.longitude;
                System.out.println("tourLatitude %%%::" + tourLatitude);
                if (tourLangitude != null && !tourLangitude.equalsIgnoreCase("") && !tourLangitude.equalsIgnoreCase("0")) {
                    LatLng latLng1 = new LatLng(Double.parseDouble(tourLatitude),
                            Double.parseDouble(tourLangitude));
                    if (latLng1 != null) {
                        Marker markers = googleMap.addMarker(new MarkerOptions().position(latLng1).title("Markers"));
//                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng1));
                        mMarkerArray.add(markers);


                    }
                }
            }
        }

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : mMarkerArray) {
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.20);
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        googleMap.animateCamera(cu);

    }
    public void mapRefresh(){
        ArrayList<Marker> mMarkerArray = new ArrayList<Marker>();
//        System.out.println("favselectedItem.tourLatitude::"+favselectedItem.tourLatitude+"::"+favselectedItem.tourLangitude);
        if (!selectedMyTourItemDO.tourLatitude.isEmpty() && !selectedMyTourItemDO.tourLangitude.isEmpty()) {

            LatLng latLng1 = new LatLng(Double.parseDouble(selectedMyTourItemDO.tourLatitude),
                    Double.parseDouble(selectedMyTourItemDO.tourLangitude));
            if (latLng1 != null) {
                Marker markers = googleMap.addMarker(new MarkerOptions().position(latLng1).title("Markers"));
                mMarkerArray.add(markers);
//                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng1));


            }
        }
        ArrayList<TourItem> subtour = selectedMyTourItemDO.subtour;
        System.out.println("subtour array size:::"+subtour.size());
        for (int i=0;i<subtour.size();i++){
            TourItem tourItem = subtour.get(i);
            if(tourItem!=null) {
                String tourLatitude = tourItem.latitude;
                String tourLangitude = tourItem.longitude;
                System.out.println("tourLatitude %%%::" + tourLatitude);
                if (tourLangitude != null && !tourLangitude.equalsIgnoreCase("") && !tourLangitude.equalsIgnoreCase("0")) {
                    LatLng latLng1 = new LatLng(Double.parseDouble(tourLatitude),
                            Double.parseDouble(tourLangitude));
                    if (latLng1 != null) {
                        Marker markers = googleMap.addMarker(new MarkerOptions().position(latLng1).title("Markers"));
//                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng1));
                        mMarkerArray.add(markers);


                    }
                }
            }
        }

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : mMarkerArray) {
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.20);
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        googleMap.animateCamera(cu);
    }

    private class PreviewAdapter extends RecyclerView.Adapter<PreviewAdapter.MyViewHolder> {


        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title,tvDesc;
            public ImageView ivImage;


            public MyViewHolder(View view) {
                super(view);
            title = (TextView) view.findViewById(R.id.tvItemName);
                tvDesc = (TextView) view.findViewById(R.id.tvDesc);
                ivImage = (ImageView)view.findViewById(R.id.ivImage);
            }
        }

        public void refresh(List<FeedsDO> mOffers){

//            this.mOffers = mOffers;
            this.notifyDataSetChanged();
        }

        public PreferenceUtils preferenceUtils = null;
        public PreviewAdapter(Context mContext) {
                preferenceUtils = new PreferenceUtils(getActivity());
//            this.mContext = mContext;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.preview_item, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
        /*FeedsAdapter mOfferDO = mOffers.get(position);
        holder.title.setText(mOfferDO.title);
        holder.tvTime.setText("posted " + mOfferDO.createdAt);

        String imageUrl = "";
        if(mOfferDO.image.startsWith("http")){
            imageUrl= mOfferDO.image;
        }else{
            imageUrl= Urls.IMAGE_BASE_URL+mOfferDO.image;
        }*/
//            LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(
            holder.ivImage.setLayoutParams(new RelativeLayout.LayoutParams(AppConstants.DEVICE_DISPLAY_WIDTH/3, AppConstants.DEVICE_DISPLAY_WIDTH/3));

            if(!images.get(position).ImagePath.equalsIgnoreCase("Plus")){
                String userId = preferenceUtils.getStringFromPreference("userId","");
                String url = images.get(position).ImagePath;
                if(url.contains(userId)&&!url.contains("https://s3.amazonaws.com/")){
                    url="https://s3.amazonaws.com/shomiround-production/"+url;
                }

                Glide.with(getActivity()).load(url)

                        .placeholder(R.color.black)
                        .override(AppConstants.DEVICE_DISPLAY_WIDTH/3, AppConstants.DEVICE_DISPLAY_WIDTH/3).into(holder.ivImage);
//                Picasso.with(getActivity())
//                        .load(url)
//                        .resize(AppConstants.DEVICE_DISPLAY_WIDTH/3, AppConstants.DEVICE_DISPLAY_WIDTH/3).into(holder.ivImage);
//
//                Glide.with(getActivity()).load(url)
//                        .placeholder(R.color.black)
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .centerCrop()
//                        .override(AppConstants.DEVICE_DISPLAY_WIDTH/2, AppConstants.DEVICE_DISPLAY_WIDTH/2)
//                        .into(holder.ivImage);
                if(!images.get(position).text.equalsIgnoreCase("No Text"))
                {
                    holder.title.setText(""+images.get(position).text);
                    holder.tvDesc.setText(""+images.get(position).summary);
                }

            }



//        holder.ivImage
        }

        @Override
        public int getItemCount() {
            //mOffers.size();
            return images.size();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mImageAdapter.notifyDataSetChanged();
    }
}