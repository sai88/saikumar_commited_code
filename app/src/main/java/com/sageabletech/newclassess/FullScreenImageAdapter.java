package com.sageabletech.newclassess;

/**
 * Created by ASMAN on 07-08-2017.
 */

//import info.androidhive.imageslider.R;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sageabletech.R;
import com.sageabletech.viewtour.TouchImageView;

public class FullScreenImageAdapter extends PagerAdapter {

    private Activity _activity;
    private ArrayList<Bitmap> _imagePaths;
    private LayoutInflater inflater;

    // constructor
    public FullScreenImageAdapter(Activity activity,
                                  ArrayList<Bitmap> imagePaths) {
        this._activity = activity;
        this._imagePaths = imagePaths;
    }

    @Override
    public int getCount() {
        return this._imagePaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        TouchImageView imgDisplay;
        ImageView btnClose;
        TextView tvTitle;
        ImageView submit;

        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,
                false);

        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);

        tvTitle=(TextView)viewLayout.findViewById(R.id.header_text);
        tvTitle.setVisibility(View.GONE);

//        tvTitle.setText("NEW TOUR");
        btnClose = (ImageView) viewLayout.findViewById(R.id.submit);
        btnClose.setImageDrawable(_activity.getResources().getDrawable(R.mipmap.ic_ic_cross_white));




        submit =(ImageView)viewLayout.findViewById(R.id.back);
        submit.setVisibility(View.GONE);

//        btnClose = (Button) viewLayout.findViewById(R.id.btnClose);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//        if(_imagePaths.get(position).contains("https://s3.amazonaws.com/")){
//            try {
//                URL url = new URL(_imagePaths.get(position));
//                Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//                imgDisplay.setImageBitmap(image);
//            } catch(IOException e) {
//                System.out.println(e);
//            }
//        }else {
//            Bitmap bitmap = BitmapFactory.decodeFile(_imagePaths.get(position), options);
//            imgDisplay.setImageBitmap(bitmap);
//        }
        imgDisplay.setImageBitmap(_imagePaths.get(position));
        // close button click event
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _activity.finish();
            }
        });

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}