package com.sageabletech.newclassess;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.sageabletech.R;
import com.sageabletech.model.AddTourDO;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.AddAudioActivity;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class AddAudioFragmentNew extends Fragment{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    public AddTourDO mAddTourDO;
    private static final String ARG_SECTION_NUMBER = "section_number";
    private ArrayList<TourItem> images=new ArrayList<>();

    public AddAudioFragmentNew() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static AddAudioFragmentNew newInstance(int sectionNumber, Context ctx) {

        AddAudioFragmentNew fragment = new AddAudioFragmentNew();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);

        fragment.setArguments(args);
        return fragment;
    }
    ImageAdapter mImageAdapter;
    int selectedPos = -1;
    MyTourItemDO selectedMyTourItemDO;
    public PreferenceUtils preferenceUtils = null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.grid_fragment, container, false);

//        images = (ArrayList<TourItem>) getArguments().getSerializable("Images");

        selectedMyTourItemDO = LocalData.getInstance().getSelectedMyTourItemDO();
        images=selectedMyTourItemDO.subtour;


        GridView gallery = (GridView) rootView.findViewById(R.id.galleryGridView);

        mImageAdapter = new ImageAdapter(getActivity());
        gallery.setAdapter(mImageAdapter);

        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedPos = position;
//                SelectedImage
                preferenceUtils = new PreferenceUtils(getActivity());
                System.out.println("images.get(position).ImagePath.audioPath()::"+images.get(position).audioPath.toString());
                String url = images.get(position).audioPath;
                String image = images.get(position).ImagePath;
                String userId = preferenceUtils.getStringFromPreference("userId","");

                if(url.contains(userId)&&!url.contains("https://s3.amazonaws.com/")){
                    url="https://s3.amazonaws.com/shomiround-production/"+url;
                }
                if(image.contains(userId)&&!image.contains("https://s3.amazonaws.com/")){
                    image="https://s3.amazonaws.com/shomiround-production/"+image;
                }

                LocalData.getInstance().setSubtourposition(position);
                LocalData.getInstance().setSubtouredittitle(true);
                LocalData.getInstance().setDataofsubtour( images.get(position));

                startActivityForResult(new Intent(getActivity(), AddAudioActivity.class)

                        .putExtra("SelectedImage",image)
                        .putExtra("path", url)
                        .putExtra("From", "AddCaption").putExtra("title", ""+images.get(position).text)
                        .putExtra("position", position), 107);


            }
        });

        return rootView;
    }

    public void refreshFragment(String path){
        if(mImageAdapter!=null){
            if(selectedPos != -1)

                images.get(selectedPos).audioPath = path;

            images = selectedMyTourItemDO.subtour;
            mImageAdapter.notifyDataSetChanged();
        }
    }
    public void refresh(){
        MyTourItemDO selectedMyTourItemDO = LocalData.getInstance().getSelectedMyTourItemDO();
        images= selectedMyTourItemDO.subtour;
        mImageAdapter.notifyDataSetChanged();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("requestCode"+requestCode, ""+resultCode);

        if(requestCode ==107 && resultCode == getActivity().RESULT_OK){
            Log.e("requestCode :"+requestCode, "resultCode :"+resultCode);
            if(data !=null){
                Log.e("selectedPos "+selectedPos, "data "+data.getStringExtra("AudioSavePathInDevice"));
                if(selectedPos != -1)
                   selectedMyTourItemDO.subtour.get(selectedPos).audioPath = data.getStringExtra("AudioSavePathInDevice");
                selectedMyTourItemDO.subtour.get(selectedPos).audioTime = data.getStringExtra("audio_time");
//                in.putExtra("audio_time",""+audio_time);
                refreshFragment(data.getStringExtra("AudioSavePathInDevice"));
                LocalData.getInstance().setSelectedMyTourItemDO(selectedMyTourItemDO);
            }

            if(LocalData.getInstance().isSubtouredittitle()){
                System.out.println("heldksfkjdsfdslf");
                int subtourposition = LocalData.getInstance().getSubtourposition();
                images.set(subtourposition,LocalData.getInstance().getDataofsubtour());
                LocalData.getInstance().setSubtouredittitle(false);

            }
        }

    }


    private class ImageAdapter extends BaseAdapter {

        /** The context. */
        private Activity context;

        /**
         * Instantiates a new image adapter.
         *
         * @param localContext
         *            the local context
         */
        public PreferenceUtils preferenceUtils = null;
        public ImageAdapter(Activity localContext) {
            preferenceUtils = new PreferenceUtils(getActivity());

            context = localContext;
        }

        public int getCount() {
            if(images == null){
                return 0;
            }else {
//                LocalData.getInstance().setSelectedMyTourItemDO(images);
                return images.size();
            }
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            final ViewHolder holder;

            if (convertView == null) {

                convertView = LayoutInflater.from(context).inflate(R.layout.grid_item,null,false);
                holder = new ViewHolder();
                holder.img = (ImageView) convertView.findViewById(R.id.imageView1);
                holder.tv = (CheckBox) convertView.findViewById(R.id.checkBox1);
                holder.ivClose = (ImageView) convertView.findViewById(R.id.ivClose);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolder)convertView.getTag();
            }

            holder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);
            convertView.setLayoutParams(new GridView.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2, (AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2));
            System.out.println("imagessssssdd12333::"+images.get(position).ImagePath);
            String userId = preferenceUtils.getStringFromPreference("userId","");


            if(!images.get(position).ImagePath.equalsIgnoreCase("Plus")){
                String url = images.get(position).ImagePath;
                if(url.contains(userId)&&!url.contains("https://s3.amazonaws.com/")){
                    url="https://s3.amazonaws.com/shomiround-production/"+url;
                }
//                Glide.with(context)
//                        .load(url)
//                        .centerCrop()
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .placeholder(R.color.black)
//                        .thumbnail(0.5f)
//                        .crossFade()
//                        .into(holder.img);

                Glide.with(getActivity()).load(url)

                        .placeholder(R.color.black)
                        .override(AppConstants.DEVICE_DISPLAY_WIDTH/2, AppConstants.DEVICE_DISPLAY_WIDTH/2).into(holder.img);
//                Picasso.with(getActivity())
//                        .load(url)
//                        .placeholder(R.color.black)
//                        .resize(AppConstants.DEVICE_DISPLAY_WIDTH/2, AppConstants.DEVICE_DISPLAY_WIDTH/2).into(holder.img);

                holder.ivClose.setVisibility(View.GONE);

            }

            if(images.get(position).audioPath.length()>0){
                holder.ivClose.setVisibility(View.VISIBLE);
                holder.ivClose.setOnClickListener(null);
                holder.ivClose.setImageResource(R.mipmap.ic_ic_play_outline_white);
            }else{
                holder.ivClose.setVisibility(View.GONE);
            }



            return convertView;
        }

        public class ViewHolder
        {
            CheckBox tv;
            ImageView img,ivClose;

        }

    }

}