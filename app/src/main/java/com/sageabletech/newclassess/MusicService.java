package com.sageabletech.newclassess;
import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sageabletech.R;

import java.util.concurrent.TimeUnit;


public class MusicService extends Activity {
    private Button b2,b3;
    private MediaPlayer mediaPlayer;

    private double startTime = 0;
    private double finalTime = 0;

    private Handler myHandler = new Handler();;
    private int forwardTime = 5000;
    private int backwardTime = 5000;
    private SeekBar seekbar;
    private TextView tx1,tx2,tx3;

    public static int oneTimeOnly = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.music);

        b2 = (Button) findViewById(R.id.button2);
        b3 = (Button)findViewById(R.id.button3);
//
        tx1 = (TextView)findViewById(R.id.textView2);
        tx2 = (TextView)findViewById(R.id.textView3);

        Uri uri = Uri.parse("https://s3.amazonaws.com/shomiround/199/NJPIC-AudioRecording.m4a");

        mediaPlayer = MediaPlayer.create(this, uri);
        seekbar = (SeekBar)findViewById(R.id.seekBar);
        seekbar.setClickable(false);
        b2.setEnabled(false);

        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Playingsound",Toast.LENGTH_SHORT).show();
                        mediaPlayer.start();

                finalTime = mediaPlayer.getDuration();
                startTime = mediaPlayer.getCurrentPosition();

                if (oneTimeOnly == 0) {
                    seekbar.setMax((int) finalTime);
                    oneTimeOnly = 1;
                }

                tx2.setText(getTimeTotal());



                tx1.setText(getRunningTime());

                seekbar.setProgress((int)startTime);
                myHandler.postDelayed(UpdateSongTime,100);
                b2.setEnabled(true);
                b3.setEnabled(false);
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Pausing sound",Toast.LENGTH_SHORT).show();
                        mediaPlayer.pause();
                b2.setEnabled(false);
                b3.setEnabled(true);
            }
        });

//
    }
    public String getTimeTotal(){
        long minutes = TimeUnit.MILLISECONDS.toMinutes((long) finalTime);
        long seconds = TimeUnit.MILLISECONDS.toSeconds((long) finalTime)-
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) finalTime));
        String sec = String.valueOf(seconds);
        String min = String.valueOf(minutes);

        if (sec.length() == 1) {
            sec = "0" + sec;
        }
        if (min.length() == 1) {
            min = "0" + min;
        }
        return "-"+min+":"+sec;

    }
    public String getTotalTimeDecrease(){
        long minutesTotal = TimeUnit.MILLISECONDS.toMinutes((long) finalTime);
        long secondstotal = TimeUnit.MILLISECONDS.toSeconds((long) finalTime)-
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) finalTime));

            long minutes = TimeUnit.MILLISECONDS.toMinutes((long) startTime);
            long seconds= TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) startTime));

        long presentMin=minutesTotal-minutes;
        long presentsec=secondstotal-seconds;

        String sec = String.valueOf(presentsec);
        String min = String.valueOf(presentMin);

        if (sec.length() == 1) {
            sec = "0" + sec;
        }
        if (min.length() == 1) {
            min = "0" + min;
        }
        return "-"+min+":"+sec;
    }
    public String getRunningTime(){
        long minutes = TimeUnit.MILLISECONDS.toMinutes((long) startTime);
        long seconds= TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) startTime));
        String sec = String.valueOf(seconds);
        String min = String.valueOf(minutes);

        if (sec.length() == 1) {
            sec = "0" + sec;
        }
        if (min.length() == 1) {
            min = "0" + min;
        }
        return min+":"+sec;

    }

    private Runnable UpdateSongTime = new Runnable() {
        public void run() {
            startTime = mediaPlayer.getCurrentPosition();
            tx1.setText(getRunningTime());
            tx2.setText(getTotalTimeDecrease());
            seekbar.setProgress((int)startTime);
            myHandler.postDelayed(this, 100);
        }
    };
}