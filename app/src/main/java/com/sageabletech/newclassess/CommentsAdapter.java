package com.sageabletech.newclassess;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.Engine;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sageabletech.R;
import com.sageabletech.model.FeedsDO;
import com.sageabletech.newclassess.CommentsPage;
import com.sageabletech.newclassess.WordUtils;
import com.sageabletech.shomiround.FinalScreen;
import com.sageabletech.shomiround.MainScreen;
import com.sageabletech.shomiround.MapsActivity;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.sageabletech.viewtour.AddAsFavourite;
import com.sageabletech.viewtour.DeleteFavourite;
import com.sageabletech.viewtour.GetComments;
import com.sageabletech.viewtour.GetTourDetails;
import com.sageabletech.viewtour.SaveViewCount;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by WIN 7 on 3/25/2017.
 */
public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MyViewHolder> {

    private  ArrayList<HashMap<String,String>> mOffers;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView user,tvItemTime,comment_txt;
        public ImageView ivImage;



        public MyViewHolder(View view) {
            super(view);

            ivImage = (ImageView) view.findViewById(R.id.comm_ivImageUser);
            tvItemTime = (TextView) view.findViewById(R.id.comm_time);
            user = (TextView) view.findViewById(R.id.comm_User);
            comment_txt= (TextView) view.findViewById(R.id.comment_txt);

        }
    }








    public CommentsAdapter(Context mContext, ArrayList<HashMap<String,String>> data) {
        this.mOffers = data;
        System.out.println("mOffers:::"+mOffers.size());
        this.mContext = mContext;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comments, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        HashMap<String, String> hashMap = mOffers.get(position);

        String tourid = hashMap.get("tourid");
        String comment = hashMap.get("comment");
//            String userid = hashMap.get("userid");
        String userFullName = hashMap.get("userFullName");
        String commentDate = hashMap.get("commentDate");
        String profilePicPath = hashMap.get("profilePicPath");
        holder.tvItemTime.setText(commentDate);
        holder.comment_txt.setText(comment);
        holder.user.setText(userFullName);




//        holder.ivImage.setLayoutParams(new FrameLayout.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH),
//                (AppConstants.DEVICE_DISPLAY_HEIGHT / 3)));

        if (!profilePicPath.equalsIgnoreCase("")) {
            Glide.with(mContext).load(profilePicPath)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                                progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            return false;
                        }
                    })
//                .placeholder(R.color.black)
                    .override(40, 40)
                    .into(holder.ivImage);
        }





    }


    @Override
    public int getItemCount() {
        //mOffers.size();
        return mOffers.size();
    }

}