package com.sageabletech.newclassess;

import java.io.File;
import java.util.ArrayList;
import java.util.SortedSet;
import java.util.TreeSet;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sageabletech.R;
import com.sageabletech.model.ImagesDO;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.Utils;

/**
 * Created by ASMAN on 19-07-2017.
 */

public class GallarySample extends Activity {

    /**
     * The images.
     */
    private ArrayList<String> images;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.galleryayout);
        getHeader();

        GridView gallery = (GridView) findViewById(R.id.galleryGridView);

        gallery.setAdapter(new ImageAdapter(this));

        gallery.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                if (null != images && !images.isEmpty())
//                    Toast.makeText(
//                            getApplicationContext(),
//                            "position " + position + " " + images.get(position), 300).show();
                WordUtils.compressImage(images.get(position));
                    if (LocalData.getInstance().getInnertitleedit().equalsIgnoreCase("yes")) {
                        LocalData.getInstance().setInnerEditpath(images.get(position));
                    } else {
                        LocalData.getInstance().setEditpicpath(images.get(position));
                    }
                finish();

            }
        });

    }

    TextView header;
    ImageView back;
    ImageView submit;

    public void getHeader() {
        header = (TextView) findViewById(R.id.header_text);
        header.setText("Gallery");
        back = (ImageView) findViewById(R.id.back);
        submit = (ImageView) findViewById(R.id.submit);
        submit.setVisibility(View.GONE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }

    /**
     * The Class ImageAdapter.
     */
    private class ImageAdapter extends BaseAdapter {

        /**
         * The context.
         */
        private Activity context;

        /**
         * Instantiates a new image adapter.
         *
         * @param localContext the local context
         */
        public ImageAdapter(Activity localContext) {
            context = localContext;
            images = getAllShownImagesPath(context);
            Utils.getFilePaths(localContext);
        }

        public int getCount() {
            return images.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            ImageView picturesView;
            if (convertView == null) {
                picturesView = new ImageView(context);
                picturesView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                picturesView.setLayoutParams(new GridView.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2, (AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2));


            } else {
                picturesView = (ImageView) convertView;
                picturesView.setLayoutParams(new GridView.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2, (AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2));
            }


            Glide.with(context).load(images.get(position))
                    .placeholder(R.drawable.background).centerCrop()
                    .into(picturesView);

            return picturesView;
        }

        /**
         * Getting All Images Path.
         *
         * @param activity the activity
         * @return ArrayList with images Path
         */
        private ArrayList<String> getAllShownImagesPath(Activity activity) {
//            Uri uri;
//            Cursor cursor;
//            int column_index_data, column_index_folder_name;
            ArrayList<String> listOfAllImages = new ArrayList<String>();
//            String absolutePathOfImage = null;
//            uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//
//            String[] projection = { MediaColumns.DATA,
//                    MediaStore.Images.Media.BUCKET_DISPLAY_NAME };
//
//            cursor = activity.getContentResolver().query(uri, projection, null,
//                    null, null);
//
//            column_index_data = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
//            column_index_folder_name = cursor
//                    .getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
//            while (cursor.moveToNext()) {
//                absolutePathOfImage = cursor.getString(column_index_data);
//
//                listOfAllImages.add("file://"+absolutePathOfImage);
//            }
//            return listOfAllImages;

            ArrayList<String> galleryImages = LocalData.getInstance().getGalleryImages();
            if (galleryImages.size() == 0) {

                Uri u = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                String[] projection = {MediaStore.Images.ImageColumns.DATA, MediaStore.Images.ImageColumns._ID};
                Cursor c = null;
                SortedSet<String> dirList = new TreeSet<String>();
                ArrayList<ImagesDO> resultIAV = new ArrayList<ImagesDO>();

                String[] directories = null;
                if (u != null) {
                    c = context.getContentResolver().query(u, projection, null, null, null);
                }

                if ((c != null) && (c.moveToFirst())) {
                    do {
                        String tempDir = c.getString(0);
                        tempDir = tempDir.substring(0, tempDir.lastIndexOf("/"));
                        try {
                            dirList.add(tempDir);
                        } catch (Exception e) {

                        }
                    }
                    while (c.moveToNext());
                    directories = new String[dirList.size()];
                    dirList.toArray(directories);

                }

                for (int i = 0; i < dirList.size(); i++) {
                    File imageDir = new File(directories[i]);
                    File[] imageList = imageDir.listFiles();
                    if (imageList == null)
                        continue;
                    for (File imagePath : imageList) {
                        try {

                            if (imagePath.isDirectory()) {
                                imageList = imagePath.listFiles();

                            }
                            if (imagePath.getName().contains(".jpg") || imagePath.getName().contains(".JPG")
                                    || imagePath.getName().contains(".jpeg") || imagePath.getName().contains(".JPEG")
                                    || imagePath.getName().contains(".png") || imagePath.getName().contains(".PNG")
                                    || imagePath.getName().contains(".gif") || imagePath.getName().contains(".GIF")
                                    || imagePath.getName().contains(".bmp") || imagePath.getName().contains(".BMP")
                                    ) {


                                listOfAllImages.add("file://" + imagePath.getAbsolutePath());

                            }
                        }
                        //  }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                LocalData.getInstance().setGalleryImages(listOfAllImages);
                return listOfAllImages;
            } else {
                return galleryImages;
            }


        }
    }
}