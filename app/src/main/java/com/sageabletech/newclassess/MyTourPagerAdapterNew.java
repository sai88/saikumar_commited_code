package com.sageabletech.newclassess;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.sageabletech.fragment.MyTourListFragment;
import com.sageabletech.fragment.MyTourGrid;
import com.sageabletech.utils.LocalData;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class MyTourPagerAdapterNew extends FragmentStatePagerAdapter {
    Context ctx;
    String draftId;
    ViewPager mViewPager;
    public MyTourPagerAdapterNew(FragmentManager fm, Context ctx, String draftId) {
        super(fm);
        this.ctx = ctx;
        this.draftId = draftId;
    }

    EditTab1 mMyTourList;
    MyTourListFragment mMyTourList1;
    MyTourGrid mMyTourGrid;
    MyTourGridNew myTourGridNew;
    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).

        if(position == 0){
            String fromDrafts = LocalData.getInstance().getFromDrafts();
            if(fromDrafts.equalsIgnoreCase("yes")){
                mMyTourList1 = MyTourListFragment.newInstance(position, ctx, draftId);
                return mMyTourList1;
            }else {
                mMyTourList = EditTab1.newInstance();
                return mMyTourList;
            }
        }else{
            String fromDrafts = LocalData.getInstance().getFromDrafts();
            if(fromDrafts.equalsIgnoreCase("yes")){

                mMyTourGrid = MyTourGrid.newInstance(position + 1,ctx,draftId);
                return mMyTourGrid;

            }else{

                myTourGridNew = MyTourGridNew.newInstance(position + 1,ctx,draftId);
                return myTourGridNew;
            }
        }




    }


    @Override
    public int getCount() {
        // Show 3 total pages.
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Grid";
            case 1:
                return "List";

        }
        return null;
    }

    public void refreshFragments(){
        Log.e("REFRESHED","refreshFragments");
//        mMyTourGrid.refresh();
//        mMyTourList.refresh();
        mMyTourList1.refresh();


    }

}
