package com.sageabletech.newclassess;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.sageabletech.R;
import com.sageabletech.dialog.AlertDialogMsg;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.shomiround.BaseActivity;
import com.sageabletech.shomiround.MapsActivity;
import com.sageabletech.shomiround.MyTourDetails;
import com.sageabletech.shomiround.MyTourList;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.Urls;
import com.sageabletech.utils.Utils;
import com.sageabletech.webservicecalls.AsyncRemoteCall;
import com.sageabletech.webservicecalls.AsyncRemoteCall.OnDataListener;
import com.sageabletech.webservicecalls.RequestParams;
import com.sageabletech.webservicecalls.ServiceCalls;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class MyTourListNew extends Fragment implements OnDataListener{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    List<MyTourItemDO> tourList = null;

    public MyTourListNew() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MyTourListNew newInstance(int sectionNumber, Context ctx, String from) {

        MyTourListNew fragment = new MyTourListNew();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString("draftId", from);
        fragment.setArguments(args);
        return fragment;
    }
     MyTourAdapter mImageAdapter;
    RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        System.out.println("xdasdasdasdasdasd....");
        View rootView = inflater.inflate(R.layout.preview_fragment, container, false);

         recyclerView = (RecyclerView) rootView.findViewById(R.id.respodedList);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if(getArguments().getString("draftId").equalsIgnoreCase("DraftTour")){
            //*Draft List
            tourList = ((BaseActivity)getActivity()).mDBCreator.getAllListFromDraft();
        }else{
            tourList = new ArrayList<>();
            String str = ((BaseActivity)getActivity()).preferenceUtils.getStringFromPreference("accessToken","");
            prepareMovieData(str);
        }

        mImageAdapter = new MyTourAdapter(getActivity(),tourList);
        recyclerView.setAdapter(mImageAdapter);

        return rootView;
    }
    private void prepareMovieData(String vendorId) {

        ((BaseActivity)getActivity()).showLoader();
        RequestParams requestParams = new RequestParams();

        String url = Urls.MY_TOUR+vendorId;
        requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
        requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;
        System.out.println("My tour data callled......");
        ServiceCalls serviceCalls = new ServiceCalls(this, "MYTOUR");
        serviceCalls.execute(requestParams);

    }


    @Override
    public void onData(Object data, String error, String tag) {
        ((BaseActivity)getActivity()).hideLoader();

        if(tag.equalsIgnoreCase("MYTOUR")){

            if (error == null && data != null) {
                System.out.println("edewfsdfdsfdsfdsf");
                ArrayList<MyTourItemDO> mFeeds = (ArrayList<MyTourItemDO>) data;
                mImageAdapter = new MyTourAdapter(getActivity(),mFeeds);
                recyclerView.setAdapter(mImageAdapter);


//                if(mFeeds.size()>0)
//                {
////                    this.tourList = (ArrayList<MyTourItemDO>) mFeeds.clone();
//                    System.out.println("dsfdsfdsfdf");
//                    this.tourList=new ArrayList<>();
//                    this.tourList=mFeeds;
//                    mImageAdapter.refresh(this.tourList);
//                }else{
//                }

            }

        }else if(tag.equalsIgnoreCase("DELETE")){
            System.out.println("edewfsdfdsfdsfdsf deleteeee");
            if (error == null && data != null) {
                String str = ((BaseActivity)getActivity()).preferenceUtils.getStringFromPreference("accessToken","");
                prepareMovieData(str);
            }
        }
    }

    private class MyTourAdapter extends RecyclerView.Adapter<MyTourAdapter.MyViewHolder> {

        private List<MyTourItemDO> mOffers;
        private Context mContext;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title,tvItemName,tvItemDesc;
            public ImageView ivImage,ivEdit,ivLocation,ivDelete,ivEye;


            public MyViewHolder(View view) {
                super(view);
                tvItemName = (TextView) view.findViewById(R.id.tvItemName);
                tvItemDesc = (TextView) view.findViewById(R.id.tvDesc);

                ivImage = (ImageView)view.findViewById(R.id.ivImage);
                ivEdit = (ImageView)view.findViewById(R.id.ivEdit);
                ivLocation = (ImageView)view.findViewById(R.id.ivLocation);
                ivEye = (ImageView)view.findViewById(R.id.ivEye);
                ivDelete = (ImageView)view.findViewById(R.id.ivDelete);
            }
        }

        public void refresh(List<MyTourItemDO> mOffers){

            this.mOffers = mOffers;
            this.notifyDataSetChanged();
        }


        public MyTourAdapter(Context mContext, List<MyTourItemDO> mOffers) {
            this.mOffers = mOffers;
            this.mContext = mContext;
            System.out.println("moffers.size::"+mOffers.size());
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.my_grid_itemnew, parent, false);

            RelativeLayout.LayoutParams lm = new RelativeLayout.LayoutParams(AppConstants.DEVICE_DISPLAY_WIDTH,
                    AppConstants.DEVICE_DISPLAY_HEIGHT/5);
            lm.setMargins(5,7,5,7);
            itemView.setLayoutParams(lm);

            return new MyViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {

            holder.tvItemName.setText(mOffers.get(position).title);
            holder.tvItemName.setTypeface(Utils.getHelvaticaBold(getActivity()));


            if(mOffers.get(position).summary.length()>0){
                holder.tvItemDesc.setText(mOffers.get(position).summary);
                holder.tvItemDesc.setTypeface(Utils.getHelvaticaMedium(getActivity()));
            }else{
                holder.tvItemDesc.setVisibility(View.INVISIBLE);
            }

            String loc = mOffers.get(position).tourLatitude+","+ mOffers.get(position).tourLangitude;


            Glide.with(mContext).load(mOffers.get(position).tourPhotoPath)
                    .placeholder(R.mipmap.app_icon)
                    .centerCrop()
                    .override(AppConstants.DEVICE_DISPLAY_WIDTH/3, AppConstants.DEVICE_DISPLAY_HEIGHT/5)
                    .into(holder.ivImage);

            holder.ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LocalData.getInstance().setFromedittour(true);
                    mContext.startActivity(new Intent(mContext, AddTourScreenNew.class)
                            .putExtra("Details",mOffers.get(position))
                            .putExtra("TourPhotoPath",""+mOffers.get(position).tourPhotoPath)
                            .putExtra("From","MyTourListNew"));
                }
            });
            holder.ivLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(new Intent(mContext, MapsActivity.class)
                            .putExtra("tourLatitude",mOffers.get(position).tourLatitude)
                            .putExtra("tourLangitude",mOffers.get(position).tourLangitude)
                            .putExtra("title",mOffers.get(position).title));
                }
            });

            holder.ivEye.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(new Intent(mContext, MyTourDetails.class)
                            .putExtra("Details",mOffers.get(position)));
                }
            });

            holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        new AlertDialogMsg(getActivity(), "Are you want to delete this tour").setPositiveButton("YES", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {


                            doDelete(mOffers.get(position));

                            }

                        }).setNegativeButton("NO",null).create().show();

                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            //mOffers.size();
            return mOffers.size();
        }
    }

    private void doDelete(MyTourItemDO myTourItemDO) {

        if(getArguments().getString("draftId").equalsIgnoreCase("DraftTour")) {

//            ((BaseActivity)getActivity()).mDBCreator.deleteTour(myTourItemDO.tourId);
            tourList = ((BaseActivity)getActivity()).mDBCreator.getAllListFromDraft();
            if(tourList.size()>0)
            {
                mImageAdapter.refresh(this.tourList);
            }


        }else{
            String accessToken = ((BaseActivity)getActivity()).preferenceUtils.getStringFromPreference("accessToken","");
            ((BaseActivity)getActivity()).showLoader();
            RequestParams requestParams = new RequestParams();
            String url = Urls.DELETE+accessToken+"?tourid="+myTourItemDO.tourId;
            requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
            requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;

            ServiceCalls serviceCalls = new ServiceCalls(this, "DELETE");
            serviceCalls.execute(requestParams);
        }


    }

    public void refresh() {

        if(getArguments().getString("draftId").equalsIgnoreCase("DraftTour")){
            tourList = ((BaseActivity)getActivity()).mDBCreator.getAllListFromDraft();
            if(tourList.size()>0)
            {
                mImageAdapter.refresh(this.tourList);
            }

        }else{
            String str = ((BaseActivity)getActivity()).preferenceUtils.getStringFromPreference("accessToken","");
            prepareMovieData(str);
        }



    }


}