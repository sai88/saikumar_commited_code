package com.sageabletech.newclassess;

/**
 * Created by ASMAN on 20-07-2017.
 */


import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.droidmentor.locationhelper.Constants;
import com.droidmentor.locationhelper.MyLocationUsingHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sageabletech.R;
import com.sageabletech.api.Location;
import com.sageabletech.api.PlaceDetailsJSONParser;
import com.sageabletech.api.PlaceJSONParser;
import com.sageabletech.dialog.AlertDialogMsg;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.google.android.gms.maps.SupportMapFragment;
import com.sageabletech.shomiround.AddTourScreen;
import com.sageabletech.shomiround.BaseActivity;
import com.sageabletech.utils.LocalData;
import com.sageabletech.webservicecalls.GPSTracker;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class EditTitleForPhoto extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback, LocationListener {

    private static final String TAG = AddTourScreenNew.class.getName();
    ImageView ivLocationIcon;
    TextView tvTitle;
    EditText tvTourTitle, tvTourSummary;
    protected double latitude = 0, longitude = 0;

    DownloadTask placesDownloadTask;
    DownloadTask placeDetailsDownloadTask;
    ParserTask placesParserTask;
    ParserTask placeDetailsParserTask;
    protected LocationManager locationManager;
    protected LocationListener locationListener;

    final int PLACES = 0;
    final int PLACES_DETAILS = 1;
    TourItem dataofsubtour;
    TourItem newdata;

    //    GoogleApiClient mGoogleApiClient;
    final static int REQUEST_LOCATION = 199;

    String from = "";
    ImageView ivSelectedImage;

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (LocalData.getInstance().getEditpicpath() != null && LocalData.getInstance().getInnertitleedit().equalsIgnoreCase("no")) {
//            MyTourItemDO selectedMyTourItemDO = LocalData.getInstance().getSelectedMyTourItemDO();

            String editpicpath = LocalData.getInstance().getEditpicpath();
            dataofsubtour.ImagePath = editpicpath;
            System.out.println("dataofsubtour.ImagePath main::" + dataofsubtour.ImagePath);
            Glide.with(EditTitleForPhoto.this).load(editpicpath).placeholder(R.drawable.background).centerCrop()
                    .into(ivSelectedImage);


        } else if( LocalData.getInstance().getInnertitleedit().equalsIgnoreCase("yes")){
            String editpicpath = LocalData.getInstance().getInnerEditpath();
            LocalData.getInstance().setInnertitleedit("");
            dataofsubtour.ImagePath = editpicpath;
            System.out.println("dataofsubtour.ImagePath inner::" + dataofsubtour.ImagePath);
            Glide.with(EditTitleForPhoto.this).load(editpicpath).placeholder(R.drawable.background).centerCrop()
                    .into(ivSelectedImage);
        }
        if (LocalData.getInstance().getLocationHit().equalsIgnoreCase("yes")) {
            latitude = Constants.getInstance().getLatitude();
            longitude = Constants.getInstance().getLongitude();
            LocalData.getInstance().setLocationHit("no");

            if (latitude != 0 && longitude != 0) {
                LatLng latLng = new LatLng(latitude, longitude);
                BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);
                System.out.println("latLng::" + latLng);
                mLocation.setText(Constants.getInstance().getLocationText());
                loader.setVisibility(View.GONE);
                GPSTracker gpsTracker = new GPSTracker(EditTitleForPhoto.this);
                LatLng currentLatLng = new LatLng(latitude, longitude);
                showLocationOnMap(bitmapDescriptor, currentLatLng);

            }


        }
    }

    private void showLocationOnMap(BitmapDescriptor icon, LatLng latLng) {
        googleMap.clear();
        latitude = latLng.latitude;
        longitude = latLng.longitude;


        MarkerOptions markerOption = new MarkerOptions();
        markerOption.position(latLng).icon(icon).flat(true).title("");
        Marker marker = googleMap.addMarker(markerOption);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        loader.setVisibility(View.GONE);

    }
ImageView cancelLoc;
    TextView loader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editsubtourdetails);
        ivSelectedImage = (ImageView) findViewById(R.id.ivSelectedImage);
        ivSelectedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalData.getInstance().setInnertitleedit("yes");
                startActivity(new Intent(EditTitleForPhoto.this, GallarySample.class));
            }
        });
        dataofsubtour = LocalData.getInstance().getDataofsubtour();
        cancelLoc = (ImageView) findViewById(R.id.cancelLoc);

        cancelLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                latitude=0;
                longitude=0;
                mLocation.setText("");
                loader.setVisibility(View.GONE);
            }
        });
        String imagePath = dataofsubtour.ImagePath;
        setImage(imagePath);
        getHeader();
        setTheMap();
        System.out.println("tour id::" + dataofsubtour.tourId);
        System.out.println("tour path ::" + imagePath);
        System.out.println("tour nameeee ::" + dataofsubtour.text);
        System.out.println("tour nameeee ::" + dataofsubtour.location);
        System.out.println("tour nameeee ::" + dataofsubtour.summary);
        System.out.println("tour nameeee ::" + dataofsubtour.audioPath);
        System.out.println("tour nameeee ::" + dataofsubtour.latitude);
        System.out.println("tour nameeee ::" + dataofsubtour.longitude);

        init();

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            public void onLocationChanged(android.location.Location location) {
                // Called when a new location is found by the network location provider.

                latitude = location.getLatitude();
                longitude = location.getLongitude();

                if (from.equalsIgnoreCase("MyTourListNew")) {

                    if (!mMyTourItemDO.tourLatitude.isEmpty() && !mMyTourItemDO.tourLangitude.isEmpty()) {
                        LatLng latLng1 = new LatLng(Double.parseDouble(mMyTourItemDO.tourLatitude),
                                Double.parseDouble(mMyTourItemDO.tourLangitude));
                        if (latLng1 != null) {
                            MarkerOptions markerOption = new MarkerOptions();
                            BitmapDescriptor icon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
                            markerOption.position(latLng1).icon(icon).flat(true).title("");
                            googleMap.addMarker(markerOption);
                            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng1));
                            googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                            loader.setVisibility(View.GONE);

                        }
                    }

                } else {
                    LatLng latLng = new LatLng(latitude, longitude);

                    if (latLng != null) {

                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                        loader.setVisibility(View.GONE);

                    }
                }


            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
//            Toast.makeText(AddTourScreenNew.this, "Not Called", Toast.LENGTH_SHORT).show();
            String[] permissions = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION};
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions, 111);
            }

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        /*if(!from.equalsIgnoreCase("MyTourListNew"))
        {*/
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

//        }


    }

    TextView header;
    ImageView back;
    ImageView submit;

    public void getHeader() {
        header = (TextView) findViewById(R.id.header_text);
        header.setText("Add Caption");
        back = (ImageView) findViewById(R.id.back);
        submit = (ImageView) findViewById(R.id.submit);
        submit.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_save_white));


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();

            }
        });


    }

    public void setImage(String path) {
        System.out.println("image path :::" + path);

        if (path != null) {

//            Glide.with(EditTitleForPhoto.this).load(path).placeholder(R.drawable.background).centerCrop()
//                    .into(ivSelectedImage);
            Picasso.with(EditTitleForPhoto.this)
                    .load(path)
                    .fit().centerCrop()

                    .into(ivSelectedImage);

        }
    }


    String address = "";

    private String getAddress(android.location.Location location) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            address += "," + addresses.get(0).getSubLocality();
            address += "," + addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return address;
    }


    AutoCompleteTextView mLocation;
    MyTourItemDO mMyTourItemDO = null;

    private void init() {

        ivSelectedImage = (ImageView) findViewById(R.id.ivSelectedImage);
        ivLocationIcon = (ImageView) findViewById(R.id.ivLocationIcon);
        cancelLoc = (ImageView) findViewById(R.id.cancelLoc);
        loader = (TextView) findViewById(R.id.loader);

        tvTourTitle = (EditText) findViewById(R.id.tvTourTitle);
        tvTourTitle.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = tvTourTitle.getText().toString().toLowerCase(Locale.getDefault());
                header.setText(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });
        tvTourSummary = (EditText) findViewById(R.id.tvTourSummary);



        mLocation = (AutoCompleteTextView) findViewById(R.id.editText3);
        mLocation.setThreshold(2);


        Log.e(TAG, "" + getIntent().getStringExtra("SelectedImage"));


        String title = dataofsubtour.text;
        System.out.println("title 123:::" + title);

        if (!title.equalsIgnoreCase("") && title != null) {
            tvTourTitle.setText("" + title);
        } else {
            tvTourTitle.setHint("Title");
        }


        String summary = dataofsubtour.summary;
        if (!summary.equalsIgnoreCase("") && summary != null) {
            tvTourSummary.setText("" + summary);
        } else {
            tvTourSummary.setHint("Summary");
        }


        String location = dataofsubtour.location;
        if (!location.equalsIgnoreCase("") && location != null && !location.equalsIgnoreCase("No text")) {
            mLocation.setText("" + location);
            loader.setVisibility(View.GONE);
        }


        mLocation.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Creating a DownloadTask to download Google Places matching "s"
                placesDownloadTask = new DownloadTask(PLACES);
                // Getting url to the Google Places Autocomplete api
                String url = Location.getAutoCompleteUrl(s.toString());
                placesDownloadTask.execute(url);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });


        mLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                hideKeyBoard(mLocation);

                ListView lv = (ListView) parent;
                SimpleAdapter adapter = (SimpleAdapter) parent.getAdapter();

                HashMap<String, String> hm = (HashMap<String, String>) adapter.getItem(position);

                placeDetailsDownloadTask = new DownloadTask(PLACES_DETAILS);
                String url = Location.getPlaceDetailsUrl(hm.get("reference"));
                placeDetailsDownloadTask.execute(url);

                mLocation.setText(hm.get("description"));




            }
        });

        ivLocationIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                LocalData.getInstance().setLocationHit("yes");
                LocalData.getInstance().setInnertitleedit("");

                Intent intent = new Intent(EditTitleForPhoto.this, MyLocationUsingHelper.class);
                startActivity(intent);


            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String from = getIntent().getStringExtra("From");
                if (validate()) {
                    dialog();
                }


//                    startActivityForResult(new Intent(EditTitleForPhoto.this, FinalScreenNew.class)
//
//                            .putExtra("From","MyTourListNew").putExtra("SelectedData",mAddTourDO),2001);
//                }


            }
        });

    }

    public boolean validate() {
        if (tvTourTitle.getText().toString().length() == 0) {
            Toast.makeText(EditTitleForPhoto.this, "Enter Title and Summary", Toast.LENGTH_LONG).show();
            return false;
        } else if (tvTourSummary.getText().toString().length() == 0) {
            Toast.makeText(EditTitleForPhoto.this, "Enter Summary", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2001 && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
//            Toast.makeText(AddTourScreenNew.this, "Not Called 1", Toast.LENGTH_SHORT).show();
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }


    public void alertDialogFinish(String msg) {

        try {

            new AlertDialogMsg(this, msg).setPositiveButton("YES", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {

                    Intent in = new Intent();
                    in.putExtra("TourTitle", "" + tvTourTitle.getText().toString());
                    in.putExtra("Summary", "" + tvTourSummary.getText().toString());
                    in.putExtra("position", getIntent().getIntExtra("position", 0));

                    setResult(RESULT_OK, in);
                    finish();

                }

            }).setNegativeButton("NO", null).create().show();

        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    /*@Override
    public void onBackPressed() {
//        super.onBackPressed();
        alertDialogFinish("back clicked");
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void setTheMap() {

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(this, "Not", Toast.LENGTH_LONG).show();
            return;
        }


//		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, this);

    }


    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private GoogleMap googleMap;

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        System.out.println("ddsfdsf::" + dataofsubtour.latitude + "::" + dataofsubtour.longitude);
        if (dataofsubtour.latitude != null && !dataofsubtour.latitude.equalsIgnoreCase("")) {

            if (dataofsubtour.longitude != null && !dataofsubtour.longitude.equalsIgnoreCase("") && !dataofsubtour.latitude.equalsIgnoreCase("0") && !dataofsubtour.longitude.equalsIgnoreCase("0")) {
                latitude = Double.parseDouble(dataofsubtour.latitude);
                longitude = Double.parseDouble(dataofsubtour.longitude);
                LatLng latLng1 = new LatLng(Double.parseDouble(dataofsubtour.latitude),
                        Double.parseDouble(dataofsubtour.longitude));
                if (latLng1 != null) {
                    MarkerOptions markerOption = new MarkerOptions();
                    BitmapDescriptor icon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
                    markerOption.position(latLng1).icon(icon).flat(true).title("");
                    googleMap.addMarker(markerOption);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng1));
                    googleMap.addMarker(new MarkerOptions().position(latLng1).title("Markers"));
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

                }
            } else {
//                getLocation();
            }
        }


    }

    @Override
    public void onLocationChanged(android.location.Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        private int downloadType = 0;

        // Constructor
        public DownloadTask(int type) {
            this.downloadType = type;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = Location.downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            switch (downloadType) {
                case PLACES:
                    // Creating ParserTask for parsing Google Places
                    placesParserTask = new ParserTask(PLACES);

                    // Start parsing google places json data
                    // This causes to execute doInBackground() of ParserTask class
                    placesParserTask.execute(result);

                    break;

                case PLACES_DETAILS:
                    // Creating ParserTask for parsing Google Places
                    placeDetailsParserTask = new ParserTask(PLACES_DETAILS);

                    // Starting Parsing the JSON string
                    // This causes to execute doInBackground() of ParserTask class
                    placeDetailsParserTask.execute(result);
            }
        }
    }


    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        int parserType = 0;

        public ParserTask(int type) {
            this.parserType = type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loader.setVisibility(View.VISIBLE);

        }

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<HashMap<String, String>> list = null;

            try {
                jObject = new JSONObject(jsonData[0]);

                switch (parserType) {
                    case PLACES:
                        PlaceJSONParser placeJsonParser = new PlaceJSONParser();
                        // Getting the parsed data as a List construct
                        list = placeJsonParser.parse(jObject);
                        break;
                    case PLACES_DETAILS:
                        PlaceDetailsJSONParser placeDetailsJsonParser = new PlaceDetailsJSONParser();
                        // Getting the parsed data as a List construct
                        list = placeDetailsJsonParser.parse(jObject);
                }

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return list;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {


            switch (parserType) {
                case PLACES:
                    String[] from = new String[]{"description"};
                    int[] to = new int[]{android.R.id.text1};

                    // Creating a SimpleAdapter for the AutoCompleteTextView
                    if (result != null) {

                        SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), result, R.layout.text_layout, from, to);

                        // Setting the adapter
                        mLocation.setAdapter(adapter);
                    }
                    break;
                case PLACES_DETAILS:
                    HashMap<String, String> hm = result.get(0);

                    // Getting latitude from the parsed data
                    latitude = Double.parseDouble(hm.get("lat"));

                    // Getting longitude from the parsed data
                    longitude = Double.parseDouble(hm.get("lng"));


                    LatLng latLng = new LatLng(latitude, longitude);

                    if (latLng != null) {
                        MarkerOptions markerOption = new MarkerOptions();
                        BitmapDescriptor icon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
                        markerOption.position(latLng).icon(icon).flat(true).title("");
                        googleMap.addMarker(markerOption);
                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                        loader.setVisibility(View.GONE);

                    }

                    break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void dialog() {
        try {

            AlertDialog.Builder builder = new AlertDialog.Builder(EditTitleForPhoto.this);
            builder.setMessage("Save changes to Sub tour ?");
            builder.setTitle(getResources().getString(R.string.app_name));

            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {


                    newdata = new TourItem();
                    newdata.text = tvTourTitle.getText().toString();
                    newdata.title = tvTourTitle.getText().toString();
                    newdata.summary = tvTourSummary.getText().toString();
                    newdata.location = mLocation.getText().toString();
                    newdata.audioPath = "";
                    try {
                        newdata.latitude = "" + latitude;
                        newdata.longitude = "" + longitude;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    newdata.ImagePath = dataofsubtour.ImagePath;

                    newdata.photoId = dataofsubtour.photoId;

                    LocalData.getInstance().setDataofsubtour(newdata);

                    System.out.println("data lodade::" + newdata.title);
                    System.out.println("data lodade::" + newdata.summary);
                    System.out.println("data lodade::" + newdata.latitude);
                    System.out.println("data lodade::" + newdata.ImagePath);
                    finish();
                }


            });
            builder.setNeutralButton("No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.dismiss();
                            finish();
//
                        }
                    });


            builder.show();

        } catch (Exception e) {
            // TODO: handle exception
        }
    }


}




