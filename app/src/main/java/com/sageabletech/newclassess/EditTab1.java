package com.sageabletech.newclassess;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.sageabletech.R;
import com.sageabletech.dialog.AlertDialogMsg;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.MapsActivity;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.sageabletech.views.RecyclerTouchListener;
import com.sageabletech.viewtour.DeleteTour;
import com.sageabletech.viewtour.FavMainTour;
import com.sageabletech.viewtour.PublishTour;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ASMAN on 26-07-2017.
 */

public class EditTab1 extends Fragment {

    RecyclerView recyclerView;
    MyTourAdapter MyTourAdapter;
    ArrayList<MyTourItemDO> tourList;

    public static EditTab1 newInstance() {

        EditTab1 fragment = new EditTab1();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.edittab1, container, false);
        recyclerView = (RecyclerView) inflate.findViewById(R.id.respodedList);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        String fromDrafts = LocalData.getInstance().getFromDrafts();
        if (fromDrafts.equalsIgnoreCase("fav")) {
            System.out.println("entererererer favaaa");
            ArrayList<MyTourItemDO> myTourItemDOs = LocalData.getInstance().getFavoritesArray();
            tourList = myTourItemDOs;
        } else {
            ArrayList<MyTourItemDO> myTourItemDOs = LocalData.getInstance().getmFeeds();
            tourList = myTourItemDOs;
        }


        MyTourAdapter = new MyTourAdapter(getActivity(), tourList);
        recyclerView.setAdapter(MyTourAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
//               if(!LocalData.getInstance().getFromDrafts().equalsIgnoreCase("yes")){
//                   MyTourItemDO myTourItemDO = tourList.get(position);
//                   LocalData.getInstance().setFavselectedItem(myTourItemDO);
//                   LocalData.getInstance().setTourSeletedItem(myTourItemDO);
//                   startActivity(new Intent(getActivity(), FavMainTour.class)
//                           .putExtra("Details",tourList.get(position)));
//
//               }

            }


            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return inflate;
    }

    private class MyTourAdapter extends RecyclerView.Adapter<MyTourAdapter.MyViewHolder> {

        private List<MyTourItemDO> mOffers;
        private Context mContext;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title, tvItemName, tvItemDesc,tv_publish;
            public ImageView ivImage, ivEdit, ivLocation, ivDelete, ivEye;
            RelativeLayout publishCont;
            Switch aSwitch;


            public MyViewHolder(View view) {
                super(view);
                tvItemName = (TextView) view.findViewById(R.id.tvItemName);
                tvItemDesc = (TextView) view.findViewById(R.id.tvDesc);

                ivImage = (ImageView) view.findViewById(R.id.ivImage);
                ivEdit = (ImageView) view.findViewById(R.id.ivEdit);
                ivLocation = (ImageView) view.findViewById(R.id.ivLocation);
                ivEye = (ImageView) view.findViewById(R.id.ivEye);
                ivDelete = (ImageView) view.findViewById(R.id.ivDelete);
                publishCont = (RelativeLayout) view.findViewById(R.id.publishCont);
                tv_publish= (TextView) view.findViewById(R.id.tv_publish);
                aSwitch = (Switch) view.findViewById(R.id.switch_btn);
            }
        }

        public void refresh(List<MyTourItemDO> mOffers) {

            this.mOffers = mOffers;
            this.notifyDataSetChanged();
        }


        public MyTourAdapter(Context mContext, List<MyTourItemDO> mOffers) {
            this.mOffers = mOffers;
            this.mContext = mContext;
            System.out.println("moffers.size::" + mOffers.size());
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.my_grid_itemnew, parent, false);

            RelativeLayout.LayoutParams lm = new RelativeLayout.LayoutParams(AppConstants.DEVICE_DISPLAY_WIDTH,
                    AppConstants.DEVICE_DISPLAY_HEIGHT / 5);
            lm.setMargins(5, 7, 5, 7);
            itemView.setLayoutParams(lm);

            return new MyViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {

            holder.tvItemName.setText(mOffers.get(position).title);
            if (LocalData.getInstance().getFromDrafts().equalsIgnoreCase("fav")) {
                holder.ivEdit.setVisibility(View.GONE);
                holder.ivDelete.setVisibility(View.GONE);
            }else{
                holder.publishCont.setVisibility(View.GONE);
            }

            if (mOffers.get(position).summary.length() > 0) {
                holder.tvItemDesc.setText(mOffers.get(position).summary);
            } else {
                holder.tvItemDesc.setVisibility(View.INVISIBLE);
            }



            String unPublishFlag = mOffers.get(position).unPublishFlag;
            if(unPublishFlag.equalsIgnoreCase("1")){
                holder.tv_publish.setText("Publish");
                holder.aSwitch.setChecked(false);
                addSwitchAction(position,holder.aSwitch,holder.tv_publish,mOffers);

            }else{
                holder.tv_publish.setText("Un Publish");
                holder.aSwitch.setChecked(true);
                addSwitchAction(position,holder.aSwitch,holder.tv_publish,mOffers);
            }


            String loc = mOffers.get(position).tourLatitude + "," + mOffers.get(position).tourLangitude;


            Glide.with(mContext).load(mOffers.get(position).tourPhotoPath)
                    .placeholder(R.mipmap.app_icon)
                    .centerCrop()
                    .override(AppConstants.DEVICE_DISPLAY_WIDTH / 3, AppConstants.DEVICE_DISPLAY_HEIGHT / 5)
                    .into(holder.ivImage);

            holder.ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LocalData.getInstance().setFromedittour(true);
                    MyTourItemDO myTourItemDO = mOffers.get(position);
                    ArrayList<TourItem> subtour = myTourItemDO.subtour;


                    LocalData.getInstance().setSelectedMyTourItemDO(myTourItemDO);
                    LocalData.getInstance().setcatTourArry(myTourItemDO.catArryId);
                    LocalData.getInstance().setTagtourArry(myTourItemDO.tagArryId);
                    System.out.println("subtour123:::" + myTourItemDO.tagArry);

                    ProgressDialog dialog = new ProgressDialog(mContext);
                    dialog.setMessage("please wait..");
                    dialog.show();
                    LocalData.getInstance().setProgressDialog(dialog);

                    mContext.startActivity(new Intent(mContext, AddTourScreenNew.class)
                            .putExtra("Details", myTourItemDO)
                            .putExtra("TourPhotoPath", "" + mOffers.get(position).tourPhotoPath)
                            .putExtra("From", "MyTourListNew"));
                }
            });
            holder.ivLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(new Intent(mContext, MapsActivity.class)
                            .putExtra("tourLatitude", mOffers.get(position).tourLatitude)
                            .putExtra("tourLangitude", mOffers.get(position).tourLangitude)
                            .putExtra("title", mOffers.get(position).title));
                }
            });

            holder.ivImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MyTourItemDO myTourItemDO = mOffers.get(position);
                    LocalData.getInstance().setFavselectedItem(myTourItemDO);
                    LocalData.getInstance().setTourSeletedItem(myTourItemDO);
                    mContext.startActivity(new Intent(mContext, FavMainTour.class)
                            .putExtra("Details", mOffers.get(position)));
                }
            });

            holder.ivEye.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (LocalData.getInstance().getFromDrafts().equalsIgnoreCase("fav")) {
                        MyTourItemDO myTourItemDO = mOffers.get(position);
                        LocalData.getInstance().setFavselectedItem(myTourItemDO);
                        LocalData.getInstance().setTourSeletedItem(myTourItemDO);
                        mContext.startActivity(new Intent(mContext, FavMainTour.class)
                                .putExtra("Details", mOffers.get(position)));
                    } else {

                        MyTourItemDO myTourItemDO = mOffers.get(position);
                        LocalData.getInstance().setFavselectedItem(myTourItemDO);
                        LocalData.getInstance().setTourSeletedItem(myTourItemDO);
                        mContext.startActivity(new Intent(mContext, FavMainTour.class)
                                .putExtra("Details", mOffers.get(position)));
                    }

                }
            });

            holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        new AlertDialogMsg(getActivity(), "Are you want to delete this tour").setPositiveButton("YES", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {


                                doDelete(mOffers.get(position));

                            }

                        }).setNegativeButton("NO", null).create().show();

                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            //mOffers.size();
            return mOffers.size();
        }
    }


    private void doDelete(MyTourItemDO myTourItemDO) {

        new DeleteTour(getActivity(), myTourItemDO.tourId).execute();


    }
    public void addSwitchAction(final int postion, final Switch button,final TextView text,final List<MyTourItemDO> mOffers){
        button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton cb, boolean on){
                if(button.isPressed()) {
                    if (on) {
                        //Do something when Switch button is on/checked
                        text.setText("UnPublish");
                        new PublishTour(getActivity(), mOffers.get(postion).tourId, 0, null, postion).execute();
                    } else {
                        //Do something when Switch is off/unchecked
                        text.setText("Publish");
                        new PublishTour(getActivity(), mOffers.get(postion).tourId, 1, null, postion).execute();
                    }
                }
            }
        });

    }

}
