package com.sageabletech.newclassess;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.sageabletech.utils.LocalData;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class FinalPageAdapterNew extends FragmentStatePagerAdapter {
    Context ctx;
    AddCaptionFragmentNew mAddCaptionFragment = null;
    GridFragmentNew mGridFragment = null;
    String draftId;

    public FinalPageAdapterNew(FragmentManager fm, Context ctx, String draftId) {
        super(fm);
        this.ctx = ctx;
        this.draftId = draftId;
    }

    AddAudioFragmentNew mAddAudioFragment = null;
    PreviewFragmentNew previewFragmentNew=null;

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        System.out.println("position::"+position);
        if (position == 0) {
            mGridFragment = GridFragmentNew.newInstance(position + 1, ctx);
            return mGridFragment;
        } else if (position == 1) {
            System.out.println("adftabaa aaaaaa::" + position);
            mAddAudioFragment = AddAudioFragmentNew.newInstance(position + 1, ctx);
            LocalData.getInstance().setAddAudioFragmentNew(mAddAudioFragment);
            return mAddAudioFragment;
        } else if (position == 2) {
            mAddCaptionFragment = AddCaptionFragmentNew.newInstance(position + 1, ctx);
            return mAddCaptionFragment;

        } else{

                    previewFragmentNew=PreviewFragmentNew.newInstance(position + 1, ctx);
            return previewFragmentNew;
        }

    }

    public void AddCaptionToImage(String text, String summary) {
        if (mAddCaptionFragment != null) {
            mAddCaptionFragment.refreshFragment(text, summary);
        }
    }
    public void AddCaptionToImage() {
        if (mAddCaptionFragment != null) {
            mAddCaptionFragment.refresh();
        }
    }

    public void AudioFragmentRefresh() {
        if (mAddAudioFragment != null) {
            mAddAudioFragment.refresh();
        }
    }
    public void refreshMap(){
        if (previewFragmentNew != null) {
            previewFragmentNew.mapRefresh();
        }
    }

    /*public void GridImagesRefresh(){
        if(mGridFragment !=null){
            mGridFragment.refreshFragment();
        }
    }*/


    @Override
    public int getCount() {
        // Show 3 total pages.
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "SIGN IN";
            case 1:
                return "SIGN UP";
            case 2:
                return "PROFILE";
            case 3:
                return "PROFILE";
            default:return null;
        }
    }
}
