package com.sageabletech.newclassess;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;


import com.sageabletech.R;
import com.sageabletech.dialog.AlertDialogMsg;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.BaseActivity;
import com.sageabletech.shomiround.MainScreen;
import com.sageabletech.shomiround.MapsActivity;
import com.sageabletech.shomiround.MyTourDetails;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.sageabletech.viewtour.DeleteTour;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class NewGragment extends BaseActivity {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    List<MyTourItemDO> tourList = null;



    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */

    MyTourAdapter mImageAdapter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preview_fragmentedit);
        recyclerView = (RecyclerView) findViewById(R.id.respodedList);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        ArrayList<MyTourItemDO> myTourItemDOs =LocalData.getInstance().getmFeeds();
        tourList=myTourItemDOs;
getHeader();
        mImageAdapter = new MyTourAdapter(this,tourList);
        recyclerView.setAdapter(mImageAdapter);
    }
    TextView header;
    ImageView back;
    ImageView submit;
    public void getHeader(){
        header=(TextView) findViewById(R.id.header_text);
        header.setText("My Tour");
        back=(ImageView)findViewById(R.id.back);
        submit=(ImageView)findViewById(R.id.submit);
        submit.setVisibility(View.GONE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                finish();
//                startActivity(new Intent(NewGragment.this, MainScreen.class));
                LocalData.getInstance().setEditpicpath(null);
                Intent intent = new Intent(NewGragment.this, MainScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
//                startActivity(new Intent(NewGragment.this, MainScreen.class));

            }
        });




    }





    private class MyTourAdapter extends RecyclerView.Adapter<MyTourAdapter.MyViewHolder> {

        private List<MyTourItemDO> mOffers;
        private Context mContext;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title,tvItemName,tvItemDesc;
            public ImageView ivImage,ivEdit,ivLocation,ivDelete,ivEye;


            public MyViewHolder(View view) {
                super(view);
                tvItemName = (TextView) view.findViewById(R.id.tvItemName);
                tvItemDesc = (TextView) view.findViewById(R.id.tvDesc);

                ivImage = (ImageView)view.findViewById(R.id.ivImage);
                ivEdit = (ImageView)view.findViewById(R.id.ivEdit);
                ivLocation = (ImageView)view.findViewById(R.id.ivLocation);
                ivEye = (ImageView)view.findViewById(R.id.ivEye);
                ivDelete = (ImageView)view.findViewById(R.id.ivDelete);
            }
        }

        public void refresh(List<MyTourItemDO> mOffers){

            this.mOffers = mOffers;
            this.notifyDataSetChanged();
        }


        public MyTourAdapter(Context mContext, List<MyTourItemDO> mOffers) {
            this.mOffers = mOffers;
            this.mContext = mContext;
            System.out.println("moffers.size::"+mOffers.size());
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.my_grid_itemnew, parent, false);

            RelativeLayout.LayoutParams lm = new RelativeLayout.LayoutParams(AppConstants.DEVICE_DISPLAY_WIDTH,
                    AppConstants.DEVICE_DISPLAY_HEIGHT/5);
            lm.setMargins(5,7,5,7);
            itemView.setLayoutParams(lm);

            return new MyViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {

            holder.tvItemName.setText(mOffers.get(position).title);


            if(mOffers.get(position).summary.length()>0){
                holder.tvItemDesc.setText(mOffers.get(position).summary);
            }else{
                holder.tvItemDesc.setVisibility(View.INVISIBLE);
            }

            String loc = mOffers.get(position).tourLatitude+","+ mOffers.get(position).tourLangitude;


            Glide.with(mContext).load(mOffers.get(position).tourPhotoPath)
                    .placeholder(R.mipmap.app_icon)
                    .centerCrop()
                    .override(AppConstants.DEVICE_DISPLAY_WIDTH/3, AppConstants.DEVICE_DISPLAY_HEIGHT/5)
                    .into(holder.ivImage);

            holder.ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LocalData.getInstance().setFromedittour(true);
                    MyTourItemDO myTourItemDO = mOffers.get(position);
                    ArrayList<TourItem> subtour = myTourItemDO.subtour;
                    System.out.println("subtour123:::"+subtour);

                    LocalData.getInstance().setSelectedMyTourItemDO(myTourItemDO);
                    mContext.startActivity(new Intent(mContext, AddTourScreenNew.class)
                            .putExtra("Details",myTourItemDO)
                            .putExtra("TourPhotoPath",""+mOffers.get(position).tourPhotoPath)
                            .putExtra("From","MyTourListNew"));
                }
            });
            holder.ivLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(new Intent(mContext, MapsActivity.class)
                            .putExtra("tourLatitude",mOffers.get(position).tourLatitude)
                            .putExtra("tourLangitude",mOffers.get(position).tourLangitude)
                            .putExtra("title",mOffers.get(position).title));
                }
            });

            holder.ivEye.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(new Intent(mContext, MyTourDetails.class)
                            .putExtra("Details",mOffers.get(position)));
                }
            });

            holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        new AlertDialogMsg(mContext, "Are you want to delete this tour").setPositiveButton("YES", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {


                                doDelete(mOffers.get(position));

                            }

                        }).setNegativeButton("NO",null).create().show();

                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            //mOffers.size();
            return mOffers.size();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        finish();
        LocalData.getInstance().setEditpicpath(null);
        Intent intent = new Intent(NewGragment.this, MainScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
//        startActivity(new Intent(NewGragment.this, MainScreen.class));
    }

    private void doDelete(MyTourItemDO myTourItemDO) {

new DeleteTour(NewGragment.this,myTourItemDO.tourId).execute();
//            String accessToken = ((BaseActivity)getActivity()).preferenceUtils.getStringFromPreference("accessToken","");
//            ((BaseActivity)getActivity()).showLoader();
//            RequestParams requestParams = new RequestParams();
//            String url = Urls.DELETE+accessToken+"?tourid="+myTourItemDO.tourId;
//            requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
//            requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;
//
//            ServiceCalls serviceCalls = new ServiceCalls(this, "DELETE");
//            serviceCalls.execute(requestParams);



    }




}