package com.sageabletech.newclassess;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


import com.sageabletech.R;
import com.sageabletech.database.ImageModel;
import com.sageabletech.shomiround.BaseActivity;
import com.sageabletech.shomiround.MainScreen;
import com.sageabletech.utils.LocalData;

import java.util.ArrayList;


/**
 * Created by ASMAN on 19-07-2017.
 */

public class EditLastScreen extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editlastscreen);
//        WordUtils.deleteFolder();

        TextView right = (TextView)findViewById(R.id.textView);
//        right.setVisibility(View.GONE);

        ArrayList<ImageModel> all = mDBCreator.getAllImages();

        /*for(ImageModel im : all)
            Log.e("TTTTT",im.name+" "+im.status);*/

        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalData.getInstance().setEditpicpath(null);
                Intent intent = new Intent(EditLastScreen.this, MainScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

//                setResult(RESULT_OK);
//                finish();
            }
        });

    }


    @Override
    public void onBackPressed() {
        LocalData.getInstance().setEditpicpath(null);
        Intent intent = new Intent(EditLastScreen.this, MainScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

        super.onBackPressed();

    }
}
