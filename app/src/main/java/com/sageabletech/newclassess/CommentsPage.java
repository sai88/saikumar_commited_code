package com.sageabletech.newclassess;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sageabletech.R;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.MainScreen;
import com.sageabletech.utils.LocalData;
import com.sageabletech.viewtour.ReportTour;
import com.sageabletech.viewtour.SaveComments;
import com.sageabletech.viewtour.ViewTourTab1Fragment;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ASMAN on 16-08-2017.
 */

public class CommentsPage extends Activity{
    EditText editText;
    ImageView imageView;
     String title;
    String path,tourname,tourId;
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addcomment);
      getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        HashMap<String, String> commenthash = LocalData.getInstance().getCommenthash();
        title = commenthash.get("title");
         path = commenthash.get("path");
        tourname = commenthash.get("tourname");
        tourId = commenthash.get("tourId");


        getHeader(title);
         init();
    }
    TextView postdata;
    RecyclerView recyclerView;
    public  void init(){
        imageView=(ImageView) findViewById(R.id.ivSelectedImage);
        editText = (EditText) findViewById(R.id.txt_comment);
        postdata = (TextView) findViewById(R.id.postdata);
        if(LocalData.getInstance().getTourCommentsArry().size()>0) {


            recyclerView = (RecyclerView) findViewById(R.id.comments_list);
            recyclerView.setVisibility(View.VISIBLE);

            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CommentsPage.this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());


            CommentsAdapter commentsAdapter = new CommentsAdapter(CommentsPage.this, LocalData.getInstance().getTourCommentsArry());

            recyclerView.setAdapter(commentsAdapter);
        }


        if(title.equalsIgnoreCase("Report")){
            editText.setHint("Report Text....");
        }else {
            editText.setHint("Comment....");
        }

        Picasso.with(CommentsPage.this).load(path).fit().centerCrop().into(imageView);

        postdata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!editText.getText().toString().equalsIgnoreCase("")){

                    if(title.equalsIgnoreCase("Report")){
                        new ReportTour(CommentsPage.this,tourId,tourname,editText.getText().toString()).execute();
                    }else{
                        new SaveComments(CommentsPage.this,tourId,editText.getText().toString()).execute();
//                        Toast.makeText(CommentsPage.this,"Enter Comment Text...",Toast.LENGTH_SHORT).show();
                    }

                }else{
                    if(title.equalsIgnoreCase("Report")){
                        Toast.makeText(CommentsPage.this,"Enter Report Text...",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(CommentsPage.this,"Enter Comment Text...",Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });
    }
    TextView header;
    ImageView back;
    ImageView submit;
    public void getHeader(String title){
        header=(TextView) findViewById(R.id.header_text);

        header.setText(title);
        back=(ImageView)findViewById(R.id.back);
        submit=(ImageView)findViewById(R.id.submit);
        submit.setVisibility(View.GONE);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                Intent intent = new Intent(CommentsPage.this, MainScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });





    }
}
