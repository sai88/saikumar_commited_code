package com.sageabletech.newclassess;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

import com.sageabletech.R;
import com.sageabletech.model.CategoryDO;
import com.sageabletech.model.ImagesDO;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TagsDo;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.BaseActivity;
import com.sageabletech.shomiround.FinalScreen;
import com.sageabletech.shomiround.LastScreen;
import com.sageabletech.shomiround.MainScreen;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;
import com.sageabletech.utils.Utils;
import com.sageabletech.viewtour.ObjectSerializer;
import com.sageabletech.webservicecalls.AsyncRemoteCall;
import com.sageabletech.webservicecalls.AsyncRemoteCall.OnDataListener;
import com.sageabletech.webservicecalls.RequestParams;
import com.sageabletech.webservicecalls.ServiceCalls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class FinalScreenNew extends BaseActivity implements OnDataListener {

    private ViewPager mViewPager;
    public ArrayList<TagsDo> mTagsDos;
    public ArrayList<CategoryDO> mCategoryDOs;
    private FinalPageAdapterNew mSectionsPagerAdapter;
    int currentPosition = -1;
    final String TAG = FinalScreenNew.class.getName();
    public MyTourItemDO myTourItemDO;
    TextView tvTitle;
    public String S3URL = "https://s3.amazonaws.com/shomiround-production";
    ImageView right, back;
    // The TransferUtility is the primary class for managing transfer to S3
//    private TransferUtility transferUtility;
//    HashMap<String,String> uploadedUrls = new HashMap<>();
    ArrayList<String> allImages = new ArrayList<>();
    ArrayList<String> temp = new ArrayList<>();
    ProgressBar progressBar;

    String from = "";
    String draftId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.final_page);


        tvTitle = (TextView) findViewById(R.id.header_text);
        LocalData.getInstance().setFinalScreen(FinalScreenNew.this);
        tvTitle.setTypeface(Utils.getHelvaticaMedium(FinalScreenNew.this));

        right = (ImageView) findViewById(R.id.submit);
        right.setImageResource(R.mipmap.ic_ic_forward_circle);


        back = (ImageView) findViewById(R.id.back);
        back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_back_white));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onbackPressed();
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.progressBar);


        myTourItemDO = LocalData.getInstance().getSelectedMyTourItemDO();
        System.out.println("mAddTourDO:::" + myTourItemDO);
        mCategoryDOs = (ArrayList<CategoryDO>) getIntent().getSerializableExtra("categories");
        mTagsDos = (ArrayList<TagsDo>) getIntent().getSerializableExtra("tags");

        tvTitle.setText("Pictures");
//        if(myTourItemDO!=null)
//        {
//            tvTitle.setText(""+myTourItemDO.title);
//        }

       /*if(mAddTourDO !=null)
        getSupportActionBar().setTitle(mAddTourDO.title);*/

        if (getIntent().getStringExtra("draftId") != null) {
            draftId = getIntent().getStringExtra("draftId");
        }

        mSectionsPagerAdapter = new FinalPageAdapterNew(getSupportFragmentManager(), this, draftId);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        final ImageView iv1 = new ImageView(this);
        iv1.setImageResource(R.mipmap.ic_cam_solid_white);
        iv1.setPadding(15, 15, 15, 15);

        final ImageView iv2 = new ImageView(this);
        iv2.setImageResource(R.mipmap.ic_mic_outline_white);
        iv2.setPadding(15, 15, 15, 15);

        final ImageView iv3 = new ImageView(this);
        iv3.setImageResource(R.mipmap.ic_note_outline_white);
        iv3.setPadding(15, 15, 15, 15);

        final ImageView iv4 = new ImageView(this);
        iv4.setImageResource(R.mipmap.ic_eye_outline_white);
        iv4.setPadding(15, 15, 15, 15);

        tabLayout.getTabAt(0).setCustomView(iv1);
        tabLayout.getTabAt(1).setCustomView(iv2);
        tabLayout.getTabAt(2).setCustomView(iv3);
        tabLayout.getTabAt(3).setCustomView(iv4);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                mViewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 0) {
                    tvTitle.setText("Pictures");
                }
                if (tab.getPosition() == 1) {
                    mSectionsPagerAdapter.AudioFragmentRefresh();
                    tvTitle.setText("Audio");
                }
                if (tab.getPosition() == 2) {
                    mSectionsPagerAdapter.AddCaptionToImage();
                    tvTitle.setText("Notes");
                }
                if (tab.getPosition() == 3) {
                    tvTitle.setText("Preview");
                }
//                if(tab.getPosition() == 1){
//                    mSectionsPagerAdapter.AudioFragmentRefresh();
//                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
//                tab.getCustomView().setAlpha(0.27f);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            public void onPageSelected(int position) {
                // Check if this is the page you want.
                if (position == 0) {

                    iv1.setImageResource(R.mipmap.ic_cam_solid_white);
                    iv2.setImageResource(R.mipmap.ic_mic_outline_white);
                    iv3.setImageResource(R.mipmap.ic_note_outline_white);
                    iv4.setImageResource(R.mipmap.ic_eye_outline_white);
                    right.setImageResource(R.mipmap.ic_ic_forward_circle);

//                    if(!draftId.equalsIgnoreCase(""))
//                    mDBCreator.updateDetailsToDraftTable(myTourItemDO,draftId);

                } else if (position == 1) {

                    iv1.setImageResource(R.mipmap.ic_cam_outline_white);
                    iv2.setImageResource(R.mipmap.ic_mic_solid_white);
                    iv3.setImageResource(R.mipmap.ic_note_outline_white);
                    iv4.setImageResource(R.mipmap.ic_eye_outline_white);
                    right.setImageResource(R.mipmap.ic_ic_forward_circle);

//                    if(!draftId.equalsIgnoreCase(""))
//                        mDBCreator.updateDetailsToDraftTable(mAddTourDO,draftId);

                } else if (position == 2) {

                    iv1.setImageResource(R.mipmap.ic_cam_outline_white);
                    iv2.setImageResource(R.mipmap.ic_mic_outline_white);
                    iv3.setImageResource(R.mipmap.ic_note_solid_white);
                    iv4.setImageResource(R.mipmap.ic_eye_outline_white);
                    right.setImageResource(R.mipmap.ic_ic_forward_circle);

//                    if(!draftId.equalsIgnoreCase(""))
//                        mDBCreator.updateDetailsToDraftTable(mAddTourDO,draftId);

                } else if (position == 3) {

                    iv1.setImageResource(R.mipmap.ic_cam_outline_white);
                    iv2.setImageResource(R.mipmap.ic_mic_outline_white);
                    iv3.setImageResource(R.mipmap.ic_note_outline_white);
                    iv4.setImageResource(R.mipmap.ic_eye_solid_white);
                    right.setImageResource(R.mipmap.ic_ic_save_white);

//                    if(!draftId.equalsIgnoreCase(""))
//                        mDBCreator.updateDetailsToDraftTable(mAddTourDO,draftId);
                }
            }
        });


        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mViewPager.getCurrentItem() == 0) {

                    mViewPager.setCurrentItem(1);
                    return;
                } else if (mViewPager.getCurrentItem() == 1) {
                    mViewPager.setCurrentItem(2);
                    return;
                } else if (mViewPager.getCurrentItem() == 2) {
                    mViewPager.setCurrentItem(3);
                    mSectionsPagerAdapter.refreshMap();

                    return;
                }

                try {
                    from = getIntent().getStringExtra("From");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

//                    String message = "Are you sure you want to upload";
//                    if(from.equalsIgnoreCase("MyTourListNew")){
//                        message = "Are you sure you want to update this tour";
//                    }
//
//                    new AlertDialogMsg(FinalScreenNew.this, message)
//                            .setPositiveButton("YES", new DialogInterface.OnClickListener() {
//
//                        public void onClick(DialogInterface dialog, int which) {
//
//                            doSubmit();
//
//                        }
//
//                    }).setNegativeButton("NO",null).create().show();
                    showOptionAlert("proceed", "Do you want to update tour?");

                } catch (Exception e) {
                    // TODO: handle exception
                }


            }
        });


    }
    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        onbackPressed();
    }

    private void onbackPressed() {
        if (mViewPager.getCurrentItem() == 3) {
            mViewPager.setCurrentItem(2);
        } else if (mViewPager.getCurrentItem() == 2) {
            mViewPager.setCurrentItem(1);
        } else if (mViewPager.getCurrentItem() == 1) {
            mViewPager.setCurrentItem(0);
        } else {
            finish();
        }


    }

     AlertDialog b;
    private void showOptionAlert(final String option, String message) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);



        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.choose_option_alert, null);
        dialogBuilder.setView(dialogView);

        TextView title = (TextView) dialogView.findViewById(R.id.dialogtitle);
        title.setTypeface(Utils.getHelvaticaBold(FinalScreenNew.this));
        TextView upload = (TextView) dialogView.findViewById(R.id.upload);
        upload.setText("Update tour");
        upload.setTypeface(Utils.getHelvaticaBold(FinalScreenNew.this));
        TextView savetodraft = (TextView) dialogView.findViewById(R.id.savetodraft);
        savetodraft.setTypeface(Utils.getHelvaticaBold(FinalScreenNew.this));
        TextView discard = (TextView) dialogView.findViewById(R.id.discard);
        discard.setTypeface(Utils.getHelvaticaBold(FinalScreenNew.this));
        TextView cancel = (TextView) dialogView.findViewById(R.id.cancel);
        cancel.setTypeface(Utils.getHelvaticaBold(FinalScreenNew.this));

        TextView msg = (TextView) dialogView.findViewById(R.id.msg);

        msg.setText(message);
        msg.setTypeface(Utils.getHelvaticaBold(FinalScreenNew.this));
        if (option.equalsIgnoreCase("back")) {

            upload.setVisibility(View.GONE);
        }

         b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        b.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_2;
        b.show();

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                WordUtils.deleteFolder();
                doSubmit();
            }
        });
        savetodraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("myTourItemDO.tourId::" + myTourItemDO.tourId);
                mDBCreator.deleteTags(myTourItemDO.tourId);
                mDBCreator.deleteCategories(myTourItemDO.tourId);

                mDBCreator.insertTags(myTourItemDO.tourId, mTagsDos);
                mDBCreator.insertCategpries(myTourItemDO.tourId, mCategoryDOs);

                mDBCreator.updateDetailsToDraftTableNew(myTourItemDO, myTourItemDO.tourId);

                b.dismiss();
               /* onBackPressed();*/
                Intent intent = new Intent(FinalScreenNew.this, MainScreen.class);
                startActivity(intent);
                finish();
            }
        });

        discard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                getDraftAlert();
               /* Intent intent = new Intent(FinalScreen.this,MainScreen.class);
                startActivity(intent);*/
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (option.equalsIgnoreCase("back")) {

                    mViewPager.setCurrentItem(2);

                }
                b.dismiss();
            }
        });


    }

    private void getDraftAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(FinalScreenNew.this);
        builder.setMessage("Are you sure do you want to discard?");
        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {



              /*  mDBCreator.deleteTags(getIntent().getStringExtra("draftId"));
                mDBCreator.deleteCategories(getIntent().getStringExtra("draftId"));

                mDBCreator.insertTags(getIntent().getStringExtra("draftId"),mTagsDos);
                mDBCreator.insertCategpries(getIntent().getStringExtra("draftId"),mCategoryDOs);
                mDBCreator.updateDetailsToDraftTable(mAddTourDO,draftId);*/

                dialog.dismiss();
               /* onBackPressed();*/
                Intent intent = new Intent(FinalScreenNew.this, MainScreen.class);
                startActivity(intent);
                finish();

            }
        });
    }


    String userId;

    private void doSubmit() {

        AmazonS3 s3 = new AmazonS3Client(new CognitoCachingCredentialsProvider(
                FinalScreenNew.this,
                AppConstants.COGNITO_POOL_ID,
                Regions.fromName(AppConstants.COGNITO_POOL_REGION)));

        System.out.println("mAddTourDO:::" + myTourItemDO.tourPhotoPath);

        System.out.println("mAddTourDO:::" + myTourItemDO.tourPhotoPath);
//        if(LocalData.getInstance().isFromedittour()){
//            myTourItemDO.tourPhotoPath= LocalData.getInstance().getEditpicpath();
//        }

        if (mViewPager.getCurrentItem() == 3) {
            userId = preferenceUtils.getStringFromPreference("userId", "");

            TransferUtility transferUtility = new TransferUtility(s3, getApplicationContext());


            Log.e("TourIcon", "" + myTourItemDO.tourPhotoPath);
            //Uploading tourIcon
            if (!myTourItemDO.tourPhotoPath.contains("https://s3.amazonaws.com")) {
                if (myTourItemDO.tourPhotoPath.contains("file://")) {
                    myTourItemDO.tourPhotoPath = myTourItemDO.tourPhotoPath.replace("file://", "");
                }
//                    WordUtils.compressImage(myTourItemDO.tourPhotoPath);
                String[] split = myTourItemDO.tourPhotoPath.split("/");
                String s = split[split.length - 1];

                File tIcon = new File(getsmallImage(s));


                boolean exists = tIcon.exists();
                if (!exists) {
                    tIcon = searchforfile(s);
                }
                String iconName[] = myTourItemDO.tourPhotoPath.split("/");
                TransferObserver obsIcon = transferUtility.upload(
                        AppConstants.BUCKET_NAME,
                        userId + "/" + iconName[iconName.length - 1],
                        tIcon
                );

//                mDBCreator.insertTourIcon(tIcon.getAbsolutePath(),"no");

//                showLoader();
                obsIcon.setTransferListener(new TransferListener() {


                    @Override
                    public void onStateChanged(int id, TransferState state) {
//                        Toast.makeText(FinalScreenNew.this,"Uploaded:"+id,Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        int percentage = (int) (bytesCurrent / bytesTotal * 100);
                        //Display percentage transfered to user
                        Log.e(TAG, "percentage : " + percentage + " " + id);

                        if (percentage == 100) {
//                            hideLoader();
//                            mDBCreator.updateTourIcon(tIcon.getAbsolutePath());
                        }

                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        // do something
                        progressBar.setVisibility(View.GONE);
//                            PreferenceUtils.saveInt("uploadId",id);
//                            System.out.println("error in upload main image::"+ex.toString());

                    }

                });
                myTourItemDO.tourPhotoPath = userId + "/" + iconName[iconName.length - 1];

            }

            //saving tourImages in Local DB.
            allImages.clear();
            System.out.println("mAddTourDO.tourPhotos.size()::" + myTourItemDO.subtour.size());
            for (int i = 0; i < myTourItemDO.subtour.size(); i++) {
                System.out.println("mAddTourDO.tourPhotos.get(i).ImagePath::" + myTourItemDO.subtour.get(i).ImagePath);
                System.out.println("mAddTourDO.tourPhotos.get(i).ImagePath::" + myTourItemDO.subtour.get(i).audioPath);
                if (!myTourItemDO.subtour.get(i).ImagePath.contains("https://s3.amazonaws.com") && !myTourItemDO.subtour.get(i).ImagePath.equalsIgnoreCase("")) {
                    if (myTourItemDO.subtour.get(i).ImagePath.contains("file://")) {
                        myTourItemDO.subtour.get(i).ImagePath = myTourItemDO.subtour.get(i).ImagePath.replace("file://", "");
                    }
//                        WordUtils.compressImage(myTourItemDO.subtour.get(i).ImagePath);
                    String[] split = myTourItemDO.subtour.get(i).ImagePath.split("/");
                    String s = split[split.length - 1];


                    File tourImage = new File(getsmallImage(s));
                    boolean exists = tourImage.exists();
                    if (!exists) {
                        tourImage = searchforfile(s);
                    }
//                    mDBCreator.insertImage(tourImage.getAbsolutePath(),"no",mAddTourDO.tourId);

                    String[] tempPhoto = myTourItemDO.subtour.get(i).ImagePath.split("/");
                    allImages.add(myTourItemDO.subtour.get(i).ImagePath);
                    myTourItemDO.subtour.get(i).ImagePath = userId + "/" + tempPhoto[tempPhoto.length - 1];

                    TransferObserver upload = transferUtility.upload(
                            AppConstants.BUCKET_NAME,
                            userId + "/" + tempPhoto[tempPhoto.length - 1],
                            tourImage
                    );
                    upload.setTransferListener(new TransferListener() {
                        @Override
                        public void onStateChanged(int id, TransferState state) {

                        }

                        @Override
                        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

                        }

                        @Override
                        public void onError(int id, Exception ex) {
                            System.out.println("sun images error::" + ex.toString());
                            String s = ex.toString();
                            if (s.contains(".png")) {
                                String[] split = s.split("/");
                                String[] nameSplit = split[2].split(":");
                                s = split[1] + "/" + nameSplit[0];
                                System.out.println("final error msg::" + s);
                                saveerror(s);
                            }

//                                com.amazonaws.AmazonClientException: Unable to execute HTTP request: Unable to resolve host "shomiround.s3.amazonaws.com": No address associated with hostname
//                                com.amazonaws.AmazonClientException: Unable to calculate MD5 hash: /199/photo_album.png: open failed: ENOENT (No such file or directory)

                        }
                    });

                }
                if (myTourItemDO.subtour.get(i).audioPath != null && !myTourItemDO.subtour.get(i).audioPath.contains("https://s3.amazonaws.com") && !myTourItemDO.subtour.get(i).audioPath.equalsIgnoreCase("")) {
                    File tourImage = new File(myTourItemDO.subtour.get(i).audioPath);
                    String[] tempPhoto = myTourItemDO.subtour.get(i).audioPath.split("/");
                    allImages.add(myTourItemDO.subtour.get(i).audioPath);
                    myTourItemDO.subtour.get(i).audioPath = userId + "/" + tempPhoto[tempPhoto.length - 1];


                    transferUtility.upload(
                            AppConstants.BUCKET_NAME,
                            userId + "/" + tempPhoto[tempPhoto.length - 1],
                            tourImage);
                }

            }

            showToast("Your Tour is updating in the back ground.");
            try {
                doUrlSubmit();
            } catch (JSONException e) {
                e.printStackTrace();
            }

//                startActivityForResult(new Intent(FinalScreenNew.this,LastScreen.class),2000);

        }

    }

    private File searchforfile(String name) {
        File tIcon = null;
        ArrayList<ImagesDO> filePaths = Utils.getFilePaths(FinalScreenNew.this);
        for (int i = 0; i < filePaths.size(); i++) {
            ImagesDO imagesDO = filePaths.get(i);
            String imageName = imagesDO.imageName;
            String[] split = imageName.split("/");
            String s = split[split.length - 1];
            System.out.println("image nameeee:::" + s);
            if (name.equalsIgnoreCase(s)) {
                System.out.println("entereeee:::" + name);
                WordUtils.compressImage(imageName);
                tIcon = new File(getsmallImage(name));
                return tIcon;

            }
        }
        return tIcon;

    }

    public String getsmallImage(String path) {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + path);
        return uriSting;
    }
//

    public void saveerror(String s) {
        String error = PreferenceUtils.getStringFromPreference("error", "");
        if (!error.equalsIgnoreCase("")) {
            System.out.println("entered if save::");
            try {
                Object deserialize = ObjectSerializer.deserialize(error);

                ArrayList<String> errorArrayTot = (ArrayList<String>) deserialize;
                errorArrayTot.add(s);
                String serialize = ObjectSerializer.serialize(errorArrayTot);
                PreferenceUtils.saveString("error", serialize);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        } else {
            ArrayList<String> errorArrayNew = new ArrayList<>();
            errorArrayNew.add(s);
            System.out.println("entered else save::");
            String serialize = null;
            try {
                serialize = ObjectSerializer.serialize(errorArrayNew);
                PreferenceUtils.saveString("error", serialize);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:


            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 101 && resultCode == 101) {
            if (data != null) {
                String title = data.getStringExtra("TourTitle");
                mSectionsPagerAdapter.AddCaptionToImage(title, data.getStringExtra("Summary"));
            }
        } else if (requestCode == 2000 && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }

//        Log.e("FinalScreenNew","ssss212121"+requestCode);
    }

    public void doUrlSubmit() throws JSONException {

        showLoader();

        JSONObject joMain = new JSONObject();

        if (from.equalsIgnoreCase("MyTourListNew")) {
            joMain.put("tourId", "" + myTourItemDO.tourId);
        }
        if (LocalData.getInstance().isFromedittour()) {
            System.out.println("edittttttttttttt");
            String locationText = LocalData.getInstance().getLocationText();
            int length = locationText.length();
            if (length > 80) {
                joMain.put("address", "" + LocalData.getInstance().getLocationText().substring(0, 70));
                joMain.put("area", "" + LocalData.getInstance().getLocationText().substring(0, 70));
                joMain.put("city", "" + LocalData.getInstance().getLocationText().substring(0, 70));
            } else {
                joMain.put("address", "" + LocalData.getInstance().getLocationText());
                joMain.put("area", "" + LocalData.getInstance().getLocationText());
                joMain.put("city", "" + LocalData.getInstance().getLocationText());
            }

        } else {
            joMain.put("address", "" + myTourItemDO.location);
            joMain.put("area", "" + myTourItemDO.area);
            joMain.put("city", "" + myTourItemDO.city);
        }

        joMain.put("summary", "" + myTourItemDO.summary);
        joMain.put("title", "" + myTourItemDO.title);
        joMain.put("tourLangitude", "" + myTourItemDO.tourLangitude);
        joMain.put("tourLatitude", "" + myTourItemDO.tourLatitude);
        joMain.put("tourPhotoExt", "" + "jpeg");
        joMain.put("tourPhotoPath", "" + myTourItemDO.tourPhotoPath);
        if (myTourItemDO.unPublishFlag.trim().equalsIgnoreCase("")) {
            myTourItemDO.unPublishFlag = "0";
        }
        joMain.put("unPublishFlag", "" + myTourItemDO.unPublishFlag);


        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < myTourItemDO.subtour.size(); i++) {
            JSONObject jPhots = new JSONObject();
            TourItem tourItem = myTourItemDO.subtour.get(i);
            System.out.println("tourItem:::" + tourItem.audioPath + "::" + tourItem.ImagePath + "::" + tourItem.latitude + "::" + tourItem.longitude);
            jPhots.put("imageExtension", "jpg");
            System.out.println("audio:: path::" + myTourItemDO.subtour.get(i).audioPath);
            try {
                jPhots.put("photoPath", myTourItemDO.subtour.get(i).ImagePath);
//
                jPhots.put("photoAudioPath", myTourItemDO.subtour.get(i).audioPath);
//
            } catch (ArrayIndexOutOfBoundsException e) {
            }
            jPhots.put("photoCaption", myTourItemDO.subtour.get(i).summary);
            jPhots.put("photoLocation", "" + myTourItemDO.subtour.get(i).location);
            jPhots.put("photoTitle", "" + myTourItemDO.subtour.get(i).title);
            jPhots.put("photolatitude", "" + myTourItemDO.subtour.get(i).latitude);
            jPhots.put("photolangitude", "" + myTourItemDO.subtour.get(i).longitude);
            jPhots.put("tourOrderNumber", "" + (i + 1));


            if (from.equalsIgnoreCase("MyTourListNew")) {
                jPhots.put("photoId", myTourItemDO.subtour.get(i).photoId);
            }

            jsonArray.put(jPhots);


//            photos= [photoId=2600, photoTitle=No Text, photoCaption=No Text,
// photoLocation=No Text, photoPath=https://s3.amazonaws.com/shomiround/199/IMG_Mar082017_120843.jpg,
// photoThumbPath=null, photoAudioPath=, audioTime=null, imageExtension=jpg,
// audioExtension=null, photolatitude=null, photolangitude=null, createdOn=0,
// tourOrderNumber=null],
        }
        JSONArray jsonDelArray = new JSONArray();
        if (LocalData.getInstance().isFromedittour()) {

            HashMap<String, TourItem> deletedSubTour = LocalData.getInstance().getDeletedSubTour();

            Set<String> deletedStr = deletedSubTour.keySet();
            for (String key : deletedStr) {
                final TourItem tourItem = deletedSubTour.get(key);
                JSONObject deleObj = new JSONObject();
                deleObj.put("photoId", tourItem.photoId);
                deleObj.put("photoTitle", tourItem.title);
                deleObj.put("photoCaption", tourItem.summary);
                deleObj.put("photoPath", tourItem.ImagePath);
                deleObj.put("photoThumbPath", tourItem.ImagePath);
                deleObj.put("photoAudioPath", tourItem.audioPath);
                deleObj.put("audioTime", "");
                deleObj.put("imageExtension", "jpg");
                deleObj.put("audioExtension", "");
                deleObj.put("photolatitude", "");
                deleObj.put("photolangitude", "");
                deleObj.put("createdOn", "");
                deleObj.put("tourOrderNumber", "");
                jsonDelArray.put(deleObj);
            }


//            deletedTours=[TourPhotosDto [photoId=2603,
// photoTitle=No Text, photoCaption=No Text, photoLocation=No Text,
// photoPath=https://s3.amazonaws.com/shomiround/199/IMG_Feb232017_131838.jpg,
// photoThumbPath=https://s3.amazonaws.com/shomiround/199/IMG_Feb232017_131838_thumb.jpg,
// photoAudioPath=, audioTime=, imageExtension=jpg,
// audioExtension=, photolatitude=0, photolangitude=0,
// createdOn=0, tourOrderNumber=null]]


        }

        JSONArray categories = new JSONArray();
        JSONArray tags = new JSONArray();

        if (mCategoryDOs != null && mCategoryDOs.size() > 0) {
            for (int i = 0; i < mCategoryDOs.size(); i++) {
                JSONObject jPhots = new JSONObject();
                jPhots.put("categorieId", mCategoryDOs.get(i).categorieId);
                jPhots.put("categorieName", mCategoryDOs.get(i).categorieName);
                jPhots.put("categorieImageLocation", mCategoryDOs.get(i).categorieImageLocation);
                categories.put(jPhots);
            }
        }
        if (mTagsDos != null && mTagsDos.size() > 0) {
            for (int i = 0; i < mTagsDos.size(); i++) {
                JSONObject jPhots = new JSONObject();
                jPhots.put("tagId", mTagsDos.get(i).tagId);
                jPhots.put("tagName", mTagsDos.get(i).tagName);
                tags.put(jPhots);

            }
        }
        joMain.put("categoriesList", categories);
        joMain.put("tagsList", tags);
        joMain.put("photos", jsonArray);
        joMain.put("deletedTours", jsonDelArray);
        System.out.println("joMain::" + joMain);

        String authid = preferenceUtils.getStringFromPreference("accessToken", "");
        RequestParams requestParams = new RequestParams();


        String url = Urls.UPDATETOUR + authid;

        requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
        requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;

        requestParams.data = joMain.toString();

        ServiceCalls serviceCalls = new ServiceCalls(this, "Update");
        serviceCalls.execute(requestParams);


    }


    @Override
    public void onData(Object data, String error, String tag) {

        progressBar.setVisibility(View.GONE);
        hideLoader();
        System.out.println("error::" + error);
        System.out.println("error:: tag" + tag);
        if (error == null) {
            String tourId = (String) data;
            for (int i = 0; i < allImages.size(); i++) {
                mDBCreator.insertImage(allImages.get(i), "no", tourId);
            }
            b.dismiss();
//            mDBCreator.insertImage(tourImage.getAbsolutePath(),"no",mAddTourDO.tourId);

//            startActivityForResult(new Intent(FinalScreenNew.this, LastScreen.class), 2000);
            getSuccessAlert();
        } else if (error.equalsIgnoreCase("Tour is updated successfully")) {
            b.dismiss();
//            startActivityForResult(new Intent(FinalScreenNew.this, EditLastScreen.class), 2000);
//            Toast.makeText(FinalScreenNew.this,""+error,Toast.LENGTH_LONG).show();
            getSuccessAlert();
        } else {
            Toast.makeText(FinalScreenNew.this, "" + error, Toast.LENGTH_LONG).show();
        }

    }

    private void getSuccessAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(FinalScreenNew.this);
        builder.setMessage("Tour Updated Successfully");

        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {



              /*  mDBCreator.deleteTags(getIntent().getStringExtra("draftId"));
                mDBCreator.deleteCategories(getIntent().getStringExtra("draftId"));

                mDBCreator.insertTags(getIntent().getStringExtra("draftId"),mTagsDos);
                mDBCreator.insertCategpries(getIntent().getStringExtra("draftId"),mCategoryDOs);
                mDBCreator.updateDetailsToDraftTable(mAddTourDO,draftId);*/

                dialog.dismiss();
               /* onBackPressed();*/
                LocalData.getInstance().setIsFromProfile("yes");
                Intent intent = new Intent(FinalScreenNew.this, MainScreen.class);
                startActivity(intent);
                finish();

            }
        });
        AlertDialog  b = builder.create();
        b.show();
    }

}



