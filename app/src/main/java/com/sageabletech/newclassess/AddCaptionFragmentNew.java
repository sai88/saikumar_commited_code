package com.sageabletech.newclassess;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sageabletech.R;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.MainScreen;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class AddCaptionFragmentNew extends Fragment{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private ArrayList<TourItem> images;
    MyTourItemDO selectedMyTourItemDO;

    public AddCaptionFragmentNew() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static AddCaptionFragmentNew newInstance(int sectionNumber, Context ctx) {

        AddCaptionFragmentNew fragment = new AddCaptionFragmentNew();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }
    ImageAdapter mImageAdapter;

    int selectedPos = -1;
    public void refreshFragment(String text,String summary){
        if(mImageAdapter!=null){
            if(selectedPos != -1)
            {
                images.get(selectedPos).text = text;
                images.get(selectedPos).summary = summary;
            }

            mImageAdapter.notifyDataSetChanged();
        }
    }
    public void refresh(){
        MyTourItemDO selectedMyTourItemDO = LocalData.getInstance().getSelectedMyTourItemDO();
        images= selectedMyTourItemDO.subtour;
        for (TourItem tourItem:images){
            String imagePath = tourItem.ImagePath;
            System.out.println("imagePath 123::"+imagePath);
        }

        mImageAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("asdfsdfdsf on resume");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.grid_fragment, container, false);

//        images = (ArrayList<TourItem>) getArguments().getSerializable("Images");
        selectedMyTourItemDO=LocalData.getInstance().getSelectedMyTourItemDO();
        images= selectedMyTourItemDO.subtour;
        System.out.println("images:::123"+images);

        GridView gallery = (GridView) rootView.findViewById(R.id.galleryGridView);

        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedPos = position;
//                SelectedImage



                LocalData.getInstance().setSubtourposition(position);
                LocalData.getInstance().setSubtouredittitle(true);
                LocalData.getInstance().setDataofsubtour( images.get(position));
                System.out.println("images.get(position).text:123:"+images.get(position).text);
                System.out.println("images.get(position).text:123:"+images.get(position).ImagePath);
                System.out.println("images.get(position).text:123:"+images.get(position).summary);
                System.out.println("images.get(position).text:123:"+images.get(position).location);



                startActivityForResult(new Intent(getActivity(), EditTitleForPhoto.class)
                    .putExtra("SelectedImage", images.get(position).ImagePath.toString())
                    .putExtra("From", "AddCaption")
                    .putExtra("title", ""+images.get(position).text)
                    .putExtra("summary", ""+images.get(position).summary)
                    .putExtra("position", position), 101);

            }
        });


        mImageAdapter = new ImageAdapter(getActivity());
        gallery.setAdapter(mImageAdapter);

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);

        Log.e("ffffffff"+requestCode, "bbbbbbbbbbbb"+resultCode);

        if(requestCode ==101 ){
            Log.e("requestCode :"+requestCode, "resultCode :"+resultCode);
            System.out.println("LocalData.getInstance().isSubtouredittitle():::"+LocalData.getInstance().isSubtouredittitle());
            if(data !=null){

                Log.e("selectedPos "+selectedPos, "data "+data.getStringExtra("TourTitle"));
                if(selectedPos != -1)
                selectedMyTourItemDO.subtour.get(selectedPos).text = data.getStringExtra("TourTitle");
                selectedMyTourItemDO.subtour.get(selectedPos).summary = data.getStringExtra("Summary");
                images=selectedMyTourItemDO.subtour;
                System.out.println("doing thisssss");
                LocalData.getInstance().setSelectedMyTourItemDO(selectedMyTourItemDO);

                refreshFragment(data.getStringExtra("TourTitle"),data.getStringExtra("Summary"));
            }

            if(LocalData.getInstance().isSubtouredittitle()){
                System.out.println("heldksfkjdsfdslf");
                int subtourposition = LocalData.getInstance().getSubtourposition();
                System.out.println("subtourposition:::"+subtourposition);
                images.set(subtourposition,LocalData.getInstance().getDataofsubtour());
                selectedMyTourItemDO.subtour=images;
                LocalData.getInstance().setSubtouredittitle(false);
                LocalData.getInstance().setSelectedMyTourItemDO(selectedMyTourItemDO);
                refresh();

            }
        }



    }


    private class ImageAdapter extends BaseAdapter {

        /** The context. */
        private Activity context;

        /**
         * Instantiates a new image adapter.
         *
         * @param localContext
         *            the local context
         */
        public PreferenceUtils preferenceUtils = null;
        public ImageAdapter(Activity localContext) {
            preferenceUtils = new PreferenceUtils(getActivity());

            context = localContext;
        }

        public int getCount() {
            if(images == null){
                return 0;
            }
            return images.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            final ViewHolder holder;

            if (convertView == null ) {

                convertView = LayoutInflater.from(context).inflate(R.layout.grid_item,null,false);
                holder = new ViewHolder();
                holder.img = (ImageView) convertView.findViewById(R.id.imageView1);
                holder.tv = (CheckBox) convertView.findViewById(R.id.checkBox1);
                holder.ivClose = (ImageView) convertView.findViewById(R.id.ivClose);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolder)convertView.getTag();
            }

            holder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);
            convertView.setLayoutParams(new GridView.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2, (AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2));

            if(!images.get(position).ImagePath.equalsIgnoreCase("Plus")){
                String url = images.get(position).ImagePath;
                String userId = preferenceUtils.getStringFromPreference("userId","");

                if(url.contains(userId)&&!url.contains("https://s3.amazonaws.com/")){
                    url="https://s3.amazonaws.com/shomiround-production/"+url;
                }
                System.out.println("url:::"+url);

                Glide.with(getActivity()).load(url)

                        .placeholder(R.color.black)
                        .override(AppConstants.DEVICE_DISPLAY_WIDTH/2, AppConstants.DEVICE_DISPLAY_WIDTH/2).into(holder.img);

//                Picasso.with(getActivity())
//                        .load(url)
//                        .placeholder(R.color.black)
//                        .resize(AppConstants.DEVICE_DISPLAY_WIDTH/2, AppConstants.DEVICE_DISPLAY_WIDTH/2).into(holder.img);
//                Glide.with(context)
//                        .load(url)
//                        .centerCrop()
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .placeholder(R.color.black)
//                        .thumbnail(0.5f)
//                        .crossFade()
//                        .into(holder.img);

                holder.ivClose.setVisibility(View.GONE);

            }else{
                holder.img.setImageResource(R.mipmap.ic_image_add_plus_outline_white);
                holder.ivClose.setVisibility(View.GONE);
                holder.img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       getActivity().finish();
                    }
                });

            }


            if(!images.get(position).text.equalsIgnoreCase("")||!images.get(position).summary.equalsIgnoreCase("")){
                holder.ivClose.setVisibility(View.VISIBLE);
                holder.ivClose.setOnClickListener(null);
                holder.ivClose.setImageResource(R.mipmap.ic_note_outline_white);
            }else{
                holder.ivClose.setVisibility(View.GONE);
            }





            return convertView;
        }

        public class ViewHolder
        {
            CheckBox tv;
            ImageView img,ivClose;

        }

    }


}