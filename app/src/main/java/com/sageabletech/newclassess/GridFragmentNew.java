package com.sageabletech.newclassess;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sageabletech.R;
import com.sageabletech.dialog.AlertDialogMsg;
import com.sageabletech.dynamicgrid.BaseDynamicGridAdapter;
import com.sageabletech.dynamicgrid.DynamicGridView;
//import com.sageabletech.example.CheeseDynamicAdapter;
import com.sageabletech.example.CheeseDynamicAdapter1;
import com.sageabletech.example.Cheeses;
import com.sageabletech.example.GridActivity;
import com.sageabletech.model.ImagesDO;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.ImagesAdding;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class GridFragmentNew extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private ArrayList<TourItem> images = new ArrayList<>();
    public static Context ctx;

    public GridFragmentNew() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static GridFragmentNew newInstance(int sectionNumber, Context ctx) {

        GridFragmentNew fragment = new GridFragmentNew();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
//        args.putSerializable("Images", ((FinalScreenNew)ctx).mAddTourDO.tourPhotos);
        fragment.setArguments(args);
        return fragment;
    }

    CheeseDynamicAdapter mImageAdapter;
    MyTourItemDO selectedMyTourItemDO;
    ArrayList<TourItem> subtour;
    View rootView;
    LayoutInflater inflaterNew;
    ViewGroup containerNew;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.grid_fragment, container, false);
        inflaterNew = inflater;
        /*TourItem ti = new TourItem();
        ti.ImagePath = "0";
        images.add(ti);*/
        images.clear();
        selectedMyTourItemDO = LocalData.getInstance().getSelectedMyTourItemDO();
        subtour = selectedMyTourItemDO.subtour;


        images.addAll(subtour);
        System.out.println("sai images:::" + images);

        final DynamicGridView gallery = (DynamicGridView) rootView.findViewById(R.id.galleryGridView);

        ImageView ivPlus = (ImageView) rootView.findViewById(R.id.plus);
        ivPlus.setVisibility(View.VISIBLE);

        ivPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivityForResult(new Intent(getActivity(), ImagesAdding.class).putExtra("Images", images).putExtra("isform", "edit"), 2017);

            }
        });

//

        mImageAdapter = new CheeseDynamicAdapter(getActivity(), images, 3);
        gallery.setAdapter(mImageAdapter);
//        CheeseDynamicAdapter1 cheeseDynamicAdapter = new CheeseDynamicAdapter1(getActivity(),
//               imageNamesArry,
//                getResources().getInteger(R.integer.column_count));
//        gallery.setAdapter(cheeseDynamicAdapter);

        /*gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(images.get(position).ImagePath.equalsIgnoreCase("Plus"))

            }
        });*/
//        LinearLayout viewById =(LinearLayout) rootView.findViewById(R.id.gridcont);
//        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT, 1);
//        viewById.setOrientation(LinearLayout.VERTICAL);
//        viewById.setLayoutParams(param);
//
////        viewById.setWeightSum(3);
//        for (int i=0;i<40;i++){
//
//            System.out.println("addeed:::"+i);
////            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1);
//            viewById.addView(new Button(getActivity()));
//
//
//
//
//        }

        gallery.setOnDragListener(new DynamicGridView.OnDragListener() {
            @Override
            public void onDragStarted(int position) {
                Log.d("sai", "drag started at position " + position);
            }

            @Override
            public void onDragPositionsChanged(int oldPosition, int newPosition) {
                Log.d("sai", String.format("drag item position changed from %d to %d", oldPosition, newPosition));
            }
        });

        gallery.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                gallery.startEditMode(position);
                return true;
            }
        });

        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), parent.getAdapter().getItem(position).toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
//        viewById.addView(innerCont);
//
//oncreate,onstart,onResume();onPause();onStop();onRestart();

        return rootView;
    }


    ImageView img;
    CheckBox tv;
    ImageView ivClose;


    public LinearLayout getGridCont() {

        View child = getActivity().getLayoutInflater().inflate(R.layout.grid_item, null);


        img = (ImageView) child.findViewById(R.id.imageView1);
        tv = (CheckBox) child.findViewById(R.id.checkBox1);
        ivClose = (ImageView) child.findViewById(R.id.ivClose);

        return (LinearLayout) child;
    }


    public void refreshFragment() {

        Log.e("GridFragmentNew", "refreshFragment");

    }

    private class ImageAdapter extends BaseAdapter {

        /**
         * The context.
         */
        private Activity context;

        /**
         * Instantiates a new image adapter.
         *
         * @param localContext
         * the local context
         */
        public PreferenceUtils preferenceUtils = null;

        public ImageAdapter(Activity localContext) {
            preferenceUtils = new PreferenceUtils(getActivity());
            context = localContext;
        }

        public int getCount() {
            if (images == null) {
                return 0;
            }
            return images.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            final ViewHolder holder;

            if (convertView == null) {

                convertView = LayoutInflater.from(context).inflate(R.layout.grid_item, null, false);
                holder = new ViewHolder();
                holder.img = (ImageView) convertView.findViewById(R.id.imageView1);
                holder.tv = (CheckBox) convertView.findViewById(R.id.checkBox1);
                holder.ivClose = (ImageView) convertView.findViewById(R.id.ivClose);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            return convertView;
        }

        public class ViewHolder {
            CheckBox tv;
            ImageView img, ivClose;
            String type = "";

        }

    }


    public void alertDialogFinish(String msg, final int position, final TourItem tourItem) {

        try {

//            DialogFrag alertdFragment = new DialogFrag();
//            // Show Alert DialogFragment
//            alertdFragment.show(gets, "Alert Dialog Fragment");
            new AlertDialogMsg(getActivity(), msg).setPositiveButton("YES", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
//                    if(selectedMyTourItemDO.subtour.)){
                    try {
                        if (selectedMyTourItemDO.subtour != null && selectedMyTourItemDO.subtour.size() > 0) {
                            System.out.println("selectedMyTourItemDO.subtour.get(position)::" + tourItem.ImagePath);
                            if ((tourItem.ImagePath.contains("https://s3.amazonaws.com"))) {

                                TourItem tourItem = selectedMyTourItemDO.subtour.get(position);

                                System.out.println("tourItem tourId::" + tourItem.photoId);
                                if (tourItem.photoId != null && !tourItem.photoId.equalsIgnoreCase("")) {
                                    HashMap<String, TourItem> deletedSubTour = LocalData.getInstance().getDeletedSubTour();
                                    deletedSubTour.put(tourItem.photoId, tourItem);
                                    System.out.println("deletedSubTour::" + deletedSubTour);
                                    LocalData.getInstance().setDeletedSubTour(deletedSubTour);
                                }

                                selectedMyTourItemDO.subtour.remove(tourItem);
                                LocalData.getInstance().setSelectedMyTourItemDO(selectedMyTourItemDO);
                                images = selectedMyTourItemDO.subtour;
                                mImageAdapter.remove(tourItem);
                                mImageAdapter.notifyDataSetChanged();
                            } else {
                                System.out.println("entreeererer");
                                ArrayList<ImagesDO> galleryList = LocalData.getInstance().getGalleryList();


                                for (int i = 0; i < galleryList.size(); i++) {
                                    ImagesDO imagesDO = galleryList.get(i);
                                    String imageName = imagesDO.imageName;
                                    System.out.println("imageName::"+imageName+"ImagePath::"+tourItem.ImagePath);

                                    if (imageName.equalsIgnoreCase(tourItem.ImagePath)) {
                                        System.out.println("cameeeeeeeeeee");
                                        imagesDO.isSelect = false;
                                        galleryList.add(i, imagesDO);
                                        break;
                                    }
                                }
                                LocalData.getInstance().setGalleryList(galleryList);
                                selectedMyTourItemDO.subtour.remove(tourItem);
                                LocalData.getInstance().setSelectedMyTourItemDO(selectedMyTourItemDO);
                                images = selectedMyTourItemDO.subtour;
                                mImageAdapter.remove(tourItem);
                                mImageAdapter.notifyDataSetChanged();
                            }
                        } else {
                            System.out.println("elseeeeeecdeeee");
                            dialog.dismiss();
                        }
                    } catch (Exception e) {

                    }
//                    }
                }

            }).setNegativeButton("NO", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }

            }).create().show();

        } catch (Exception e) {
            // TODO: handle exception
        }

    }


    @Override

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        if (data != null && LocalData.getInstance().getImageselection().equalsIgnoreCase("yes")) {
            LocalData.getInstance().setImageselection("no");
            images = new ArrayList<TourItem>();
            images = (ArrayList<TourItem>) data.getSerializableExtra("Images");
            Log.e("FinalScreenNew", "images : " + images.size());
            selectedMyTourItemDO.subtour = images;
            mImageAdapter.clear();
            mImageAdapter.add(images);

            mImageAdapter.notifyDataSetChanged();
        }
    }

    public class CheeseDynamicAdapter extends BaseDynamicGridAdapter {
        PreferenceUtils preferenceUtils = null;
        ArrayList<TourItem> imagesLocal;

        public CheeseDynamicAdapter(Context context, ArrayList<TourItem> images, int columnCount) {
            super(context, images, columnCount);
            this.imagesLocal = images;

            preferenceUtils = new PreferenceUtils(getActivity());
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            CheeseViewHolder holder;
            images = mImageAdapter.getItems();
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.grid_item, null);
                holder = new CheeseViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (CheeseViewHolder) convertView.getTag();
            }
            convertView.setLayoutParams(new GridView.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2, (AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2));

            holder.build((TourItem) getItem(position), position);
            return convertView;
        }

        private class CheeseViewHolder {

            CheckBox tv;
            ImageView img, ivClose;
            String type = "";


            private CheeseViewHolder(View view) {
                img = (ImageView) view.findViewById(R.id.imageView1);
                tv = (CheckBox) view.findViewById(R.id.checkBox1);
                ivClose = (ImageView) view.findViewById(R.id.ivClose);


            }

            void build(final TourItem data, final int position) {
                img.setScaleType(ImageView.ScaleType.CENTER_CROP);

                System.out.println("Imagesss:::::SAI::" + data.ImagePath);

                String url = data.ImagePath;
                String userId = preferenceUtils.getStringFromPreference("userId", "");

                if (url.contains(userId) && !url.contains("https://s3.amazonaws.com/")) {
                    url = "https://s3.amazonaws.com/shomiround-production/" + url;
                }
                System.out.println("url loaded::" + url);

//                Picasso.with(getActivity())
//                        .load(url)
//                        .placeholder(R.color.black)
//                        .resize(AppConstants.DEVICE_DISPLAY_WIDTH / 2, AppConstants.DEVICE_DISPLAY_WIDTH / 2).into(img);

                Glide.with(getActivity()).load(url)

                        .placeholder(R.color.black)
                        .override(AppConstants.DEVICE_DISPLAY_WIDTH / 2, AppConstants.DEVICE_DISPLAY_WIDTH / 2).into(img);

//            Glide.with(getActivity()).load(url).into(holder.img);


                ivClose.setVisibility(View.VISIBLE);
                System.out.println("holder.ivClose" + ivClose.isShown());
                System.out.println("holder.img" + ivClose.isShown());


                ivClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        System.out.println("clickeddddd::::::::");
                        if(ivClose.isPressed())
                        alertDialogFinish("Are you want to delete this photo.", position, data);

                    }
                });
            }
        }
    }
}