package com.sageabletech.newclassess;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.droidmentor.locationhelper.Constants;
import com.droidmentor.locationhelper.MyLocationUsingHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sageabletech.R;
import com.sageabletech.api.Location;
import com.sageabletech.api.PlaceDetailsJSONParser;
import com.sageabletech.api.PlaceJSONParser;
import com.sageabletech.dialog.AlertDialogMsg;
import com.sageabletech.model.ImagesDO;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.AddTourScreen;
import com.sageabletech.shomiround.BaseActivity;
import com.sageabletech.shomiround.CategoriesActivity;
import com.sageabletech.shomiround.MainScreen;
import com.sageabletech.shomiround.TourIconSelection;
import com.sageabletech.utils.AppConstants;
import com.google.android.gms.maps.SupportMapFragment;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.Utils;
import com.sageabletech.webservicecalls.GPSTracker;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class AddTourScreenNew extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener,OnMapReadyCallback {

    private static final String TAG = AddTourScreenNew.class.getName();
    ImageView right, ivLocationIcon,back;
    TextView tvTitle;
    EditText tvTourTitle, tvTourSummary;
    protected double latitude = 0, longitude = 0;

    DownloadTask placesDownloadTask;
    DownloadTask placeDetailsDownloadTask;
    ParserTask placesParserTask;
    ParserTask placeDetailsParserTask;
    BitmapDescriptor map_icon = null;
    protected LocationManager locationManager;
    protected LocationListener locationListener;

    final int PLACES = 0;
    final int PLACES_DETAILS = 1;

    //    GoogleApiClient mGoogleApiClient;
    final static int REQUEST_LOCATION = 199;

    MyTourItemDO mAddTourDONew = null;
    String from = "";
    ImageView ivSelectedImage;

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (LocalData.getInstance().getEditpicpath() != null) {
//            MyTourItemDO selectedMyTourItemDO = LocalData.getInstance().getSelectedMyTourItemDO();

            String editpicpath = LocalData.getInstance().getEditpicpath();
            mMyTourItemDO.tourPhotoPath=editpicpath;
            LocalData.getInstance().setSelectedMyTourItemDO(mMyTourItemDO);
            Glide.with(AddTourScreenNew.this).load(editpicpath).placeholder(R.drawable.background).centerCrop()
                    .into(ivSelectedImage);


        }
        if(LocalData.getInstance().getLocationHit().equalsIgnoreCase("yes")){
            latitude=  Constants.getInstance().getLatitude();
            longitude= Constants.getInstance().getLongitude();
            LocalData.getInstance().setLocationHit("no");
            mMyTourItemDO.tourLangitude=""+longitude;
            mMyTourItemDO.tourLatitude=""+latitude;
            LocalData.getInstance().setSelectedMyTourItemDO(mMyTourItemDO);
            if (latitude != 0&&longitude!=0) {
                LatLng latLng = new LatLng(latitude, longitude);
                System.out.println("latLng::" + latLng);
                mLocation.setText(Constants.getInstance().getLocationText());
                GPSTracker gpsTracker = new GPSTracker(AddTourScreenNew.this);
                LatLng currentLatLng = new LatLng(latitude, longitude);
                showLocationOnMap(map_icon, currentLatLng);

            }


        }
    }
TextView loader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.tour_add);
        loader = (TextView) findViewById(R.id.loader);
        ProgressDialog progressDialog = LocalData.getInstance().getProgressDialog();
        if(progressDialog!=null){
            progressDialog.dismiss();
        }
        map_icon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
        ivSelectedImage = (ImageView) findViewById(R.id.ivSelectedImage);
        ivSelectedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MyTourListNew==edit//
                LocalData.getInstance().setInnertitleedit("no");
                startActivity(new Intent(AddTourScreenNew.this, GallarySample.class));
            }
        });

        setHeader();

        from = getIntent().getStringExtra("From");
        System.out.println("from:::" + from);


        setTheMap();

        init();

        /*mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();*/

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            public void onLocationChanged(android.location.Location location) {
                // Called when a new location is found by the network location provider.

                latitude = location.getLatitude();
                longitude = location.getLongitude();

                if (from.equalsIgnoreCase("MyTourListNew")) {

                    if (!mMyTourItemDO.tourLatitude.isEmpty() && !mMyTourItemDO.tourLangitude.isEmpty()) {
                        LatLng latLng1 = new LatLng(Double.parseDouble(mMyTourItemDO.tourLatitude),
                                Double.parseDouble(mMyTourItemDO.tourLangitude));
                        if (latLng1 != null) {
                            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng1));
                            googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

                        }
                    }

                } else {
                    LatLng latLng = new LatLng(latitude, longitude);

                    if (latLng != null) {

                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

                    }
                }


            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
//            Toast.makeText(AddTourScreenNew.this, "Not Called", Toast.LENGTH_SHORT).show();
            String[] permissions = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION};
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions, 111);
            }

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        /*if(!from.equalsIgnoreCase("MyTourListNew"))
        {*/
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

//        }


    }

    private void setHeader() {

        tvTitle=(TextView)findViewById(R.id.header_text);

        tvTitle.setText("NEW TOUR");
        tvTitle.setTypeface(Utils.getHelvaticaMedium(AddTourScreenNew.this));
        right = (ImageView) findViewById(R.id.submit);



        right.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_forward_circle));

        back =(ImageView)findViewById(R.id.back);
        back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_cross_white));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    String address = "";

    private String getAddress(android.location.Location location) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            address += "," + addresses.get(0).getSubLocality();
            address += "," + addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return address;
    }


    AutoCompleteTextView mLocation;
    ImageView cancelLoc;
    MyTourItemDO mMyTourItemDO = null;

    private void init() {
        mMyTourItemDO = LocalData.getInstance().getSelectedMyTourItemDO();
        ivSelectedImage = (ImageView) findViewById(R.id.ivSelectedImage);
        ivLocationIcon = (ImageView) findViewById(R.id.ivLocationIcon);


        tvTourTitle = (EditText) findViewById(R.id.tvTourTitle);
        tvTourTitle.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = tvTourTitle.getText().toString().toLowerCase(Locale.getDefault());
                tvTitle.setText(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });
        tvTourSummary = (EditText) findViewById(R.id.tvTourSummary);
        cancelLoc = (ImageView)findViewById(R.id.cancelLoc);
        cancelLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLocation.setText("");
                mMyTourItemDO.tourLatitude="";
                mMyTourItemDO.tourLangitude="";

            }
        });

        //tvTitle.setText("NEW TOUR");
        right.setVisibility(View.VISIBLE);

        mLocation = (AutoCompleteTextView) findViewById(R.id.editText3);
        mLocation.setThreshold(2);

        Log.e(TAG, "" + getIntent().getStringExtra("SelectedImage"));

        if (getIntent().getStringExtra("SelectedImage") != null) {

        }
        System.out.println("seledeeeddd:" + mMyTourItemDO.tourPhotoPath);
        Glide.with(this)
                .load(mMyTourItemDO.tourPhotoPath)
                .placeholder(R.color.black)
                .centerCrop()
                .override(AppConstants.DEVICE_DISPLAY_WIDTH, AppConstants.DEVICE_DISPLAY_WIDTH / 2)
                .crossFade()
                .into(ivSelectedImage);


        String from = getIntent().getStringExtra("From");


        tvTitle.setText("My Tour");


        int size = mMyTourItemDO.subtour.size();
        ArrayList<TourItem> subtour = mMyTourItemDO.subtour;
        System.out.println("subtour 123::" + subtour);
        System.out.println("subtour size::" + size);

        tvTourTitle.setText("" + mMyTourItemDO.title);
        tvTourSummary.setText("" + mMyTourItemDO.summary);
        mLocation.setText("" + mMyTourItemDO.city);

        System.out.println("lat long::" + mMyTourItemDO.tourLatitude + ":::" + mMyTourItemDO.tourLangitude);
//            if(!mMyTourItemDO.tourLatitude.isEmpty() && !mMyTourItemDO.tourLangitude.isEmpty()){
//                LatLng latLng1 = new LatLng(Double.parseDouble(mMyTourItemDO.tourLatitude),
//                        Double.parseDouble(mMyTourItemDO.tourLangitude));
//                if (latLng1 != null) {
//                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng1));
//                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
//
//                }
//            }


        ivSelectedImage.setLayoutParams(new LinearLayout.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH),
                (AppConstants.DEVICE_DISPLAY_WIDTH / 2)));
        LocalData.getInstance().setEditpicpath(mMyTourItemDO.tourPhotoPath);
        Glide.with(this)
                .load(mMyTourItemDO.tourPhotoPath)
                .placeholder(R.color.black)
                .centerCrop()
                .override(AppConstants.DEVICE_DISPLAY_WIDTH, AppConstants.DEVICE_DISPLAY_WIDTH / 2)
                .into(ivSelectedImage);

        mLocation.setText(mMyTourItemDO.area + " " + mMyTourItemDO.city);
        System.out.println("googleMap:::" + googleMap);


        mLocation.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Creating a DownloadTask to download Google Places matching "s"
                placesDownloadTask = new DownloadTask(PLACES);
                // Getting url to the Google Places Autocomplete api
                String url = Location.getAutoCompleteUrl(s.toString());
                placesDownloadTask.execute(url);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });


        mLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                hideKeyBoard(mLocation);

                ListView lv = (ListView) parent;
                SimpleAdapter adapter = (SimpleAdapter) parent.getAdapter();

                HashMap<String, String> hm = (HashMap<String, String>) adapter.getItem(position);

                placeDetailsDownloadTask = new DownloadTask(PLACES_DETAILS);
                String url = Location.getPlaceDetailsUrl(hm.get("reference"));
                placeDetailsDownloadTask.execute(url);

                mLocation.setText(hm.get("description"));


            }
        });

        ivLocationIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCurrentLocation();

            }
        });


        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalData.getInstance().setGalleryList(new ArrayList<ImagesDO>());

                String from = getIntent().getStringExtra("From");

                if(tvTourTitle.getText().toString().length()==0){
                    Toast.makeText(AddTourScreenNew.this,"Enter Title and Summary",Toast.LENGTH_LONG).show();
                    return;
                }else if(mMyTourItemDO.tourLatitude.equalsIgnoreCase("")){
                    Toast.makeText(AddTourScreenNew.this,"Enter Location",Toast.LENGTH_LONG).show();
                    return;
                }


                    LocalData.getInstance().setLocationText(mLocation.getText().toString());
                    mAddTourDONew = new MyTourItemDO();
                    mAddTourDONew.title = tvTourTitle.getText().toString();
                    mAddTourDONew.summary = tvTourSummary.getText().toString();
                    mAddTourDONew.location = mLocation.getText().toString();
                    try{
                        mAddTourDONew.tourLatitude = ""+Double.parseDouble(mMyTourItemDO.tourLatitude);
                        mAddTourDONew.tourLangitude = ""+Double.parseDouble(mMyTourItemDO.tourLangitude);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    mAddTourDONew.tourId = mMyTourItemDO.tourId;
                    mAddTourDONew.tourPhotoPath = LocalData.getInstance().getEditpicpath();

                    mAddTourDONew.subtour = mMyTourItemDO.subtour;
                    System.out.println(" mAddTourDONew.subtour::"+ mAddTourDONew.subtour);

                    String id = getIntent().getStringExtra("draftId");
                    if(id == null){
                        id = mAddTourDONew.tourId;
                    }
                    System.out.println("mAddTourDONew.title::"+mAddTourDONew.title);
                    LocalData.getInstance().setSelectedMyTourItemDO(mAddTourDONew);
                LocalData.getInstance().setCatnew(true);


                startActivityForResult(new Intent(AddTourScreenNew.this, CategoriesActivity.class)

                            .putExtra("draftId",id)
                            .putExtra("From","MyTourListNew"),2001);




            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2001 && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
//            Toast.makeText(AddTourScreenNew.this, "Not Called 1", Toast.LENGTH_SHORT).show();
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }


    public void alertDialogFinish(String msg){

        try {

            new AlertDialogMsg(this, msg).setPositiveButton("YES", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {


                    LocalData.getInstance().setEditpicpath(null);
                    Intent intent = new Intent(AddTourScreenNew.this, MainScreen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                }

            }).setNegativeButton("NO",null).create().show();

        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        alertDialogFinish("Do you want to exit?");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void setTheMap() {

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(this, "Not", Toast.LENGTH_LONG).show();
            return;
        }




//		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, this);

    }


    @Override
    public void onConnected(Bundle bundle) {






    }

    @Override
    public void onConnectionSuspended(int i) {

    }
    private GoogleMap googleMap;
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap  = googleMap;
        if(LocalData.getInstance().isFromedittour()) {
            if (!mMyTourItemDO.tourLatitude.isEmpty() && !mMyTourItemDO.tourLangitude.isEmpty()) {
                LatLng latLng1 = new LatLng(Double.parseDouble(mMyTourItemDO.tourLatitude),
                        Double.parseDouble(mMyTourItemDO.tourLangitude));
                if (latLng1 != null) {

                    MarkerOptions markerOption = new MarkerOptions();
                    BitmapDescriptor icon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
                    markerOption.position(latLng1).icon(icon).flat(true).title("");
                    googleMap.addMarker(markerOption);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng1));
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

                }
            }
        }




    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        private int downloadType=0;

        // Constructor
        public DownloadTask(int type){
            this.downloadType = type;
        }

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = Location.downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            switch(downloadType){
                case PLACES:
                    // Creating ParserTask for parsing Google Places
                    placesParserTask = new ParserTask(PLACES);

                    // Start parsing google places json data
                    // This causes to execute doInBackground() of ParserTask class
                    placesParserTask.execute(result);

                    break;

                case PLACES_DETAILS :
                    // Creating ParserTask for parsing Google Places
                    placeDetailsParserTask = new ParserTask(PLACES_DETAILS);

                    // Starting Parsing the JSON string
                    // This causes to execute doInBackground() of ParserTask class
                    placeDetailsParserTask.execute(result);
            }
        }
    }


    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String,String>>>
    {

        int parserType = 0;

        public ParserTask(int type){
            this.parserType = type;
        }

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<HashMap<String, String>> list = null;

            try{
                jObject = new JSONObject(jsonData[0]);

                switch(parserType){
                    case PLACES :
                        PlaceJSONParser placeJsonParser = new PlaceJSONParser();
                        // Getting the parsed data as a List construct
                        list = placeJsonParser.parse(jObject);
                        break;
                    case PLACES_DETAILS :
                        PlaceDetailsJSONParser placeDetailsJsonParser = new PlaceDetailsJSONParser();
                        // Getting the parsed data as a List construct
                        list = placeDetailsJsonParser.parse(jObject);
                }

            }catch(Exception e){
                Log.d("Exception",e.toString());
            }
            return list;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {

            switch(parserType){
                case PLACES :
                    String[] from = new String[] { "description"};
                    int[] to = new int[] { android.R.id.text1 };

                    // Creating a SimpleAdapter for the AutoCompleteTextView
                    SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), result, R.layout.text_layout, from, to);

                    // Setting the adapter
                    mLocation.setAdapter(adapter);
                    break;
                case PLACES_DETAILS :
                    HashMap<String, String> hm = result.get(0);

                    // Getting latitude from the parsed data
                    latitude = Double.parseDouble(hm.get("lat"));

                    // Getting longitude from the parsed data
                    longitude = Double.parseDouble(hm.get("lng"));


                    LatLng latLng = new LatLng(latitude, longitude);

                    if (latLng != null) {

                        showLocationOnMap(map_icon, latLng);

                    }

                    break;
            }
        }
    }
    private void showLocationOnMap(BitmapDescriptor icon, LatLng latLng) {
        googleMap.clear();
        latitude = latLng.latitude;
        longitude = latLng.longitude;


        mMyTourItemDO.tourLangitude=""+longitude;
        mMyTourItemDO.tourLatitude=""+latitude;
        LocalData.getInstance().setSelectedMyTourItemDO(mMyTourItemDO);

        MarkerOptions markerOption = new MarkerOptions();
        markerOption.position(latLng).icon(icon).flat(true).title("");
        Marker marker = googleMap.addMarker(markerOption);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

    }

    GPSTracker gpsTracker;

    public void getCurrentLocation() {
        LocalData.getInstance().setLocationHit("yes");

        Intent intent=new Intent(AddTourScreenNew.this, MyLocationUsingHelper.class);
        startActivity(intent);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}



