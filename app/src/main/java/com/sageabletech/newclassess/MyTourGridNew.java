package com.sageabletech.newclassess;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.sageabletech.*;
import com.sageabletech.dialog.AlertDialogMsg;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.BaseActivity;
import com.sageabletech.shomiround.MapsActivity;
import com.sageabletech.shomiround.MyTourList;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.sageabletech.viewtour.DeleteTour;
import com.sageabletech.viewtour.FavMainTour;
import com.sageabletech.webservicecalls.AsyncRemoteCall.OnDataListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class MyTourGridNew extends Fragment implements OnDataListener{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    List<MyTourItemDO> tourList = null;

    public MyTourGridNew() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MyTourGridNew newInstance(int sectionNumber, Context ctx,String draftId) {

        MyTourGridNew fragment = new MyTourGridNew();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString("draftId", draftId);
        fragment.setArguments(args);
        return fragment;
    }
    ImageAdapter mImageAdapterGridView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.my_grid_fragment, container, false);

        GridView gallery = (GridView) rootView.findViewById(R.id.galleryGridView);


//        if(getArguments().getString("draftId").equalsIgnoreCase("DraftTour")){
//            //*Draft List
//            tourList = ((BaseActivity)getActivity()).mDBCreator.getAllListFromDraft();
//        }else{
//            tourList = new ArrayList<>();
//            String str = ((BaseActivity)getActivity()).preferenceUtils.getStringFromPreference("accessToken","");
//            prepareMovieData(str);
//        }
        String fromDrafts = LocalData.getInstance().getFromDrafts();
        if(fromDrafts.equalsIgnoreCase("fav")){
            ArrayList<MyTourItemDO> myTourItemDOs =LocalData.getInstance().getFavoritesArray();
            tourList=myTourItemDOs;
        }else{
            ArrayList<MyTourItemDO> myTourItemDOs =LocalData.getInstance().getmFeeds();
            tourList=myTourItemDOs;
        }


        System.out.println("tourListINGrids::"+tourList.size());

        mImageAdapterGridView = new ImageAdapter(getActivity(),(ArrayList<MyTourItemDO>) this.tourList);
        gallery.setAdapter(mImageAdapterGridView);

        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                LocalData.getInstance().setSelectedMyTourItemDO(myTourItemDOs.get(position));
//                mContext.startActivity(new Intent(mContext, AddTourScreenNew.class)
//                        .putExtra("Details",myTourItemDO)
//                        .putExtra("TourPhotoPath",""+mOffers.get(position).tourPhotoPath)
//                        .putExtra("From","MyTourListNew"));
//                startActivity(new Intent(getActivity(), MyTourDetails.class).putExtra("Details",tourList.get(position)));
            }
        });


        return rootView;
    }
//    private void prepareMovieData(String vendorId) {
//
//        ((BaseActivity)getActivity()).showLoader();
//        RequestParams requestParams = new RequestParams();
//        String url = Urls.MY_TOUR+vendorId;
//        requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
//        requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;
//
//        ServiceCalls serviceCalls = new ServiceCalls(this, "MYTOUR");
//        serviceCalls.execute(requestParams);
//
//    }



    @Override
    public void onData(Object data, String error, String tag) {
        try{

            ((BaseActivity)getActivity()).hideLoader();
        }catch (Exception e){

        }
        if(tag.equalsIgnoreCase("MYTOUR")){

            if (error == null && data != null) {

                ArrayList<MyTourItemDO> mFeeds = (ArrayList<MyTourItemDO>) data;
                if(mFeeds.size()>0)
                {
                    this.tourList = (ArrayList<MyTourItemDO>) mFeeds.clone();
                    mImageAdapterGridView.refresh((ArrayList<MyTourItemDO>) this.tourList);
                }

            }

        }else if(tag.equalsIgnoreCase("DELETE")){
            if (error == null && data != null) {
                /*String str = ((BaseActivity)getActivity()).preferenceUtils.getStringFromPreference("accessToken","");
                prepareMovieData(str);*/

                ((MyTourList)getActivity()).refreshData();
            }
        }
    }

//    public void refresh() {
//
//        if(getArguments().getString("draftId").equalsIgnoreCase("DraftTour")){
//            tourList = ((BaseActivity)getActivity()).mDBCreator.getAllListFromDraft();
//            if(tourList.size()>0)
//            {
//                mImageAdapterGridView.refresh((ArrayList<MyTourItemDO>) this.tourList);
//            }
//
//        }else{
//            String str = ((BaseActivity)getActivity()).preferenceUtils.getStringFromPreference("accessToken","");
////            prepareMovieData(str);
//        }
//
//
//    }

    private class ImageAdapter extends BaseAdapter {

        /** The context. */
        private Activity context;
        ArrayList<MyTourItemDO> mFeeds1;

        /**
         * Instantiates a new image adapter.
         *
         * @param localContext
         *            the local context
         */
        public ImageAdapter(Activity localContext,ArrayList<MyTourItemDO> mFeeds) {
            context = localContext;
            this.mFeeds1 = mFeeds;
        }

        public int getCount() {
            if(this.mFeeds1 == null){
                return 0;
            }
            return this.mFeeds1.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            final ViewHolder holder;

            if (convertView == null) {

                convertView = LayoutInflater.from(context).inflate(R.layout.mytour_grid_itemnew,null,false);
                holder = new ViewHolder();
                holder.img = (ImageView) convertView.findViewById(R.id.imageView1);
                holder.ivEdit = (ImageView) convertView.findViewById(R.id.ivEdit);
                holder.ivLocation = (ImageView) convertView.findViewById(R.id.ivLocation);
                holder.ivDelete = (ImageView) convertView.findViewById(R.id.ivDelete);
                convertView.setTag(holder);

            } else {
                holder = (ViewHolder)convertView.getTag();
            }

            if(LocalData.getInstance().getFromDrafts().equalsIgnoreCase("fav")){
                holder.ivDelete.setBackgroundDrawable(null);
                holder.ivDelete.setImageDrawable(null);
                holder.ivDelete.setBackgroundDrawable(getResources().getDrawable(R.mipmap.ic_map_outline_white));
                holder.ivEdit.setVisibility(View.GONE);
            }
            holder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);
            convertView.setLayoutParams(new GridView.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2, (AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2));

            //Check availablity in Local Db

            Glide.with(context)
                    .load(this.mFeeds1.get(position).tourPhotoPath)
                    .centerCrop()
                    .placeholder(R.color.black)
                    .thumbnail(0.5f)
                    .crossFade()
                    .into(holder.img);

            holder.ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    LocalData.getInstance().setFromedittour(true);
                    MyTourItemDO myTourItemDO = mFeeds1.get(position);
                    ArrayList<TourItem> subtour = myTourItemDO.subtour;


                    LocalData.getInstance().setSelectedMyTourItemDO(myTourItemDO);
                    LocalData.getInstance().setcatTourArry(myTourItemDO.catArry);
                    LocalData.getInstance().setTagtourArry(myTourItemDO.tagArry);
                    System.out.println("subtour123:::"+myTourItemDO.tagArry);

                    ProgressDialog dialog=new ProgressDialog(context);
                    dialog.setMessage("please wait..");
                    dialog.show();
                    LocalData.getInstance().setProgressDialog(dialog);

                    context.startActivity(new Intent(context, AddTourScreenNew.class)
                            .putExtra("Details",myTourItemDO)
                            .putExtra("TourPhotoPath",""+mFeeds1.get(position).tourPhotoPath)
                            .putExtra("From","MyTourListNew"));
                }
            });

            holder.ivLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//
                    if(LocalData.getInstance().getFromDrafts().equalsIgnoreCase("fav")){
                        MyTourItemDO myTourItemDO = mFeeds1.get(position);
                        LocalData.getInstance().setFavselectedItem(myTourItemDO);
                        LocalData.getInstance().setTourSeletedItem(myTourItemDO);
                        context.startActivity(new Intent(context, FavMainTour.class)
                                .putExtra("Details",mFeeds1.get(position)));
                    }else{

                        MyTourItemDO myTourItemDO = mFeeds1.get(position);
                        LocalData.getInstance().setFavselectedItem(myTourItemDO);
                        LocalData.getInstance().setTourSeletedItem(myTourItemDO);
                        context.startActivity(new Intent(context, FavMainTour.class)
                                .putExtra("Details",mFeeds1.get(position)));
                    }
                }
            });


            holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (LocalData.getInstance().getFromDrafts().equalsIgnoreCase("fav")) {
                        context.startActivity(new Intent(context, MapsActivity.class)
                                .putExtra("tourLatitude", mFeeds1.get(position).tourLatitude)
                                .putExtra("tourLangitude", mFeeds1.get(position).tourLangitude)
                                .putExtra("title", mFeeds1.get(position).title));
                    } else {


                        try {

                            new AlertDialogMsg(getActivity(), "Are you want to delete this tour").setPositiveButton("YES", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {


                                    doDelete(mFeeds1.get(position));

                                }

                            }).setNegativeButton("NO", null).create().show();

                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                    }
                }
            });

            return convertView;
        }

        public void refresh(ArrayList<MyTourItemDO> mFeeds) {
            ImageAdapter.this.mFeeds1 = (ArrayList<MyTourItemDO>) mFeeds.clone();
            ImageAdapter.this.notifyDataSetChanged();

        }

        public class ViewHolder{
            ImageView img,ivEdit,ivLocation,ivDelete;
        }

    }

    private void doDelete(MyTourItemDO myTourItemDO) {

        new DeleteTour(getActivity(),myTourItemDO.tourId).execute();


    }

    @Override
    public void onResume() {
        super.onResume();
        /*String str = ((BaseActivity)getActivity()).preferenceUtils.getStringFromPreference("accessToken","");
//        prepareMovieData(str);

        if(getArguments().getString("draftId").equalsIgnoreCase("DraftTour")){

            ArrayList<MyTourItemDO> mFeeds = ((MyTourListNew)getActivity()).mDBCreator.getAllListFromDraft();
            Log.e("mFeeds",""+mFeeds.size());

            if(mFeeds.size()>0)
            {
                this.mFeeds = (ArrayList<MyTourItemDO>) mFeeds.clone();
                mImageAdapter.notifyDataSetChanged();
            }

        }else{
            prepareMovieData(str);
        }
*/
    }

}