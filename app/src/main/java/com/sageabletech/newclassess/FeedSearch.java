package com.sageabletech.newclassess;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.services.securitytoken.model.FederatedUser;
import com.sageabletech.R;
import com.sageabletech.adapter.FeedsAdapter;
import com.sageabletech.model.CategoryDO;
import com.sageabletech.model.FeedsDO;
import com.sageabletech.model.TagsDo;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.Utils;
import com.sageabletech.viewtour.GetCategories;
import com.sageabletech.viewtour.SearchFeeds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import io.fabdialogmorph.DialogActivity;

/**
 * Created by ASMAN on 17-08-2017.
 */

public class FeedSearch extends Activity{
    EditText editText;
    FeedsAdapter mOfferAdapter;
    LinearLayout horizontalCont;
    int num = 0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.searchfeed);
        horizontalCont = (LinearLayout) findViewById(R.id.catList);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.feedList);
        TextView searchError = (TextView) findViewById(R.id.searchError);
        LocalData.getInstance().setSearchError(searchError);


        List<FeedsDO> existingFeeds = LocalData.getInstance().getExistingFeeds();
        if (existingFeeds == null || existingFeeds.size() == 0) {
            existingFeeds = new ArrayList<>();
        }

        mOfferAdapter = new FeedsAdapter(FeedSearch.this, existingFeeds, "search");

        LocalData.getInstance().setFeedsAdapter(mOfferAdapter);
        mOfferAdapter.refresh();
        LocalData.getInstance().setFeedSearchActivity(FeedSearch.this);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(FeedSearch.this);
        recyclerView.setLayoutManager(mLayoutManager);
        LocalData.getInstance().setSearchRecycle(recyclerView);

//        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mOfferAdapter);

        mOfferAdapter.notifyDataSetChanged();
        getHeader();
        editText = (EditText) findViewById(R.id.search_feed);
        ImageView search = (ImageView) findViewById(R.id.search_feed_button);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText().toString().length() != 0) {
                    String s = editText.getText().toString();

                    new SearchFeeds(FeedSearch.this, s, mOfferAdapter, true).execute();
                } else {
                    Toast.makeText(FeedSearch.this, "Add text to serach.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });

        horizontalCont.removeAllViews();

        ArrayList<CategoryDO> categoryDOs = LocalData.getInstance().getmCategoryDOs();
        for (int i = 0; i < categoryDOs.size(); i++) {
            CategoryDO categoryDO = categoryDOs.get(i);

            final View inflate = LayoutInflater.from(FeedSearch.this).inflate(R.layout.textviewcustom, null, false);
            final TextView text = (TextView) inflate.findViewById(R.id.datatext);
            text.setText(categoryDO.categorieName);
            text.setTypeface(Utils.getHelvaticaMedium(FeedSearch.this));
            num = i;
            makeActionEvnt(num, text);
            reloaddata(text, num, categoryDO.categorieId);


            horizontalCont.addView(inflate);
        }

    }
    public void reloaddata(TextView text, int count, String catId) {
        JSONObject searchJson = LocalData.getInstance().getSearchJson();
        System.out.println("reload data::"+searchJson.length());
        if (searchJson.length() > 4) {
            try {


                JSONArray categoryIds = searchJson.getJSONArray("categoryIds");
                System.out.println("categoryIds::"+categoryIds.length());
                for (int i=0;i<categoryIds.length();i++){
                    int anInt = categoryIds.getInt(i);
                    System.out.println("anInt or::"+anInt);
                    System.out.println("anInt sr::"+catId);
                    String s=""+anInt;
                    if (s.equalsIgnoreCase(catId)) {
                        text.setBackgroundColor(getResources().getColor(R.color.com_facebook_button_background_color));
                        text.setTextColor(getResources().getColor(R.color.white));
                        selected[count] = true;
                    }
                }



            } catch (JSONException e) {
                e.printStackTrace();
                System.out.println("erooor:::::"+e.toString());
            }
        }


    }
    boolean selected[] = new boolean[LocalData.getInstance().getmCategoryDOs().size() + 1];
    public void makeActionEvnt(final int count, final TextView text) {
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("selecteddd indexxx:::" + count);
                boolean b = selected[count];
                System.out.println("selected i===" + num);
                if (b == false) {
                    text.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_border_bule));
                    text.setTextColor(getResources().getColor(R.color.white));
                    selected[count] = true;
                    applyFliter();

                } else {
                    text.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_border));
                    text.setTextColor(getResources().getColor(R.color.com_facebook_button_background_color));
                    selected[count] = false;
                    applyFliter();
                }
//
            }
        });

    }
    public void applyFliter() {
         JSONObject searchJson = LocalData.getInstance().getSearchJson();
        int length = searchJson.length();
        System.out.println("json Lenght:::" + length);
        ArrayList<CategoryDO> categoryDOs = LocalData.getInstance().getmCategoryDOs();
        JSONArray catArry = new JSONArray();
        JSONArray tagArry = new JSONArray();
        for (int i = 0; i < selected.length; i++) {
            boolean b = selected[i];

            if (b) {
                CategoryDO categoryDO = categoryDOs.get(i);
                String categorieId = categoryDO.categorieId;
                if (categorieId != null && !categorieId.equalsIgnoreCase("")) {
                    catArry.put(Integer.parseInt(categorieId));
                }
            }
        }
        if (length > 5) {
            try {
                searchJson.put("categoryIds", catArry);
                new SearchFeeds(FeedSearch.this, editText.getText().toString(), mOfferAdapter, true).execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }else {

            JSONObject jsonObject = new JSONObject();

            String catList = "";



            try {
                jsonObject.put("searchStr", "");
                jsonObject.put("tagids", tagArry);

                jsonObject.put("categoryIds", catArry);
                jsonObject.put("author", "");
                jsonObject.put("location", "");

                jsonObject.put("radius", "" + 0);


                jsonObject.put("userLatitude", 0);
                jsonObject.put("userLangitude", 0);

                jsonObject.put("pageLastRecord", 0);

            } catch (Exception e) {

            }

            LocalData.getInstance().setSearchJson(jsonObject);
        }


        }
        private void performSearch() {
            editText.clearFocus();
            InputMethodManager in = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(editText.getWindowToken(), 0);
            if (editText.getText().toString().length() != 0) {
                String s = editText.getText().toString();

                new SearchFeeds(FeedSearch.this, s, mOfferAdapter, true).execute();
            } else {
                Toast.makeText(FeedSearch.this, "Add text to serach.", Toast.LENGTH_SHORT).show();
            }


        }


    TextView header;
    ImageView back;
    ImageView submit;
    public void getHeader(){
        header=(TextView) findViewById(R.id.header_text);
        header.setText("Search");
        header.setTypeface(Utils.getHelvaticaBold(FeedSearch.this));
        back=(ImageView)findViewById(R.id.back);
        back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_back_white));

        submit=(ImageView)findViewById(R.id.submit);

        submit.setVisibility(View.VISIBLE);

        submit.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_filter_outline_white));
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<CategoryDO> categoryDOs = LocalData.getInstance().getmCategoryDOs();
                if(categoryDOs.size()==0) {

                    new GetCategories(FeedSearch.this,"filter").execute();
                }else{
                    Intent intent=new Intent(FeedSearch.this,FilterSearch.class);
                    startActivity(intent);
                }
//                Intent intent = new Intent(FeedSearch.this, DialogActivity.class);
//                intent.putExtra("name","sai kumar name got");
//                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(FeedSearch.this, submit,"help");
//             startActivityForResult(intent, 1, options.toBundle());


            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager in = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(editText.getWindowToken(), 0);

                finish();
            }
        });







    }
}
