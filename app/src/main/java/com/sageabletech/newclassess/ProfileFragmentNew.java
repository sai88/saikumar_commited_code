package com.sageabletech.newclassess;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import com.sageabletech.R;
import com.sageabletech.dialog.AlertDialogMsg;
import com.sageabletech.model.LoginDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.settings.EditProfile;
import com.sageabletech.settings.SettingsActivity;
import com.sageabletech.shomiround.BaseActivity;
import com.sageabletech.shomiround.Login;
import com.sageabletech.shomiround.MyTourList;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;
import com.sageabletech.utils.Utils;
import com.sageabletech.viewtour.GetFavouriteList;
import com.sageabletech.viewtour.GetMyToursList;
import com.sageabletech.webservicecalls.AsyncRemoteCall;
import com.sageabletech.webservicecalls.AsyncRemoteCall.OnDataListener;
import com.sageabletech.webservicecalls.RequestParams;
import com.sageabletech.webservicecalls.ServiceCalls;

import java.util.HashMap;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class ProfileFragmentNew extends Fragment implements View.OnClickListener,OnDataListener{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private CircleImageView ivProfileImage;

    private static final String ARG_SECTION_NUMBER = "section_number";
    TextView tvLogout,tvMyTour,tvDraftTour,editProfile,favorites;

    public ProfileFragmentNew() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ProfileFragmentNew newInstance(int sectionNumber) {
        ProfileFragmentNew fragment = new ProfileFragmentNew();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    private TextView tvSubmit,header,tvSettings,profilename;
    private EditText input_user,input_password;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        progressBar = (ProgressBar) rootView.findViewById(R.id.imageprogressbar);
//        header=(TextView) rootView.findViewById(R.id.header_text);
//        header.setText("Profile");
        profilename=(TextView)rootView.findViewById(R.id.profilename);
        profilename.setTypeface(Utils.getHelvaticaBold(getActivity()));
        tvLogout = (TextView)rootView.findViewById(R.id.tvLogout);
        tvLogout.setTypeface(Utils.getHelvaticaMedium(getActivity()));
        ivProfileImage = (CircleImageView) rootView.findViewById(R.id.id_edit_profile_image);
        tvMyTour = (TextView)rootView.findViewById(R.id.tvMyTour);
        tvMyTour.setTypeface(Utils.getHelvaticaBold(getActivity()));
        editProfile = (TextView)rootView.findViewById(R.id.editprofile);
        editProfile.setTypeface(Utils.getHelvaticaBold(getActivity()));
        tvDraftTour = (TextView)rootView.findViewById(R.id.tvDraftTour);
        tvDraftTour.setTypeface(Utils.getHelvaticaBold(getActivity()));
        favorites = (TextView)rootView.findViewById(R.id.favorites);
        favorites.setTypeface(Utils.getHelvaticaBold(getActivity()));
        tvSettings = (TextView)rootView.findViewById(R.id.id_settings);
        tvSettings.setTypeface(Utils.getHelvaticaBold(getActivity()));

        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog("Are you sure you want to log out?");
            }
        });
        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), EditProfile.class));
            }
        });
        tvMyTour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                HashMap<>
                LocalData.getInstance().setImageListFlag(true);
                LocalData.getInstance().setFromDrafts("no");
                HashMap<String, TourItem> deletedSubTour=new HashMap<String, TourItem>();
                LocalData.getInstance().setNewTour("no");
                 LocalData.getInstance().setDeletedSubTour(deletedSubTour);
                new GetMyToursList(getActivity()).execute();



            }
        });

        favorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalData.getInstance().setImageListFlag(true);
                LocalData.getInstance().setNewTour("no");
                 new GetFavouriteList(getActivity()).execute();
//                startActivity(new Intent(getActivity(), MyTourListFragment.class).putExtra("From","DraftTour"));
            }
        });
        tvDraftTour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalData.getInstance().setNewTour("new");
                LocalData.getInstance().setFromDrafts("yes");
                System.out.println("asdsadds");
                startActivity(new Intent(getActivity(), MyTourList.class).putExtra("From","DraftTour"));
            }
        });
        tvSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
            }
        });

        return rootView;
    }

    @Override
    public void onClick(View v) {



    }

    private ProgressBar progressBar;
    @Override
    public void onResume() {
        super.onResume();
        String profileImage = new PreferenceUtils(getActivity()).getProfileImageURL("SelectedImage",null);
        String firstName = new PreferenceUtils(getActivity()).getStringFromPreference("firstName","");
        String lastName = new PreferenceUtils(getActivity()).getStringFromPreference("lastName","");
        String name="";
        if(!firstName.equalsIgnoreCase("")){
            name=firstName;

        }
        if(!lastName.equalsIgnoreCase("")){
            name=name+" "+lastName;

        }
        profilename.setText(name);
        if(profileImage==null){

            LoginDO loginDO = LocalData.getInstance().getLoginDO();
            String userPicPath = loginDO.userPicPath;
            profileImage=userPicPath;
            new PreferenceUtils(getActivity()).saveProfileImageURL("SelectedImage",userPicPath);
            new PreferenceUtils(getActivity()).saveString("firstName",loginDO.firstName);
            new PreferenceUtils(getActivity()).saveString("lastName",loginDO.lastName);

        }
        System.out.println( "profileImage:123:"+profileImage);

        if(profileImage !=null&& !profileImage.isEmpty()){
            Glide.with(this)
                    .load(profileImage)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
//                            System.out.println("profileImage:123: erorrrrr ecexception::::");
                            ivProfileImage.setBackground(getResources().getDrawable(R.mipmap.ic_launcher_round));
                            System.out.println("profileImage:123: erorrrrr ecexception::::");
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            System.out.println("profileImage:123: erorrrrr valid::::");
                            return false;
                        }
                    })
                    .centerCrop()
                    .into(ivProfileImage);
        }
    }

    @Override
    public void onData(Object data, String error, String tag) {

        if(tag.equalsIgnoreCase("LOGOUT")){
            if(error == null){
                PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
                preferenceUtils.clearData();
                Toast.makeText(getActivity(), ""+data.toString(), Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getActivity(), Login.class));
                getActivity().finish();
            }else{
                Toast.makeText(getActivity(), ""+error, Toast.LENGTH_SHORT).show();
            }
        }

    }


    // To logout application

    public void alertDialog(String msg){

        try {

            new AlertDialogMsg(getActivity(), msg).setNegativeButton("Cancel",new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    //logout application
                    dialog.dismiss();
                }

            })
                    .setPositiveButton("logout", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    //logout application
                    logoutServiceCall();
                }

            }).setCancelable(true).create().show();

        } catch (Exception e) {
            // TODO: handle exception
        }

    }
    private void logoutServiceCall(){
        RequestParams requestParams = new RequestParams();


        String accessToken = ((BaseActivity)getActivity()).preferenceUtils.getStringFromPreference("accessToken",null);
        String url = Urls.LOGOUT+accessToken;
        requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
        requestParams.typeOfRequest = AsyncRemoteCall.GET_REQUEST;
        ServiceCalls serviceCalls = new ServiceCalls(this, "LOGOUT");
        System.out.println("logout caledddd:::");
        serviceCalls.execute(requestParams);
    }
}