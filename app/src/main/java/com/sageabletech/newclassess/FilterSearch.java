package com.sageabletech.newclassess;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.droidmentor.locationhelper.Constants;
import com.droidmentor.locationhelper.MyLocationUsingHelper;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.sageabletech.R;
import com.sageabletech.adapter.FeedsAdapter;
import com.sageabletech.model.CategoryDO;
import com.sageabletech.model.FeedsDO;
import com.sageabletech.model.TagsDo;
import com.sageabletech.shomiround.AddTourScreen;
import com.sageabletech.shomiround.BaseActivity;
import com.sageabletech.shomiround.CategoriesActivity;
import com.sageabletech.shomiround.TagsActivity;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.Utils;
import com.sageabletech.viewtour.SearchFeeds;
import com.sageabletech.webservicecalls.GPSTracker;
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ASMAN on 05-09-2017.
 */

public class FilterSearch extends Activity {
    EditText editText;
    FeedsAdapter mOfferAdapter;
    LinearLayout horizontalCont,horizontalContTag;
    EditText locationEdit, authorEdit;
    int num = 0;
    SeekBar seekBar;
    RangeSeekBar rangeSeekBar;
    int minimumInt=0;
    int maximumInt=100;
    TextView addTags;

    TextView rangetext, clear, applyFliter, clearAll,location;
    boolean selected[] = new boolean[LocalData.getInstance().getmCategoryDOs().size() + 1];

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filtersearch);
        TextView  authortitle = (TextView) findViewById(R.id.authortitle);
        TextView  locationtitle = (TextView) findViewById(R.id.locationtitle);
        authortitle.setTypeface(Utils.getHelvaticaMedium(FilterSearch.this));
        locationtitle.setTypeface(Utils.getHelvaticaMedium(FilterSearch.this));

        horizontalCont = (LinearLayout) findViewById(R.id.catList);
        horizontalContTag = (LinearLayout) findViewById(R.id.TagList);
        locationEdit = (EditText) findViewById(R.id.locationEdit);
        addTags = (TextView) findViewById(R.id.addTags);
        addTags.setTypeface(Utils.getHelvaticaBold(FilterSearch.this));
        TextView tagsText = (TextView) findViewById(R.id.tagsText);
        tagsText.setTypeface(Utils.getHelvaticaBold(FilterSearch.this));

//        locationEdit.setEnabled(false);
        authorEdit = (EditText) findViewById(R.id.authorEdit);

        rangetext = (TextView) findViewById(R.id.rangetext);
        rangetext.setTypeface(Utils.getHelvaticaMedium(FilterSearch.this));

        clear = (TextView) findViewById(R.id.clear);
        location = (TextView) findViewById(R.id.location);
        applyFliter = (TextView) findViewById(R.id.applyFliter);
        applyFliter.setTypeface(Utils.getHelvaticaMedium(FilterSearch.this));
        clearAll = (TextView) findViewById(R.id.clearAll);
        clearAll.setTypeface(Utils.getHelvaticaMedium(FilterSearch.this));
        applyFliter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyFliter();

            }
        });
        clearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationEdit.setText("");
                authorEdit.setText("");
                rangeSeekBar.setSelectedMaxValue(100);
                rangeSeekBar.setSelectedMinValue(0);
                rangetext.setText("Radius: 0 to " + 100 + " miles");
                LocalData.getInstance().setSearchJson(new JSONObject());

            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationEdit.setText("");
                if (!locationEdit.getText().toString().equalsIgnoreCase("")) {
                    clear.setVisibility(View.VISIBLE);
                } else {
                    clear.setVisibility(View.GONE);
                }
            }
        });
        if (!locationEdit.getText().toString().equalsIgnoreCase("")) {
            clear.setVisibility(View.VISIBLE);
        } else {
            clear.setVisibility(View.GONE);
        }
        addTags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<TagsDo> filterTags = LocalData.getInstance().getFilterTags();

                    startActivity(new Intent(FilterSearch.this, TagsActivity.class)

//                    startActivityForResult(new Intent(AddTourScreen.this, FinalScreen.class)
                            .putExtra("addTourDO", "")
                            .putExtra("From", "filter")
                            .putExtra("draftId", "")
                            .putExtra("SelectedData", ""));



            }
        });

        seekBar = (SeekBar) findViewById(R.id.milesSeekbar);
        seekBar.setMax(100);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                rangetext.setText("Radius: 0 to " + progress + " miles");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        rangeSeekBar=(RangeSeekBar) findViewById(R.id.rangeSeekbar);
        RangeSeekBar<Integer> seekBar = new RangeSeekBar<Integer>(FilterSearch.this);
        seekBar.setRangeValues(0, 100);
        rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                //Now you have the minValue and maxValue of your RangeSeekbar
//                Toast.makeText(getApplicationContext(), minValue + "-" + maxValue, Toast.LENGTH_LONG).show();
                rangetext.setText("Radius : "+minValue+" to " + maxValue + " miles");
                minimumInt=minValue;
                maximumInt=maxValue;

            }
        });

// Get noticed while dragging
        rangeSeekBar.setNotifyWhileDragging(true);


        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getCurrentLocation();
                LocalData.getInstance().setLocationHit("yes");

                Intent intent=new Intent(FilterSearch.this, MyLocationUsingHelper.class);
                startActivity(intent);
            }
        });

        horizontalCont.removeAllViews();
        horizontalContTag.removeAllViews();
        getHeader();
        ArrayList<CategoryDO> categoryDOs = LocalData.getInstance().getmCategoryDOs();
        for (int i = 0; i < categoryDOs.size(); i++) {
            CategoryDO categoryDO = categoryDOs.get(i);

            final View inflate = LayoutInflater.from(FilterSearch.this).inflate(R.layout.textviewcustom, null, false);
            final TextView text = (TextView) inflate.findViewById(R.id.datatext);
            text.setText(categoryDO.categorieName);
            text.setTypeface(Utils.getHelvaticaMedium(FilterSearch.this));
            num = i;
            makeActionEvnt(num, text);
            reloaddata(text, num, categoryDO.categorieId);

            horizontalCont.addView(inflate);
        }

        if(LocalData.getInstance().getFilterTags().size()>0){
            ArrayList<TagsDo> filterTags = LocalData.getInstance().getFilterTags();
            for (int i = 0; i < filterTags.size(); i++) {
                TagsDo tagsDo = filterTags.get(i);

                final View inflate = LayoutInflater.from(FilterSearch.this).inflate(R.layout.textviewtags, null, false);
                final TextView text = (TextView) inflate.findViewById(R.id.datatext);
                text.setText(tagsDo.tagName);
                text.setTypeface(Utils.getHelvaticaMedium(FilterSearch.this));
//                num = i;
//                makeActionEvnt(num, text);
//                reloaddata(text, num, tagsDo.tagId);

                horizontalContTag.addView(inflate);
            }
        }




    }

    public void reloaddata(TextView text, int count, String catId) {
        JSONObject searchJson = LocalData.getInstance().getSearchJson();
        System.out.println("reload data::"+searchJson.length());
        if (searchJson.length() > 4) {
            try {
                String maxradius = searchJson.getString("radius");
                if (!maxradius.equalsIgnoreCase("")) {
                    seekBar.setProgress(Integer.parseInt(maxradius));
                    rangetext.setText("Radius: 0 to " + maxradius + " miles");
                }
                String author = searchJson.getString("author");
                authorEdit.setText(author);

                JSONArray categoryIds = searchJson.getJSONArray("categoryIds");
                System.out.println("categoryIds::"+categoryIds.length());
                for (int i=0;i<categoryIds.length();i++){
                    int anInt = categoryIds.getInt(i);
                    System.out.println("anInt or::"+anInt);
                    System.out.println("anInt sr::"+catId);
                    String s=""+anInt;
                    if (s.equalsIgnoreCase(catId)) {
                        text.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_border_bule));
                        text.setTextColor(getResources().getColor(R.color.white));
                        selected[count] = true;
                    }
                }



            } catch (JSONException e) {
                e.printStackTrace();
                System.out.println("erooor:::::"+e.toString());
            }
        }


    }

    public void makeActionEvnt(final int count, final TextView text) {
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("selecteddd indexxx:::" + count);
                boolean b = selected[count];
                System.out.println("selected i===" + num);
                if (b == false) {
                    text.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_border_bule));
                    text.setTextColor(getResources().getColor(R.color.white));
                    selected[count] = true;
                } else {
                    text.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_border));
                    text.setTextColor(getResources().getColor(R.color.com_facebook_button_background_color));
                    selected[count] = false;
                }
//
            }
        });

    }

    GPSTracker gpsTracker;

    public void getCurrentLocation() {
        gpsTracker = new GPSTracker(FilterSearch.this);
        LatLng currentLatLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
        String current_address = gpsTracker.getCompleteAddressString(FilterSearch.this, gpsTracker.getLatitude(), gpsTracker.getLongitude());
        double latitude = gpsTracker.getLatitude();
        double longitude = gpsTracker.getLongitude();

        locationEdit.setText(current_address);
        if (!locationEdit.getText().toString().equalsIgnoreCase("")) {
            clear.setVisibility(View.VISIBLE);
        } else {
            clear.setVisibility(View.GONE);
        }
    }

    TextView header;
    ImageView back;
    ImageView submit;
double latitude;
    double longitude;
    public void getHeader() {
        header = (TextView) findViewById(R.id.header_text);
        header.setText("Filter");
        header.setTypeface(Utils.getHelvaticaBold(FilterSearch.this));
        back = (ImageView) findViewById(R.id.back);
        back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_back_white));
        submit = (ImageView) findViewById(R.id.submit);

        submit.setVisibility(View.GONE);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    public void applyFliter() {
//        searchStr":"test",
//        "categoryIds":[],
//        "tagids":[],
//        "author":"",
//                "pageLastRecord": 0,
//                "location":"",
//                "radius":0,
//                "userLatitude":0,
//                "userLangitude":0
//        }
if(!Validate()) {
    JSONObject jsonObject = new JSONObject();
    ArrayList<CategoryDO> categoryDOs = LocalData.getInstance().getmCategoryDOs();
    String catList = "";
    JSONArray catArry = new JSONArray();
    JSONArray tagArry = new JSONArray();
    for (int i = 0; i < selected.length; i++) {
        boolean b = selected[i];

        if (b) {
            CategoryDO categoryDO = categoryDOs.get(i);
            String categorieId = categoryDO.categorieId;
            if (categorieId != null && !categorieId.equalsIgnoreCase("")) {
                catArry.put(Integer.parseInt(categorieId));
            }
        }
    }
    if(LocalData.getInstance().getFilterTags().size()>0){
        ArrayList<TagsDo> filterTags = LocalData.getInstance().getFilterTags();
        for (int i = 0; i < filterTags.size(); i++) {
            TagsDo tagsDo = filterTags.get(i);

            String tagId = tagsDo.tagId;
            if (tagId != null && !tagId.equalsIgnoreCase("")) {
                tagArry.put(Integer.parseInt(tagId));
            }
        }
    }

    System.out.println("categories List::" + catList);
    try {
        jsonObject.put("searchStr", "");
        jsonObject.put("tagids", tagArry);

        jsonObject.put("categoryIds", catArry);
        jsonObject.put("author", authorEdit.getText().toString());
        jsonObject.put("location", "");

        jsonObject.put("radius", "" + rangeSeekBar.getSelectedMaxValue());
        if (!locationEdit.getText().toString().equalsIgnoreCase("")) {
            jsonObject.put("userLatitude", ""+latitude);
            jsonObject.put("userLongitude", ""+longitude);
        } else {
            jsonObject.put("userLatitude", 0);
            jsonObject.put("userLangitude", 0);
        }
        jsonObject.put("pageLastRecord", 0);

    } catch (Exception e) {

    }
    LocalData.getInstance().setSearchJson(jsonObject);
    System.out.println("jsonObject List::" + jsonObject);
    mOfferAdapter = LocalData.getInstance().getFeedsAdapter();

    new SearchFeeds(FilterSearch.this, "", mOfferAdapter, false).execute();
}else {
    JSONObject jsonObject = new JSONObject();
    ArrayList<CategoryDO> categoryDOs = LocalData.getInstance().getmCategoryDOs();
    String catList = "";
    JSONArray catArry = new JSONArray();
    for (int i = 0; i < selected.length; i++) {
        boolean b = selected[i];

        if (b) {
            CategoryDO categoryDO = categoryDOs.get(i);
            String categorieId = categoryDO.categorieId;
            if (categorieId != null && !categorieId.equalsIgnoreCase("")) {
                catArry.put(Integer.parseInt(categorieId));
            }
        }
    }
    System.out.println("categories List::" + catList);
    try {
        jsonObject.put("searchStr", "");
        jsonObject.put("tagids", new JSONArray());

        jsonObject.put("categoryIds", catArry);
        jsonObject.put("author", authorEdit.getText().toString());
        jsonObject.put("location", "");

        jsonObject.put("radius", "" + rangeSeekBar.getSelectedMaxValue());
        if (!locationEdit.getText().toString().equalsIgnoreCase("")) {
            jsonObject.put("userLatitude", ""+latitude);
            jsonObject.put("userLongitude", ""+longitude);
        } else {
            jsonObject.put("userLatitude", 0);
            jsonObject.put("userLangitude", 0);
        }
        jsonObject.put("pageLastRecord", 0);

    } catch (Exception e) {

    }
    LocalData.getInstance().setSearchJson(jsonObject);
        finish();
}



    }
    public boolean Validate(){
        if(! authorEdit.getText().toString().equalsIgnoreCase("")){
            return  false;
        }
        if( minimumInt!=0|| maximumInt!=100){
            return  false;
        }
        if(!locationEdit.getText().toString().equalsIgnoreCase("")){
            return  false;
        }
        if(!locationEdit.getText().toString().equalsIgnoreCase("")){
            return  false;
        }
        for(int i=0;i<selected.length;i++){
            boolean b = selected[i];
            System.out.println("valeeeee:::"+b);
            if(b==true){
                return  false;
            }
        }
        if(LocalData.getInstance().getFilterTags().size()>0){
            return  false;
        }
        return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(LocalData.getInstance().getLocationHit().equalsIgnoreCase("yes")){

            latitude=  Constants.getInstance().getLatitude();
            longitude= Constants.getInstance().getLongitude();
            LocalData.getInstance().setLocationHit("no");
            if (latitude != 0&&longitude!=0) {

                locationEdit.setText(Constants.getInstance().getLocationText());


            }


        }
        System.out.println("LocalData.getInstance().getFilterTags().size()::"+LocalData.getInstance().getFilterTags().size());
        if(LocalData.getInstance().getFilterTags().size()>0){
            horizontalContTag.removeAllViews();
            ArrayList<TagsDo> filterTags = LocalData.getInstance().getFilterTags();
            for (int i = 0; i < filterTags.size(); i++) {
                TagsDo tagsDo = filterTags.get(i);

                final View inflate = LayoutInflater.from(FilterSearch.this).inflate(R.layout.textviewtags, null, false);
                final TextView text = (TextView) inflate.findViewById(R.id.datatext);
                text.setText(tagsDo.tagName);
                text.setTypeface(Utils.getHelvaticaMedium(FilterSearch.this));
//                num = i;
//                makeActionEvnt(num, text);
//                reloaddata(text, num, tagsDo.tagId);

                horizontalContTag.addView(inflate);
            }
        }else{
            horizontalContTag.removeAllViews();
        }
    }
}
