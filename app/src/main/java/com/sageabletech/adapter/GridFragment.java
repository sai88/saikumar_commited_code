package com.sageabletech.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import com.sageabletech.R;
import com.sageabletech.dialog.AlertDialogMsg;
import com.sageabletech.dynamicgrid.BaseDynamicGridAdapter;
import com.sageabletech.dynamicgrid.DynamicGridView;
import com.sageabletech.model.ImagesDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.newclassess.GridFragmentNew;
import com.sageabletech.shomiround.FinalScreen;
import com.sageabletech.shomiround.ImagesAdding;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class GridFragment extends Fragment{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private ArrayList<TourItem> images = new ArrayList<>();
    public static Context ctx;
    public GridFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static GridFragment newInstance(int sectionNumber,Context ctx) {

        GridFragment fragment = new GridFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
//        LocalData.getInstance().save
        args.putSerializable("Images", ((FinalScreen)ctx).mAddTourDO.tourPhotos);
        fragment.setArguments(args);
        return fragment;
    }
    CheeseDynamicAdapter mImageAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.grid_fragment, container, false);

        /*TourItem ti = new TourItem();
        ti.ImagePath = "0";
        images.add(ti);*/
        images.clear();
        images.addAll((ArrayList<TourItem>) getArguments().getSerializable("Images"));

        final DynamicGridView gallery = (DynamicGridView) rootView.findViewById(R.id.galleryGridView);

        ImageView ivPlus = (ImageView)rootView.findViewById(R.id.plus);
        ivPlus.setVisibility(View.VISIBLE);

        ivPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), ImagesAdding.class)
                        .putExtra("Images",images).putExtra("isform","new"),2017);

            }
        });

        mImageAdapter= new CheeseDynamicAdapter(getActivity(), images, 3);
        gallery.setAdapter(mImageAdapter);

//        mImageAdapter = new ImageAdapter(getActivity());
//        gallery.setAdapter(mImageAdapter);

        /*gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(images.get(position).ImagePath.equalsIgnoreCase("Plus"))

            }
        });*/

        gallery.setOnDragListener(new DynamicGridView.OnDragListener() {
            @Override
            public void onDragStarted(int position) {
                Log.d("sai", "drag started at position " + position);
            }

            @Override
            public void onDragPositionsChanged(int oldPosition, int newPosition) {
                Log.d("sai", String.format("drag item position changed from %d to %d", oldPosition, newPosition));
            }
        });

        gallery.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                gallery.startEditMode(position);
                return true;
            }
        });

        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), parent.getAdapter().getItem(position).toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });

        return rootView;
    }

    public void refreshFragment() {

        Log.e("GridFragment","refreshFragment");

    }

    private class ImageAdapter extends BaseAdapter {

        /** The context. */
        private Activity context;

        /**
         * Instantiates a new image adapter.
         *
         * @param localContext
         *            the local context
         */
        public ImageAdapter(Activity localContext) {
            context = localContext;
        }

        public int getCount() {
            if(images == null){
                return 0;
            }
            return images.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            final ViewHolder holder;

            if (convertView == null) {

                convertView = LayoutInflater.from(context).inflate(R.layout.grid_item,null,false);
                holder = new ViewHolder();
                holder.img = (ImageView) convertView.findViewById(R.id.imageView1);
                holder.tv = (CheckBox) convertView.findViewById(R.id.checkBox1);
                holder.ivClose = (ImageView) convertView.findViewById(R.id.ivClose);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolder)convertView.getTag();
            }

            holder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);
            convertView.setLayoutParams(new GridView.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2, (AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2));

            if(!images.get(position).ImagePath.equalsIgnoreCase("Plus")){

                String url = images.get(position).ImagePath;

                System.out.println("IMAGE URL::"+url);

                Log.e("GridFragment","images.get(position).tourId : "+images.get(position).tourId);

             if(images.get(position).tourId.length()>0 && !images.get(position).tourId.contains("DRAFT")){
                String str = ((FinalScreen)context).mDBCreator.getImage(images.get(position).tourId,images.get(position).ImagePath);
                if(str.length()>0){
                    Log.e("Found",url+" "+str);
                    url = str;
                }
              }




                Picasso.with(getActivity())

                        .load(url)
                        .placeholder(R.color.black)
                        .resize(AppConstants.DEVICE_DISPLAY_WIDTH/2, AppConstants.DEVICE_DISPLAY_WIDTH/2).into(holder.img);



                holder.img.setOnClickListener(null);

                holder.ivClose.setVisibility(View.VISIBLE);

            }else if(images.get(position).ImagePath.equalsIgnoreCase("Plus")){
                holder.img.setImageResource(R.mipmap.ic_image_add_plus_outline_white);
                holder.img.setPadding(5,5,5,5);
                holder.ivClose.setVisibility(View.GONE);
//                holder.type = "Plus";
//                convertView.setTag("Plus");
               /* holder.img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivityForResult(new Intent(getActivity(), ImagesAdding.class).putExtra("Images",images),2017);
                    }
                });*/

            }

            holder.ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    //Show Popup asking delete.
//                    alertDialogFinish("Are you want to delete this photo.",position);
                    /*if(position <= ((FinalScreen)getActivity()).mAddTourDO.tourPhotos.size()-1){
                        if(!((FinalScreen)getActivity()).mAddTourDO.tourPhotos.get(position).ImagePath.equalsIgnoreCase("Plus")){
                            ((FinalScreen)getActivity()).mAddTourDO.tourPhotos.remove(position);
                            images = (ArrayList<TourItem>) ((FinalScreen)getActivity()).mAddTourDO.tourPhotos.clone();
                            mImageAdapter.notifyDataSetChanged();
                        }
                    }*/
                }
            });

            return convertView;
        }

        public class ViewHolder
        {
            CheckBox tv;
            ImageView img,ivClose;
            String type = "";

        }

    }


    public void alertDialogFinish(String msg,final int position,final TourItem data){

        try {

            new AlertDialogMsg(getActivity(), msg).setPositiveButton("YES", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    if(position <= ((FinalScreen)getActivity()).mAddTourDO.tourPhotos.size()-1){
                        if((!data.ImagePath.equalsIgnoreCase("Plus"))){
                            ArrayList<ImagesDO> galleryList = LocalData.getInstance().getGalleryList();
                            for (int i=0;i<galleryList.size();i++){
                                ImagesDO imagesDO = galleryList.get(i);
                                String imageName = imagesDO.imageName;
                                if(imageName.equalsIgnoreCase(data.ImagePath)){
                                    imagesDO.isSelect=false;
                                    galleryList.add(i,imagesDO);
                                    break;
                                }
                            }
                            LocalData.getInstance().setGalleryList(galleryList);

                            ((FinalScreen)getActivity()).mAddTourDO.tourPhotos.remove(data);
                            images = (ArrayList<TourItem>) ((FinalScreen)getActivity()).mAddTourDO.tourPhotos.clone();
                            mImageAdapter.notifyDataSetChanged();
                        }
                    }
                }

            }).setNegativeButton("NO",null).create().show();

        } catch (Exception e) {
            // TODO: handle exception
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        if(data !=null&&LocalData.getInstance().getImageselection().equalsIgnoreCase("yes")){
            LocalData.getInstance().setImageselection("no");
            images = new ArrayList<TourItem>();
            images = (ArrayList<TourItem>) data.getSerializableExtra("Images");
            Log.e("FinalScreen","images : "+images.size());
            System.out.println("images SIZE:: BEFORE"+images.size());

            images=removeDuplicates(images);
            System.out.println("images SIZE:: AFTER"+images.size());

            ((FinalScreen)getActivity()).mAddTourDO.tourPhotos = images;

            mImageAdapter.clear();
            mImageAdapter.add(images);
            mImageAdapter.notifyDataSetChanged();
        }else{
            System.out.println("eleleleleseeeee.....");
        }
    }
    public ArrayList<TourItem>  removeDuplicates(ArrayList<TourItem> list){
        Set set = new TreeSet(new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                if(((TourItem)o1).ImagePath.equalsIgnoreCase(((TourItem)o2).ImagePath)){
                    return 0;
                }
                return 1;
            }
        });
        set.addAll(list);

        final ArrayList newList = new ArrayList(set);
        return newList;
    }

    public class CheeseDynamicAdapter extends BaseDynamicGridAdapter {
        PreferenceUtils preferenceUtils = null;
        ArrayList<TourItem> imagesLocal;

        public CheeseDynamicAdapter(Context context, ArrayList<TourItem> images, int columnCount) {
            super(context, images, columnCount);
            this.imagesLocal = images;
            System.out.println("imagesLocal:::"+images.size());

            preferenceUtils = new PreferenceUtils(getActivity());
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
           CheeseViewHolder holder;
            images=mImageAdapter.getItems();
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.grid_item, null);
                holder = new CheeseViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (CheeseViewHolder) convertView.getTag();
            }
            convertView.setLayoutParams(new GridView.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2, (AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2));

            holder.build((TourItem) getItem(position), position);
            return convertView;
        }

        private class CheeseViewHolder {

            CheckBox tv;
            ImageView img, ivClose;
            String type = "";


            private CheeseViewHolder(View view) {
                img = (ImageView) view.findViewById(R.id.imageView1);
                tv = (CheckBox) view.findViewById(R.id.checkBox1);
                ivClose = (ImageView) view.findViewById(R.id.ivClose);


            }

            void build(final TourItem data, final int position) {
                img.setScaleType(ImageView.ScaleType.CENTER_CROP);

                System.out.println("Imagesss:::::SAI::" + data.ImagePath);

                String url = data.ImagePath;
                String userId = preferenceUtils.getStringFromPreference("userId", "");

                if (url.contains(userId) && !url.contains("https://s3.amazonaws.com/")) {
                    url = "https://s3.amazonaws.com/shomiround-production/" + url;
                }
                System.out.println("url loaded::" + url);

//                Glide.with(getActivity()).load(images.get(position).ImagePath)
//                        .placeholder(R.color.black)
//                        .centerCrop()
//                        .override(AppConstants.DEVICE_DISPLAY_WIDTH/3, AppConstants.DEVICE_DISPLAY_WIDTH/3)
//                        .into(holder.ivImage);

                Glide.with(getActivity()).load(images.get(position).ImagePath)

                        .placeholder(R.color.black)
                        .override(AppConstants.DEVICE_DISPLAY_WIDTH / 2, AppConstants.DEVICE_DISPLAY_WIDTH / 2).into(img);

//                Picasso.with(getActivity())
//                        .load(url)
//                        .placeholder(R.color.black)
//                        .resize(AppConstants.DEVICE_DISPLAY_WIDTH / 2, AppConstants.DEVICE_DISPLAY_WIDTH / 2).into(img);
//            Glide.with(getActivity()).load(url).into(holder.img);


                ivClose.setVisibility(View.VISIBLE);
                System.out.println("holder.ivClose" + ivClose.isShown());
                System.out.println("holder.img" + ivClose.isShown());


                ivClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        System.out.println("clickeddddd::::::::");
                        alertDialogFinish("Are you want to delete this photo.", position,data);

                    }
                });
            }
        }
    }
}