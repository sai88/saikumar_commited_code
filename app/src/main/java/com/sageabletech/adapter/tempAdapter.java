package com.sageabletech.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sageabletech.fragment.TempFragment;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class tempAdapter extends FragmentPagerAdapter {

    public tempAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).

        return TempFragment.newInstance(position + 1);
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "SIGN IN";
            case 1:
                return "SIGN UP";
            case 2:
                return "PROFILE";

        }
        return null;
    }
}
