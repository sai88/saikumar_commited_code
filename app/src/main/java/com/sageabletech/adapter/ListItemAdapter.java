package com.sageabletech.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sageabletech.R;
import com.sageabletech.model.SubTourItemsDO;
import com.sageabletech.shomiround.MapsActivity;

import java.util.ArrayList;

/**
 * Created by Uday on 14-07-2017.
 */

public class ListItemAdapter extends ArrayAdapter<SubTourItemsDO> {
    private Activity activity;
    private ArrayList<SubTourItemsDO> list;
    private SubTourItemsDO vo;

    public ListItemAdapter(Activity activity, ArrayList<SubTourItemsDO> list) {
        super(activity, R.layout.list_item, list);
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View vi;
        vi = convertView;
        final ViewHolder holder;

        if (vi == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            vi = inflater.inflate(R.layout.list_item, null, true);
            holder = new ViewHolder();
            holder.icon = (ImageView) vi.findViewById(R.id.subImageItemList);
            holder.title = (TextView) vi.findViewById(R.id.tv_sub_title);
            holder.caption = (TextView) vi.findViewById(R.id.tv_caption);
            holder.subTourImageLocation = (ImageView) vi.findViewById(R.id.sub_tour_image_location);
            holder.progressBar = (ProgressBar) vi.findViewById(R.id.imageprogressbar);
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }
        vo = list.get(position);
        holder.title.setText(""+vo.photoTitle);
        holder.caption.setText(""+vo.photoCaption);
        holder.subTourImageLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(activity, MapsActivity.class);
                i.putExtra("tourLangitude",vo.photolangitude);
                i.putExtra("tourLatitude",vo.photolatitude);
                i.putExtra("title",vo.photoTitle);
                activity.startActivity(i);
            }
        });
        Glide.with(activity).load(vo.photoPath)
//                .placeholder(R.mipmap.ic_progressing)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .centerCrop()
//                .override((AppConstants.DEVICE_DISPLAY_WIDTH), (AppConstants.DEVICE_DISPLAY_HEIGHT / 3))
                .into(holder.icon);

        return vi;
    }


    static class ViewHolder {
        ImageView icon;
        TextView title;
        TextView caption;
        ImageView subTourImageLocation;
        ProgressBar progressBar;
    }
}
