package com.sageabletech.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.sageabletech.R;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.AddAudioActivity;
import com.sageabletech.shomiround.FinalScreen;
import com.sageabletech.utils.AppConstants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class AddAudioFragment extends Fragment{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private ArrayList<TourItem> images;
    private TextView errorMsg;

    public AddAudioFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static AddAudioFragment newInstance(int sectionNumber,Context ctx) {

        AddAudioFragment fragment = new AddAudioFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putSerializable("Images", ((FinalScreen)ctx).mAddTourDO.tourPhotos);
        fragment.setArguments(args);
        return fragment;
    }
    ImageAdapter mImageAdapter;
    int selectedPos = -1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.grid_fragment, container, false);

        images = (ArrayList<TourItem>) getArguments().getSerializable("Images");
        errorMsg = (TextView)rootView.findViewById(R.id.error_text);



        GridView gallery = (GridView) rootView.findViewById(R.id.galleryGridView);

        mImageAdapter = new ImageAdapter(getActivity());
        gallery.setAdapter(mImageAdapter);

        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedPos = position;
//                SelectedImage
                startActivityForResult(new Intent(getActivity(), AddAudioActivity.class)
                        .putExtra("SelectedImage", images.get(position).ImagePath.toString())
                        .putExtra("From", "AddCaption").putExtra("title", ""+images.get(position).text)
                        .putExtra("position", position).putExtra("Audio",""+images.get(position).audioPath)
                     .putExtra("position", position).putExtra("audio_time",""+images.get(position).audio_duration), 107);


            }
        });

        return rootView;
    }

    public void refreshFragment(String path){
        if(mImageAdapter!=null){
            if(selectedPos != -1)
                images.get(selectedPos).audioPath = path;

            images = ((FinalScreen)getActivity()).mAddTourDO.tourPhotos;
            mImageAdapter.notifyDataSetChanged();
            if(images.size() == 0) {
                errorMsg.setVisibility(View.VISIBLE);
            }else{
                errorMsg.setVisibility(View.GONE);
            }
        }
    }
    public void refreshFragment(){
        images = ((FinalScreen)getActivity()).mAddTourDO.tourPhotos;
        mImageAdapter.notifyDataSetChanged();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("requestCode"+requestCode, ""+resultCode);

        if(requestCode ==107 && resultCode == getActivity().RESULT_OK){
            Log.e("requestCode :"+requestCode, "resultCode :"+resultCode);
            System.out.println("audiodddd::"+data.getStringExtra("AudioSavePathInDevice"));
            if(data !=null){
                Log.e("selectedPos "+selectedPos, "data "+data.getStringExtra("AudioSavePathInDevice"));
                if(selectedPos != -1)
                    ((FinalScreen)getActivity()).mAddTourDO.tourPhotos.get(selectedPos).audioPath = data.getStringExtra("AudioSavePathInDevice");

                refreshFragment(data.getStringExtra("AudioSavePathInDevice"));
            }
        }

    }


    private class ImageAdapter extends BaseAdapter {

        /** The context. */
        private Activity context;

        /**
         * Instantiates a new image adapter.
         *
         * @param localContext
         *            the local context
         */
        public ImageAdapter(Activity localContext) {
            context = localContext;
        }

        public int getCount() {
            if(images == null){
                return 0;
            }
            return images.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            final ViewHolder holder;

            if (convertView == null) {

                convertView = LayoutInflater.from(context).inflate(R.layout.grid_item,null,false);
                holder = new ViewHolder();
                holder.img = (ImageView) convertView.findViewById(R.id.imageView1);
                holder.tv = (CheckBox) convertView.findViewById(R.id.checkBox1);
                holder.ivClose = (ImageView) convertView.findViewById(R.id.ivClose);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolder)convertView.getTag();
            }

            holder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);
            convertView.setLayoutParams(new GridView.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2, (AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2));

            if(!images.get(position).ImagePath.equalsIgnoreCase("Plus")){
//                Glide.with(context)
//                        .load(images.get(position).ImagePath)
//                        .centerCrop()
//                        .placeholder(R.color.black)
//                        .thumbnail(0.5f)
//                        .crossFade()
//                        .into(holder.img);








                Glide.with(getActivity()).load(images.get(position).ImagePath)

                        .placeholder(R.color.black)
                        .override(AppConstants.DEVICE_DISPLAY_WIDTH/2, AppConstants.DEVICE_DISPLAY_WIDTH/2).into(holder.img);

//                Picasso.with(getActivity())
//                        .load(images.get(position).ImagePath)
//                        .placeholder(R.color.black)
//                        .resize(AppConstants.DEVICE_DISPLAY_WIDTH/2, AppConstants.DEVICE_DISPLAY_WIDTH/2).into(holder.img);

                holder.ivClose.setVisibility(View.GONE);

            }

            if(images.get(position).audioPath.length()>0){
                holder.ivClose.setVisibility(View.VISIBLE);
                holder.ivClose.setImageResource(R.mipmap.ic_ic_play_outline_white);
            }else{
                holder.ivClose.setVisibility(View.GONE);
            }



            return convertView;
        }

        public class ViewHolder
        {
            CheckBox tv;
            ImageView img,ivClose;

        }

    }

}