package com.sageabletech.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;



/**
 * Created by WIN 7 on 4/11/2017.
 */
public class FinalPageAdapter extends FragmentStatePagerAdapter {
    Context ctx;
    AddCaptionFragment mAddCaptionFragment = null;
    GridFragment mGridFragment = null;
    String draftId;
    PreviewFragment previewFragment;
    public FinalPageAdapter(FragmentManager fm,Context ctx,String draftId) {
        super(fm);
        this.ctx = ctx;
        this.draftId = draftId;
    }
    AddAudioFragment mAddAudioFragment = null;
    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).

        if(position == 0){
            mGridFragment = GridFragment.newInstance(position + 1,ctx);
            return mGridFragment;
        }else if(position == 1){
            mAddAudioFragment = AddAudioFragment.newInstance(position + 1,ctx);
            return mAddAudioFragment;
        }
        else if(position == 2) {
            mAddCaptionFragment = AddCaptionFragment.newInstance(position + 1, ctx);
            return mAddCaptionFragment;

        }else {

                    previewFragment=PreviewFragment.newInstance(position + 1, ctx);
            return previewFragment;
        }
    }

    public void AddCaptionToImage(String text,String summary){
        if(mAddCaptionFragment !=null){
                mAddCaptionFragment.refreshFragment(text,summary);
        }
    }

    public void AudioFragmentRefresh(){
        if(mAddAudioFragment !=null){
            mAddAudioFragment.refreshFragment();
        }
    }
    public void CaptionRefresh(){
        if(mAddCaptionFragment !=null){
            mAddCaptionFragment.refresh();
        }
    }
    public void refreshMap(){
        if (previewFragment != null) {
            previewFragment.mapRefresh();
        }
    }
    /*public void GridImagesRefresh(){
        if(mGridFragment !=null){
            mGridFragment.refreshFragment();
        }
    }*/


    @Override
    public int getCount() {
        // Show 3 total pages.
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "SIGN IN";
            case 1:
                return "SIGN UP";
            case 2:
                return "PROFILE";
            case 3:
                return "PROFILE";

        }
        return null;
    }
}
