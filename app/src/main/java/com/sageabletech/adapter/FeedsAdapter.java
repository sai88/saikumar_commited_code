package com.sageabletech.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.Engine;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
//import com.bumptech.glide.request.target.Target;
import com.droidmentor.locationhelper.MyLocationUsingHelper;
import com.sageabletech.R;
import com.sageabletech.model.FeedsDO;
import com.sageabletech.newclassess.CommentsPage;
import com.sageabletech.newclassess.MusicService;
import com.sageabletech.newclassess.WordUtils;
import com.sageabletech.settings.EditProfile;
import com.sageabletech.shomiround.FinalScreen;
import com.sageabletech.shomiround.MainScreen;
import com.sageabletech.shomiround.MapsActivity;
import com.sageabletech.shomiround.MyLocation;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Utils;
import com.sageabletech.viewtour.AddAsFavourite;
import com.sageabletech.viewtour.DeleteFavourite;
import com.sageabletech.viewtour.GetComments;
import com.sageabletech.viewtour.GetTourDetails;
import com.sageabletech.viewtour.PublishTour;
import com.sageabletech.viewtour.SaveViewCount;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.fabdialogmorph.DialogActivity;
import io.fabdialogmorph.MainActivity;

/**
 * Created by WIN 7 on 3/25/2017.
 */
public class FeedsAdapter extends RecyclerView.Adapter<FeedsAdapter.MyViewHolder> {

    public List<FeedsDO> mOffers;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, tvItemName, tvItemTime, tv_publish, tvItemDesc, tvItemLocation, tvUser, addFavorite, AddAll, fav_count, addedTagList, addedcatList;
        public ImageView ivImage, userPicPath;
        public ProgressBar progressBar;
        RelativeLayout bottomCont;
        RelativeLayout publishCont;
        Switch aSwitch;
        RelativeLayout feedCont;

        public MyViewHolder(View view) {
            super(view);
            tvItemName = (TextView) view.findViewById(R.id.tvItemName);
            tvItemTime = (TextView) view.findViewById(R.id.feedtime);
            tvItemDesc = (TextView) view.findViewById(R.id.tvItemDesc);
            addedcatList = (TextView) view.findViewById(R.id.addedcatList);
            addedTagList = (TextView) view.findViewById(R.id.addedTagList);
            addFavorite = (TextView) view.findViewById(R.id.addFavorite);
            AddAll = (TextView) view.findViewById(R.id.AddAll);
            fav_count = (TextView) view.findViewById(R.id.fav_count);
            bottomCont = (RelativeLayout) view.findViewById(R.id.bottomCont);


            tvItemLocation = (TextView) view.findViewById(R.id.tvItemLocation);
            publishCont = (RelativeLayout) view.findViewById(R.id.publishCont);
            tv_publish = (TextView) view.findViewById(R.id.tv_publish);
            aSwitch = (Switch) view.findViewById(R.id.switch_btn);
//            tvItemComments = (TextView) view.findViewById(R.id.tvItemComments);
//            tvItemViews = (TextView) view.findViewById(R.id.tvItemViews);
            tvUser = (TextView) view.findViewById(R.id.tvUser);

            progressBar = (ProgressBar) view.findViewById(R.id.imageprogressbar);
            ivImage = (ImageView) view.findViewById(R.id.ivImage);
            userPicPath = (ImageView) view.findViewById(R.id.ivImageUser);
            feedCont = (RelativeLayout) view.findViewById(R.id.feedCont);

        }
    }

    public void refresh(List<FeedsDO> mOffers) {
        this.mOffers = mOffers;
        this.notifyDataSetChanged();

    }

    public void refreshAdd(List<FeedsDO> mOffers, FeedsAdapter mOfferAdapter) {
        System.out.println("mOffers size:::" + mOffers);
        mOfferAdapter.clear();
        mOfferAdapter.addAll(mOffers);


        this.notifyDataSetChanged();

    }

    // Clean all elements of the recycler
    public void clear() {
        mOffers.clear();
        notifyDataSetChanged();
    }


    // Add a list of items -- change to type used
    public void addAll(List<FeedsDO> list) {
        mOffers.addAll(list);
        notifyDataSetChanged();
    }

    String isfrom = "";
    PreferenceUtils preferenceUtils;

    public FeedsAdapter(Context mContext, List<FeedsDO> mOffers, String isfrom) {
        this.mOffers = mOffers;
        this.mContext = mContext;
        this.isfrom = isfrom;
        preferenceUtils = new PreferenceUtils(mContext);


    }

    public void refresh() {
        System.out.println("moffers.size::::" + this.mOffers.size());
        if (this.mOffers.size() == 0) {
            TextView searchError = LocalData.getInstance().getSearchError();
            searchError.setVisibility(View.VISIBLE);

        } else {
            TextView searchError = LocalData.getInstance().getSearchError();
            searchError.setVisibility(View.GONE);
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feed_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tvItemName.setText(WordUtils.capitalize(mOffers.get(position).tourTitle));
        holder.tvItemTime.setText(mOffers.get(position).tourTime);

//        holder.tvItemName.setTypeface(Utils.getHelvaticaBold(mContext));
        holder.tvItemTime.setTypeface(Utils.getHelvaticaBold(mContext));


        holder.setIsRecyclable(false);

        if (mOffers.get(position).tourSummary.length() > 0) {
            holder.tvItemDesc.setText(makeFirstLetterCapital(mOffers.get(position).tourSummary));
            holder.tvItemDesc.setTypeface(Utils.getHelvaticaBold(mContext));
        } else {
            holder.tvItemDesc.setVisibility(View.GONE);
        }
//if(isfrom.equalsIgnoreCase("search")){
//    holder.bottomCont.setVisibility(View.GONE);
//}

//        holder.tvItemViews.setText(mOffers.get(position).viewCount);
//        holder.tvItemComments.setText(mOffers.get(position).commentsCount);
        String userId = preferenceUtils.getStringFromPreference("userId", "");
//        if(userId.equalsIgnoreCase(mOffers.get(position).userId)){
//            holder.publishCont.setVisibility(View.VISIBLE);
//            addSwitchAction(position,holder.aSwitch,holder.tv_publish);
//
//        }else{
        holder.publishCont.setVisibility(View.GONE);
//        }
        String loc = mOffers.get(position).tourLatitude + "," + mOffers.get(position).tourLangitude;
        holder.tvItemLocation.setTag(loc);
        holder.tvUser.setText(mOffers.get(position).userName);
        holder.tvUser.setTypeface(Utils.getHelvaticaBold(mContext));
        ArrayList<String> catList = mOffers.get(position).catList;
        ArrayList<String> tagList = mOffers.get(position).tagList;
        if (mOffers.get(position).catList.size() > 0) {
            String data = "";
            for (int i = 0; i < catList.size(); i++) {
                String s = catList.get(i);
                data = data + s;

            }
            holder.addedcatList.setText(data);
            holder.addedcatList.setTypeface(Utils.getHelvaticaBold(mContext));

        } else {
            holder.addedcatList.setVisibility(View.GONE);
        }
        if (mOffers.get(position).tagList.size() > 0) {
            String data = "";
            for (int i = 0; i < tagList.size(); i++) {
                String s = tagList.get(i);
                data = data + s;

            }
            holder.addedTagList.setText(data);
            holder.addedTagList.setTypeface(Utils.getHelvaticaBold(mContext));

        } else {
            holder.addedTagList.setVisibility(View.GONE);
        }

        holder.ivImage.setLayoutParams(new FrameLayout.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH),
                (int) (AppConstants.DEVICE_DISPLAY_HEIGHT / 1.6)));


        Glide.with(mContext).load(mOffers.get(position).tourPhotoPath)
                .placeholder(R.color.border_grey)
                .fitCenter().listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, com.bumptech.glide.request.target.Target<GlideDrawable> target, boolean isFirstResource) {
                holder.progressBar.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, com.bumptech.glide.request.target.Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                holder.progressBar.setVisibility(View.GONE);
                return false;
            }


                })


                .override((AppConstants.DEVICE_DISPLAY_WIDTH), (int) (AppConstants.DEVICE_DISPLAY_HEIGHT / 1.6))
                .into(holder.ivImage);



//        Picasso.with(mContext).load(mOffers.get(position).tourPhotoPath).into(new Target() {
//            @Override
//            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                holder.progressBar.setVisibility(View.GONE);
//                int width = bitmap.getWidth();
//                int height = bitmap.getHeight();
//
////                holder.ivImage.setLayoutParams(new FrameLayout.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH),
////                        height));
//                holder.ivImage.setImageBitmap(bitmap);
//            }
//
//            @Override
//            public void onBitmapFailed(Drawable errorDrawable) {
//                holder.progressBar.setVisibility(View.GONE);
//
//            }
//
//            @Override
//            public void onPrepareLoad(Drawable placeHolderDrawable) {
//                holder.progressBar.setVisibility(View.GONE);
//
//            }
//        });


        if (!mOffers.get(position).userPicPath.equalsIgnoreCase(""))
            Glide.with(mContext).load(mOffers.get(position).userPicPath)

//                .placeholder(R.color.black)
                    .override(40, 40)
                    .into(holder.userPicPath);
        addImageAction(holder.feedCont, position);
//        adduserPicPathAction(position,holder.userPicPath);


        addMapAction(position, holder.tvItemLocation);
        String favcount = mOffers.get(position).favcount;
        if (favcount.equalsIgnoreCase("0")) {
            holder.fav_count.setVisibility(View.VISIBLE);
        } else {
            holder.fav_count.setText(mOffers.get(position).favcount);
        }


        if (mOffers.get(position).isfavorite.equalsIgnoreCase("true")) {
            deletefavotiteAction(position, holder.addFavorite);

            holder.addFavorite.setBackgroundDrawable(null);
            holder.addFavorite.setBackgroundDrawable(mContext.getResources().getDrawable(R.mipmap.ic_ic_favourite_solid_blue));
        } else {
            addfavouriteAction(position, holder.addFavorite);
            holder.addFavorite.setBackgroundDrawable(null);
            holder.addFavorite.setBackgroundDrawable(mContext.getResources().getDrawable(R.mipmap.ic_ic_favourite_outline_blue));
        }
        System.out.println("isfavorite:::" + mOffers.get(position).isfavorite);
        holder.AddAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOptionAlert(position);
            }
        });


    }

    private void deletefavotiteAction(final int position, final TextView delFavorite) {
        delFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (delFavorite.isPressed()) {
                    totalflag = true;
                    String userId = mOffers.get(position).userId;
                    String tourId = mOffers.get(position).tourId;

                    new DeleteFavourite((Activity) mContext, tourId, userId, position, mOffers, isfrom).execute();
//                delFavorite.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.favouriteselect));
                }
            }
        });
    }

    public void addfavouriteAction(final int postion, final TextView add) {
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (add.isPressed()) {
                    totalflag = true;
                    String userId = mOffers.get(postion).userId;
                    String tourId = mOffers.get(postion).tourId;

                    new AddAsFavourite((Activity) mContext, tourId, userId, postion, mOffers, isfrom).execute();
                }
//                add.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.favouriteselect));

            }
        });
    }

    public void addSwitchAction(final int postion, final Switch button, final TextView text) {
        button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton cb, boolean on) {
                if (button.isPressed()) {
                    totalflag = true;
                    if (on) {
                        //Do something when Switch button is on/checked
                        text.setText("UnPublish");
                        new PublishTour((Activity) mContext, mOffers.get(postion).tourId, 1, mOffers, postion).execute();
                    } else {
                        //Do something when Switch is off/unchecked
                        text.setText("Publish");
                        new PublishTour((Activity) mContext, mOffers.get(postion).tourId, 1, mOffers, postion).execute();
                    }
                }
            }
        });

    }

    boolean totalflag = false;

    //    public void adduserPicPathAction(final int postion, final ImageView button) {
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(button.isPressed()) {
//                    totalflag=true;
//                    String userPicPath = mOffers.get(postion).userPicPath;
//                    Intent intent = new Intent(mContext, DialogActivity.class);
//                    intent.putExtra("name",userPicPath);
//                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, button,"help");
//                    mContext.startActivity(intent, options.toBundle());
//                }
//            }
//        });
//    }
    public void addMapAction(final int postion, final TextView button) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (button.isPressed()) {
                    totalflag = true;
                    String tourId = mOffers.get(postion).tourId;
                    new GetTourDetails((Activity) mContext, tourId, "location").execute();
//                    System.out.println("mOffers.get(postion).tourLangitude::" + mOffers.get(postion).tourLangitude + "" + mOffers.get(postion).tourLatitude);
//                    Intent i = new Intent(mContext, MapsActivity.class);
//                    i.putExtra("tourLangitude", mOffers.get(postion).tourLangitude);
//                    i.putExtra("tourLatitude", mOffers.get(postion).tourLatitude);
//                    i.putExtra("path", mOffers.get(postion).tourPhotoPath);
//                    i.putExtra("title", mOffers.get(postion).tourTitle);
//                    mContext.startActivity(i);
                }
            }
        });
    }

    public void addImageAction(final RelativeLayout imageView, final int postion) {
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageView.isPressed() && !totalflag) {
                    String tourId = mOffers.get(postion).tourId;
                    new SaveViewCount((Activity) mContext, "" + tourId).execute();
//
                    new GetTourDetails((Activity) mContext, tourId, "direct").execute();
//                    com.droidmentor.locationhelper.MyLocationUsingHelper myLocationUsingHelper;
//                    Intent intent=new Intent(mContext, MyLocationUsingHelper.class);
//                    mContext.startActivity(intent);


                } else {
                    totalflag = false;
                }

//                Intent intent=new Intent(mContext, MusicService.class);
//                mContext.startActivity(intent);
            }
        });

    }

    public String makeFirstLetterCapital(String name) {
        StringBuilder sb = new StringBuilder(name);
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        return sb.toString();
    }

    @Override
    public int getItemCount() {
        //mOffers.size();
        return mOffers.size();
    }

    private void showOptionAlert(final int position) {
        AlertDialog.Builder dialogBuilder;
        LayoutInflater inflater;
        if (isfrom.equalsIgnoreCase("search")) {
            System.out.println("DSDASDASDASD");
            dialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(LocalData.getInstance().getFeedSearchActivity(), R.style.DialogSlideAnim));

            inflater = LocalData.getInstance().getFeedSearchActivity().getLayoutInflater();

        } else {
            dialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(LocalData.getInstance().getMainActivity(), R.style.DialogSlideAnim));

            inflater = LocalData.getInstance().getMainActivity().getLayoutInflater();
        }

        final View dialogView = inflater.inflate(R.layout.commentsdialog, null);
        dialogBuilder.setView(dialogView);


        TextView views = (TextView) dialogView.findViewById(R.id.views);
        TextView comments = (TextView) dialogView.findViewById(R.id.comments);
        TextView reports = (TextView) dialogView.findViewById(R.id.reports);
        TextView cancel = (TextView) dialogView.findViewById(R.id.cancel_comments);
        views.setText("Views (" + (mOffers.get(position).viewCount + ")"));


        final AlertDialog b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        b.getWindow().setGravity(Gravity.BOTTOM);
//        b.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        b.show();
        comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                String tourPhotoPath = mOffers.get(position).tourPhotoPath;
                String tourTitle = mOffers.get(position).tourTitle;
                String tourId = mOffers.get(position).tourId;
                HashMap<String, String> hash = new HashMap<String, String>();
                hash.put("title", "Add Comment");
                hash.put("path", tourPhotoPath);
                hash.put("tourname", tourTitle);
                hash.put("tourId", tourId);
                LocalData.getInstance().setCommenthash(hash);

                new GetComments(LocalData.getInstance().getMainActivity(), tourId).execute();

            }
        });
        reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                String tourPhotoPath = mOffers.get(position).tourPhotoPath;
                String tourTitle = mOffers.get(position).tourTitle;
                String tourId = mOffers.get(position).tourId;
                HashMap<String, String> hash = new HashMap<String, String>();
                hash.put("title", "Report");
                hash.put("path", tourPhotoPath);
                hash.put("tourname", tourTitle);
                hash.put("tourId", tourId);
                LocalData.getInstance().setCommenthash(hash);
                LocalData.getInstance().setTourCommentsArry(new ArrayList<HashMap<String, String>>());
                Intent intent = new Intent(mContext, CommentsPage.class);
                mContext.startActivity(intent);
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                b.dismiss();
            }
        });


    }
}