package com.sageabletech.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sageabletech.model.SubTourItemsDO;
import com.sageabletech.model.TourItem;

import java.util.ArrayList;

/**
 * Created by arige on 7/18/2017.
 */

public class SubTourDetailsViewPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<TourItem> list;

    public SubTourDetailsViewPagerAdapter(FragmentManager fm, ArrayList<SubTourItemsDO> list) {
        super(fm);
//        this.list = list;
    }


    @Override
    public Fragment getItem(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
