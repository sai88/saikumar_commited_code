package com.sageabletech.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sageabletech.R;
import com.sageabletech.model.SubTourItemsDO;
import com.sageabletech.shomiround.MapsActivity;

import java.util.ArrayList;

/**
 * Created by Uday on 14-07-2017.
 */

public class GridItemAdapter extends ArrayAdapter<SubTourItemsDO> {
    private Activity activity;
    private ArrayList<SubTourItemsDO> list;
    private SubTourItemsDO vo;


    public GridItemAdapter(Activity activity, ArrayList<SubTourItemsDO> list) {
        super(activity, R.layout.sub_grid_item, list);
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View vi;
        vi = convertView;
        final ViewHolder holder;
        if (vi == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            vi = inflater.inflate(R.layout.sub_grid_item, null, true);
            holder = new ViewHolder();
            holder.icon = (ImageView) vi.findViewById(R.id.gvsubImageItemList);
            holder.location = (ImageView) vi.findViewById(R.id.gdsubTextItemList);
            holder.progressBar = (ProgressBar) vi.findViewById(R.id.imageprogressbar);
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }
         vo = list.get(position);
        holder.location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(activity, MapsActivity.class);
                i.putExtra("tourLangitude",vo.photolangitude);
                i.putExtra("tourLatitude",vo.photolatitude);
                i.putExtra("title",vo.photoTitle);
                activity.startActivity(i);
            }
        });
        Glide.with(activity).load(vo.photoPath)
//                .placeholder(R.color.black)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .centerCrop()
//                .override((AppConstants.DEVICE_DISPLAY_WIDTH), (AppConstants.DEVICE_DISPLAY_HEIGHT / 3))
                .into(holder.icon);

        return vi;
    }


    static class ViewHolder {
        ImageView icon;
        ImageView location;
        ProgressBar progressBar;
    }
}
