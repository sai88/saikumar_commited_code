package com.sageabletech.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.sageabletech.fragment.MyTourGrid;
import com.sageabletech.fragment.MyTourListFragment;
import com.sageabletech.newclassess.MyTourListNew;
import com.sageabletech.utils.LocalData;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class MyTourPagerAdapter extends FragmentStatePagerAdapter {
    Context ctx;
    String draftId;

    private ViewPager mViewPager;

    public MyTourPagerAdapter(FragmentManager fm, Context ctx, String draftId, ViewPager viewPager) {
        super(fm);
        this.ctx = ctx;
        this.draftId = draftId;
        this.mViewPager=viewPager;

    }

    MyTourListNew mMyTourList;
    MyTourListFragment mMyTourList1;
    MyTourGrid mMyTourGrid;
    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).


        if(position == 0){
            String fromDrafts = LocalData.getInstance().getFromDrafts();
            if(fromDrafts.equalsIgnoreCase("yes")){
                mMyTourList1 = MyTourListFragment.newInstance(position, ctx, draftId);
                return mMyTourList1;
            }else {
                mMyTourList = MyTourListNew.newInstance(position, ctx, draftId);
                return mMyTourList;
            }
        }

        mMyTourGrid = MyTourGrid.newInstance(position + 1,ctx,draftId);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
                //Not used
            }

            @Override
            public void onPageSelected(int position) {
                //Force the fragment to reload its data
                System.out.println("position::"+position);
                if(position == 0) {
                    String fromDrafts = LocalData.getInstance().getFromDrafts();


                    if(fromDrafts.equalsIgnoreCase("yes")) {

                        mMyTourList1.refresh();
                    }else {
                        mMyTourList.refresh();

                    }
                }

                mMyTourGrid.refresh();
            }

            @Override
            public void onPageScrollStateChanged(int i) {
                //Not used
            }
        });
        return mMyTourGrid;
    }
    @Override
    public int getItemPosition(Object object) {

        refreshFragments();
        return POSITION_NONE;
    }
    @Override
    public int getCount() {
        // Show 3 total pages.
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Grid";
            case 1:
                return "List";

        }
        return null;
    }

    public void refreshFragments(){
        Log.e("REFRESHED","refreshFragments");
        mMyTourGrid.refresh();
        mMyTourList.refresh();
        mMyTourList1.refresh();

    }

}
