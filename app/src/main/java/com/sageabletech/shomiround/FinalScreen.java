package com.sageabletech.shomiround;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

import com.bumptech.glide.util.Util;
import com.sageabletech.R;
import com.sageabletech.adapter.FinalPageAdapter;
import com.sageabletech.model.AddTourDO;
import com.sageabletech.model.CategoryDO;
import com.sageabletech.model.ImagesDO;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TagsDo;
import com.sageabletech.model.TourItem;
import com.sageabletech.newclassess.FinalScreenNew;
import com.sageabletech.newclassess.WordUtils;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.Urls;
import com.sageabletech.utils.Utils;
import com.sageabletech.webservicecalls.AsyncRemoteCall;
import com.sageabletech.webservicecalls.AsyncRemoteCall.OnDataListener;
import com.sageabletech.webservicecalls.RequestParams;
import com.sageabletech.webservicecalls.ServiceCalls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class FinalScreen extends BaseActivity implements OnDataListener{

    private ViewPager mViewPager;
    private FinalPageAdapter mSectionsPagerAdapter;
    int currentPosition = -1;
    final String TAG = FinalScreen.class.getName();
    public AddTourDO mAddTourDO;
    public ArrayList<TagsDo> mTagsDos;
    public ArrayList<CategoryDO> mCategoryDOs;

    public String S3URL = "https://s3.amazonaws.com/shomiround/";
    ImageView right,back;
    // The TransferUtility is the primary class for managing transfer to S3
//    private TransferUtility transferUtility;
//    HashMap<String,String> uploadedUrls = new HashMap<>();
    ArrayList<String> allImages = new ArrayList<>();
    ArrayList<String> temp = new ArrayList<>();
    ProgressBar progressBar;

    String from = "";
    String draftId = "";
    TextView tvTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.final_page);

            tvTitle=(TextView)findViewById(R.id.header_text);
        tvTitle.setTypeface(Utils.getHelvaticaBold(FinalScreen.this));

        right = (ImageView) findViewById(R.id.submit);
        right.setImageResource(R.mipmap.ic_ic_forward_circle);


        back =(ImageView)findViewById(R.id.back);
        back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_back_white));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onbackPressed();
            }
        });

//        tvTitle = (TextView)toolbar.findViewById(R.id.tvTitle);
//        right= (ImageView)toolbar.findViewById(R.id.right);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);



        mAddTourDO = (AddTourDO) getIntent().getSerializableExtra("SelectedData");
        for (int i=0;i<mAddTourDO.tourPhotos.size();i++){
            TourItem tourItem = mAddTourDO.tourPhotos.get(i);
            String photoId = tourItem.photoId;
            System.out.println("photo id sai::"+photoId);
        }
        mCategoryDOs = (ArrayList<CategoryDO>) getIntent().getSerializableExtra("categories");
        mTagsDos = (ArrayList<TagsDo>) getIntent().getSerializableExtra("tags");

        Log.e(TAG,""+mAddTourDO.tourPhotos.size());
        tvTitle.setText("Picture");

        /*if(mAddTourDO!=null)
        {
            tvTitle.setText(""+mAddTourDO.title);
        }*/

       /*if(mAddTourDO !=null)
        getSupportActionBar().setTitle(mAddTourDO.title);*/

       if(getIntent().getStringExtra("draftId") !=null){
           draftId = getIntent().getStringExtra("draftId");
       }

        mSectionsPagerAdapter = new FinalPageAdapter(getSupportFragmentManager(),this,draftId);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        final ImageView iv1 = new ImageView(this);
        iv1.setImageResource(R.mipmap.ic_cam_solid_white);
        iv1.setPadding(15,15,15,15);

        final ImageView iv2 = new ImageView(this);
        iv2.setImageResource(R.mipmap.ic_mic_outline_white);
        iv2.setPadding(15, 15, 15, 15);

        final ImageView iv3 = new ImageView(this);
        iv3.setImageResource(R.mipmap.ic_note_outline_white);
        iv3.setPadding(15, 15, 15, 15);

        final ImageView iv4 = new ImageView(this);
        iv4.setImageResource(R.mipmap.ic_eye_outline_white);
        iv4.setPadding(15, 15, 15, 15);

        tabLayout.getTabAt(0).setCustomView(iv1);
        tabLayout.getTabAt(1).setCustomView(iv2);
        tabLayout.getTabAt(2).setCustomView(iv3);
        tabLayout.getTabAt(3).setCustomView(iv4);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                mViewPager.setCurrentItem(tab.getPosition());
                if(tab.getPosition() == 1){
                    mSectionsPagerAdapter.AudioFragmentRefresh();
                }
                if(tab.getPosition()==2){
                    mSectionsPagerAdapter.CaptionRefresh();
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
//                tab.getCustomView().setAlpha(0.27f);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            public void onPageSelected(int position) {
                // Check if this is the page you want.
                if (position == 0) {

                    iv1.setImageResource(R.mipmap.ic_cam_solid_white);
                    iv2.setImageResource(R.mipmap.ic_mic_outline_white);
                    iv3.setImageResource(R.mipmap.ic_note_outline_white);
                    iv4.setImageResource(R.mipmap.ic_eye_outline_white);
                    right.setImageResource(R.mipmap.ic_ic_forward_circle);
                    System.out.println("position::"+" "+1);

                    if(!draftId.equalsIgnoreCase(""))
                    mDBCreator.updateDetailsToDraftTable(mAddTourDO,draftId);
                    tvTitle.setText("Picture");


                } else if (position == 1) {

                    iv1.setImageResource(R.mipmap.ic_cam_outline_white);
                    iv2.setImageResource(R.mipmap.ic_mic_solid_white);
                    iv3.setImageResource(R.mipmap.ic_note_outline_white);
                    iv4.setImageResource(R.mipmap.ic_eye_outline_white);
                    right.setImageResource(R.mipmap.ic_ic_forward_circle);
                    tvTitle.setText("Audio");

                    if(!draftId.equalsIgnoreCase(""))
                        mDBCreator.updateDetailsToDraftTable(mAddTourDO,draftId);

                } else if (position == 2) {

                    iv1.setImageResource(R.mipmap.ic_cam_outline_white);
                    iv2.setImageResource(R.mipmap.ic_mic_outline_white);
                    iv3.setImageResource(R.mipmap.ic_note_solid_white);
                    iv4.setImageResource(R.mipmap.ic_eye_outline_white);
                    right.setImageResource(R.mipmap.ic_ic_forward_circle);
                    tvTitle.setText("Note");

                    System.out.println("position::"+" "+2);

                    if(!draftId.equalsIgnoreCase(""))
                        mDBCreator.updateDetailsToDraftTable(mAddTourDO,draftId);

                } else if (position == 3) {

                    iv1.setImageResource(R.mipmap.ic_cam_outline_white);
                    iv2.setImageResource(R.mipmap.ic_mic_outline_white);
                    iv3.setImageResource(R.mipmap.ic_note_outline_white);
                    iv4.setImageResource(R.mipmap.ic_eye_solid_white);
                    right.setImageResource(R.mipmap.ic_ic_save_white);
                    tvTitle.setText("Preview");
                    System.out.println("position::"+" "+3);

                    if(!draftId.equalsIgnoreCase(""))
                        mDBCreator.updateDetailsToDraftTable(mAddTourDO,draftId);
                }
            }
        });


        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mViewPager.getCurrentItem() == 0){
                    mViewPager.setCurrentItem(1);
                    return;
                }else if(mViewPager.getCurrentItem() == 1){

                    mViewPager.setCurrentItem(2);
                    return;
                }else if(mViewPager.getCurrentItem() == 2){
                    mViewPager.setCurrentItem(3);
                    mSectionsPagerAdapter.refreshMap();
                    return;
                }

                try{
                    from = getIntent().getStringExtra("From");
                }catch (Exception e){
                  e.printStackTrace();
                }

                try {


                    showOptionAlert("proceed","Do you want to upload tour?");
                   /* String message = "Are you sure you want to upload";
                    if(from.equalsIgnoreCase("MyTourListFragment")){
                        message = "Are you sure you want to update this tour";
                    }

                    new AlertDialogMsg(FinalScreen.this, message)
                            .setPositiveButton("YES", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {

                            doSubmit();

                        }

                    }).setNegativeButton("NO",null).create().show();*/

                } catch (Exception e) {
                    // TODO: handle exception
                }


            }
        });


    }

    private void doSubmit() {

        AmazonS3 s3 = new AmazonS3Client(new CognitoCachingCredentialsProvider(
                FinalScreen.this,
                AppConstants.COGNITO_POOL_ID,
                Regions.fromName(AppConstants.COGNITO_POOL_REGION)));

            if (mViewPager.getCurrentItem() == 3) {
                String userId = preferenceUtils.getStringFromPreference("userId","");

                TransferUtility transferUtility = new TransferUtility(s3, getApplicationContext());

                Log.e("TourIcon",""+mAddTourDO.tourIcon);
                if(!mAddTourDO.tourIcon.contains("https://s3.amazonaws.com")) {
                    if (mAddTourDO.tourIcon.contains("file://")) {
                        mAddTourDO.tourIcon = mAddTourDO.tourIcon.replace("file://", "");
                    }
//                    WordUtils.compressImage(mAddTourDO.tourIcon);
                    String[] split = mAddTourDO.tourIcon.split("/");
                    String s = split[split.length - 1];
//                    final File tIcon = new File(myTourItemDO.tourPhotoPath);

                     File tIcon = new File( getsmallImage(s) );
                    boolean exists = tIcon.exists();
                    if(!exists){
                        tIcon=searchforfile(s);
                    }
//                    final File tIcon = new File(mAddTourDO.tourIcon);
                    String iconName[] = mAddTourDO.tourIcon.split("/");
//                String audiopath[] = mAddTourDO.audioPath.split("/");

                    TransferObserver obsIcon = transferUtility.upload(
                            AppConstants.BUCKET_NAME,
                            userId + "/" + iconName[iconName.length - 1],
                            tIcon
                    );

//                mDBCreator.insertTourIcon(tIcon.getAbsolutePath(),"no");

//                showLoader();
                    obsIcon.setTransferListener(new TransferListener() {

                        @Override
                        public void onStateChanged(int id, TransferState state) {
//                        Toast.makeText(FinalScreen.this,"Uploaded:"+id,Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                            int percentage = (int) (bytesCurrent / bytesTotal * 100);
                            //Display percentage transfered to user
                            Log.e(TAG, "percentage : " + percentage + " " + id);

                            if (percentage == 100) {
//                            hideLoader();
//                            mDBCreator.updateTourIcon(tIcon.getAbsolutePath());
                            }

                        }

                        @Override
                        public void onError(int id, Exception ex) {
                            // do something
                            progressBar.setVisibility(View.GONE);
                        }

                    });
                    mAddTourDO.tourIcon = userId + "/" + iconName[iconName.length - 1];

                }

                //saving tourImages in Local DB.
                allImages.clear();
                for(int i=0;i<mAddTourDO.tourPhotos.size();i++) {
                    if (!mAddTourDO.tourPhotos.get(i).ImagePath.contains("https://s3.amazonaws.com") && !mAddTourDO.tourPhotos.get(i).ImagePath.equalsIgnoreCase("")) {
                        if (mAddTourDO.tourPhotos.get(i).ImagePath.contains("file://")) {
                            mAddTourDO.tourPhotos.get(i).ImagePath = mAddTourDO.tourPhotos.get(i).ImagePath.replace("file://", "");
                        }

//                        WordUtils.compressImage(mAddTourDO.tourPhotos.get(i).ImagePath);
                        String[] split1 = mAddTourDO.tourPhotos.get(i).ImagePath.split("/");
                        String s1 = split1[split1.length - 1];
                        System.out.println("inner images:::"+s1);

//                        final File tourImage = new File(mAddTourDO.tourPhotos.get(i).ImagePath);
                        File tourImage = new File(getsmallImage(s1));
                        boolean exists = tourImage.exists();
                        if(!exists){
                            tourImage=searchforfile(s1);
                        }

//                        File tourImage = new File(mAddTourDO.tourPhotos.get(i).ImagePath);

//                    mDBCreator.insertImage(tourImage.getAbsolutePath(),"no",mAddTourDO.tourId);

                        String[] tempPhoto = mAddTourDO.tourPhotos.get(i).ImagePath.split("/");
                        allImages.add(mAddTourDO.tourPhotos.get(i).ImagePath);
                        mAddTourDO.tourPhotos.get(i).ImagePath = userId + "/" + tempPhoto[tempPhoto.length - 1];

                        transferUtility.upload(
                                AppConstants.BUCKET_NAME,
                                userId + "/" + tempPhoto[tempPhoto.length - 1],
                                tourImage
                        );

                    }
                }
                for(int i=0;i<mAddTourDO.tourPhotos.size();i++){
                    if(mAddTourDO.tourPhotos.get(i).audioPath!=null&&!mAddTourDO.tourPhotos.get(i).audioPath.contains("https://s3.amazonaws.com")&&!mAddTourDO.tourPhotos.get(i).audioPath.equalsIgnoreCase("")) {

                        if (mAddTourDO.tourPhotos.get(i).audioPath == null || "".equalsIgnoreCase(mAddTourDO.tourPhotos.get(i).audioPath)) {
                            mAddTourDO.tourPhotos.get(i).audioPath = "";
                        } else {
                            File tourImage = new File(mAddTourDO.tourPhotos.get(i).audioPath);

//                    mDBCreator.insertImage(tourImage.getAbsolutePath(),"no",mAddTourDO.tourId);

                            String[] tempPhoto = mAddTourDO.tourPhotos.get(i).audioPath.split("/");
                            allImages.add(mAddTourDO.tourPhotos.get(i).audioPath);
                            mAddTourDO.tourPhotos.get(i).audioPath = userId + "/" + tempPhoto[tempPhoto.length - 1];

                            transferUtility.upload(
                                    AppConstants.BUCKET_NAME,
                                    userId + "/" + tempPhoto[tempPhoto.length - 1],
                                    tourImage
                            );
                        }
                    }

                }
                showToast("Your Tour is uploading in the back ground.");
                try {
                    doUrlSubmit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

//                startActivityForResult(new Intent(FinalScreen.this,LastScreen.class),2000);

            }

    }

    private File searchforfile(String name) {
        File tIcon=null;
        ArrayList<ImagesDO> filePaths = Utils.getFilePaths(FinalScreen.this);
      for(int i=0;i<filePaths.size();i++){
          ImagesDO imagesDO = filePaths.get(i);
          String imageName = imagesDO.imageName;
          String[] split = imageName.split("/");
          String s = split[split.length - 1];
          System.out.println("image nameeee:::"+s);
          if(name.equalsIgnoreCase(s)){
              System.out.println("entereeee:::"+name);
              WordUtils.compressImage(imageName);
                tIcon = new File( getsmallImage(name) );
              return  tIcon;

          }
      }
        return  tIcon;

    }

    public String getsmallImage(String path){
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" +path);
        return uriSting;
    }


    public void makeCheck(){
        String status = mAddTourDO.status;
        System.out.println("entereddddd status:......."+status);
        if(status!=null){

            if(status.equalsIgnoreCase("yes")){
                System.out.println("entereddddd:::.......");
                 myTourItemDO=new MyTourItemDO();
                myTourItemDO.tourId=mAddTourDO.tourId;
                myTourItemDO.title= mAddTourDO.title;
                myTourItemDO.tourPhotoPath=mAddTourDO.tourIcon;
                myTourItemDO.summary=mAddTourDO.tourSummary;
                myTourItemDO.tourLatitude=""+mAddTourDO.lat;
                myTourItemDO.tourLangitude=""+mAddTourDO.lang;
                myTourItemDO.location=mAddTourDO.tourLocation;
                myTourItemDO.area=mAddTourDO.tourLocation;
                myTourItemDO.city=mAddTourDO.tourLocation;

                ArrayList<TourItem> tourPhotos = mAddTourDO.tourPhotos;
                ArrayList<TourItem> subTours=new ArrayList<>();

                for (int i=0;i<tourPhotos.size();i++){
                    TourItem tourItem = tourPhotos.get(i);
                    tourItem.title=tourItem.text;
                    System.out.println("photo id@@@::"+tourItem.photoId);
                    subTours.add(tourItem);
                }
                myTourItemDO.subtour = subTours;
                doSubmitNew();

            }else{
                doSubmit();
            }
        }





//



    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        onbackPressed();
    }

    public void onbackPressed(){
        if(mViewPager.getCurrentItem() == 3){

//                    showOptionAlert("back");
            mViewPager.setCurrentItem(2);




//                    getDraftAlert();
//                    mViewPager.setCurrentItem(2)
        }else if(mViewPager.getCurrentItem() == 2){
            mViewPager.setCurrentItem(1);
        }else if(mViewPager.getCurrentItem() == 1){
            mViewPager.setCurrentItem(0);
        }else{
            finish();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:



                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getDraftAlert(){

        AlertDialog.Builder builder=new AlertDialog.Builder(FinalScreen.this);
        builder.setMessage("Are you sure do you want to discard?");
        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {



              /*  mDBCreator.deleteTags(getIntent().getStringExtra("draftId"));
                mDBCreator.deleteCategories(getIntent().getStringExtra("draftId"));

                mDBCreator.insertTags(getIntent().getStringExtra("draftId"),mTagsDos);
                mDBCreator.insertCategpries(getIntent().getStringExtra("draftId"),mCategoryDOs);
                mDBCreator.updateDetailsToDraftTable(mAddTourDO,draftId);*/

                dialog.dismiss();
               /* onBackPressed();*/
                Intent intent = new Intent(FinalScreen.this,MainScreen.class);
                startActivity(intent);
                finish();

            }
        });

      /*  builder.setNeutralButton("Cancel",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {

                        dialog.dismiss();

//                        mViewPager.setCurrentItem(2);

//                        context.startActivity(new Intent(context, Setup.class));
                        //dialog.cancel();
                    }
                });*/
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

//                onBackPressed();

                dialog.dismiss();

            }
        });

        builder.show();


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 101 && resultCode == 101){
                if(data!=null)
                {
                    String title = data.getStringExtra("TourTitle");
                    mSectionsPagerAdapter.AddCaptionToImage(title, data.getStringExtra("Summary"));
                }
        }else if (requestCode == 2000 && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }

//        Log.e("FinalScreen","ssss212121"+requestCode);
    }

    public void doUrlSubmit() throws JSONException {

//        Log.e(TAG,""+allImages.size()+" "+allImages+" "+temp);
//        progressBar.setVisibility(View.VISIBLE);
        showLoader();
        String userId = preferenceUtils.getStringFromPreference("userId","");

        JSONObject joMain = new JSONObject();

//       if(from.equalsIgnoreCase("MyTourListFragment")){
        String tourId = mAddTourDO.tourId;
        if(tourId.length()>1){
            joMain.put("tourId","");
        }else{
            joMain.put("tourId",""+"");
        }

//        }

        joMain.put("userId=",""+userId);

        joMain.put("address",""+mAddTourDO.tourLocation);
        joMain.put("area",""+mAddTourDO.tourLocation);
        joMain.put("city",""+mAddTourDO.tourLocation);
        joMain.put("summary",""+mAddTourDO.tourSummary);
        joMain.put("title",""+mAddTourDO.title);
        joMain.put("tourLangitude",""+mAddTourDO.lang);
        joMain.put("tourLatitude",""+mAddTourDO.lat);
        joMain.put("tourPhotoExt",""+"jpeg");

        joMain.put("tourPhotoPath",""+mAddTourDO.tourIcon);
        joMain.put("unPublishFlag", "" + 0);

        JSONArray jsonArray =new JSONArray();
        JSONArray categories =new JSONArray();
        JSONArray tags =new JSONArray();
        JSONArray comments =new JSONArray();

        for(int i=0;i<mAddTourDO.tourPhotos.size();i++){

            System.out.println("photolatitude::"+mAddTourDO.tourPhotos.get(i).photLat);
            JSONObject jPhots = new JSONObject();
            jPhots.put("imageExtension","jpg");
            try{
                mAddTourDO.tourPhotos.get(i);
                jPhots.put("photoPath",mAddTourDO.tourPhotos.get(i).ImagePath);
                jPhots.put("photoAudioPath",mAddTourDO.tourPhotos.get(i).audioPath);
                jPhots.put("audioTime",mAddTourDO.tourPhotos.get(i).audio_duration);

                jPhots.put("photolatitude",mAddTourDO.tourPhotos.get(i).latitude);
                jPhots.put("photolangitude",mAddTourDO.tourPhotos.get(i).longitude);
                jPhots.put("tourOrderNumber", "" +(i+1));

            }catch (ArrayIndexOutOfBoundsException e){
            }

            jPhots.put("photoLocation",""+mAddTourDO.tourPhotos.get(i).location);
            if(!mAddTourDO.tourPhotos.get(i).text.equalsIgnoreCase("")) {
                jPhots.put("photoTitle", "" + mAddTourDO.tourPhotos.get(i).text);

            }else {
                jPhots.put("photoTitle", "" );
            }
            if(!mAddTourDO.tourPhotos.get(i).summary.equalsIgnoreCase("")) {
                jPhots.put("photoCaption", mAddTourDO.tourPhotos.get(i).summary);
            }else{
                jPhots.put("photoCaption", "");
            }


            if(mAddTourDO.tourPhotos.get(i).photoId!=null&&!mAddTourDO.tourPhotos.get(i).photoId.equalsIgnoreCase("")){
                jPhots.put("photoId",mAddTourDO.tourPhotos.get(i).photoId);
            }

            jsonArray.put(jPhots);
        }

        if(mCategoryDOs!=null &&mCategoryDOs.size()>0) {
            for (int i = 0; i < mCategoryDOs.size(); i++) {
                JSONObject jPhots = new JSONObject();
                jPhots.put("categorieId", mCategoryDOs.get(i).categorieId);
                jPhots.put("categorieName", mCategoryDOs.get(i).categorieName);
                jPhots.put("categorieImageLocation", mCategoryDOs.get(i).categorieImageLocation);
                categories.put(jPhots);
            }
        }
        if(mTagsDos!=null &&mTagsDos.size()>0) {
            for (int i = 0; i < mTagsDos.size(); i++) {
                JSONObject jPhots = new JSONObject();
                jPhots.put("tagId", mTagsDos.get(i).tagId);
                jPhots.put("tagName", mTagsDos.get(i).tagName);
                tags.put(jPhots);

            }
        }

        joMain.put("photos",jsonArray);
        joMain.put("categoriesList",categories);
        joMain.put("tagsList",tags);
        joMain.put("comments",comments);
            System.out.println("SENDING JSONOBJECT::"+joMain);

        String authid = preferenceUtils.getStringFromPreference("accessToken","");
        RequestParams requestParams = new RequestParams();
        String url = Urls.CREATE+authid;

     /*   if(from.equalsIgnoreCase("MyTourListFragment")){
            url = Urls.UPDATETOUR+authid;
        }*/

       requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
        requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;

        requestParams.data = joMain.toString();

       ServiceCalls serviceCalls = new ServiceCalls(this, "Create");
        serviceCalls.execute(requestParams);


    }


    @Override
    public void onData(Object data, String error, String tag) {

        progressBar.setVisibility(View.GONE);
        hideLoader();

        mDBCreator.deleteDrafts(draftId);
        mDBCreator.deleteDraftsPhotos(draftId);
        mDBCreator.deleteCategories(draftId);
        mDBCreator.deleteTags(draftId);
        if(error == null){
            String tourId = (String) data;
            System.out.println("tourId::Success"+tourId+" draftId:: "+draftId);

            for(int i=0;i<allImages.size();i++){
                mDBCreator.insertImage(allImages.get(i),"no",tourId);
            }
            dialogOriginal.dismiss();
//            mDBCreator.insertImage(tourImage.getAbsolutePath(),"no",mAddTourDO.tourId);
            getSuccessAlert();
//            startActivityForResult(new Intent(FinalScreen.this,LastScreen.class),2000);
        }else{
            dialogOriginal.dismiss();
            getSuccessAlert();
//            startActivityForResult(new Intent(FinalScreen.this,LastScreen.class),2000);
          Toast.makeText(FinalScreen.this,""+error,Toast.LENGTH_LONG).show();
        }
    }
    AlertDialog dialogOriginal;
    private void showOptionAlert(final String option,String message){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);




        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.choose_option_alert, null);
        dialogBuilder.setView(dialogView);

        TextView title =(TextView) dialogView.findViewById(R.id.dialogtitle);
        title.setTypeface(Utils.getHelvaticaBold(FinalScreen.this));
        TextView upload =(TextView)dialogView.findViewById(R.id.upload);
        upload.setTypeface(Utils.getHelvaticaBold(FinalScreen.this));
        TextView savetodraft =(TextView)dialogView.findViewById(R.id.savetodraft);
        savetodraft.setTypeface(Utils.getHelvaticaBold(FinalScreen.this));
        TextView discard =(TextView)dialogView.findViewById(R.id.discard);
        discard.setTypeface(Utils.getHelvaticaBold(FinalScreen.this));
        TextView cancel =(TextView)dialogView.findViewById(R.id.cancel);
        cancel.setTypeface(Utils.getHelvaticaBold(FinalScreen.this));

        TextView msg =(TextView)dialogView.findViewById(R.id.msg);
        msg.setText(message);
        msg.setTypeface(Utils.getHelvaticaBold(FinalScreen.this));
        if(option.equalsIgnoreCase("back")){

             upload.setVisibility(View.GONE);
        }

        dialogOriginal = dialogBuilder.create();
        dialogOriginal.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

//        dialogOriginal.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_2;
        dialogOriginal.show();

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                WordUtils.deleteFolder();
             makeCheck();
            }
        });
        savetodraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mDBCreator.deleteTags(getIntent().getStringExtra("draftId"));
                mDBCreator.deleteCategories(getIntent().getStringExtra("draftId"));

                mDBCreator.insertTags(getIntent().getStringExtra("draftId"),mTagsDos);
                mDBCreator.insertCategpries(getIntent().getStringExtra("draftId"),mCategoryDOs);
                mDBCreator.updateDetailsToDraftTable(mAddTourDO,draftId);

                dialogOriginal.dismiss();
               /* onBackPressed();*/
                Intent intent = new Intent(FinalScreen.this,MainScreen.class);
                startActivity(intent);
                finish();
            }
        });

        discard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogOriginal.dismiss();
                getDraftAlert();
               /* Intent intent = new Intent(FinalScreen.this,MainScreen.class);
                startActivity(intent);*/
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(option.equalsIgnoreCase("back")){

                  mViewPager.setCurrentItem(2);

                }
                dialogOriginal.dismiss();
            }
        });


    }
    String userId;
    public MyTourItemDO myTourItemDO;
    private void doSubmitNew() {

        AmazonS3 s3 = new AmazonS3Client(new CognitoCachingCredentialsProvider(
                FinalScreen.this,
                AppConstants.COGNITO_POOL_ID,
                Regions.fromName(AppConstants.COGNITO_POOL_REGION)));

        System.out.println("mAddTourDO:::"+myTourItemDO.tourPhotoPath);

        System.out.println("mAddTourDO:::"+myTourItemDO.tourPhotoPath);
//        if(LocalData.getInstance().isFromedittour()){
//            myTourItemDO.tourPhotoPath= LocalData.getInstance().getEditpicpath();
//        }

        if (mViewPager.getCurrentItem() == 3) {
            userId = preferenceUtils.getStringFromPreference("userId","");

            TransferUtility transferUtility = new TransferUtility(s3, getApplicationContext());

            Log.e("TourIcon",""+myTourItemDO.tourPhotoPath);
            //Uploading tourIcon
            if(!myTourItemDO.tourPhotoPath.contains("https://s3.amazonaws.com")) {
                if(myTourItemDO.tourPhotoPath.contains("file://")){
                    myTourItemDO.tourPhotoPath = myTourItemDO.tourPhotoPath.replace("file://", "");
                }

                String[] split1 = myTourItemDO.tourPhotoPath.split("/");
                String s1 = split1[split1.length - 1];
                 File tIcon = new File(getsmallImage(s1));
                boolean exists = tIcon.exists();
                if(!exists){
                    tIcon=searchforfile(s1);
                }
                String iconName[] = myTourItemDO.tourPhotoPath.split("/");
                TransferObserver obsIcon = transferUtility.upload(
                        AppConstants.BUCKET_NAME,
                        userId + "/" + iconName[iconName.length - 1],
                        tIcon
                );

//                mDBCreator.insertTourIcon(tIcon.getAbsolutePath(),"no");

//                showLoader();
                obsIcon.setTransferListener(new TransferListener() {

                    @Override
                    public void onStateChanged(int id, TransferState state) {
//                        Toast.makeText(FinalScreenNew.this,"Uploaded:"+id,Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        int percentage = (int) (bytesCurrent / bytesTotal * 100);
                        //Display percentage transfered to user
                        Log.e(TAG, "percentage : " + percentage + " " + id);

                        if (percentage == 100) {
//                            hideLoader();
//                            mDBCreator.updateTourIcon(tIcon.getAbsolutePath());
                        }

                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        // do something
                        progressBar.setVisibility(View.GONE);
                    }

                });
                myTourItemDO.tourPhotoPath = userId + "/" + iconName[iconName.length - 1];

            }

            //saving tourImages in Local DB.
            allImages.clear();
            System.out.println("mAddTourDO.tourPhotos.size()::"+myTourItemDO.subtour.size());
            for(int i=0;i<myTourItemDO.subtour.size();i++){
                System.out.println("mAddTourDO.tourPhotos.get(i).ImagePath::"+myTourItemDO.subtour.get(i).ImagePath);
                System.out.println("mAddTourDO.tourPhotos.get(i).ImagePath::"+myTourItemDO.subtour.get(i).audioPath);
                if(!myTourItemDO.subtour.get(i).ImagePath.contains("https://s3.amazonaws.com")&&!myTourItemDO.subtour.get(i).ImagePath.equalsIgnoreCase("")) {
                    if(myTourItemDO.subtour.get(i).ImagePath.contains("file://")){
                        myTourItemDO.subtour.get(i).ImagePath=myTourItemDO.subtour.get(i).ImagePath.replace("file://", "");
                    }


                    String[] split1 = myTourItemDO.subtour.get(i).ImagePath.split("/");
                    String s1 = split1[split1.length - 1];
                    System.out.println("inner images:::"+s1);

//                        final File tourImage = new File(mAddTourDO.tourPhotos.get(i).ImagePath);
                    File tourImage = new File(getsmallImage(s1));
                    boolean exists = tourImage.exists();
                    if(!exists){
                        tourImage=searchforfile(s1);
                    }

//                    File tourImage = new File(myTourItemDO.subtour.get(i).ImagePath);
//                    mDBCreator.insertImage(tourImage.getAbsolutePath(),"no",mAddTourDO.tourId);

                    String[] tempPhoto = myTourItemDO.subtour.get(i).ImagePath.split("/");
                    allImages.add(myTourItemDO.subtour.get(i).ImagePath);
                    myTourItemDO.subtour.get(i).ImagePath = userId + "/" + tempPhoto[tempPhoto.length - 1];

                    transferUtility.upload(
                            AppConstants.BUCKET_NAME,
                            userId + "/" + tempPhoto[tempPhoto.length - 1],
                            tourImage
                    );

                }
                if(myTourItemDO.subtour.get(i).audioPath!=null&&!myTourItemDO.subtour.get(i).audioPath.contains("https://s3.amazonaws.com")&&!myTourItemDO.subtour.get(i).audioPath.equalsIgnoreCase("")){
                    File tourImage = new File(myTourItemDO.subtour.get(i).audioPath);
                    String[] tempPhoto= myTourItemDO.subtour.get(i).audioPath.split("/");
                    allImages.add(myTourItemDO.subtour.get(i).audioPath);
                    myTourItemDO.subtour.get(i).audioPath = userId + "/" + tempPhoto[tempPhoto.length - 1];


                    transferUtility.upload(
                            AppConstants.BUCKET_NAME,
                            userId + "/" + tempPhoto[tempPhoto.length - 1],
                            tourImage);
                }

            }

            showToast("Your Tour is uploading in the back ground.");
            try {
                doUrlSubmitNew();
            } catch (JSONException e) {
                e.printStackTrace();
            }

//                startActivityForResult(new Intent(FinalScreenNew.this,LastScreen.class),2000);

        }

    }

    public void doUrlSubmitNew() throws JSONException {

        showLoader();

        JSONObject joMain = new JSONObject();


            joMain.put("tourId", "" + myTourItemDO.tourId);

        if (LocalData.getInstance().isFromedittour()) {
            System.out.println("edittttttttttttt");
            String locationText = LocalData.getInstance().getLocationText();
            int length = locationText.length();
            if(length>80){
                joMain.put("address", "" + LocalData.getInstance().getLocationText().substring(0,50));
                joMain.put("area", "" + LocalData.getInstance().getLocationText().substring(0,50));
                joMain.put("city", "" + LocalData.getInstance().getLocationText().substring(0,50));
            }else{
                joMain.put("address", "" + LocalData.getInstance().getLocationText());
                joMain.put("area", "" + LocalData.getInstance().getLocationText());
                joMain.put("city", "" + LocalData.getInstance().getLocationText());
            }

        } else {
            joMain.put("address", "" + myTourItemDO.location);
            joMain.put("area", "" + myTourItemDO.area);
            joMain.put("city", "" + myTourItemDO.city);
        }

        joMain.put("summary", "" + myTourItemDO.summary);
        joMain.put("title", "" + myTourItemDO.title);
        joMain.put("tourLangitude", "" + myTourItemDO.tourLangitude);
        joMain.put("tourLatitude", "" + myTourItemDO.tourLatitude);
        joMain.put("tourPhotoExt", "" + "jpeg");
        joMain.put("tourPhotoPath", "" + myTourItemDO.tourPhotoPath);
        joMain.put("unPublishFlag", "" + 0);

        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < myTourItemDO.subtour.size(); i++) {
            JSONObject jPhots = new JSONObject();
            TourItem tourItem = myTourItemDO.subtour.get(i);
            System.out.println("tourItem:::"+tourItem.audioPath+"::"+tourItem.ImagePath+"::"+tourItem.latitude+"::"+tourItem.longitude);
            jPhots.put("imageExtension", "jpg");
            System.out.println("audio:: path::"+myTourItemDO.subtour.get(i).audioPath);
            try {
                jPhots.put("photoPath", myTourItemDO.subtour.get(i).ImagePath);
//                if(!mAddTourDO.tourPhotos.contains(userId)){
//                    String[] tempPhoto1 = mAddTourDO.tourPhotos.get(i).audioPath.split("/");
//                    mAddTourDO.tourPhotos.get(i).audioPath = userId + "/" + tempPhoto1[tempPhoto1.length - 1];
//                    jPhots.put("photoAudioPath", mAddTourDO.tourPhotos.get(i).audioPath);
//                }else {
                jPhots.put("photoAudioPath",myTourItemDO.subtour.get(i).audioPath);
//                }
            } catch (ArrayIndexOutOfBoundsException e) {
            }
            jPhots.put("photoCaption", myTourItemDO.subtour.get(i).summary);
            jPhots.put("photoLocation", "" + myTourItemDO.subtour.get(i).location);
            jPhots.put("photoTitle", "" + myTourItemDO.subtour.get(i).title);
            jPhots.put("photolatitude", "" +myTourItemDO.subtour.get(i).latitude);
            jPhots.put("photolangitude", "" + myTourItemDO.subtour.get(i).longitude);


                jPhots.put("photoId", myTourItemDO.subtour.get(i).photoId);


            jsonArray.put(jPhots);




//            photos= [photoId=2600, photoTitle=No Text, photoCaption=No Text,
// photoLocation=No Text, photoPath=https://s3.amazonaws.com/shomiround/199/IMG_Mar082017_120843.jpg,
// photoThumbPath=null, photoAudioPath=, audioTime=null, imageExtension=jpg,
// audioExtension=null, photolatitude=null, photolangitude=null, createdOn=0,
// tourOrderNumber=null],
        }
        JSONArray jsonDelArray = new JSONArray();
        if (LocalData.getInstance().isFromedittour()) {

            HashMap<String, TourItem> deletedSubTour = LocalData.getInstance().getDeletedSubTour();

            Set<String> deletedStr = deletedSubTour.keySet();
            for (String key : deletedStr) {
                final TourItem tourItem = deletedSubTour.get(key);
                JSONObject deleObj = new JSONObject();
                deleObj.put("photoId", tourItem.photoId);
                deleObj.put("photoTitle", tourItem.title);
                deleObj.put("photoCaption", tourItem.summary);
                deleObj.put("photoPath", tourItem.ImagePath);
                deleObj.put("photoThumbPath", tourItem.ImagePath);
                deleObj.put("photoAudioPath", tourItem.audioPath);
                deleObj.put("audioTime", "");
                deleObj.put("imageExtension", "jpg");
                deleObj.put("audioExtension", "");
                deleObj.put("photolatitude", "");
                deleObj.put("photolangitude", "");
                deleObj.put("createdOn", "");
                deleObj.put("tourOrderNumber", "");
                jsonDelArray.put(deleObj);
            }



//            deletedTours=[TourPhotosDto [photoId=2603,
// photoTitle=No Text, photoCaption=No Text, photoLocation=No Text,
// photoPath=https://s3.amazonaws.com/shomiround/199/IMG_Feb232017_131838.jpg,
// photoThumbPath=https://s3.amazonaws.com/shomiround/199/IMG_Feb232017_131838_thumb.jpg,
// photoAudioPath=, audioTime=, imageExtension=jpg,
// audioExtension=, photolatitude=0, photolangitude=0,
// createdOn=0, tourOrderNumber=null]]


        }

        JSONArray categories =new JSONArray();
        JSONArray tags =new JSONArray();

        if(mCategoryDOs!=null &&mCategoryDOs.size()>0) {
            for (int i = 0; i < mCategoryDOs.size(); i++) {
                JSONObject jPhots = new JSONObject();
                jPhots.put("categorieId", mCategoryDOs.get(i).categorieId);
                jPhots.put("categorieName", mCategoryDOs.get(i).categorieName);
                jPhots.put("categorieImageLocation", mCategoryDOs.get(i).categorieImageLocation);
                categories.put(jPhots);
            }
        }
        if(mTagsDos!=null &&mTagsDos.size()>0) {
            for (int i = 0; i < mTagsDos.size(); i++) {
                JSONObject jPhots = new JSONObject();
                jPhots.put("tagId", mTagsDos.get(i).tagId);
                jPhots.put("tagName", mTagsDos.get(i).tagName);
                tags.put(jPhots);

            }
        }
        joMain.put("categoriesList",categories);
        joMain.put("tagsList",tags);
        joMain.put("photos",jsonArray);
        joMain.put("deletedTours",jsonDelArray);
        System.out.println("joMain::"+joMain);

        String authid = preferenceUtils.getStringFromPreference("accessToken","");
        RequestParams requestParams = new RequestParams();


        String  url = Urls.UPDATETOUR+authid;

        requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
        requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;

        requestParams.data = joMain.toString();

        ServiceCalls serviceCalls = new ServiceCalls(this, "Update");
        serviceCalls.execute(requestParams);


    }
    private void getSuccessAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(FinalScreen.this);
        builder.setMessage("Tour uploaded Successfully");

        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {



              /*  mDBCreator.deleteTags(getIntent().getStringExtra("draftId"));
                mDBCreator.deleteCategories(getIntent().getStringExtra("draftId"));

                mDBCreator.insertTags(getIntent().getStringExtra("draftId"),mTagsDos);
                mDBCreator.insertCategpries(getIntent().getStringExtra("draftId"),mCategoryDOs);
                mDBCreator.updateDetailsToDraftTable(mAddTourDO,draftId);*/

                dialog.dismiss();
               /* onBackPressed();*/
                Intent intent = new Intent(FinalScreen.this, MainScreen.class);
                startActivity(intent);
                finish();

            }
        });
        AlertDialog  b = builder.create();

        b.show();
    }



}

