package com.sageabletech.shomiround;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.crashlytics.android.answers.Answers;
import com.sageabletech.R;
import com.sageabletech.adapter.MainAdapter;
import com.sageabletech.adapter.tempAdapter;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.newclassess.FinalScreenNew;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.viewtour.ObjectSerializer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class MainScreen extends BaseActivity implements LocationListener{

    private ViewPager mViewPager,mTempViewPager;
    private MainAdapter mSectionsPagerAdapter;
    private tempAdapter mtempAdapter;
    int currentPosition = -1;
    TabLayout tabLayout;

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getLocation();
        setContentView(R.layout.main_screen);
        Fabric.with(this, new Answers());
//        Fabric.with(this, new Crashlytics());


//        getSupportActionBar().hide();

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        getSupportActionBar().hide();

//        getSupportActionBar().setTitle("");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        getSupportActionBar().setDisplayShowHomeEnabled(false);
LocalData.getInstance().setMainActivity(MainScreen.this);
        mSectionsPagerAdapter = new MainAdapter(getSupportFragmentManager());
        mtempAdapter = new tempAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
//       if (LocalData.getInstance().getIsFromProfile().equalsIgnoreCase("yes")) {
//           LocalData.getInstance()
//           mViewPager.setCurrentItem(2);
//       }

        mTempViewPager = (ViewPager) findViewById(R.id.tempcontainer);
        mTempViewPager.setAdapter(mtempAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mTempViewPager);


        final ImageView iv1 = new ImageView(this);
        iv1.setImageResource(R.mipmap.ic_home_outline_white);
        iv1.setPadding(15, 15, 15, 15);

        final ImageView iv2 = new ImageView(this);
        iv2.setImageResource(R.mipmap.ic_plus_outline_white);
        iv2.setPadding(15, 15, 15, 15);

        final ImageView iv3 = new ImageView(this);
        iv3.setImageResource(R.mipmap.ic_profile_outline_white);
        iv3.setPadding(15, 15, 15, 15);

        tabLayout.getTabAt(0).setCustomView(iv1);
        tabLayout.getTabAt(1).setCustomView(iv2);
        tabLayout.getTabAt(2).setCustomView(iv3);

            mViewPager.setCurrentItem(2);
            mTempViewPager.setCurrentItem(2);

//        getSupportActionBar().setTitle("Profile");
        iv1.setImageResource(R.mipmap.ic_home_outline_white);
        iv3.setImageResource(R.mipmap.ic_profile_solid_white);

        currentPosition = 0;



        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
//                super.onTabSelected(tab);
                if (tab.getPosition() == 0) {
                    mViewPager.setCurrentItem(0);
                    iv1.setImageResource(R.mipmap.ic_home_solid_white);
                    iv3.setImageResource(R.mipmap.ic_profile_outline_white);



                } else if (tab.getPosition() == 1) {
//
                    startActivity(new Intent(MainScreen.this, TourIconSelection.class).putExtra("From","AddTour"));
                } else if (tab.getPosition() == 2) {

                    mViewPager.setCurrentItem(1);
                    iv1.setImageResource(R.mipmap.ic_home_outline_white);
                    iv3.setImageResource(R.mipmap.ic_profile_solid_white);


                }
                Log.e("HH", "" + tab.getPosition());
//                tab.getCustomView().setAlpha(.10f);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
//                tab.getCustomView().setAlpha(0.27f);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {}
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            public void onPageSelected(int position) {
                if(position==0){
                    System.out.println("called..........");
//                    checkImagesUploadedCorrectly();
                }
                // Check if this is the page you want.
                if(position == 1){
//                    getSupportActionBar().setTitle("Profile");
                    iv1.setImageResource(R.mipmap.ic_home_outline_white);
                    iv3.setImageResource(R.mipmap.ic_profile_solid_white);
                }else{

//                    getSupportActionBar().setTitle("Shomi Round");
                    iv1.setImageResource(R.mipmap.ic_home_solid_white);
                    iv3.setImageResource(R.mipmap.ic_profile_outline_white);

                }
            }
        });


    }
    public void checkImagesUploadedCorrectly(){
        Thread t=new Thread(){
            @Override
            public void run() {
                ArrayList<String> errorArray = getErrorArray("error");
                System.out.println("errorArray::"+errorArray);
                if(errorArray.size()>0){
                    for (int i=0;i<errorArray.size();i++){
                        String s = errorArray.get(i);
                        reloadPicsToAmazon(s);

                    }
                }

            }
        };
        t.start();


    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalData.getInstance().setCameraTourItem(new ArrayList<TourItem>());
        if(LocalData.getInstance().getIsFromProfile().equalsIgnoreCase("yes")) {
            LocalData.getInstance().setIsFromProfile("no");
            mViewPager.setCurrentItem(2);
            mTempViewPager.setCurrentItem(2);
            tabLayout.getTabAt(2).select();
        }else {
            mViewPager.setCurrentItem(0);
            tabLayout.getTabAt(0).select();
        }
//

    }


    public void reloadPicsToAmazon(String imageName){
        String update = PreferenceUtils.getStringFromPreference("update", "");
        if(!update.equalsIgnoreCase("")){
            try {
                Object deserialize = ObjectSerializer.deserialize(update);
                ArrayList<String> myTourItemDO=(ArrayList<String>)deserialize;
                System.out.println("myTourItemDO:::"+myTourItemDO);
                ArrayList<String> subtour = myTourItemDO;
                for (int i=0;i<subtour.size();i++){
                    String imagePath= subtour.get(i);
                    if(imagePath.contains(imageName)){
                        System.out.println("image path for recall::"+imagePath);
                        System.out.println("image path for recall::"+imageName);
                        recallAmazon(imagePath);
                    }

                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        }
    }
    public void recallAmazon(String path){
        if(!path.contains("https://s3.amazonaws.com")&&!path.equalsIgnoreCase("")) {
            if(path.contains("file://")){
                path=path.replace("file://", "");
            }
            System.out.println("path:::"+path);
            File tourImage = new File(path);
//                    mDBCreator.insertImage(tourImage.getAbsolutePath(),"no",mAddTourDO.tourId);

            String[] tempPhoto = path.split("/");
            String userId=PreferenceUtils.getStringFromPreference("userId","");

            path = userId + "/" + tempPhoto[tempPhoto.length - 1];
            System.out.println("reload path::"+path);

            AmazonS3 s3 = new AmazonS3Client(new CognitoCachingCredentialsProvider(
                    MainScreen.this,
                    AppConstants.COGNITO_POOL_ID,
                    Regions.fromName(AppConstants.COGNITO_POOL_REGION)));
            TransferUtility transferUtility = new TransferUtility(s3, getApplicationContext());
            TransferObserver upload = transferUtility.upload(
                    AppConstants.BUCKET_NAME,
                    userId + "/" + tempPhoto[tempPhoto.length - 1],
                    tourImage
            );
            upload.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                }
                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
//                    int percentage = (int) (bytesCurrent / bytesTotal * 100);
                    //Display percentage transfered to user
//                    System.out.println("percentage : " + percentage + " " + id);
//
//                    if (percentage == 100) {
//
//                    }

                }

                @Override
                public void onError(int id, Exception ex) {
                    System.out.println("sun images error::"+ex.toString());
                    String s = ex.toString();
                    saveerror(s);


                }
            });

        }
    }
    public void saveerror(String s){
        String error = PreferenceUtils.getStringFromPreference("error", "");
        if(!error.equalsIgnoreCase("")){
            try {
                Object deserialize = ObjectSerializer.deserialize(error);

                ArrayList<String> errorArrayTot=(ArrayList<String>)deserialize;
                errorArrayTot.add(s);
                String serialize = ObjectSerializer.serialize(errorArrayTot);
                PreferenceUtils.saveString("error",serialize);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        }
    }
    public ArrayList<String> getErrorArray(String type){
        String error =PreferenceUtils.getStringFromPreference(type, "");
        if(!error.equalsIgnoreCase("")){
            try {
                Object deserialize = ObjectSerializer.deserialize(error);
                ArrayList<String> errorArrayTot=(ArrayList<String>)deserialize;
                return  errorArrayTot;

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }


        }else{
            return  new ArrayList<String>();
        }
        return  new ArrayList<String>();
    }
    private Location getLastKnownLocation() {
        LocationManager mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);
//            Log.d("last known location, provider: %s, location: %s", provider,
//                    l);

            if (l == null) {
                continue;
            }
            if (bestLocation == null
                    || l.getAccuracy() < bestLocation.getAccuracy()) {
//                ALog.d("found best last known location: %s", l);
                bestLocation = l;
            }
        }
        if (bestLocation == null) {
            return null;
        }
        return bestLocation;
    }
    public void getLocation() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//
        android.location.Location location = getLastKnownLocation();
//
//

        if (location == null) {
            Toast.makeText(MainScreen.this, "Location Not found", Toast.LENGTH_LONG).show();
            //				if(AppBean.getAppBeanInstance().isLocationAlert())
            buildAlertMessageNoGps();
            //				getFromSim();
        } else {
            Geocoder geocoder = new Geocoder(MainScreen.this);
            try {
                List<Address> user = null;
                user = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                double latitude = (double) user.get(0).getLatitude();
                double  longitude = (double) user.get(0).getLongitude();
//                System.out.println(" DDD lat: " + latitude + ",  longitude: " + longitude);
//                Toast.makeText(MainScreen.this, "lat:::" + latitude, 2000).show();
//

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainScreen.this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}



