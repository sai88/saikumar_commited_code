package com.sageabletech.shomiround;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sageabletech.R;
import com.sageabletech.model.AddTourDO;
import com.sageabletech.model.ImagesDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.newclassess.WordUtils;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.GeoDegree;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.Utils;
import com.sageabletech.webservicecalls.GPSTracker;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class ImagesAdding extends BaseActivity {

    static int selectedPos = -1;
    ImageAdapter mImageAdapter;
    private static final int TAKE_PICTURE = 1;
    private Uri imageUri;

    private ArrayList<ImagesDO> images;
    AddTourDO mAddTourDO = null;

    ImageView right,back;
    TextView tvTitle;
    private ArrayList<TourItem> tourPhotos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_gallery);



        tvTitle=(TextView)findViewById(R.id.header_text);



        tvTitle=(TextView)findViewById(R.id.header_text);

        right = (ImageView) findViewById(R.id.submit);
        right.setVisibility(View.GONE);

        right.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_forward_circle));
        tvTitle.setText("CAMERA ROLL");

        back =(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        System.out.println("CAME" + "ImagesAdding");




        images = new ArrayList<>();
        System.out.println("ImagesAdding::" + "CAME");


//        checkPermissionAlert(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
//            Toast.makeText(AddTourScreen.this, "Not Called", Toast.LENGTH_SHORT).show();
            String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE};
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions, 111);
            }

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }


        init();


    }

    String isform;
    String presentFileName="";
    File photo = null;

    private void init() {


        tourPhotos = (ArrayList<TourItem>) getIntent().getSerializableExtra("Images");
        if (getIntent().hasExtra("isform")) {
            isform = getIntent().getStringExtra("isform");
            System.out.println("sai isform:::" + isform);
        }


        System.out.println("tourPhotos:::123" + tourPhotos.size());

        Log.e("TAG", "SS" + tourPhotos.size());

        GridView gallery = (GridView) findViewById(R.id.galleryGridView);

        mImageAdapter = new ImageAdapter(this);

        gallery.setAdapter(mImageAdapter);


        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v,
                                    int position, long arg3) {
                if (null != images && !images.isEmpty()) {

                    if (images.get(position).isSelect) {
                        images.get(position).isSelect = false;
                    } else {
                        images.get(position).isSelect = true;
                    }


                    right.setVisibility(View.VISIBLE);
                    mImageAdapter.notifyDataSetChanged();

                }

            }
        });


        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isform.equalsIgnoreCase("new")) {
                    tourPhotos.clear();
                }
                if (isform.equalsIgnoreCase("edit")) {
                    Log.e("sai", "imagePath: before:" + tourPhotos.size());
                    ArrayList<TourItem> newTourPhotos = new ArrayList<TourItem>();
                    for (int i = 0; i < tourPhotos.size(); i++) {
                        TourItem tourItem = tourPhotos.get(i);
                        String imagePath = tourItem.ImagePath;
                        Log.e("sai", "imagePath::" + imagePath);
                        if (imagePath.contains("https://s3.amazonaws.com/")) {
                            newTourPhotos.add(tourItem);
                        }
                    }
                    tourPhotos.clear();
                    tourPhotos = newTourPhotos;
                    Log.e("sai", "imagePath:after:" + tourPhotos.size());
//    System.out.println("tourPhotos size after delete:::"+tourPhotos.size());
                    tourPhotos = tourPhotos;

                }
                ArrayList<TourItem> cameraTourItem = LocalData.getInstance().getCameraTourItem();
               for(int i=0;i<cameraTourItem.size();i++){
                   TourItem tourItem = cameraTourItem.get(i);
                   tourPhotos.add(tourItem);
               }
//

                for (ImagesDO d : images) {
                    if (d.isSelect) {
                        System.out.println("photo lat longggg::::"+d.imageName);
                        String imageName = d.imageName;
                        if(imageName.contains("file://")){
                            imageName=imageName.replace("file://", "");
                        }
                        try {
                            TourItem mTourItem = new TourItem();
                            ExifInterface exif = new ExifInterface(imageName);
                            GeoDegree geoDegree = new GeoDegree(exif);
                            double lat = geoDegree.getLatitudeE6();
                            double lng = geoDegree.getLongitudeE6();
                            if (lat == 0.0 & lng == 0.0) {

                                GPSTracker gpsTracker = new GPSTracker(ImagesAdding.this);
                                mTourItem.photLat = String.valueOf(gpsTracker.getLatitude());
                                mTourItem.photLng = String.valueOf(gpsTracker.getLongitude());
                            } else {
                                mTourItem.photLat = "" + lat;
                                mTourItem.photLng = "" + lng;
                                mTourItem.longitude = "" + lng;
                                mTourItem.latitude = "" + lat;
                            }
                            WordUtils.compressImage(d.imageName);
                            mTourItem.ImagePath = d.imageName;
                            System.out.println("LAT LNG::" + mTourItem.photLat + "::" + mTourItem.photLng);
                            System.out.println("selectedImagesSIZEE" + tourPhotos.size());


//

                            tourPhotos.add(mTourItem);

//
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                }


                LocalData.getInstance().setImageselection("yes");
                setResult(RESULT_OK, new Intent().putExtra("Images", tourPhotos));
                finish();

            }
        });


        ImageView cam = (ImageView) findViewById(R.id.cam);
        cam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                File photo = new File(Environment.getExternalStorageDirectory(), "Pic.jpg");
//                intent.putExtra(MediaStore.EXTRA_OUTPUT,
//                        Uri.fromFile(photo));
//                imageUri = Uri.fromFile(photo);
//                startActivityForResult(intent, TAKE_PICTURE);

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                presentFileName= Calendar.getInstance().getTimeInMillis()+".jpg";
                photo = new File(Environment.getExternalStorageDirectory(), presentFileName);

//                imageUri = Uri.fromFile(photo);


                Uri uriForFile = FileProvider.getUriForFile(getApplicationContext(), "com.sageabletech.shomiround.fileprovider", photo);
                List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    grantUriPermission(packageName, uriForFile, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                imageUri=uriForFile;
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(intent, TAKE_PICTURE);


            }
        });

    }

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    /*private boolean checkPermissionAlert(final Context context) {

        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,Manifest.permission.READ_EXTERNAL_STORAGE)) {
//                    showDialog("External storage", context,Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat.requestPermissions((Activity) context,
                                    new String[] { Manifest.permission.READ_EXTERNAL_STORAGE },
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }

    }*/


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
//            Toast.makeText(AddTourScreen.this, "Not Called 1", Toast.LENGTH_SHORT).show();
        }

        init();
    }
    public String getsmallImage(String path){
        File file = new File(Environment.getExternalStorageDirectory().getPath());
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" +path);
        return uriSting;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2002 && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            System.out.println("AFDASfdsfdsf");
            finish();
        } else if (requestCode == TAKE_PICTURE && resultCode == Activity.RESULT_OK) {
            if (!presentFileName.equalsIgnoreCase("")) {
                System.out.println("adsfsdfsdsfdsf");
                File tempFile = new File(getsmallImage(presentFileName));
                Uri selectedImage = Uri.fromFile(tempFile);
//            Uri selectedImage = FileProvider.getUriForFile(getApplicationContext(), "com.sageabletech.shomiround.fileprovider", tempFile);
//            crop(photoURI);
//            Uri selectedImage = imageUri;

//            Uri selectedImage = data.getData();
                System.out.println("camera pic::123:" + selectedImage.toString());
                WordUtils.compressImage(selectedImage.toString());
//            Uri selectedImage = imageUri;
//            System.out.println("camera imageeee::"+selectedImage.getPath());
//            WordUtils.compressImage(selectedImage.getPath());

                doActionForCamera(selectedImage.toString());
            }
        } else if (requestCode == 2002 && requestCode == 143) {

            tourPhotos = (ArrayList<TourItem>) data.getSerializableExtra("images");
            mImageAdapter.notifyDataSetChanged();

        }

    }
    public void doActionForCamera(String imageName){

//        if (isform.equalsIgnoreCase("new")) {
//            tourPhotos.clear();
//        }
//        if (isform.equalsIgnoreCase("edit")) {
//            Log.e("sai", "imagePath: before:" + tourPhotos.size());
//            ArrayList<TourItem> newTourPhotos = new ArrayList<TourItem>();
//            for (int i = 0; i < tourPhotos.size(); i++) {
//                TourItem tourItem = tourPhotos.get(i);
//                String imagePath = tourItem.ImagePath;
//                Log.e("sai", "imagePath::" + imagePath);
//                if (imagePath.contains("https://s3.amazonaws.com/")) {
//                    newTourPhotos.add(tourItem);
//                }
//            }
//            tourPhotos.clear();
//            tourPhotos = newTourPhotos;
//            Log.e("sai", "imagePath:after:" + tourPhotos.size());
////    System.out.println("tourPhotos size after delete:::"+tourPhotos.size());
//            tourPhotos = tourPhotos;
//
//        }
//


                if(imageName.contains("file://")){
                    imageName=imageName.replace("file://", "");
                }
                try {
                    TourItem mTourItem = new TourItem();
                    ExifInterface exif = new ExifInterface(imageName);
                    GeoDegree geoDegree = new GeoDegree(exif);
                    double lat = geoDegree.getLatitudeE6();
                    double lng = geoDegree.getLongitudeE6();
                    if (lat == 0.0 & lng == 0.0) {

                        GPSTracker gpsTracker = new GPSTracker(ImagesAdding.this);
                        mTourItem.photLat = String.valueOf(gpsTracker.getLatitude());
                        mTourItem.photLng = String.valueOf(gpsTracker.getLongitude());
                    } else {
                        mTourItem.photLat = "" + lat;
                        mTourItem.photLng = "" + lng;
                        mTourItem.longitude = "" + lng;
                        mTourItem.latitude = "" + lat;
                    }
                    WordUtils.compressImage(imageName);
                    mTourItem.ImagePath = imageName;
                    System.out.println("LAT LNG::" + mTourItem.photLat + "::" + mTourItem.photLng);
                    System.out.println("selectedImagesSIZEE" + tourPhotos.size());

                    ArrayList<TourItem> cameraTourItem = LocalData.getInstance().getCameraTourItem();
                    cameraTourItem.add(mTourItem);
//

                    tourPhotos.add(mTourItem);

//
                } catch (Exception e) {
                    e.printStackTrace();
                }



        LocalData.getInstance().setImageselection("yes");
        setResult(RESULT_OK, new Intent().putExtra("Images", tourPhotos));
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        selectedPos = -1;

    }


    private class ImageAdapter extends BaseAdapter {

        /**
         * The context.
         */
        private Activity context;

        /**
         * Instantiates a new image adapter.
         *
         * @param localContext the local context
         */
        public ImageAdapter(Activity localContext) {
            context = localContext;

            images = Utils.getFilePaths(localContext);

        }

        public int getCount() {
            return images.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            final ViewHolder holder;

            if (convertView == null) {

                convertView = LayoutInflater.from(context).inflate(R.layout.grid_item, null, false);
                holder = new ViewHolder();
                holder.img = (ImageView) convertView.findViewById(R.id.imageView1);
                holder.tv = (CheckBox) convertView.findViewById(R.id.checkBox1);
                holder.imageselect = (ImageView) convertView.findViewById(R.id.imageselect);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);
            convertView.setLayoutParams(new GridView.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2, (AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2));


                /*Picasso.with(context)
                .load(images.get(position).imagePath)
            	.centerCrop().resize((width/3)-2, (width/3)-2)
            	.placeholder(R.color.black)
            	.into(holder.img);*/

            Glide.with(context)
                    .load(images.get(position).imageName)
                    .centerCrop()
                    .placeholder(R.color.black)
//                	.diskCacheStrategy(DiskCacheStrategy.ALL)
//                	.override((width/3)-2, (width/3)-2)
                    .thumbnail(0.5f)
                    .crossFade()
                    .into(holder.img);

//                if(tourPhotos.size()>0){
//
//                    for(int i = 0; i< tourPhotos.size(); i++){
//                        if(tourPhotos.get(i).ImagePath.contains(images.get(position).imageName)){
//                            images.get(position).isSelect = true;
//                            holder.tv.setChecked(true);
//                            break;
//                        }
//                    }
//
//
//                }

            holder.tv.setChecked(images.get(position).isSelect);
            if(images.get(position).isSelect){
                holder.imageselect.setVisibility(View.VISIBLE);
            }else{
                holder.imageselect.setVisibility(View.GONE);
            }

//            if (holder.tv.isChecked()) {
//                holder.tv.setVisibility(View.VISIBLE);
//            } else {
//                holder.tv.setVisibility(View.GONE);
//            }

                /*holder.img.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(holder.tv.isChecked()){
							holder.tv.setChecked(false);
						}else{
							holder.tv.setChecked(true);
						}

						images.get(position).isSelect = holder.tv.isChecked();

					}
				});*/


            return convertView;
        }

        public class ViewHolder {
            CheckBox tv;
            ImageView img;
            ImageView imageselect;
        }

    }

    @SuppressLint("NewApi")
    private ArrayList<ImagesDO> allImages() {

        ArrayList<ImagesDO> imag = new ArrayList<ImagesDO>();

        String[] projection = {
                MediaStore.Files.FileColumns._ID,
                MediaStore.Files.FileColumns.DATE_ADDED,
                MediaStore.Files.FileColumns.MEDIA_TYPE,
                MediaStore.Files.FileColumns.MIME_TYPE
        };


        // Return only video and image metadata.
        String selection = MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

        Uri queryUri = MediaStore.Files.getContentUri("external");

        CursorLoader cursorLoader = new CursorLoader(
                this,
                queryUri,
                projection,
                selection,
                null, // Selection args (none).
                MediaStore.Files.FileColumns.DATE_ADDED + " DESC" // Sort order.
        );

//    	    images.clear();

        Cursor imagecursor = cursorLoader.loadInBackground();
    	    /*imagecursor = activity.getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
            		projection, null, null, "image_id DESC");*/


        if (imagecursor != null) {
            int image_column_index = imagecursor.getColumnIndex(MediaStore.Files.FileColumns._ID);
            int type_column_index = imagecursor.getColumnIndex(MediaStore.Files.FileColumns.MIME_TYPE);
            int count = imagecursor.getCount();
            for (int i = 0; i < count; i++) {
                imagecursor.moveToPosition(i);
                int id = imagecursor.getInt(image_column_index);
                String mime_type = imagecursor.getString(type_column_index);
                ImagesDO imageItem = new ImagesDO();
                imageItem.id = id;
                //lastId = id;

                if (!mime_type.contains("video")) {
                    imageItem.imageName = MediaStore.Images.Media.EXTERNAL_CONTENT_URI.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, String.valueOf(id)).toString();
                    imag.add(imageItem);
                }


            }

            //add ImageItem at top of list

            imagecursor.close();
        }
        return imag;


    }


}



