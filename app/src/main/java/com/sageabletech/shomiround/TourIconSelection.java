package com.sageabletech.shomiround;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sageabletech.R;
import com.sageabletech.model.AddTourDO;
import com.sageabletech.model.ImagesDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.newclassess.AddTourScreenNew;
import com.sageabletech.newclassess.WordUtils;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Utils;
import com.sageabletech.webservicecalls.GPSTracker;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class TourIconSelection extends BaseActivity {

    static int selectedPos = -1;
    ImageAdapter mImageAdapter;
    private static final int TAKE_PICTURE = 1;
    private Uri imageUri;

    private ArrayList<ImagesDO> images;
    String from;
    AddTourDO mAddTourDO = null;

    ImageView right,back;
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_gallery);
        selectedPos=-1;

        LocalData.getInstance().setImageListFlag(true);
        tvTitle=(TextView)findViewById(R.id.header_text);


        tvTitle.setText("CAMERA ROLL");
        tvTitle.setTypeface(Utils.getHelvaticaBold(TourIconSelection.this));
        tvTitle=(TextView)findViewById(R.id.header_text);

        right = (ImageView) findViewById(R.id.submit);
        right.setVisibility(View.GONE);

        right.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_forward_circle));



        back =(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

//        checkPermissionAlert(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
//            Toast.makeText(AddTourScreen.this, "Not Called", Toast.LENGTH_SHORT).show();
            String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE};
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions,111);
            }

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }


        init();






    }


    private String selectedTourPhotoFromDraft = "";
    private String draftId = "";
    String presentFileName="";
    private void init() {

        from = getIntent().getStringExtra("From");


        if(from.equalsIgnoreCase("AddTour")){
            mAddTourDO = (AddTourDO) getIntent().getSerializableExtra("SelectedData");
        }

        if(from.equalsIgnoreCase("draft")){
            selectedTourPhotoFromDraft = getIntent().getStringExtra("SelectedTourPhotoFromDraft");
            draftId = getIntent().getStringExtra("draftId");
        }

        GridView gallery = (GridView)findViewById(R.id.galleryGridView);

        mImageAdapter = new ImageAdapter(this);

        gallery.setAdapter(mImageAdapter);



        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v,
                                    int position, long arg3) {
                if (null != images && !images.isEmpty() && from.equalsIgnoreCase("AddTour")||from.equalsIgnoreCase("EditProfile")||from.equalsIgnoreCase("ReplaceImage")){
                    System.out.println("addddddddd");

                    selectedPos = position;
                    right.setVisibility(View.VISIBLE);

                    if(images.get(position).isSelect){
                        images.get(position).isSelect = false;
                        mImageAdapter.notifyDataSetChanged();
                    }else{
                        images.get(position).isSelect = true;

                        mImageAdapter.notifyDataSetChanged();
                    }
                    for(int i=0;i<images.size();i++){
                        ImagesDO imagesDO = images.get(i);
                        if(position!=i)
                        imagesDO.isSelect=false;
                    }

                    mImageAdapter.notifyDataSetChanged();

                }else if(from.equalsIgnoreCase("AddTour")){
                    System.out.println("sdfsdfdsfdsfdsfdsdsfff");
                    images.get(position).isSelect = true;
                    right.setVisibility(View.VISIBLE);
                    if(images.get(position).isSelect){
                        images.get(position).isSelect = false;
                        mImageAdapter.notifyDataSetChanged();
                    }else{
                        images.get(position).isSelect = true;

                        mImageAdapter.notifyDataSetChanged();
                    }
                    for(int i=0;i<images.size();i++){
                        ImagesDO imagesDO = images.get(i);
                        if(position!=i)
                            imagesDO.isSelect=false;
                    }
                    mImageAdapter.notifyDataSetChanged();
                }

            }
        });

        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean fromedittour = LocalData.getInstance().isFromedittour();
                WordUtils.compressImage(images.get(selectedPos).imageName.toString());
                if(selectedPos >= 0 && null != images && !images.isEmpty() && from.equalsIgnoreCase("AddTour")){
                    String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
                    AddTourDO mAddTourDO = new AddTourDO();
                    if(selectedTourPhotoFromDraft.length()>0
                            && !images.get(selectedPos).imageName.toString().equalsIgnoreCase(selectedTourPhotoFromDraft)){

                        mDBCreator.updateTourPhotoInDraft(draftId,images.get(selectedPos).imageName.toString(),date);

                    }else{
                        mAddTourDO.tourIcon = images.get(selectedPos).imageName.toString();
                        mAddTourDO.date = date;

                        int max  = mDBCreator.getLastRecordId();
                        draftId = AppConstants.DRAFT_NAME+"_"+max;
                        mDBCreator.saveDetailsToDraftTable(mAddTourDO,draftId);
                    }
                    final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
                    if(!manager.isProviderEnabled( LocationManager.GPS_PROVIDER)){
                        GPSTracker gpsTracker = new GPSTracker(TourIconSelection.this);
                        gpsTracker.showSettingsAlert();

                    }else{
                        if (fromedittour) {
                            startActivityForResult(new Intent(TourIconSelection.this, AddTourScreenNew.class)
                                    .putExtra("SelectedImage", images.get(selectedPos).imageName.toString())
                                    .putExtra("draftId", "" + draftId)
                                    .putExtra("From", "Tour"), 2002);

                        } else {
                            startActivityForResult(new Intent(TourIconSelection.this, AddTourScreen.class)
                                    .putExtra("SelectedImage", images.get(selectedPos).imageName.toString())
                                    .putExtra("draftId", "" + draftId)
                                    .putExtra("From", "Tour"), 2002);

                        }
                    }
                    startActivityForResult(new Intent(TourIconSelection.this, AddTourScreen.class)
                            .putExtra("SelectedImage",images.get(selectedPos).imageName.toString())
                            .putExtra("draftId",""+draftId)
                            .putExtra("From", "AddTour"),2002);

                }else if(selectedPos >= 0 && null != images && !images.isEmpty() && from.equalsIgnoreCase("EditProfile")){
                    Intent intent=new Intent();
                    String SelectedImage = images.get(selectedPos).imageName.toString();
                    new PreferenceUtils(TourIconSelection.this).saveProfileImageURL("SelectedImage",SelectedImage);
                    intent.putExtra("SelectedImage",SelectedImage);
                    setResult(2003,intent);
                    finish();

                }else if(selectedPos >= 0 && null != images && !images.isEmpty() && from.equalsIgnoreCase("ReplaceImage")){
                    String SelectedImage = images.get(selectedPos).imageName.toString();
                    TourItem mTourItem = new TourItem();
                    mTourItem.ImagePath = SelectedImage;
                    Intent intent=new Intent();
                     intent.putExtra("ReplacedImage",mTourItem);
                    setResult(2018,intent);
                    finish();

                }
//                Runnable runnable=new Runnable() {
//                    @Override
//                    public void run() {
//                        LocalData.getInstance().setGalleryList(new ArrayList<ImagesDO>());
//                        Utils.getFilePaths(TourIconSelection.this);
//                    }
//                };
//                runnable.run();


                /*else if(from.equalsIgnoreCase("AddTour")){

                    ArrayList<TourItem> selectedImages = new ArrayList<TourItem>();
                    for(ImagesDO d: images)
                    {
                        if(d.isSelect){
                            TourItem mTourItem = new TourItem();
                            mTourItem.ImagePath = d.imageName;
                            selectedImages.add(mTourItem);
                        }

                    }
                    TourItem img = new TourItem();
                    img.ImagePath ="Plus";
                    selectedImages.add(img);

                    mAddTourDO.tourPhotos = selectedImages;

                    startActivityForResult(new Intent(TourIconSelection.this, FinalScreen.class).putExtra("SelectedData",mAddTourDO),2002);
                }*/
            }
        });

        ImageView cam= (ImageView)findViewById(R.id.cam);
        cam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent i=new Intent(Intent.ACTION_VIEW, FileProvider.getUriForFile(this, AUTHORITY, f));
//
//                i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                startActivity(i);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                presentFileName=Calendar.getInstance().getTimeInMillis()+".jpg";
                photo = new File(Environment.getExternalStorageDirectory(), presentFileName);



//                imageUri = Uri.fromFile(photo);


                Uri uriForFile = FileProvider.getUriForFile(getApplicationContext(), "com.sageabletech.shomiround.fileprovider", photo);
                List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    grantUriPermission(packageName, uriForFile, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
               imageUri=uriForFile;
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(intent, TAKE_PICTURE);
            }
        });

    }
    File photo = null;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    /*private boolean checkPermissionAlert(final Context context) {

        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,Manifest.permission.READ_EXTERNAL_STORAGE)) {
//                    showDialog("External storage", context,Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat.requestPermissions((Activity) context,
                                    new String[] { Manifest.permission.READ_EXTERNAL_STORAGE },
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }

    }*/


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            Toast.makeText(TourIconSelection.this, "Permission Denied", Toast.LENGTH_LONG).show();
        }else{
            init();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*if (requestCode == 2003 && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }else*/ if(requestCode == TAKE_PICTURE && resultCode == Activity.RESULT_OK) {
            Uri selectedImage=null;
            if (!presentFileName.equalsIgnoreCase("")) {

                System.out.println("presentfile:::" + presentFileName);
                System.out.println("get small image presentfile:::" + getsmallImage(presentFileName));
                File tempFile = new File(getsmallImage(presentFileName));
                System.out.println("tempFile image presentfile:::" + tempFile);
                 selectedImage = Uri.fromFile(tempFile);
            }
//            Uri selectedImage = FileProvider.getUriForFile(getApplicationContext(), "com.sageabletech.shomiround.fileprovider", tempFile);
//            crop(photoURI);
//            Uri selectedImage = imageUri;

//            Uri selectedImage = data.getData();


                if (selectedImage != null && from.equalsIgnoreCase("AddTour")) {
                    System.out.println("camera pic:::" + selectedImage.toString());
                    WordUtils.compressImage(selectedImage.toString());
                    String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
                    AddTourDO mAddTourDO = new AddTourDO();
                    mAddTourDO.tourIcon = photo.getAbsolutePath();
                    mAddTourDO.date = date;
                    int max = mDBCreator.getLastRecordId();
                    draftId = AppConstants.DRAFT_NAME + "_" + max;

                    System.out.println("draftId in right::" + draftId + "  " + mAddTourDO.tourIcon + "  " + mAddTourDO.date);
                    mDBCreator.saveDetailsToDraftTable(mAddTourDO, draftId);

                    startActivityForResult(new Intent(TourIconSelection.this, AddTourScreen.class)
                            .putExtra("SelectedImage", photo.getAbsolutePath())
                            .putExtra("draftId", "" + draftId)

                            .putExtra("From", "Tour"), 2002);

                } else if (selectedImage != null && from.equalsIgnoreCase("EditProfile")) {
                    Intent intent = new Intent();
                    String SelectedImage = photo.getAbsolutePath();
                    new PreferenceUtils(TourIconSelection.this).saveProfileImageURL("SelectedImage", SelectedImage);
                    intent.putExtra("SelectedImage", SelectedImage);
                    setResult(2003, intent);
                    finish();

                } else if (selectedImage != null && from.equalsIgnoreCase("ReplaceImage")) {
                    String SelectedImage = photo.getAbsolutePath();
                    TourItem mTourItem = new TourItem();
                    mTourItem.ImagePath = SelectedImage;
                    Intent intent = new Intent();
                    intent.putExtra("ReplacedImage", mTourItem);
                    setResult(2018, intent);
                    finish();

                }

            }


    }
    public String getsmallImage(String path){
        File file = new File(Environment.getExternalStorageDirectory().getPath());
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" +path);
        return uriSting;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
//        selectedPos = -1;
    }



        private class ImageAdapter extends BaseAdapter {

            /** The context. */
            private Activity context;

            /**
             * Instantiates a new image adapter.
             *
             * @param localContext
             *            the local context
             */
            public ImageAdapter(Activity localContext) {
                context = localContext;
                images = Utils.getFilePaths(localContext);
            }

            public int getCount() {
                return images.size();
            }

            public Object getItem(int position) {
                return position;
            }

            public long getItemId(int position) {
                return position;
            }

            public View getView(final int position, View convertView,
                                ViewGroup parent) {

                final ViewHolder holder;

                if (convertView == null) {

                    convertView = LayoutInflater.from(context).inflate(R.layout.grid_item,null,false);
                    holder = new ViewHolder();
                    holder.img = (ImageView) convertView.findViewById(R.id.imageView1);
                    holder.tv = (CheckBox) convertView.findViewById(R.id.checkBox1);
                    holder.imageselect = (ImageView) convertView.findViewById(R.id.imageselect);
                    holder.ivClose = (ImageView) convertView.findViewById(R.id.ivClose);
                    convertView.setTag(holder);


                } else {
                    holder = (ViewHolder)convertView.getTag();
                }
holder.ivClose.setVisibility(View.GONE);
                holder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);
                convertView.setLayoutParams(new GridView.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH/3)-2, (AppConstants.DEVICE_DISPLAY_WIDTH/3)-2));


                /*Picasso.with(context)
            	.load(images.get(position).imagePath)
            	.centerCrop().resize((width/3)-2, (width/3)-2)
            	.placeholder(R.color.black)
            	.into(holder.img);*/

                String url = images.get(position).imageName;
//                if(url.equalsIgnoreCase(selectedTourPhotoFromDraft)){
//                    url = selectedTourPhotoFromDraft;
//                    images.get(position).isSelect = true;
//                }

                Glide.with(context)
                        .load(url)
                        .centerCrop()
                        .placeholder(R.color.black)
//                	.diskCacheStrategy(DiskCacheStrategy.ALL)
//                	.override((width/3)-2, (width/3)-2)
                        .thumbnail(0.5f)
                        .crossFade()
                        .into(holder.img);


                if(from.equalsIgnoreCase("AddTour")||from.equalsIgnoreCase("EditProfile")){
                    if(selectedPos != -1 && selectedPos == position){

                        holder.tv.setChecked(true);

                    }else{
                        holder.tv.setChecked(false);
                    }
                }else if(from.equalsIgnoreCase("AddTour")){
                        holder.tv.setChecked(images.get(position).isSelect);

                }


                if(images.get(position).isSelect){
                    holder.imageselect.setVisibility(View.VISIBLE);
                }else{
                    holder.imageselect.setVisibility(View.GONE);
                }

                /*holder.img.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(holder.tv.isChecked()){
							holder.tv.setChecked(false);
						}else{
							holder.tv.setChecked(true);
						}

						images.get(position).isSelect = holder.tv.isChecked();

					}
				});*/


                return convertView;
            }

            public class ViewHolder
            {
                CheckBox tv;
                ImageView img;
                ImageView ivClose;
                ImageView imageselect;
            }

        }

        @SuppressLint("NewApi")
        private ArrayList<ImagesDO> allImages(){

            ArrayList<ImagesDO> imag = new ArrayList<ImagesDO>();

            String[] projection = {
                    MediaStore.Files.FileColumns._ID,
                    MediaStore.Files.FileColumns.DATE_ADDED,
                    MediaStore.Files.FileColumns.MEDIA_TYPE,
                    MediaStore.Files.FileColumns.MIME_TYPE
            };


            // Return only video and image metadata.
            String selection = MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

            Uri queryUri = MediaStore.Files.getContentUri("external");

            CursorLoader cursorLoader = new CursorLoader(
                    this,
                    queryUri,
                    projection,
                    selection,
                    null, // Selection args (none).
                    MediaStore.Files.FileColumns.DATE_ADDED + " DESC" // Sort order.
            );

//    	    images.clear();

            Cursor imagecursor = cursorLoader.loadInBackground();
    	    /*imagecursor = activity.getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
            		projection, null, null, "image_id DESC");*/



            if (imagecursor != null) {
                int image_column_index = imagecursor.getColumnIndex(MediaStore.Files.FileColumns._ID);
                int type_column_index = imagecursor.getColumnIndex(MediaStore.Files.FileColumns.MIME_TYPE);
                int count = imagecursor.getCount();
                for (int i = 0; i < count; i++) {
                    imagecursor.moveToPosition(i);
                    int id = imagecursor.getInt(image_column_index);
                    String mime_type = imagecursor.getString(type_column_index);
                    ImagesDO imageItem = new ImagesDO();
                    imageItem.id = id;
                    //lastId = id;

                    if(!mime_type.contains("video"))
                    {
                        imageItem.imageName = MediaStore.Images.Media.EXTERNAL_CONTENT_URI.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, String.valueOf(id)).toString();
                        imag.add(imageItem);
                    }



                }

                //add ImageItem at top of list

                imagecursor.close();
            }
            return imag;



        }



    }



