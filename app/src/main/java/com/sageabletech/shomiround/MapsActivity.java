package com.sageabletech.shomiround;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sageabletech.R;
import com.sageabletech.utils.Utils;

import java.io.IOException;
import java.net.URL;

public class MapsActivity extends BaseActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    String tourLangitude, tourLatitude, title, imagePath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);


        TextView tvTitle = (TextView) findViewById(R.id.header_text);
        tvTitle.setTypeface(Utils.getHelvaticaBold(MapsActivity.this));

        ImageView back = (ImageView) findViewById(R.id.back);
        back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_back_white));
        ImageView submit = (ImageView) findViewById(R.id.submit);
        submit.setVisibility(View.GONE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });







        tourLangitude = getIntent().getStringExtra("tourLangitude");
        tourLatitude = getIntent().getStringExtra("tourLatitude");
        if (getIntent().hasExtra("path")) {
            imagePath = getIntent().getStringExtra("path");
        }
        System.out.println("tourLangitude::" + tourLangitude + " #### " + tourLatitude);
        String title = getIntent().getStringExtra("title");
        if (title.equalsIgnoreCase("No Text") || title.equalsIgnoreCase("No Title")) {
            title = "";
        }

        title = title.toUpperCase();
        this.title = title;
        tvTitle.setText(this.title);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng latLng = new LatLng(Double.parseDouble(tourLatitude), Double.parseDouble(tourLangitude));
        System.out.println("imagePath sai:::" + imagePath);
        Bitmap bitMap = null;
        MarkerOptions options = new MarkerOptions();
        options.position(latLng);
        if (!imagePath.equalsIgnoreCase("")) {
            bitMap = getBitMap(imagePath);


        }


        if (bitMap != null) {
            BitmapDescriptorFactory.fromBitmap(bitMap);
//            Bitmap smallMarker = Bitmap.createScaledBitmap(mainTourImage, 60, 60, false);
            BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitMap);
            options.icon(bitmapDescriptor);
        } else {

            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }
        mMap.addMarker(options);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
    }


    public Bitmap getBitMap(String path) {
        Bitmap nextImageBitmap = null;

        System.out.println("sdsdsdsddsdsfdfsgdfgfg");
        try {
            URL url = new URL(path);
            nextImageBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            return getResizedBitmap(nextImageBitmap, 80);
        } catch (IOException e) {
            System.out.println(e);
        }
        return getResizedBitmap(nextImageBitmap, 80);

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        System.out.println("resizeeee:::" + maxSize);
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
}
