package com.sageabletech.shomiround;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sageabletech.R;
import com.sageabletech.adapter.MyTourPagerAdapter;
import com.sageabletech.newclassess.MyTourPagerAdapterNew;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.Utils;

public class MyTourList extends BaseActivity {
    RecyclerView recyclerView;
    MyTourPagerAdapterNew mSectionsPagerAdapter;
    MyTourPagerAdapter myTourPagerAdapter;
    ViewPager mViewPager;
    ImageView back,ivRight;
    TextView tvTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_tour_list);

        ivRight = (ImageView) findViewById(R.id.submit);
        ivRight.setVisibility(View.GONE);

        tvTitle=(TextView)findViewById(R.id.header_text);
        tvTitle.setTypeface(Utils.getHelvaticaBold(MyTourList.this));



//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        getSupportActionBar().setTitle("");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_cross_white);

//        TextView tvTitle = (TextView)findViewById(R.id.tvTitle);

        if(LocalData.getInstance().getFromDrafts().equalsIgnoreCase("no")){
            tvTitle.setText("MY TOUR");
        }else if(LocalData.getInstance().getFromDrafts().equalsIgnoreCase("fav")) {
            tvTitle.setText("Favourites");

        }else{
            tvTitle.setText("DRAFT TOUR");
        }
        mViewPager = (ViewPager) findViewById(R.id.container);



        System.out.println("sdfsdfdsfdsfdsf");
        back =(ImageView)findViewById(R.id.back);
        back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_back_white));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("my conditon::");
//                if(LocalData.getInstance().getFromDrafts().equalsIgnoreCase("no")){
                LocalData.getInstance().setIsFromProfile("yes");
                    Intent intent=new Intent(MyTourList.this,MainScreen.class);
                    startActivity(intent);
//                }

            }
        });
        if(LocalData.getInstance().getFromDrafts().equalsIgnoreCase("yes")){
            myTourPagerAdapter = new MyTourPagerAdapter(getSupportFragmentManager(),this,getIntent().getStringExtra("From"),mViewPager);
            mViewPager.setAdapter(myTourPagerAdapter);
        }else{
            mSectionsPagerAdapter = new MyTourPagerAdapterNew(getSupportFragmentManager(),this,getIntent().getStringExtra("From"));
            mViewPager.setAdapter(mSectionsPagerAdapter);
        }


        // Set up the ViewPager with the sections adapter.


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        final ImageView iv1 = new ImageView(this);
        iv1.setImageResource(R.mipmap.ic_list_solid_blue);
        iv1.setPadding(15,15,15,15);

        final ImageView iv2 = new ImageView(this);
        iv2.setImageResource(R.mipmap.ic_grid_solid_gray);
        iv2.setPadding(15, 15, 15, 15);

        tabLayout.getTabAt(0).setCustomView(iv1);
        tabLayout.getTabAt(1).setCustomView(iv2);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        LocalData.getInstance().setIsFromProfile("yes");
        Intent intent=new Intent(MyTourList.this,MainScreen.class);
        startActivity(intent);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


public void refreshData(){
    Log.e("REFRESHED","refreshData");
    mSectionsPagerAdapter.refreshFragments();
    /*mSectionsPagerAdapter = new MyTourPagerAdapterNew(getSupportFragmentManager(),this);
    if(mViewPager !=null)
    mViewPager.setAdapter(mSectionsPagerAdapter);*/
}

}
