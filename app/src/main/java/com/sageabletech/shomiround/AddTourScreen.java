package com.sageabletech.shomiround;


import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.droidmentor.locationhelper.Constants;
import com.droidmentor.locationhelper.MyLocationUsingHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sageabletech.R;
import com.sageabletech.api.Location;
import com.sageabletech.api.PlaceDetailsJSONParser;
import com.sageabletech.api.PlaceJSONParser;
import com.sageabletech.model.AddTourDO;
import com.sageabletech.model.ImagesDO;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.newclassess.AddTourScreenNew;
import com.sageabletech.newclassess.GallarySample;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.GeoDegree;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.Utils;
import com.sageabletech.webservicecalls.GPSTracker;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class AddTourScreen extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback, LocationListener {

    private static final String TAG = AddTourScreen.class.getName();
    ImageView right, ivSelectedImage, ivLocationIcon, back;
    TextView tvTitle, loader;
    EditText tvTourTitle, tvTourSummary;
    protected double latitude = 0, longitude = 0;

    DownloadTask placesDownloadTask;
    DownloadTask placeDetailsDownloadTask;
    ParserTask placesParserTask;
    ParserTask placeDetailsParserTask;
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    private GoogleMap googleMap;

    final int PLACES = 0;
    final int PLACES_DETAILS = 1;

    //    GoogleApiClient mGoogleApiClient;
    final static int REQUEST_LOCATION = 199;

    AddTourDO mAddTourDO = null;
    String from = "";
    LatLng latlng = null;
    BitmapDescriptor map_icon = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tour_add);
        loader = (TextView) findViewById(R.id.loader);

        LocalData.getInstance().setSelectedMyTourItemDO(null);

        from = getIntent().getStringExtra("From");



        System.out.println("DRAFTID::" + getIntent().getStringExtra("draftId"));

        init();
        setHeader();
//        setTheMap();

        /*mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();*/

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        LocalData.getInstance().setLocationManager(locationManager);


    }


    private void setHeader() {


        if (from.equalsIgnoreCase("AddCaption")) {

            right.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_save_white));

        }

    }


    String address = "";

    private String getAddress(android.location.Location location) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            address += "," + addresses.get(0).getSubLocality();
            address += "," + addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return address;
    }


    AutoCompleteTextView mLocation;
    ImageView cancelLoc;
    MyTourItemDO mMyTourItemDO = null;

    public void getLactionFromPic(String picpath) {
        try {
            System.out.println("photo lat longggg::::" + picpath);
            ExifInterface exif = new ExifInterface(picpath);

            String mlat = "0.0";
            String mlng = "0.0";
            GeoDegree geoDegree = new GeoDegree(exif);
            double lat = geoDegree.getLatitudeE6();
            double lng = geoDegree.getLongitudeE6();
            GPSTracker gpsTracker = new GPSTracker(AddTourScreen.this);

            if (lat == 0.0 & lng == 0.0) {

                mlat = String.valueOf(gpsTracker.getLatitude());
                mlng = String.valueOf(gpsTracker.getLongitude());

            } else {
                mlat = String.valueOf(lat);
                mlng = String.valueOf(lng);
            }
            latitude = Double.parseDouble(mlat);
            longitude = Double.parseDouble(mlng);
            latlng = new LatLng(Double.parseDouble(mlat), Double.parseDouble(mlng));
            map_icon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
            try {
                String current_address = gpsTracker.getCompleteAddressString(AddTourScreen.this, latitude, longitude);
                mLocation.setText(current_address);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {

        }

    }

    boolean locationSelected = false;

    private void init() {

        ivSelectedImage = (ImageView) findViewById(R.id.ivSelectedImage);
        ivLocationIcon = (ImageView) findViewById(R.id.ivLocationIcon);


        tvTourTitle = (EditText) findViewById(R.id.tvTourTitle);
        tvTourTitle.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = tvTourTitle.getText().toString().toLowerCase(Locale.getDefault());
                tvTitle.setText(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });
        tvTourSummary = (EditText) findViewById(R.id.tvTourSummary);
        cancelLoc = (ImageView) findViewById(R.id.cancelLoc);
        cancelLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                latitude = 0;
                longitude = 0;
                mLocation.setText("");
            }
        });

       /* SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);*/

        setTheMap();


        tvTitle = (TextView) findViewById(R.id.header_text);

        tvTitle.setText("NEW TOUR");
        tvTitle.setTypeface(Utils.getHelvaticaBold(AddTourScreen.this));
        right = (ImageView) findViewById(R.id.submit);


        right.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_forward_circle));

        back = (ImageView) findViewById(R.id.back);
        back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_cross_white));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backAction();
            }
        });

        mLocation = (AutoCompleteTextView) findViewById(R.id.editText3);
        mLocation.setThreshold(2);

        try {
            picpath = getIntent().getStringExtra("SelectedImage");
            System.out.println("selectedImage" + picpath);
            if (picpath.contains("file://")) {
                picpath = picpath.replace("file://", "");
            }
            Log.e(TAG, "" + getIntent().getStringExtra("SelectedImage"));
            getLactionFromPic(picpath);

//            showLocationOnMap(map_icon,latlng);
//            System.out.println("LAT LNG::" + mlat + "::" + mlng);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (picpath != null)
            Glide.with(this)
                    .load(picpath)
                    .placeholder(R.color.black)
                    .centerCrop()
                    .override(AppConstants.DEVICE_DISPLAY_WIDTH, AppConstants.DEVICE_DISPLAY_WIDTH / 2)
                    .crossFade()
                    .into(ivSelectedImage);

        ivSelectedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MyTourListNew==edit//
                startActivity(new Intent(AddTourScreen.this, GallarySample.class));
            }
        });


        String from = getIntent().getStringExtra("From");

        System.out.println("FROM::" + from);

        if (from != null)
            if (from.equalsIgnoreCase("AddCaption")) {
                tvTitle.setText("ADD CAPTION");

                String text = getIntent().getStringExtra("title").equalsIgnoreCase("No Text") ? "" : getIntent().getStringExtra("title");
                tvTourTitle.setText("" + text);
                tvTourTitle.setHint("Title");

                String summary = getIntent().getStringExtra("summary").equalsIgnoreCase("No Text") ? "" : getIntent().getStringExtra("summary");
                tvTourSummary.setText("" + summary);
                tvTourSummary.setHint("Summary");

            } else if (from.equalsIgnoreCase("MyTourListFragment")) {
                tvTitle.setText("Draft Tour");

                mMyTourItemDO = (MyTourItemDO) getIntent().getSerializableExtra("Details");


                tvTourTitle.setText("" + mMyTourItemDO.title);
                tvTourSummary.setText("" + mMyTourItemDO.summary);
                mLocation.setText("" + mMyTourItemDO.city);

                ivSelectedImage.setLayoutParams(new LinearLayout.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH),
                        (AppConstants.DEVICE_DISPLAY_WIDTH / 2)));

                if (mMyTourItemDO.tourLatitude != null && !mMyTourItemDO.tourLatitude.equalsIgnoreCase("") &&
                        mMyTourItemDO.tourLangitude != null && !mMyTourItemDO.tourLangitude.equalsIgnoreCase("")) {
                    System.out.println("TOURLATLNGINCODITION::" + mMyTourItemDO.tourLatitude + "  " + mMyTourItemDO.tourLangitude);

                    latitude = Double.parseDouble(mMyTourItemDO.tourLatitude);
                    longitude = Double.parseDouble(mMyTourItemDO.tourLangitude);
                    latlng = new LatLng(latitude, longitude);
                }


                Glide.with(this)
                        .load(picpath)
                        .placeholder(R.color.black)
                        .centerCrop()
                        .override(AppConstants.DEVICE_DISPLAY_WIDTH, AppConstants.DEVICE_DISPLAY_WIDTH / 2)
                        .into(ivSelectedImage);

                mLocation.setText(mMyTourItemDO.area + " " + mMyTourItemDO.city);

            }

        mLocation.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!locationSelected) {

                    // Creating a DownloadTask to download Google Places matching "s"
                    placesDownloadTask = new DownloadTask(PLACES);
                    // Getting url to the Google Places Autocomplete api
                    String url = Location.getAutoCompleteUrl(s.toString());
                    placesDownloadTask.execute(url);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });


        mLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                hideKeyBoard(mLocation);

                ListView lv = (ListView) parent;
                SimpleAdapter adapter = (SimpleAdapter) parent.getAdapter();

                HashMap<String, String> hm = (HashMap<String, String>) adapter.getItem(position);

                placeDetailsDownloadTask = new DownloadTask(PLACES_DETAILS);
                String url = Location.getPlaceDetailsUrl(hm.get("reference"));
                placeDetailsDownloadTask.execute(url);

                mLocation.setText(hm.get("description"));
                loader.setVisibility(View.GONE);


            }
        });

        ivLocationIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalData.getInstance().setLocationHit("yes");
                locationSelected = true;

                Intent intent = new Intent(AddTourScreen.this, MyLocationUsingHelper.class);
                startActivity(intent);
//
//                if (latitude != 0&&longitude!=0) {
//                    LatLng latLng = new LatLng(latitude, longitude);
//                    System.out.println("latLng::" + latLng);
//
//                    mLocation.setText("");
//                    GPSTracker gpsTracker = new GPSTracker(AddTourScreen.this);
//                    LatLng currentLatLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
//                    String current_address = gpsTracker.getCompleteAddressString(AddTourScreen.this, gpsTracker.getLatitude(), gpsTracker.getLongitude());
//                    latitude = gpsTracker.getLatitude();
//                    longitude = gpsTracker.getLongitude();
//
//                    mLocation.setText(current_address);
//                    showLocationOnMap(map_icon, currentLatLng);
//
//
//                }
//                else{
////                    getLocation();
//                }
            }
        });


        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalData.getInstance().setGalleryList(new ArrayList<ImagesDO>());

                String from = getIntent().getStringExtra("From");

                if (tvTourTitle.getText().toString().length() == 0) {
                    Toast.makeText(AddTourScreen.this, "Enter Title and Summary", Toast.LENGTH_LONG).show();
                    return;
                }  else if (latitude == 0) {
                    Toast.makeText(AddTourScreen.this, "please click the locaion icon or enter location", Toast.LENGTH_LONG).show();
                    return;
                } else if (longitude == 0) {
                    Toast.makeText(AddTourScreen.this, "please click the locaion icon or enter location", Toast.LENGTH_LONG).show();
                    return;
                }

                if (tvTourTitle.getText().toString().length() > 0

                        && !from.equalsIgnoreCase("AddCaption")
                        && !from.equalsIgnoreCase("MyTourListFragment")) {
                    System.out.println("dsfsdfdf1");

                    mAddTourDO = new AddTourDO();
                    mAddTourDO.title = tvTourTitle.getText().toString();
                    mAddTourDO.tourSummary = tvTourSummary.getText().toString();
                    mAddTourDO.tourLocation = mLocation.getText().toString();
                    mAddTourDO.tourIcon = picpath;
                    mAddTourDO.lat = latitude;
                    mAddTourDO.lang = longitude;
                    LocalData.getInstance().setCreateAddTourData(mAddTourDO);

                    mAddTourDO.tourPhotos = mDBCreator.getSubTourItems(getIntent().getStringExtra("draftId"));

                    LocalData.getInstance().setNewTour("new");

                    if (getIntent().getStringExtra("draftId").contains("DRAFT"))
                        mDBCreator.updateDetailsToDraftTable(mAddTourDO, getIntent().getStringExtra("draftId"));

                    startActivityForResult(new Intent(AddTourScreen.this, CategoriesActivity.class)

//                    startActivityForResult(new Intent(AddTourScreen.this, FinalScreen.class)
                            .putExtra("draftId", getIntent().getStringExtra("draftId"))
                            .putExtra("From", "AddTour")
                            .putExtra("SelectedData", mAddTourDO), 2001);

                } else if (from.equalsIgnoreCase("AddCaption")) {
                    System.out.println("dsfsdfdf2");

                    if (tvTourTitle.getText().toString().length() > 0) {
                        alertDialogFinish("Save Caption");

                    } else {
                        Toast.makeText(AddTourScreen.this, "Enter Title", Toast.LENGTH_LONG).show();
                    }

                } else if (from.equalsIgnoreCase("MyTourListFragment")) {
                    System.out.println("dsfsdfdf3");
                    mAddTourDO = new AddTourDO();
                    mAddTourDO.title = tvTourTitle.getText().toString();
                    mAddTourDO.tourSummary = tvTourSummary.getText().toString();
                    mAddTourDO.tourLocation = mLocation.getText().toString();
                    try {
                        mAddTourDO.lat = Double.parseDouble(mMyTourItemDO.tourLatitude);
                        mAddTourDO.lang = Double.parseDouble(mMyTourItemDO.tourLangitude);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mAddTourDO.tourId = mMyTourItemDO.tourId;
                    mAddTourDO.tourIcon = picpath;
                    mAddTourDO.status = mMyTourItemDO.status;

                    mAddTourDO.lang = longitude;
                    mAddTourDO.tourPhotos = mMyTourItemDO.subtour;

                    String id = getIntent().getStringExtra("draftId");
                    if (id == null) {
                        id = mAddTourDO.tourId;

                    }

                    if (id.equalsIgnoreCase(""))
                        mDBCreator.updateDetailsToDraftTable(mAddTourDO, id);
                    LocalData.getInstance().setCatnew(true);
                    LocalData.getInstance().setCreateAddTourData(mAddTourDO);

                    startActivityForResult(new Intent(AddTourScreen.this, CategoriesActivity.class)
                            .putExtra("draftId", id)
                            .putExtra("From", "MyTourListFragment").putExtra("SelectedData", mAddTourDO), 2001);
//                    finish();
                }

               /* try {
                    mDBCreator.backupDatabase(AddTourScreen.this);
                }catch (Exception e){
                    e.printStackTrace();

                }*/
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2001 && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }

    }

    private void showLocationOnMap(BitmapDescriptor icon, LatLng latLng) {
        googleMap.clear();
        latitude = latLng.latitude;
        longitude = latLng.longitude;

        MarkerOptions markerOption = new MarkerOptions();
        markerOption.position(latLng).icon(icon).flat(true).title("");
        Marker marker = googleMap.addMarker(markerOption);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        loader.setVisibility(View.GONE);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
//            Toast.makeText(AddTourScreen.this, "Not Called 1", Toast.LENGTH_SHORT).show();
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }


    public void alertDialogFinish(final String msg) {

        String alter = "";


        if (msg.equalsIgnoreCase(getResources().getString(R.string.save_to_draft_request))) {

            alter = "Discard";

        } else {
            alter = "No";

        }
        try {

            AlertDialog.Builder builder = new AlertDialog.Builder(AddTourScreen.this);
            builder.setMessage(msg + " ?");
            builder.setTitle(getResources().getString(R.string.app_name));
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {


                    Intent in = new Intent();
                    in.putExtra("TourTitle", "" + tvTourTitle.getText().toString());
                    in.putExtra("Summary", "" + tvTourSummary.getText().toString());
                    in.putExtra("latitude", "" + latitude);
                    in.putExtra("longitude", "" + longitude);
                    in.putExtra("location", "" + mLocation.getText());
                    setResult(RESULT_OK, in);
                    finish();


                }

            });
            builder.setNeutralButton("No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.dismiss();
//                        context.startActivity(new Intent(context, Setup.class));
                            //dialog.cancel();
                        }
                    });
          /*  builder.setNegativeButton(alter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    System.out.println("BUILDER::"+"NO");

                    if (msg.equalsIgnoreCase(getResources().getString(R.string.save_to_draft_request))) {

                        Intent intent=new Intent(AddTourScreen.this,MainScreen.class);
                        startActivity(intent);
                    } else {

                        dialog.dismiss();
                    }
                }

            });*/


            builder.show();

        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    /*@Override
    public void onBackPressed() {
//        super.onBackPressed();
        alertDialogFinish("back clicked");
    }*/

    public void backAction() {
        if (tvTourTitle.getText().toString().length() > 0
                && tvTourSummary.getText().toString().length() > 0
                && !from.equalsIgnoreCase("AddCaption")
                && !from.equalsIgnoreCase("MyTourListFragment")) {


//                    finish();
            showOptionAlert("back", "Do you want to exit ?");
//                          getDraftAlert("new");

        } else if (from.equalsIgnoreCase("MyTourListFragment")) {

            showOptionAlert("back", "Do you want to exit ?");

//                    finish();

//                    getDraftAlert("MyTourListFragment");

        } else if (tvTourTitle.getText().toString().length() == 0
                && tvTourSummary.getText().toString().length() > 0
                && !from.equalsIgnoreCase("AddCaption")
                && !from.equalsIgnoreCase("MyTourListFragment")) {
            showOptionAlert("back", "Do you want to exit ?");

        } else if (tvTourTitle.getText().toString().length() > 0
                && tvTourSummary.getText().toString().length() == 0
                && !from.equalsIgnoreCase("AddCaption")
                && !from.equalsIgnoreCase("MyTourListFragment")) {
            showOptionAlert("back", "Do you want to exit ?");

        } else if (tvTourTitle.getText().toString().length() == 0
                && tvTourSummary.getText().toString().length() == 0
                && !from.equalsIgnoreCase("AddCaption")
                && !from.equalsIgnoreCase("MyTourListFragment")) {


            Intent intent = new Intent(AddTourScreen.this, MainScreen.class);
            startActivity(intent);
        } else {
            finish();

//                    alertDialogFinish("Save Caption");

//                    alertDialogFinish(getResources().getString(R.string.save_to_draft_request));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:


                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void setTheMap() {

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

//        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(this, "Not", Toast.LENGTH_LONG).show();
            return;
        }


//		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, this);

    }


    @Override
    public void onConnected(Bundle bundle) {


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

//        this.googleMap.setMyLocationEnabled(true);
        showLocationOnMap(map_icon, latlng);


    }

    @Override
    public void onLocationChanged(android.location.Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        private int downloadType = 0;

        // Constructor
        public DownloadTask(int type) {
            this.downloadType = type;
        }

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = Location.downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            switch (downloadType) {
                case PLACES:
                    // Creating ParserTask for parsing Google Places
                    placesParserTask = new ParserTask(PLACES);

                    // Start parsing google places json data
                    // This causes to execute doInBackground() of ParserTask class
                    placesParserTask.execute(result);

                    break;

                case PLACES_DETAILS:
                    // Creating ParserTask for parsing Google Places
                    placeDetailsParserTask = new ParserTask(PLACES_DETAILS);

                    // Starting Parsing the JSON string
                    // This causes to execute doInBackground() of ParserTask class
                    placeDetailsParserTask.execute(result);
            }
        }
    }


    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        int parserType = 0;

        public ParserTask(int type) {
            this.parserType = type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loader.setVisibility(View.VISIBLE);

        }

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<HashMap<String, String>> list = null;

            try {
                jObject = new JSONObject(jsonData[0]);

                switch (parserType) {
                    case PLACES:
                        PlaceJSONParser placeJsonParser = new PlaceJSONParser();
                        // Getting the parsed data as a List construct
                        list = placeJsonParser.parse(jObject);
                        break;
                    case PLACES_DETAILS:
                        PlaceDetailsJSONParser placeDetailsJsonParser = new PlaceDetailsJSONParser();
                        // Getting the parsed data as a List construct
                        list = placeDetailsJsonParser.parse(jObject);
                }

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return list;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {

            switch (parserType) {
                case PLACES:
                    String[] from = new String[]{"description"};
                    int[] to = new int[]{android.R.id.text1};

                    // Creating a SimpleAdapter for the AutoCompleteTextView
                    SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), result, R.layout.text_layout, from, to);

                    // Setting the adapter
                    mLocation.setAdapter(adapter);
                    break;
                case PLACES_DETAILS:
                    HashMap<String, String> hm = result.get(0);

                    // Getting latitude from the parsed data
                    latitude = Double.parseDouble(hm.get("lat"));

                    // Getting longitude from the parsed data
                    longitude = Double.parseDouble(hm.get("lng"));


                    LatLng latLng = new LatLng(latitude, longitude);

                    if (latLng != null) {

                        showLocationOnMap(map_icon, latLng);


                        /*googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));*/

                    }

                    break;
            }
        }
    }

    private void getDraftAlert(final String type) {

        AlertDialog.Builder builder = new AlertDialog.Builder(AddTourScreen.this);
        builder.setMessage(getResources().getString(R.string.save_to_draft_request));
        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (type.equalsIgnoreCase("new")) {
                    mAddTourDO = new AddTourDO();
                    mAddTourDO.title = tvTourTitle.getText().toString();
                    mAddTourDO.tourSummary = tvTourSummary.getText().toString();
                    mAddTourDO.tourLocation = mLocation.getText().toString();
                    mAddTourDO.tourIcon = picpath;
               /*mAddTourDO.lat = latitude;
               mAddTourDO.lang = longitude;*/
                    System.out.println("DRAFTID::" + mAddTourDO.title + "  " + mAddTourDO.tourSummary + "  " + mAddTourDO.tourLocation + "  " + mAddTourDO.tourIcon + latitude + " " + longitude);

                    System.out.println("DRAFTID::" + getIntent().getStringExtra("draftId"));
                    mDBCreator.updateDetailsToDraftTable(mAddTourDO, getIntent().getStringExtra("draftId"));


                } else if (type.equalsIgnoreCase("MyTourListFragment")) {


                    mAddTourDO = new AddTourDO();
                    mAddTourDO.title = tvTourTitle.getText().toString();
                    mAddTourDO.tourSummary = tvTourSummary.getText().toString();
                    mAddTourDO.tourLocation = mLocation.getText().toString();
                    try {
                        mAddTourDO.lat = Double.parseDouble(mMyTourItemDO.tourLatitude);
                        mAddTourDO.lang = Double.parseDouble(mMyTourItemDO.tourLangitude);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mAddTourDO.tourId = mMyTourItemDO.tourId;
                    mAddTourDO.tourIcon = picpath;


                    mAddTourDO.lang = longitude;
                    mAddTourDO.tourPhotos = mMyTourItemDO.subtour;

                    String id = getIntent().getStringExtra("draftId");
                    if (id == null) {
                        id = mAddTourDO.tourId;

                    }

                    if (id.contains("DRAFT"))
                        mDBCreator.updateDetailsToDraftTable(mAddTourDO, id);

                }


                Intent intent = new Intent(AddTourScreen.this, MainScreen.class);
                startActivity(intent);
            }
        });
      /* builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialog, int which) {

//               onBackPressed();

               dialog.dismiss();
           }
       });*/
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                dialog.dismiss();

            /*   Intent intent = new Intent(AddTourScreen.this,MainScreen.class);
               startActivity(intent);*/
            }
        });

        builder.show();


    }

    private void showOptionAlert(final String option, String mesage) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.choose_option_alert, null);
        dialogBuilder.setView(dialogView);


        TextView upload = (TextView) dialogView.findViewById(R.id.upload);
        TextView savetodraft = (TextView) dialogView.findViewById(R.id.savetodraft);
        TextView discard = (TextView) dialogView.findViewById(R.id.discard);
        TextView cancel = (TextView) dialogView.findViewById(R.id.cancel);
        TextView msg = (TextView) dialogView.findViewById(R.id.msg);

        msg.setText(mesage);
        if (option.equalsIgnoreCase("back")) {

            upload.setVisibility(View.GONE);
        }

        final AlertDialog b = dialogBuilder.create();
        b.show();

        savetodraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mAddTourDO = new AddTourDO();
                mAddTourDO.title = tvTourTitle.getText().toString();
                mAddTourDO.tourSummary = tvTourSummary.getText().toString();
                mAddTourDO.tourLocation = mLocation.getText().toString();
                mAddTourDO.tourIcon = picpath;
               /*mAddTourDO.lat = latitude;
               mAddTourDO.lang = longitude;*/
                System.out.println("DRAFTID::" + mAddTourDO.title + "  " + mAddTourDO.tourSummary + "  " + mAddTourDO.tourLocation + "  " + mAddTourDO.tourIcon + latitude + " " + longitude);

                System.out.println("DRAFTID::" + getIntent().getStringExtra("draftId"));
                mDBCreator.updateDetailsToDraftTable(mAddTourDO, getIntent().getStringExtra("draftId"));
                Intent intent = new Intent(AddTourScreen.this, MainScreen.class);
                startActivity(intent);

            }
        });

        discard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();

                mDBCreator.deleteDrafts(getIntent().getStringExtra("draftId"));
                mDBCreator.deleteCategories(getIntent().getStringExtra("draftId"));
                mDBCreator.deleteTags(getIntent().getStringExtra("draftId"));
                mDBCreator.deleteDraftsPhotos(getIntent().getStringExtra("draftId"));
                Intent intent = new Intent(AddTourScreen.this, MainScreen.class);
                startActivity(intent);
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                b.dismiss();
            }
        });


    }

//    public void  getLocation() {
//
//        String best = LocationManager.NETWORK_PROVIDER;
//        LocationManager loc = (LocationManager) getSystemService(LOCATION_SERVICE);
//        googleMap.setMyLocationEnabled(true);
//        loc.requestLocationUpdates(best, 1000, 0, this);
//        android.location.Location location = loc.getLastKnownLocation(best);
////        android.location.Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
////
//
//        if (location == null) {
//            Toast.makeText(AddTourScreen.this, "Location Not found", Toast.LENGTH_LONG).show();
//            //				if(AppBean.getAppBeanInstance().isLocationAlert())
//            buildAlertMessageNoGps();
//            //				getFromSim();
//        } else {
//            Geocoder geocoder = new Geocoder(AddTourScreen.this);
//            try {
//                List<Address> user = null;
//                user = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
//                latitude = (double) user.get(0).getLatitude();
//                longitude = (double) user.get(0).getLongitude();
//                LatLng latLng = new LatLng(latitude, longitude);
//                mLocation.setText("");
//                GPSTracker gpsTracker = new GPSTracker(AddTourScreen.this);
//                LatLng currentLatLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
//                String current_address = gpsTracker.getCompleteAddressString(AddTourScreen.this, gpsTracker.getLatitude(), gpsTracker.getLongitude());
//                latitude = gpsTracker.getLatitude();
//                longitude = gpsTracker.getLongitude();
//
//                mLocation.setText(current_address);
//                showLocationOnMap(map_icon, currentLatLng);
//
//
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//
//    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(AddTourScreen.this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();

    }
    String picpath;
    @Override
    protected void onRestart() {
        super.onRestart();
        loader.setVisibility(View.GONE);
        if (LocalData.getInstance().getEditpicpath() != null) {
//            MyTourItemDO selectedMyTourItemDO = LocalData.getInstance().getSelectedMyTourItemDO();

            String editpicpath = LocalData.getInstance().getEditpicpath();
            picpath=editpicpath;
            if(picpath.contains("file://")){
                picpath=picpath.replace("file://", "");
            }
            Log.e(TAG, "" + getIntent().getStringExtra("SelectedImage"));
            getLactionFromPic(picpath);
            Glide.with(AddTourScreen.this).load(editpicpath).placeholder(R.drawable.background).centerCrop()
                    .into(ivSelectedImage);
            LocalData.getInstance().setEditpicpath(null);


        }

        if(LocalData.getInstance().getLocationHit().equalsIgnoreCase("yes")){

            latitude=  Constants.getInstance().getLatitude();
            longitude= Constants.getInstance().getLongitude();
            LocalData.getInstance().setLocationHit("no");
            if (latitude != 0&&longitude!=0) {
                    LatLng latLng = new LatLng(latitude, longitude);
                    System.out.println("latLng::" + latLng);
                    mLocation.setText(Constants.getInstance().getLocationText());
                    GPSTracker gpsTracker = new GPSTracker(AddTourScreen.this);
                    LatLng currentLatLng = new LatLng(latitude, longitude);
                    showLocationOnMap(map_icon, currentLatLng);

                }


        }
        locationSelected=false;
    }

}



