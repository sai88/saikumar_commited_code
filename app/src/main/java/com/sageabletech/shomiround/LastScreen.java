package com.sageabletech.shomiround;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.sageabletech.R;
import com.sageabletech.database.ImageModel;
import com.sageabletech.newclassess.WordUtils;

import java.util.ArrayList;

public class LastScreen extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.last_screen);
//        WordUtils.deleteFolder();
        TextView right = (TextView)findViewById(R.id.textView);

        ArrayList<ImageModel> all = mDBCreator.getAllImages();

        /*for(ImageModel im : all)
            Log.e("TTTTT",im.name+" "+im.status);*/

        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LastScreen.this,MainScreen.class));
                setResult(RESULT_OK);
//                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(LastScreen.this,MyTourList.class));
        setResult(RESULT_OK);
        super.onBackPressed();

    }
}
