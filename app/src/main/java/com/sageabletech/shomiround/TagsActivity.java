package com.sageabletech.shomiround;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sageabletech.R;
import com.sageabletech.database.DBCreator;
import com.sageabletech.model.AddTourDO;
import com.sageabletech.model.CategoryDO;
import com.sageabletech.model.TagsDo;
import com.sageabletech.newclassess.FeedSearch;
import com.sageabletech.newclassess.FinalScreenNew;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.NetworkUtils;
import com.sageabletech.utils.Urls;
import com.sageabletech.utils.Utils;
import com.sageabletech.viewtour.SearchFeeds;
import com.sageabletech.webservicecalls.AsyncRemoteCall;
import com.sageabletech.webservicecalls.RequestParams;
import com.sageabletech.webservicecalls.ServiceCalls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Pattern;

public class TagsActivity extends BaseActivity implements AsyncRemoteCall.OnDataListener  {
    static int selectedPos = -1;
    ArrayList<TagsDo> mTags = new ArrayList<TagsDo>();
    private ListView categories_list;
    CategoriesAdapter1 categoriesAdapter1;
    ImageView back;
    ImageView right;
    Button add_tag;
    EditText save_tag_tex;
    String ServiceCall = "";
    ArrayList<CategoryDO> categoryDO;
    String access_tocken;
    AddTourDO addTourDO;
    ArrayList<TagsDo> tagsArr;
    //    private LocalData localData;
    private ArrayList<TagsDo> selectedTags;
    private String from;
    EditText search;
    Button search_cancel;
    RelativeLayout extraAdd;
    TextView extratag_name,addtag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitytagsold);
        categories_list = (ListView) findViewById(R.id.categories_list);
        search = (EditText) findViewById(R.id.search_edit);
        extraAdd = (RelativeLayout) findViewById(R.id.extraAdd);
        extratag_name = (TextView) findViewById(R.id.extratag_name);
        addtag = (TextView) findViewById(R.id.addtag);
        search_cancel = (Button) findViewById(R.id.search_cancel);
        search_cancel.setTypeface(Utils.getHelvaticaBold(TagsActivity.this));
        search_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search.setText("");
                mTags=mTagsHold;
                categoriesAdapter1.notifyDataSetChanged();

//                extraAdd.setVisibility(View.GONE);
//                 new GetsTagsDynamic(TagsActivity.this,search.getText().toString(),categoriesAdapter1).execute();

            }
        });


        registerForContextMenu(categories_list);
        mDBCreator = new DBCreator(this);
        SQLiteDatabase db = mDBCreator.getWritableDatabase();
        db.close();
        back = (ImageView) findViewById(R.id.back);
        right = (ImageView) findViewById(R.id.right);
        TextView tvTitle=(TextView)findViewById(R.id.tvTitle);
        tvTitle.setTypeface(Utils.getHelvaticaBold(TagsActivity.this));
        add_tag = (Button) findViewById(R.id.add);
        add_tag.setTypeface(Utils.getHelvaticaBold(TagsActivity.this));
        save_tag_tex = (EditText) findViewById(R.id.save_tag_tex);
//        localData =LocalData.getInstance();


        from = getIntent().getStringExtra("From");

        System.out.println("FROMINCATE" + from);
        if(from.equalsIgnoreCase("filter")) {

        }else {

            if (LocalData.getInstance().getNewTour().equalsIgnoreCase("new")) {
                selectedTags = mDBCreator.getTags(getIntent().getStringExtra("draftId"));
//          if(selectedTags.size()>=1){
//              TagsDo tagsDo = selectedTags.get(0);
//              extraAdd.setVisibility(View.GONE);
//              search.setText(tagsDo.tagSearch);
////              new GetsTagsDynamic(TagsActivity.this,tagsDo.tagSearch,categoriesAdapter1).execute();
//
//          }

                System.out.println("selectedTags" + selectedTags.size());

            }
            if (!LocalData.getInstance().getNewTour().equalsIgnoreCase("new") && LocalData.getInstance().getFromDrafts().equalsIgnoreCase("no")) {
                extraAdd.setVisibility(View.GONE);
                search.setText("");
//            new GetsTagsDynamic(TagsActivity.this,"",categoriesAdapter1).execute();
            }

            categoryDO = (ArrayList<CategoryDO>) getIntent().getSerializableExtra("SelectedData");
            addTourDO = (AddTourDO) getIntent().getSerializableExtra("addTourDO");
        }

        String str = ((BaseActivity) this).preferenceUtils.getStringFromPreference("accessToken", "");

        access_tocken = str;



        // Capture Text in EditText
        search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search.getText().toString().toLowerCase(Locale.getDefault());
                categoriesAdapter1.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });
        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });

        System.out.println("accessToken" + str);
        if (NetworkUtils.isNetworkConnectionAvailable(this)) {
            if(from.equalsIgnoreCase("filter")) {
                ArrayList<TagsDo> tagsDos = LocalData.getInstance().getfilterTagServiceData();
                int size = tagsDos.size();
                if (size == 0) {
                    prepareMovieData(str);
                }else{
                    mTags=tagsDos;
                }
            }else {
                prepareMovieData(str);
            }


        } else {

            Toast.makeText(getApplicationContext(), "There is No Internet", Toast.LENGTH_LONG).show();
        }
        categoriesAdapter1 = new CategoriesAdapter1(this, mTags);
        categories_list.setAdapter(categoriesAdapter1);
        categories_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedPos = position;


                if (mTags.get(position).isSelect) {
                    mTags.get(position).isSelect = false;
                    categoriesAdapter1.notifyDataSetChanged();
                } else {
                    mTags.get(position).isSelect = true;
                    categoriesAdapter1.notifyDataSetChanged();

                }


            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                finish();

            }
        });
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(from.equalsIgnoreCase("filter")) {
                    tagsArr = new ArrayList<TagsDo>();

                    for (int i = 0; i < mTags.size(); i++) {
//                    CheckBox tv = (CheckBox) parent.findViewById(R.id.checkBox);
                        if (mTags.get(i).isSelect) {
//                        System.out.println("mCategoriesNAME  "+mTags.get(i).categorieName);
                            tagsArr.add(mTags.get(i));
                        }
                    }
                    LocalData.getInstance().setFilterTags(tagsArr);
                    finish();

                }else {


                    if (LocalData.getInstance().isFromedittour() && !LocalData.getInstance().getNewTour().equalsIgnoreCase("new")) {
                        tagsArr = new ArrayList<TagsDo>();

                        for (int i = 0; i < mTags.size(); i++) {
//                    CheckBox tv = (CheckBox) parent.findViewById(R.id.checkBox);
                            if (mTags.get(i).isSelect) {
//                        System.out.println("mCategoriesNAME  "+mTags.get(i).categorieName);
                                tagsArr.add(mTags.get(i));
                            } else {
                                tagsArr.remove(mTags.get(i));

                            }
                        }
                        startActivityForResult(new Intent(TagsActivity.this, FinalScreenNew.class)
                                .putExtra("draftId", getIntent().getStringExtra("draftId"))
                                .putExtra("categories", categoryDO)
                                .putExtra("tags", tagsArr)
                                .putExtra("From", "MyTourListNew"), 2001);

                    } else {
                        addTourDO.tourPhotos = mDBCreator.getSubTourItems(getIntent().getStringExtra("draftId"));
                        tagsArr = new ArrayList<TagsDo>();

                        for (int i = 0; i < mTags.size(); i++) {
//                    CheckBox tv = (CheckBox) parent.findViewById(R.id.checkBox);
                            if (mTags.get(i).isSelect) {
//                        System.out.println("mCategoriesNAME  "+mTags.get(i).categorieName);
                                tagsArr.add(mTags.get(i));
                            } else {
                                tagsArr.remove(mTags.get(i));

                            }
                        }

                        System.out.println("SelectedData" + addTourDO);
                        startActivityForResult(new Intent(TagsActivity.this, FinalScreen.class)
                                .putExtra("draftId", getIntent().getStringExtra("draftId"))
                                .putExtra("From", "AddTour")
                                .putExtra("categories", categoryDO)
                                .putExtra("tags", tagsArr)
                                .putExtra("SelectedData", addTourDO), 2001);

                        mDBCreator.deleteTags(getIntent().getStringExtra("draftId"));
                        mDBCreator.insertTags(getIntent().getStringExtra("draftId"), tagsArr);
                    }
                }


            }
        });
        addtag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String tag_text = extratag_name.getText().toString();
                if (tag_text == null || tag_text.equalsIgnoreCase("")) {

                    Toast.makeText(TagsActivity.this, "Please provide some text for adding Tags", Toast.LENGTH_SHORT).show();
                } else {
                    SaveTag(tag_text, access_tocken);
                }
//                new SaveTag(TagsActivity.this,tag_text.toString(),categoriesAdapter1).execute();

            }
        });
    }
    private void performSearch() {
        search.clearFocus();
        InputMethodManager in = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(search.getWindowToken(), 0);



    }

    private void SaveTag(String tagName, String vendorId) {

//        ((BaseActivity)this).showLoader();

        RequestParams requestParams = new RequestParams();
        String url = Urls.ADD_Tags + vendorId;
        requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");

        System.out.println("Categories URL::" + url);
        requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;
        JSONObject sendingJsonObject = new JSONObject();

        JSONObject tagJaJsonObject = new JSONObject();

        try {
            tagJaJsonObject.put("tagName", tagName);
            /*System.out.println("tagJaJsonObjectSIZE::"+categoryDO.size());

            for(int i=0;i<categoryDO.size();i++) {
                JSONObject categoriesJsonObject = new JSONObject();

                categoriesJsonObject.put("categorieId", categoryDO.get(i).categorieId);
                categoriesJsonObject.put("categorieName", categoryDO.get(i).categorieName);
                categoriesJsonObject.put("categorieImageLocation", categoryDO.get(i).categorieImageLocation);


                tagJaJsonObject.put("categorie", categoriesJsonObject);
            }
*/
            System.out.println("tagJaJsonObject" + tagJaJsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        requestParams.data = tagJaJsonObject.toString();
        ServiceCall = "addTag";

        ServiceCalls serviceCalls = new ServiceCalls(this, "savetag");
        serviceCalls.execute(requestParams);


    }

    private void prepareMovieData(String vendorId) {

        ((BaseActivity) this).showLoader();

        RequestParams requestParams = new RequestParams();
        String url = Urls.Tags_LIST + vendorId;
        requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");

        System.out.println("Categories URL::" + url);
        requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;
        JSONObject loginRequest = new JSONObject();
        try {
            loginRequest.put("tourEndOrderNumber", "0");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        requestParams.data = loginRequest.toString();

        ServiceCall = "getTags";

        ServiceCalls serviceCalls = new ServiceCalls(this, "listCategoryAndTags");
        serviceCalls.execute(requestParams);


    }


    @Override
    public void onData(Object data, String error, String tag) {
        ((BaseActivity) this).hideLoader();

        if (error == null) {

            System.out.println("CATLIST::" + data);
            if (ServiceCall.equalsIgnoreCase("getTags")) {

                mTags = (ArrayList<TagsDo>) data;
                LocalData.getInstance().setfilterTagServiceData(mTags);
                doAction();
                categoriesAdapter1.notifyDataSetChanged();
            } else {
                TagsDo tagsDo = (TagsDo) data;

                mTags.add(mTags.size(), tagsDo);
                doAction();
                categoriesAdapter1.notifyDataSetChanged();

                save_tag_tex.setText("");
                if (categories_list.getFirstVisiblePosition() > mTags.size() - 1 || categories_list.getLastVisiblePosition() < mTags.size() - 1)
                    categories_list.setSelection(mTags.size() - 1);
            }
        } else {

        }

    }

    public void doAction() {
//

        if (!LocalData.getInstance().getNewTour().equalsIgnoreCase("new") && LocalData.getInstance().getFromDrafts().equalsIgnoreCase("no")) {
            ArrayList<String> tagtourArry = LocalData.getInstance().getTagtourArry();
            System.out.println("tagtourArry size::"+tagtourArry.size());
            if (tagtourArry != null && tagtourArry.size() > 0) {
                for (int i = 0; i < tagtourArry.size(); i++) {
                    String s = tagtourArry.get(i);
                    for (int j = 0; j < mTags.size(); j++) {
                        TagsDo tagsDo = mTags.get(j);
                        String categorieId = tagsDo.tagId;
                        if (s.equalsIgnoreCase(categorieId)) {
                            tagsDo.isSelect = true;
//                            System.out.println("categoryDO:::"+categoryDO.isSelect);
//                            mCategories.add(j, categoryDO);
                            break;
                        }
                    }
                }
            }

        } else {

            if (selectedTags != null && selectedTags.size() > 0) {
                for (int i = 0; i < selectedTags.size(); i++) {
                    TagsDo tagsDo = selectedTags.get(i);
                    String categorieId1 = tagsDo.tagId;
                    for (int j = 0; j < mTags.size(); j++) {
                        TagsDo tagsDo1 = mTags.get(j);
                        String categorieId = tagsDo1.tagId;
                        if (categorieId1.equalsIgnoreCase(categorieId)) {
                            tagsDo1.isSelect = true;
//                            mCategories.add( categoryDO);
                            break;
                        }
                    }
                }
            }
        }
    }
    ArrayList<TagsDo> mTagsHold = new ArrayList<TagsDo>();
    ArrayList<TagsDo> mTagsHoldSearch = new ArrayList<TagsDo>();

    public class CategoriesAdapter1 extends BaseAdapter {


        public void doAction(ArrayList<TagsDo> mTags) {
//

            if (!LocalData.getInstance().getNewTour().equalsIgnoreCase("new") && LocalData.getInstance().getFromDrafts().equalsIgnoreCase("no")) {
                System.out.println("sdfdsfsdfdsfdsf");


                ArrayList<String> tagtourArry = LocalData.getInstance().getTagtourArry();
                System.out.println("tagtourArry size::"+tagtourArry.size());
                if (tagtourArry != null && tagtourArry.size() > 0) {
                    for (int i = 0; i < tagtourArry.size(); i++) {
                        String s = tagtourArry.get(i);
                        System.out.println("tag size:"+s);
                        for (int j = 0; j < mTags.size(); j++) {
                            TagsDo tagsDo = mTags.get(j);
                            String categorieId = tagsDo.tagId;
                            System.out.println("tag size::"+categorieId);
                            if (s.equalsIgnoreCase(categorieId)) {
                                System.out.println("enterrerrerre");
                                tagsDo.isSelect = true;
//                            System.out.println("categoryDO:::"+categoryDO.isSelect);
//                            mCategories.add(j, categoryDO);
                                break;
                            }
                        }
                    }
                }

            } else {

                System.out.println("elseeesdfdsfsdfdsfdsf");
                if (selectedTags != null && selectedTags.size() > 0) {
                    for (int i = 0; i < selectedTags.size(); i++) {
                        TagsDo tagsDo = selectedTags.get(i);
                        String categorieId1 = tagsDo.tagId;
                        for (int j = 0; j < mTags.size(); j++) {
                            TagsDo tagsDo1 = mTags.get(j);
                            String categorieId = tagsDo1.tagId;
                            if (categorieId1.equalsIgnoreCase(categorieId)) {
                                tagsDo1.isSelect = true;
//                            mCategories.add( categoryDO);
                                break;
                            }
                        }
                    }
                }
            }
        }
        private Context mContext;
        public  ArrayList<TagsDo>  mtagsnew=new ArrayList<>();

        //        private ArrayList<CategoryDO> mCategoryDOs;
        public CategoriesAdapter1(Context context, ArrayList<TagsDo> mtagsnew) {


            this.mtagsnew=mtagsnew;
            this.mContext = context;
//            mTags=mtagsnew;
            System.out.println("mTags size::con::"+mTags.size());
        }
        public void checkAddCont(String text){

            extraAdd.setVisibility(View.VISIBLE);
            extratag_name.setText(text);

        }
        public void disableCont(){

            extraAdd.setVisibility(View.GONE);

        }

        @Override
        public int getCount() {
            System.out.println("mTags size::con1::"+mtagsnew.size());
//            return mtagsnew.size();
            return mTags.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
//            mTags=mtagsnew;
            if (convertView == null) {
                holder = new ViewHolder();

                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.tags_list_adapter, null);
                holder.title = (TextView) convertView.findViewById(R.id.tag_name);
                holder.addtag = (TextView) convertView.findViewById(R.id.addtag);
                holder.imageselect = (ImageView) convertView.findViewById(R.id.imageselect);
                holder.tv = (CheckBox) convertView.findViewById(R.id.checkBox);
                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.title.setText(mTags.get(position).tagName);

            holder.title.setTypeface(Utils.getHelvaticaBold(TagsActivity.this));
            String tagId = mTags.get(position).tagId;


            System.out.println("selectedPos::" + mTags.get(position).tagName);
            holder.tv.setTag(position); // This line is important.
            holder.tv.setChecked(mTags.get(position).isSelect);
            holder.tv.setEnabled(false);
            if(mTags.get(position).isSelect){
                holder.imageselect.setVisibility(View.VISIBLE);
            }else{
                holder.imageselect.setVisibility(View.GONE);
            }

           /* if(selectedPos != -1 && selectedPos == position){

                holder.tv.setChecked(true);

            }else{
                holder.tv.setChecked(false);
            }*/

//            if (holder.tv.isChecked()) {
//                holder.tv.setVisibility(View.VISIBLE);
//            } else {
//                holder.tv.setVisibility(View.GONE);
//            }
            return convertView;
        }

        public void filter(String charText) {
            charText = charText.toLowerCase();
            System.out.println("charText::"+charText);
            if(mTagsHold.size()==0) {
                mTagsHold = mTags;

            }
            mTags=new ArrayList<>();

            if (charText.length() == 0) {
                mTags.addAll(mTagsHold);
            } else {
                System.out.println("mTagsHold::"+mTagsHold.size());
                for (int i=0;i<mTagsHold.size();i++){
                    TagsDo tagsDo = mTagsHold.get(i);
                    System.out.println("tagsDo::"+tagsDo.tagName);
                    if (tagsDo.tagName.toLowerCase().contains(charText)) {
                        mTags.add(tagsDo);
                    }
//                }
                }
//                for (WorldPopulation wp : arraylist) {
//                    if (wp.getCountry().toLowerCase(Locale.getDefault()).contains(charText)) {
//                        worldpopulationlist.add(wp);
//                    }
//                }
            }
            System.out.println("mTags::"+mTags.size());
            notifyDataSetChanged();
        }

        public void refresh(ArrayList<TagsDo> data) {
            mTags = data;
        }

        public class ViewHolder {
            CheckBox tv;
            TextView addtag;
            TextView title;
            ImageView imageselect;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    private void getDraftAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(TagsActivity.this);
        builder.setMessage(getResources().getString(R.string.save_to_draft_request));
        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tagsArr = new ArrayList<TagsDo>();

               /* if( mCategories.get(position).isSelect==true) {
                    categoryDO = mCategories.get(position);
                }*/
                for (int i = 0; i < mTags.size(); i++) {
//                    CheckBox tv = (CheckBox) parent.findViewById(R.id.checkBox);
                    if (mTags.get(i).isSelect) {
//                        System.out.println("mCategoriesNAME  "+mTags.get(i).categorieName);
                        tagsArr.add(mTags.get(i));
                    } else {
                        tagsArr.remove(mTags.get(i));

                    }
                }

                System.out.println("SelectedData" + addTourDO);


                mDBCreator.deleteTags(getIntent().getStringExtra("draftId"));
                mDBCreator.insertTags(getIntent().getStringExtra("draftId"), tagsArr);
                dialog.dismiss();
                onBackPressed();

            }
        });
        builder.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                onBackPressed();

                dialog.dismiss();
            }
        });

        builder.show();


    }

//==========================
//
//<?xml version="1.0" encoding="utf-8"?>
//<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
//        xmlns:tools="http://schemas.android.com/tools"
//        android:layout_width="match_parent"
//        android:layout_height="match_parent"
//        >
//<RelativeLayout
//        android:layout_width="match_parent"
//                android:layout_height="wrap_content"
//                android:orientation="vertical"
//                android:layout_alignParentTop="true"
//                android:layout_alignParentLeft="true"
//                android:layout_alignParentStart="true">
//
//<android.support.design.widget.AppBarLayout
//        android:id="@+id/appbar"
//        android:layout_width="match_parent"
//        android:layout_height="wrap_content"
//        android:layout_alignParentTop="true"
//        android:theme="@style/AppTheme.AppBarOverlay">
//
//<android.support.v7.widget.Toolbar
//        android:id="@+id/toolbar"
//        android:layout_width="match_parent"
//        android:layout_height="?attr/actionBarSize"
//        android:background="?attr/colorPrimary"
//        >
//<ImageView
//                    android:id="@+id/back"
//                            android:layout_width="wrap_content"
//                            android:layout_height="wrap_content"
//                            android:layout_gravity="left"
//                            android:padding="15dp"
//                            android:src="@mipmap/ic_back_arrow"
//                            android:visibility="visible" />
//
//<TextView
//                    android:id="@+id/tvTitle"
//                            android:layout_width="wrap_content"
//                            android:layout_height="wrap_content"
//                            android:layout_gravity="center"
//                            android:text="Tags"
//                            android:textAppearance="?android:attr/textAppearanceMedium" />
//
//<ImageView
//                    android:id="@+id/right"
//                            android:layout_width="wrap_content"
//                            android:layout_height="wrap_content"
//                            android:layout_gravity="right"
//                            android:padding="15dp"
//                            android:src="@mipmap/ic_forward_circle"
//                            android:visibility="visible" />
//
//</android.support.v7.widget.Toolbar>
//
//</android.support.design.widget.AppBarLayout>
//<RelativeLayout
//            android:layout_below="@+id/appbar"
//                    android:layout_width="match_parent"
//                    android:layout_height="wrap_content"
//                    android:id="@+id/search_cont"
//                    android:layout_marginTop="2dp"
//                    android:layout_marginLeft="5dp"
//                    android:layout_marginRight="5dp"
//                    >
//<EditText
//                android:layout_width="match_parent"
//                        android:layout_height="40dp"
//                        android:id="@+id/search_edit"
//                        android:hint="Enter search text"
//                        android:background="@drawable/layout_border"
//                        android:drawableRight="@drawable/magnifier"
//                        android:paddingRight="5dp"
//                        android:paddingLeft="5dp"
//                        android:layout_alignParentLeft="true"
//                        android:layout_alignParentStart="true" />
//<Button
//                android:layout_width="wrap_content"
//                        android:layout_height="40dp"
//                        android:id="@+id/search_cancel"
//                        android:text="Cancel"
//                        android:textAllCaps="false"
//                        android:layout_alignRight="@+id/search_edit"
//                        android:textColor="@color/colorAccent"
//                        android:background="@color/border_grey"
//                        android:layout_alignParentRight="true"/>
//</RelativeLayout>
//<RelativeLayout
//            android:id="@+id/extraAdd"
//                    android:visibility="gone"
//                    android:layout_width="wrap_content"
//
//                    android:layout_height="wrap_content"
//                    android:layout_below="@+id/search_cont"
//                    android:background="@color/white">
//
//<TextView
//                android:layout_width="wrap_content"
//                        android:layout_height="wrap_content"
//                        android:id="@+id/extratag_name"
//                        android:padding="10dp"
//                        android:text="jjkkkd"
//                        android:textColor="@color/black"
//                        android:textSize="16sp"
//                        android:layout_alignParentLeft="true"/>
//
//<TextView
//                android:id="@+id/addtag"
//                        android:layout_width="100dp"
//                        android:layout_height="wrap_content"
//                        android:text="Add"
//                        android:visibility="visible"
//                        android:gravity="center_horizontal"
//                        android:textColor="@color/com_facebook_button_background_color"
//
//                        android:layout_centerVertical="true"
//                        android:layout_alignParentRight="true"
//                        android:layout_alignParentEnd="true" />
//</RelativeLayout>
//
//<ListView
//            android:layout_width="match_parent"
//                    android:layout_height="match_parent"
//                    android:id="@+id/categories_list"
//                    android:layout_marginBottom="50dp"
//                    android:layout_below="@+id/extraAdd"></ListView>
//</RelativeLayout>
//
//
//<RelativeLayout
//        android:visibility="visible"
//                android:layout_width="match_parent"
//                android:layout_height="wrap_content"
//                android:id="@+id/save_tag_tex_layout"
//                android:layout_alignParentBottom="true"
//                android:layout_marginLeft="5dp"
//                android:layout_marginRight="5dp"
//                android:layout_marginBottom="10dp">
//<EditText
//        android:layout_width="match_parent"
//                android:layout_height="40dp"
//                android:id="@+id/save_tag_tex"
//                android:background="@drawable/layout_border"
//                android:layout_alignParentLeft="true"
//                android:hint="Add Tag"
//                android:paddingLeft="5dp"
//                android:layout_toLeftOf="@+id/add"
//                />
//<Button
//            android:layout_width="50dp"
//                    android:layout_height="40dp"
//                    android:id="@+id/add"
//                    android:text="Add"
//                    android:textAllCaps="false"
//                    android:textColor="@color/white"
//                    android:background="@color/appColor"
//                    android:layout_alignParentRight="true"/>
//</RelativeLayout>
//</RelativeLayout>





}
