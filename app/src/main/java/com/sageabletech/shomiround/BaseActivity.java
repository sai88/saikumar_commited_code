package com.sageabletech.shomiround;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.sageabletech.database.DBCreator;
import com.sageabletech.dialog.AlertDialogMsg;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.PreferenceUtils;

import java.text.SimpleDateFormat;
import java.util.Locale;


public class BaseActivity extends AppCompatActivity {

	public static final int REQ_FLAG = 1021;
	public static final int SEARCH_FLAG = 1022;
	public PreferenceUtils preferenceUtils = null;
	public static final String USER_ID = "USER_ID";
	public static final String EMAIL = "EMAIL";
	public static final String PROFILE_IMAGE = "PROFILE_IMAGE";
	public static final String PASSWORD = "PASSWORD";
	public static final String FIRST_NAME = "FIRST_NAME";
	public static final String LAST_NAME = "LAST_NAME";
	public static final String DOB = "DOB";
	public static final String GENDER = "GENDER";
	public static final String IDCOUNTRY = "IDCOUNTRY";
	public static final String CONTACT = "CONTACT";
	public static final String POSTCODE = "POSTCODE";


	public SimpleDateFormat dateFormatter;
	public DBCreator mDBCreator;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		

		preferenceUtils = new PreferenceUtils(BaseActivity.this);
		//"yyyy-MM-dd"
		dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

		mDBCreator = new DBCreator(this);
		SQLiteDatabase db = mDBCreator.getWritableDatabase();
		db.close();

		/*bold_face = Typeface.createFromAsset(getAssets(),
				"fonts/Lato-Bold.ttf");*/

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		DisplayMetrics metrics = this.getResources().getDisplayMetrics();
		AppConstants.DEVICE_DISPLAY_WIDTH = metrics.widthPixels;
		AppConstants.DEVICE_DISPLAY_HEIGHT = metrics.heightPixels;
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		DisplayMetrics metrics = this.getResources().getDisplayMetrics();
		AppConstants.DEVICE_DISPLAY_WIDTH = metrics.widthPixels;
		AppConstants.DEVICE_DISPLAY_HEIGHT = metrics.heightPixels;

	}
	
	
	

	public void alertDialog(String msg){
		
		try {
			
			new AlertDialogMsg(BaseActivity.this, msg).setPositiveButton("OK", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					
					
				}
				
			}).create().show();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public boolean isLogin()
	{
		String custId = preferenceUtils.getStringFromPreference(USER_ID, "");
		
		Log.e("custIdcustIdcustId",""+custId);
		
		if(custId.equalsIgnoreCase("")) {
			return false;
		}
		return true;
	}



	public void alertDialogFinish(String msg){
		 
		 try {
			 
			 new AlertDialogMsg(BaseActivity.this, msg).setPositiveButton("OK", new DialogInterface.OnClickListener() {
				 
				 public void onClick(DialogInterface dialog, int which) {
					 
					 finish();
					 
				 }
				 
			 }).create().show();
			 
		 } catch (Exception e) {
			 // TODO: handle exception
		 }
		 
	 }

	public void Logout(){
		preferenceUtils.saveString(USER_ID,"");
		preferenceUtils.saveString("isMenuAdded", "");
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
			Log.e("aaaaaaaaaaaaaaaaaaaaaaa"+requestCode, "dddddddd"+resultCode);
       /* List<Fragment> fragments = getSupportFragmentManager().getFragments();

        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }*/

	}

	private ProgressDialog progressDialog;
	 protected Dialog dialog;
	 private boolean isCancelableLoader;
	 public AlertDialog alertDialog;
	 public AlertDialog.Builder alertBuilder;
			
	 public void showLoader() {
			runOnUiThread(new RunShowLoader("Loading..."));
		}

		// Method to show loader with text
		public void showLoader(String msg) {
			runOnUiThread(new RunShowLoader(msg));
		}

		// Method to hide loader
		public void hideLoader() {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					try {
						if (progressDialog != null && progressDialog.isShowing())
							progressDialog.dismiss();
						progressDialog = null;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}

		// Method to show toast
		public void showToast(String strMsg) {
			Toast.makeText(BaseActivity.this, strMsg, Toast.LENGTH_SHORT).show();
		}


		public void onButtonNoClick(String from) {

		}

		public void onButtonYesClick(String from) {

		}

		class RunShowLoader implements Runnable {
			private String strMsg;

			public RunShowLoader(String strMsg) {
				this.strMsg = strMsg;
			}

			@Override
			public void run() {
				try {
					if (progressDialog == null
							|| (progressDialog != null && !progressDialog
									.isShowing())) {
						progressDialog = ProgressDialog.show(BaseActivity.this, "",
								strMsg);
					}
				} catch (Exception e) {
					progressDialog = null;
				}
			}
		}


		public void hideKeyBoard(View v) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

		}

		public void showSoftKeyboard(View view) {
			if (view.requestFocus()) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
			}
		}

		public static void hideKeyboard(Activity activity) {
			if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
				InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
			}
		}
		
		public void showSettingsScreen()
		{
			 Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
		     intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		     startActivity(intent);
		}


}
