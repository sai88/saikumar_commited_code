package com.sageabletech.shomiround;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sageabletech.R;
import com.sageabletech.utils.Urls;
import com.sageabletech.webservicecalls.AsyncRemoteCall;
import com.sageabletech.webservicecalls.AsyncRemoteCall.OnDataListener;
import com.sageabletech.webservicecalls.RequestParams;
import com.sageabletech.webservicecalls.ServiceCalls;

import java.util.regex.Pattern;

public class ForgetPasswordScreen extends BaseActivity implements OnDataListener {

    private EditText mEmail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_forget);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView tvTitle = (TextView)toolbar.findViewById(R.id.tvTitle);
        tvTitle.setText("Forgot Password");

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_cross_white);


        TextView tvSubmit = (TextView)findViewById(R.id.tvSubmit);
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               doSubmit();
            }
        });

        mEmail = (EditText)findViewById(R.id.input_user);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void doSubmit() {

      if(mEmail.getText().toString().length()>0){
          showLoader();

          RequestParams requestParams = new RequestParams();
          String url = Urls.FORGET_PASSWORD+mEmail.getText().toString();
          requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
          requestParams.typeOfRequest = AsyncRemoteCall.GET_REQUEST;

          ServiceCalls serviceCalls = new ServiceCalls(this, "FORGET");
          serviceCalls.execute(requestParams);
      }else{
          Toast.makeText(ForgetPasswordScreen.this, "'Please enter registered email ID", Toast.LENGTH_SHORT).show();
      }
    }

    @Override
    public void onData(Object data, String error, String tag) {

        hideLoader();
        if(error == null){
            Toast.makeText(this, ""+data.toString(), Toast.LENGTH_SHORT).show();
            finish();
        }else{
            Toast.makeText(this, ""+error, Toast.LENGTH_SHORT).show();
        }

    }
}
