package com.sageabletech.shomiround;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.sageabletech.R;
import com.sageabletech.adapter.SectionsPagerAdapter;
import com.sageabletech.model.LoginDO;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;
import com.sageabletech.webservicecalls.AsyncRemoteCall;
import com.sageabletech.webservicecalls.AsyncRemoteCall.OnDataListener;
import com.sageabletech.webservicecalls.RequestParams;
import com.sageabletech.webservicecalls.ServiceCalls;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.regex.Pattern;

public class Login extends AppCompatActivity implements OnDataListener {

    private CallbackManager callbackManager;
    private TextView textView;

    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;

    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            Profile profile = Profile.getCurrentProfile();
            Uri profilePictureUri = profile.getProfilePictureUri(100, 100);
            System.out.println("profilePictureUri::"+profilePictureUri.getPath());
            preferenceUtils.saveProfileImageURL("SelectedImage",profilePictureUri.getPath());

            getEmailFromFacebook(loginResult,profile.getId());
            displayMessage(profile);
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };

    private void getEmailFromFacebook(LoginResult loginResult,final String fId) {
//        getemailFormFacebook(loginResult);
        // Facebook Email address
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        Log.e("LoginActivity Response ", "facebook response:::"+response.toString());

                        try {
                            String FEmail="";
                            if (object.has("email")) {
                                 FEmail = object.getString("email");
                                System.out.println("email::" + FEmail);
                                if(object.has("first_name")){
                                    firstName = object.getString("first_name");

                                }
                                if(object.has("last_name")){
                                    LastName = object.getString("last_name");

                                }

                                System.out.println("FEmail:::"+FEmail);
                                Log.e("Email = ", " " + FEmail);
                                Toast.makeText(getApplicationContext(), "Login as " + FEmail, Toast.LENGTH_LONG).show();


                            }
                            doLogin(FEmail,fId);



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
//        parameters.putString("fields", "id,name,email,gender, birthday");
        parameters.putString("fields", "id,name,first_name, last_name, email,link");
        request.setParameters(parameters);
        request.executeAsync();


    }


    private ProgressDialog progressDialog;

    private void doLogin(String fEmail,String fId) {



                JSONObject loginRequest = new JSONObject();
                try {
                    loginRequest.put("deviceType","mobile");
                    loginRequest.put("userLoginId",fId);
                    loginRequest.put("emailId",""+fEmail);
                    loginRequest.put("password","");
                    loginRequest.put("loginUsing","FACEBOOK");
                    loginRequest.put("deviceToken","3BBB848D-79D6-4880-BBF6-DF645756493B");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    if (progressDialog == null
                            || (progressDialog != null && !progressDialog
                            .isShowing())) {
                        progressDialog = ProgressDialog.show(Login.this, "", "Logging in..");
                    }
                } catch (Exception e) {
                    progressDialog = null;
                }

                RequestParams requestParams = new RequestParams();
                String url = Urls.POST_LOGIN;
//                response.getStatusLine().getStatusCode()
                requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
                requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;
                requestParams.data = loginRequest.toString();

                ServiceCalls serviceCalls = new ServiceCalls(this, "Login");
                serviceCalls.execute(requestParams);
//new MakeLogin(Login.this).execute();





    }

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    public PreferenceUtils preferenceUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferenceUtils = new PreferenceUtils(Login.this);

        preferenceUtils.saveBoolean("isFirstTime",true);
        if(preferenceUtils.getStringFromPreference("userId","").length()>0){
            startActivity(new Intent(this, MainScreen.class));
            finish();
            return;
        }else{


        }

        initFacebook();

        setContentView(R.layout.login_signup);
        callFbAction();

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());


        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        LocalData.getInstance().setLoginadapter(mViewPager);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);





    }
    LinearLayout loginButton;
    public void callFbAction() {

        loginButton = (LinearLayout) findViewById(R.id.loginButtonFacebook);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facebookAction();

            }
        });

    }

String facebookprofilepic="";
    String firstName="";
    String LastName="";

    public void facebookAction() {

        System.out.println("facebook login::"+"came");

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {


                        System.out.println("fb login sucess"+loginResult);
                        AccessToken token = loginResult.getAccessToken();
                        String tokenTxt = token.getToken();
                        System.out.println("tokenTxt::" + tokenTxt);
                        System.out.println("name::" + token.getSource());

                        System.out.println("fb userId::" + token.getUserId());


                        facebookprofilepic="https://graph.facebook.com/"+ token.getUserId()+"/picture?type=large";
                        facebookFlag=true;
                            System.out.println("profile pic path:::"+facebookprofilepic);


                        getEmailFromFacebook(loginResult,token.getUserId());
                    }

                    @Override
                    public void onCancel() {
                        System.out.println("fb login canceled");
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        System.out.println("fb login failed");
                        Toast.makeText(Login.this,"Login failed",Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void initFacebook() {

        FacebookSdk.sdkInitialize(getApplicationContext());

        callbackManager = CallbackManager.Factory.create();
//
//        accessTokenTracker= new AccessTokenTracker() {
//            @Override
//            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {
//
//            }
//        };
//
//        profileTracker = new ProfileTracker() {
//            @Override
//            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
//                displayMessage(newProfile);
//            }
//        };
//
//        accessTokenTracker.startTracking();
//        profileTracker.startTracking();



    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    private void displayMessage(Profile profile){
        if(profile != null){
            Log.e("profile.getName()",""+profile.getName());

        }
    }

    @Override
    public void onStop() {
        super.onStop();
//        accessTokenTracker.stopTracking();
//        profileTracker.stopTracking();
    }

    @Override
    public void onResume() {
        super.onResume();



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

boolean facebookFlag=false;
    boolean uploadFlag=false;
    @Override
    public void onData(Object data, String error, String tag) {
        System.out.println("LOGIN DATasdasdasdAA::"+error);
        if(progressDialog !=null)
            if(progressDialog.isShowing())
                progressDialog.cancel();

        if(error == null){
            LoginDO mLogin = (LoginDO) data;
            preferenceUtils.saveString("userId",mLogin.userId);
            preferenceUtils.saveString("accessToken",mLogin.accessToken);
//            System.out.println("image saveddd:::"+mLogin.userPicPath);
            if(mLogin.userPicPath==null||mLogin.userPicPath.equalsIgnoreCase("")){
                if(facebookFlag){
                    facebookFlag=false;
                    preferenceUtils.saveProfileImageURL("SelectedImage",facebookprofilepic);
                    uploadFlag=true;
                }

            }else{
                preferenceUtils.saveProfileImageURL("SelectedImage",mLogin.userPicPath);
            }

            preferenceUtils.saveString("emailId",mLogin.emailId);

            if(mLogin.firstName==null||mLogin.firstName.equalsIgnoreCase("")){
               if(firstName.equalsIgnoreCase("")){
                   uploadFlag=true;
               }
                preferenceUtils.saveString("firstName",firstName);

            }else{
                preferenceUtils.saveString("firstName",mLogin.firstName);
            }
            if(mLogin.lastName==null||mLogin.lastName.equalsIgnoreCase("")){
                if(LastName.equalsIgnoreCase("")){
                    uploadFlag=true;
                }
                preferenceUtils.saveString("lastName",LastName);


            }else{
                preferenceUtils.saveString("lastName",mLogin.lastName);
            }
            if(uploadFlag){
                String userId = preferenceUtils.getStringFromPreference("userId", "");
                String firstName = preferenceUtils.getStringFromPreference("firstName", "");
                String lastName = preferenceUtils.getStringFromPreference("lastName", "");
                String SelectedImage = preferenceUtils.getStringFromPreference("SelectedImage", "");
                serviceCallToUpdateProfile(userId,firstName,lastName,SelectedImage);
                startActivity(new Intent(Login.this, MainScreen.class));
                finish();

//                serviceCallToUpdateProfile(userId,firstName,lastName,SelectedImage);
            }else{
                startActivity(new Intent(Login.this, MainScreen.class));
                finish();
            }


        }else if(error.contains("Invalid Username")) {
            mViewPager.setCurrentItem(1);

        }else{
//            Toast.makeText(Login.this,""+error,Toast.LENGTH_LONG).show();
        }






    }
    private void serviceCallToUpdateProfile(String userId,String firstNameString,String lastNameString,String userPic) {
        System.out.println("profile updateddddddddd...........");
//        showLoader();
        RequestParams requestParams = new RequestParams();
        String accessToken = this.preferenceUtils.getStringFromPreference("accessToken", null);
        String url = Urls.EDIT_PROFILE + accessToken;
        requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");
        requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;

        JSONObject loginRequest = new JSONObject();
        try {
            loginRequest.put("email", preferenceUtils.getStringFromPreference("emailId", ""));
            loginRequest.put("userId", userId);
            loginRequest.put("firstName", firstNameString);
            loginRequest.put("lastName", lastNameString);
            loginRequest.put("userPic",userPic);
            loginRequest.put("userLoginId", "");
            requestParams.data = loginRequest.toString();
            ServiceCalls serviceCalls = new ServiceCalls(this, "edit");
            serviceCalls.execute(requestParams);
            startActivity(new Intent(Login.this, MainScreen.class));
            finish();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    public void showLoader() {
        runOnUiThread(new RunShowLoader("Loading..."));
    }

    public void hideLoader() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    progressDialog = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void changeTabToLogin() {
        mViewPager.setCurrentItem(0);

    }

    class RunShowLoader implements Runnable {
        private String strMsg;

        public RunShowLoader(String strMsg) {
            this.strMsg = strMsg;
        }

        @Override
        public void run() {
            try {
                if (progressDialog == null
                        || (progressDialog != null && !progressDialog
                        .isShowing())) {
                    progressDialog = ProgressDialog.show(Login.this, "",
                            strMsg);
                }
            } catch (Exception e) {
                progressDialog = null;
            }
        }
    }

}
