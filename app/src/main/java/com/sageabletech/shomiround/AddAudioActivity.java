package com.sageabletech.shomiround;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.session.MediaController;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sageabletech.R;
import com.sageabletech.dialog.AlertDialogMsg;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AddAudioActivity extends BaseActivity {

    ImageView ivStart, ivStop, ivPlay,
            buttonStopPlayingRecording;
    String AudioSavePathInDevice = null;
    MediaRecorder mediaRecorder;
    Random random;
    String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
    public static final int RequestPermissionCode = 1;
    boolean isPressed = false;
    String audioPath = "";
    TextView tvTime;
    String path = "";
    LinearLayout AudioCont;
    TextView currenttime_rec;
    TextView timeAudio_rec;
    SeekBar recordSeek;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.audio_screen);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");


        ImageView imageView = (ImageView) findViewById(R.id.imageView);

        tvTime = (TextView) findViewById(R.id.tvtime);
        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                finish();
            }
        });


        requestPermission();

        if (getIntent().getStringExtra("SelectedImage") != null) {


//

            Glide.with(this)
                    .load(getIntent().getStringExtra("SelectedImage"))
                    .override(AppConstants.DEVICE_DISPLAY_WIDTH / 2, AppConstants.DEVICE_DISPLAY_WIDTH / 2).into(imageView);
        }
        if (getIntent().getStringExtra("path") != null) {
            String path1 = getIntent().getStringExtra("path");
            System.out.println("path1123::" + path1);
            if (path1.contains("https://s3.amazonaws.com")) {
                path = path1;
            }


        }


        if (!path.equalsIgnoreCase("")) {

            AudioCont = (LinearLayout) findViewById(R.id.ll_audio_play);

            AudioCont.setVisibility(View.VISIBLE);
            getMuusicCont(path);

        } else {
            AudioCont = (LinearLayout) findViewById(R.id.ll_audio_play);
            AudioCont.setVisibility(View.GONE);

        }


        ivStart = (ImageView) findViewById(R.id.img_record);
        ivStop = (ImageView) findViewById(R.id.img_stop);
        ivPlay = (ImageView) findViewById(R.id.img_play);
        buttonStopPlayingRecording = (ImageView) findViewById(R.id.button4);

        ivStop.setEnabled(true);
        ivPlay.setEnabled(true);
        buttonStopPlayingRecording.setEnabled(true);

        handler = new Handler();
        isRecording = false;

        random = new Random();

        ivStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(AddAudioActivity.this,"start clicked",Toast.LENGTH_SHORT).show();
                currenttime_rec = (TextView) findViewById(R.id.currenttime_rec);
                recordSeek = (SeekBar) findViewById(R.id.seekBar1);
                recordSeek.setMax(300);

                ivStop.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_stop_white));
                ivStart.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_record_gray));
                ivPlay.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_play_outline_white));
                ivStart.setEnabled(false);
                ivStop.setEnabled(true);
                ivPlay.setEnabled(false);


                System.out.println("checkPermission::" + checkPermission());

                if (checkPermission()) {
                    System.out.println("audio recordinggggg.... with path");
                    makeFloder();
                    AudioSavePathInDevice =
                            Environment.getExternalStorageDirectory().getAbsolutePath() + "/shomi/" +
                                    CreateRandomAudioFileName(5) + "-AudioRecording.m4a";

                    MediaRecorderReady();

                    try {
                        mediaRecorder.prepare();
                        mediaRecorder.start();


                        isRecording = true;

                    } catch (IllegalStateException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }



                    startTimer();
                    Toast.makeText(AddAudioActivity.this, "Recording...",
                            Toast.LENGTH_LONG).show();



                } else {
                    requestPermission();
                }

            }


        });
        ivStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ivStop.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_stop_gray));
                ivStart.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_record_white));
                ivPlay.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_play_outline_white));
                ivStart.setEnabled(true);
                ivStop.setEnabled(false);
                ivPlay.setEnabled(true);
                String audio_time = tvTime.getText().toString();

                if (audio_time == null || "".equalsIgnoreCase(audio_time)) {


                } else {
                    savetoDraft();

                }
            }
        });

        ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) throws IllegalArgumentException,
                    SecurityException, IllegalStateException {

                ivStop.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_stop_gray));
                ivStart.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_record_white));
                ivPlay.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_play_outline_gray));
                ivStart.setEnabled(true);
                ivStop.setEnabled(false);
                ivPlay.setEnabled(false);



                if (getIntent().getStringExtra("Audio") != null) {

                    AudioCont = (LinearLayout) findViewById(R.id.ll_audio_play);

                    AudioCont.setVisibility(View.VISIBLE);


                    getMuusicCont(getIntent().getStringExtra("Audio"));


//                    tvTime.setText(mediaPlayer.getDuration());
                } else {
                    Toast.makeText(AddAudioActivity.this, "No New Recording", Toast.LENGTH_LONG).show();
                }

            }
        });

    }


    private void savetoDraft() {
        try {

            new AlertDialogMsg(AddAudioActivity.this, "Are you want to save this Audio").setPositiveButton("YES", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    String audio_time = tvTime.getText().toString();
                    if (audio_time == null || "".equalsIgnoreCase(audio_time)) {
                        audio_time = "0.00";
                    }


                    try {
                        mediaRecorder.stop();
                    } catch (RuntimeException stopException) {
                        //handle cleanup here
                    }


                    ivStop.setEnabled(false);
                    ivPlay.setEnabled(true);
                    ivStart.setEnabled(true);
                    buttonStopPlayingRecording.setEnabled(false);
//                    ivStart.setImageResource(R.mipmap.ic_record_red);

                    isRecording = false;
                    stopTimer();
//                    play.setVisibility(View.GONE);
//                    existingText.setVisibility(View.GONE);
//                    stop.setVisibility(View.GONE);
                    System.out.println("AudioSavePathInDevice::" + AudioSavePathInDevice);
                    Intent in = new Intent();
                    in.putExtra("AudioSavePathInDevice", "" + AudioSavePathInDevice);
                    in.putExtra("audio_time", "" + audio_time);
                    setResult(RESULT_OK, in);
                    if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                        mediaPlayer.stop();
                    }
                    finish();

                }

            }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    mediaRecorder.stop();

                    if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                        mediaPlayer.stop();
                    }
                    finish();
                }
            }).create().show();

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isRecording = false;
        stopTimer();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                isRecording = false;
                if (!LocalData.getInstance().isFromedittour())
                    savetoDraft();

//                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void MediaRecorderReady() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        mediaRecorder.setAudioSamplingRate(16000);
        mediaRecorder.setAudioChannels(1);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
    }

    public String CreateRandomAudioFileName(int string) {
        StringBuilder stringBuilder = new StringBuilder(string);
        int i = 0;
        while (i < string) {
            stringBuilder.append(RandomAudioFileName.
                    charAt(random.nextInt(RandomAudioFileName.length())));

            i++;
        }
        return stringBuilder.toString();
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(AddAudioActivity.this, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {
                        /*Toast.makeText(AddAudioActivity.this, "Permission Granted",
                                Toast.LENGTH_LONG).show();*/
                    } else {
                        Toast.makeText(AddAudioActivity.this, "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

    Handler handler = null;
    int count = 0;

    public void startTimer() {
        count = 0;
        handler.post(UpdateRecordTime);
    }

    public void stopTimer() {
        if (handler != null) {
            handler.removeCallbacks(UpdateRecordTime);
        }
    }

    Boolean isRecording;
    Runnable UpdateRecordTime = new Runnable() {
        public void run() {
            if (isRecording) {
//                tvTime.setText(Utils.recordTime(count));
                if (count > 299) {
                    stopTimer();
                    mediaRecorder.stop();
                } else {

                    tvTime.setText(secToTime(count));
                    currenttime_rec.setText(secToTime(count) + " /");
                    recordSeek.setProgress(count);

//                    recordSeek.setpr

                    count += 1;
                    // Delay 1s before next call
                    handler.postDelayed(this, 1000);
                }
            }
        }
    };

    String secToTime(int sec) {
        int second = sec % 60;
        int minute = sec / 60;
        if (minute >= 60) {
            int hour = sec / 60;
            minute %= 60;
            return hour + ":" + (minute < 10 ? "0" + minute : minute) + ":" + (second < 10 ? "0" + second : second);
        }
        return minute + ":" + (second < 10 ? "0" + second : second);
    }

    public void makeFloder() {
        File f = new File(Environment.getExternalStorageDirectory() + "/shomi");
        if (f.isDirectory()) {
            //Write code for the folder exist condition

        } else {

            // create a File object for the parent directory
            File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/shomi");
            // have the object build the directory structure, if needed.
            wallpaperDirectory.mkdirs();
            // create a File object for the output file


        }
    }



    @Override
    protected void onStop() {
        super.onStop();
        MediaPlayer mediaPlayer = LocalData.getInstance().getMediaPlayer();
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
    }


    private Button b2,b3;
    private MediaPlayer mediaPlayer;

    private double startTime = 0;
    private double finalTime = 0;

    private Handler myHandler = new Handler();;

    private SeekBar seekbar;
    private TextView tx1,tx2;

    public static int oneTimeOnly = 0;
    public void getMuusicCont(String path) {
        b2 = (Button) findViewById(R.id.button2);
        b3 = (Button)
                findViewById(R.id.button3);
//
        tx1 = (TextView) findViewById(R.id.textView2);
        tx2 = (TextView) findViewById(R.id.textView3);

        Uri uri = Uri.parse(path);

        mediaPlayer = MediaPlayer.create(this, uri);
        LocalData.getInstance().setMediaPlayer(mediaPlayer);
        seekbar = (SeekBar) findViewById(R.id.seekBar);
        seekbar.setClickable(false);
        LocalData.getInstance().setSeekBar(seekbar);
        b2.setEnabled(false);
        if (mediaPlayer != null) {
            finalTime = mediaPlayer.getDuration();
            tx2.setText(getTimeTotal());


        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(), "Playingsound",Toast.LENGTH_SHORT).show();
                mediaPlayer.start();
                b2.setVisibility(View.VISIBLE);
                b3.setVisibility(View.GONE);

                finalTime = mediaPlayer.getDuration();
                startTime = mediaPlayer.getCurrentPosition();

//                if (oneTimeOnly == 0) {
                seekbar.setMax((int) finalTime);
                oneTimeOnly = 1;
//                }

                tx2.setText(getTimeTotal());


                tx1.setText(getRunningTime());

                seekbar.setProgress((int) startTime);
                myHandler.postDelayed(UpdateSongTime, 100);
                b2.setEnabled(true);
                b3.setEnabled(false);

            }
        });
            b2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                Toast.makeText(getActivity(), "Pausing sound",Toast.LENGTH_SHORT).show();
                    mediaPlayer.pause();
                    b2.setEnabled(false);
                    b2.setVisibility(View.GONE);
                    b3.setVisibility(View.VISIBLE);
                    b3.setEnabled(true);
                }
            });
    }



    }

    public String getTimeTotal(){
        long minutes = TimeUnit.MILLISECONDS.toMinutes((long) finalTime);
        long seconds = TimeUnit.MILLISECONDS.toSeconds((long) finalTime)-
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) finalTime));
        String sec = String.valueOf(seconds);
        String min = String.valueOf(minutes);

        if (sec.length() == 1) {
            sec = "0" + sec;
        }
        if (min.length() == 1) {
            min = "0" + min;
        }
        return "-"+min+":"+sec;

    }
    public String getTotalTimeDecrease(){
        long minutesTotal = TimeUnit.MILLISECONDS.toMinutes((long) finalTime);
        long secondstotal = TimeUnit.MILLISECONDS.toSeconds((long) finalTime)-
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) finalTime));

        long minutes = TimeUnit.MILLISECONDS.toMinutes((long) startTime);
        long seconds= TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) startTime));

        long presentMin=minutesTotal-minutes;
        long presentsec=secondstotal-seconds;

        String sec = String.valueOf(presentsec);
        String min = String.valueOf(presentMin);

        if (sec.length() == 1) {
            sec = "0" + sec;
        }
        if (min.length() == 1) {
            min = "0" + min;
        }
        return "-"+min+":"+sec;
    }
    public String getRunningTime(){
        long minutes = TimeUnit.MILLISECONDS.toMinutes((long) startTime);
        long seconds= TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) startTime));
        String sec = String.valueOf(seconds);
        String min = String.valueOf(minutes);

        if (sec.length() == 1) {
            sec = "0" + sec;
        }
        if (min.length() == 1) {
            min = "0" + min;
        }
        return min+":"+sec;

    }

    private Runnable UpdateSongTime = new Runnable() {
        public void run() {
            startTime = mediaPlayer.getCurrentPosition();
            tx1.setText(getRunningTime());
            tx2.setText(getTotalTimeDecrease());
            seekbar.setProgress((int)startTime);
            myHandler.postDelayed(this, 100);
        }
    };


}