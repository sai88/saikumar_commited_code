package com.sageabletech.shomiround;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sageabletech.R;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.utils.AppConstants;

public class MyTourDetails extends AppCompatActivity {

    MyTourItemDO mTourItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_tour_add);

        mTourItem = (MyTourItemDO) getIntent().getSerializableExtra("Details");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_cross_white);

        TextView tvTitle = (TextView)findViewById(R.id.tvTitle);
        tvTitle.setText(mTourItem.title);


        ImageView ivMap = (ImageView)findViewById(R.id.ivMap);
        ivMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyTourDetails.this,MapsActivity.class)
                        .putExtra("tourLangitude",mTourItem.tourLangitude)
                        .putExtra("tourLatitude",mTourItem.tourLatitude)
                        .putExtra("title",mTourItem.title));
            }
        });

        ImageView ivSelectedImage = (ImageView)findViewById(R.id.ivSelectedImage);

        ivSelectedImage.setLayoutParams(new LinearLayout.LayoutParams(AppConstants.DEVICE_DISPLAY_WIDTH , AppConstants.DEVICE_DISPLAY_WIDTH/2));

        Glide.with(this)
                .load(mTourItem.tourPhotoPath)
                .placeholder(R.color.black)
                .centerCrop()
                .override(AppConstants.DEVICE_DISPLAY_WIDTH , AppConstants.DEVICE_DISPLAY_WIDTH/2)
                .crossFade()
                .into(ivSelectedImage);


        TextView tvDetails = (TextView)findViewById(R.id.tvDetails);
        tvDetails.setText(""+mTourItem.summary);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
