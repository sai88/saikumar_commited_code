package com.sageabletech.shomiround;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sageabletech.R;
import com.sageabletech.model.AddTourDO;
import com.sageabletech.model.ImagesDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.newclassess.AddTourScreenNew;
import com.sageabletech.newclassess.FinalScreenNew;
import com.sageabletech.newclassess.WordUtils;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.Utils;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class ImagesSelection extends BaseActivity {

    static int selectedPos = -1;
    ImageAdapter mImageAdapter;
    private static final int TAKE_PICTURE = 1;
    private Uri imageUri;

    private ArrayList<ImagesDO> images;
    String from;
    AddTourDO mAddTourDO = null;

    ImageView right;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_gallery);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView tvTitle = (TextView) toolbar.findViewById(R.id.tvTitle);
        tvTitle.setText("CAMERA ROLL");

        System.out.println("CAME" + "IMAGESELECTIOn");

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_cross_white);

//        checkPermissionAlert(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
//            Toast.makeText(AddTourScreenNew.this, "Not Called", Toast.LENGTH_SHORT).show();
            String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE};
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions, 111);
            }

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }


        init();


    }

    public ArrayList<TourItem> tourPhotos = new ArrayList<>();

    private void init() {

        from = getIntent().getStringExtra("From");

        if (from.equalsIgnoreCase("AddTour")) {
            mAddTourDO = (AddTourDO) getIntent().getSerializableExtra("SelectedData");
            tourPhotos = mAddTourDO.tourPhotos;
        }

        GridView gallery = (GridView) findViewById(R.id.galleryGridView);

        mImageAdapter = new ImageAdapter(this);

        gallery.setAdapter(mImageAdapter);

        right = (ImageView) findViewById(R.id.right);

        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v,
                                    int position, long arg3) {
                if (null != images && !images.isEmpty() && !from.equalsIgnoreCase("AddTour")) {

                    selectedPos = position;
                    right.setVisibility(View.VISIBLE);
                    System.out.println("ifffffff");
                    if(images.get(position).isSelect){
                        images.get(position).isSelect = false;
                        mImageAdapter.notifyDataSetChanged();
                    }else{
                        images.get(position).isSelect = true;

                        mImageAdapter.notifyDataSetChanged();
                    }
                    mImageAdapter.notifyDataSetChanged();

                } else if (from.equalsIgnoreCase("AddTour")) {
                    images.get(position).isSelect = true;
                    System.out.println("eleeeeeee");
                    right.setVisibility(View.VISIBLE);
                    if(images.get(position).isSelect){
                        images.get(position).isSelect = false;
                        mImageAdapter.notifyDataSetChanged();
                    }else{
                        images.get(position).isSelect = true;

                        mImageAdapter.notifyDataSetChanged();
                    }
                    mImageAdapter.notifyDataSetChanged();
                }

            }
        });

        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedPos >= 0 && null != images && !images.isEmpty() && !from.equalsIgnoreCase("AddTour")) {
                    startActivityForResult(new Intent(ImagesSelection.this, AddTourScreenNew.class).putExtra("SelectedImage", images.get(selectedPos).imageName.toString()).putExtra("From", "Tour"), 2002);
                } else if (from.equalsIgnoreCase("AddTour")) {

                    ArrayList<TourItem> selectedImages = new ArrayList<TourItem>();
                    for (ImagesDO d : images) {
                        if (d.isSelect) {
                            TourItem mTourItem = new TourItem();
                            mTourItem.ImagePath = d.imageName;
                            selectedImages.add(mTourItem);
                        }

                    }
                    /*TourItem img = new TourItem();
                    img.ImagePath ="Plus";
                    selectedImages.add(img);*/

                    mAddTourDO.tourPhotos = selectedImages;

                    startActivityForResult(new Intent(ImagesSelection.this, FinalScreenNew.class).putExtra("SelectedData", mAddTourDO), 2002);
                }
            }
        });

        ImageView cam = (ImageView) findViewById(R.id.cam);
        cam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File photo = new File(Environment.getExternalStorageDirectory(), "Pic.jpg");
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photo));
                imageUri = Uri.fromFile(photo);
                startActivityForResult(intent, TAKE_PICTURE);
            }
        });

    }

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    /*private boolean checkPermissionAlert(final Context context) {

        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,Manifest.permission.READ_EXTERNAL_STORAGE)) {
//                    showDialog("External storage", context,Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat.requestPermissions((Activity) context,
                                    new String[] { Manifest.permission.READ_EXTERNAL_STORAGE },
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }

    }*/


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
//            Toast.makeText(AddTourScreenNew.this, "Not Called 1", Toast.LENGTH_SHORT).show();
        }

        init();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2002 && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else if (requestCode == TAKE_PICTURE && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = imageUri;

            if (selectedImage != null && !from.equalsIgnoreCase("AddTour")) {
                System.out.println("camera pic:::"+selectedImage.toString());
                WordUtils.compressImage(selectedImage.toString());
                startActivityForResult(new Intent(ImagesSelection.this, AddTourScreenNew.class).putExtra("SelectedImage", selectedImage.toString()).putExtra("From", "Tour"), 2002);
            }

        } else if (requestCode == 2002 && requestCode == 143) {

            tourPhotos = (ArrayList<TourItem>) data.getSerializableExtra("images");
            mImageAdapter.notifyDataSetChanged();

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        selectedPos = -1;

    }


    private class ImageAdapter extends BaseAdapter {

        /**
         * The context.
         */
        private Activity context;

        /**
         * Instantiates a new image adapter.
         *
         * @param localContext the local context
         */
        public ImageAdapter(Activity localContext) {
            context = localContext;
            images = Utils.getFilePaths(localContext);
        }

        public int getCount() {
            return images.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            final ViewHolder holder;

            if (convertView == null) {

                convertView = LayoutInflater.from(context).inflate(R.layout.grid_item, null, false);
                holder = new ViewHolder();
                holder.img = (ImageView) convertView.findViewById(R.id.imageView1);
                holder.tv = (CheckBox) convertView.findViewById(R.id.checkBox1);
                holder.imageselect = (ImageView) convertView.findViewById(R.id.imageselect);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);
            convertView.setLayoutParams(new GridView.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2, (AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2));


                /*Picasso.with(context)
                .load(images.get(position).imagePath)
            	.centerCrop().resize((width/3)-2, (width/3)-2)
            	.placeholder(R.color.black)
            	.into(holder.img);*/

            Glide.with(context)
                    .load(images.get(position).imageName)
                    .centerCrop()
                    .placeholder(R.color.black)
//                	.diskCacheStrategy(DiskCacheStrategy.ALL)
//                	.override((width/3)-2, (width/3)-2)
                    .thumbnail(0.5f)
                    .crossFade()
                    .into(holder.img);


            if (!from.equalsIgnoreCase("AddTour")) {
                if (selectedPos != -1 && selectedPos == position) {

                    holder.tv.setChecked(true);

                } else {
                    holder.tv.setChecked(false);
                }
            } else {
                holder.tv.setChecked(images.get(position).isSelect);
            }

            if (tourPhotos.size() > 0) {
                if (images.contains(images.get(position).imageName)) {
                    holder.tv.setChecked(true);
                }
            }
            if(images.get(position).isSelect){
                holder.imageselect.setVisibility(View.VISIBLE);
            }else{
                holder.imageselect.setVisibility(View.GONE);
            }



//            if (holder.tv.isChecked()) {
//                holder.tv.setVisibility(View.VISIBLE);
//            } else {
//                holder.tv.setVisibility(View.GONE);
//            }

                /*holder.img.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(holder.tv.isChecked()){
							holder.tv.setChecked(false);
						}else{
							holder.tv.setChecked(true);
						}

						images.get(position).isSelect = holder.tv.isChecked();

					}
				});*/


            return convertView;
        }

        public class ViewHolder {
            CheckBox tv;
            ImageView img;
            ImageView imageselect;
        }

    }

    @SuppressLint("NewApi")
    private ArrayList<ImagesDO> allImages() {

        ArrayList<ImagesDO> imag = new ArrayList<ImagesDO>();

        String[] projection = {
                MediaStore.Files.FileColumns._ID,
                MediaStore.Files.FileColumns.DATE_ADDED,
                MediaStore.Files.FileColumns.MEDIA_TYPE,
                MediaStore.Files.FileColumns.MIME_TYPE
        };


        // Return only video and image metadata.
        String selection = MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

        Uri queryUri = MediaStore.Files.getContentUri("external");

        CursorLoader cursorLoader = new CursorLoader(
                this,
                queryUri,
                projection,
                selection,
                null, // Selection args (none).
                MediaStore.Files.FileColumns.DATE_ADDED + " DESC" // Sort order.
        );

//    	    images.clear();

        Cursor imagecursor = cursorLoader.loadInBackground();
    	    /*imagecursor = activity.getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
            		projection, null, null, "image_id DESC");*/


        if (imagecursor != null) {
            int image_column_index = imagecursor.getColumnIndex(MediaStore.Files.FileColumns._ID);
            int type_column_index = imagecursor.getColumnIndex(MediaStore.Files.FileColumns.MIME_TYPE);
            int count = imagecursor.getCount();
            for (int i = 0; i < count; i++) {
                imagecursor.moveToPosition(i);
                int id = imagecursor.getInt(image_column_index);
                String mime_type = imagecursor.getString(type_column_index);
                ImagesDO imageItem = new ImagesDO();
                imageItem.id = id;
                //lastId = id;

                if (!mime_type.contains("video")) {
                    imageItem.imageName = MediaStore.Images.Media.EXTERNAL_CONTENT_URI.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, String.valueOf(id)).toString();
                    imag.add(imageItem);
                }


            }

            //add ImageItem at top of list

            imagecursor.close();
        }
        return imag;


    }


}



