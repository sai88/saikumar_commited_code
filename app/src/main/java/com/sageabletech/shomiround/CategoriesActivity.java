package com.sageabletech.shomiround;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sageabletech.R;

import com.sageabletech.database.DBCreator;
import com.sageabletech.model.AddTourDO;
import com.sageabletech.model.CategoryDO;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.NetworkUtils;
import com.sageabletech.utils.Urls;
import com.sageabletech.utils.Utils;
import com.sageabletech.webservicecalls.AsyncRemoteCall;
import com.sageabletech.webservicecalls.RequestParams;
import com.sageabletech.webservicecalls.ServiceCalls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class CategoriesActivity extends BaseActivity implements AsyncRemoteCall.OnDataListener {

    static int selectedPos = -1;
    ArrayList<CategoryDO> mCategories=new ArrayList<CategoryDO>();
    ArrayList<CategoryDO> categoriesArr;
    private ListView categories_list;
    CategoriesAdapter1 categoriesAdapter1;
    ImageView back;
    ImageView right;
    CategoryDO categoryDO;
//    LocalData localData;
    AddTourDO addTourDO;
    ArrayList<CategoryDO> selectedCategoryDOs;
    private String from;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        categories_list=(ListView)findViewById(R.id.categories_list);
        mDBCreator = new DBCreator(this);
        SQLiteDatabase db = mDBCreator.getWritableDatabase();
        db.close();
        TextView tvTitle=(TextView)findViewById(R.id.tvTitle);
        tvTitle.setTypeface(Utils.getHelvaticaBold(CategoriesActivity.this));
        back =(ImageView)findViewById(R.id.back);
        right =(ImageView)findViewById(R.id.right);

        addTourDO= (AddTourDO) getIntent().getSerializableExtra("SelectedData");
//        String draftId=  getIntent().getStringExtra("From");
        String draftId=getIntent().getStringExtra("draftId");
        from = getIntent().getStringExtra("From");

        System.out.println("draftId::"+draftId);

        System.out.println("FROMINCATE"+from);
        if(LocalData.getInstance().getNewTour().equalsIgnoreCase("new")) {
            System.out.println("new entereddd....");
           selectedCategoryDOs= mDBCreator.getCategories(getIntent().getStringExtra("draftId"));
            System.out.println("FROMINCATE sai"+selectedCategoryDOs.size());

        }


        String str = ((BaseActivity)this).preferenceUtils.getStringFromPreference("accessToken","");



        categoriesAdapter1=new CategoriesAdapter1(this,mCategories);
        categories_list.setAdapter(categoriesAdapter1);

        System.out.println("accessToken"+str);
        if(NetworkUtils.isNetworkConnectionAvailable(this)) {
            prepareMovieData(str);
        }else {
            Toast.makeText(getApplicationContext(),"There is No Internet",Toast.LENGTH_LONG).show();
        }
        categories_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {



                selectedPos = position;




                System.out.println("mCategories.get(position).isSelect::"+mCategories.get(position).isSelect);
                if(mCategories.get(position).isSelect){
                    mCategories.get(position).isSelect = false;
                    categoriesAdapter1.notifyDataSetChanged();
                }else{
                    mCategories.get(position).isSelect = true;

                    categoriesAdapter1.notifyDataSetChanged();
                }


               /* if( mCategories.get(position).isSelect==true) {
                    categoryDO = mCategories.get(position);
                }*/


            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<CategoryDO> arrayList=new ArrayList<CategoryDO>();
//                int len = categories_list.getCount();


               /* SparseBooleanArray checked = categories_list.getCheckedItemPositions();
                for (int i = 0; i < mCategories.size(); i++)
                    if (checked.get(i)) {
                        CategoryDO item = mCategories.get(i);
                        arrayList.add(categoryDO);
           *//* do whatever you want with the checked item *//*
                    }*/

                categoriesArr=new ArrayList<CategoryDO>();

                for(int i=0;i<mCategories.size();i++) {
//                    CheckBox tv = (CheckBox) parent.findViewById(R.id.checkBox);
                    if(mCategories.get(i).isSelect){
                        System.out.println("mCategoriesNAME  "+mCategories.get(i).categorieName);
                        categoriesArr.add(mCategories.get(i));
                    }else{
                        categoriesArr.remove(mCategories.get(i));

                    }
                }

                if(categoriesArr.size()>0) {
                if(addTourDO!=null) {
                    addTourDO.tourPhotos = mDBCreator.getSubTourItems(getIntent().getStringExtra("draftId"));
                }
                    startActivityForResult(new Intent(CategoriesActivity.this, TagsActivity.class)

//                    startActivityForResult(new Intent(AddTourScreen.this, FinalScreen.class)
                            .putExtra("addTourDO", addTourDO)
                            .putExtra("From", getIntent().getStringExtra("From"))
                            .putExtra("draftId", getIntent().getStringExtra("draftId"))
                            .putExtra("SelectedData", categoriesArr), 2001);


                    mDBCreator.deleteCategories(getIntent().getStringExtra("draftId"));
                    mDBCreator.insertCategpries(getIntent().getStringExtra("draftId"), categoriesArr);
                }else{
                    Toast.makeText(CategoriesActivity.this,"Please select atleast one category",Toast.LENGTH_SHORT).show();
                }


                System.out.println("FORMIDDDD"+LocalData.getInstance().getFrom());

               /* if(localData.getFrom().equalsIgnoreCase("NEW")) {



                }else{
                    mDBCreator.updateCategories(localData.getSelectedDraftId(),categoriesArr);

                }*/


//                        .putExtra("addTourDO",addTourDO),2002);



            }
        });
    }
    public void doAction(){
        if(!LocalData.getInstance().getNewTour().equalsIgnoreCase("new")&&LocalData.getInstance().getFromDrafts().equalsIgnoreCase("no")){
            ArrayList<String> strings = LocalData.getInstance().getcatTourArry();
            System.out.println("strings::"+strings);
            if (strings != null && strings.size() > 0) {
                for (int i = 0; i < strings.size(); i++) {
                    String s = strings.get(i);
                    for (int j = 0; j < mCategories.size(); j++) {
                        CategoryDO categoryDO = mCategories.get(j);
                        String categorieId = categoryDO.categorieId;
                        if (s.equalsIgnoreCase(categorieId)) {
                            categoryDO.isSelect = true;
                            System.out.println("categoryDO:::"+categoryDO.isSelect);
//                            mCategories.add(j, categoryDO);
                            break;
                        }
                    }
                }
            }

        }else {

            if (selectedCategoryDOs != null && selectedCategoryDOs.size() > 0) {
                for (int i = 0; i < selectedCategoryDOs.size(); i++) {
                    CategoryDO categoryDO1 = selectedCategoryDOs.get(i);
                    String categorieId1 = categoryDO1.categorieId;
                    for (int j = 0; j < mCategories.size(); j++) {
                        CategoryDO categoryDO = mCategories.get(j);
                        String categorieId = categoryDO.categorieId;
                        if (categorieId1.equalsIgnoreCase(categorieId)) {
                            categoryDO.isSelect = true;
//                            mCategories.add( categoryDO);
                            break;
                        }
                    }
                }
            }
        }


    }

    private void prepareMovieData(String vendorId) {

        ((BaseActivity)this).showLoader();

        RequestParams requestParams = new RequestParams();
        String url = Urls.CATEGORIES_LIST+vendorId;
        requestParams.url = url.trim().replaceAll(Pattern.quote("+"), "%2B");

        System.out.println("Categories URL::"+url);
        requestParams.typeOfRequest = AsyncRemoteCall.POST_REQUEST_XML;
        JSONObject loginRequest = new JSONObject();
        try {
            loginRequest.put("tourEndOrderNumber","0");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        requestParams.data = loginRequest.toString();


        ServiceCalls serviceCalls = new ServiceCalls(CategoriesActivity.this, "listCategories");
        serviceCalls.execute(requestParams);


    }

    @Override
    public void onData(Object data, String error, String tag) {
        ((BaseActivity)this).hideLoader();
        if(error==null) {
            mCategories = (ArrayList<CategoryDO>) data;
            doAction();
            categoriesAdapter1.notifyDataSetChanged();
        }else{
            Toast.makeText(CategoriesActivity.this,"Could not connect server please try again..",Toast.LENGTH_SHORT).show();
        }



    }

    public class CategoriesAdapter1 extends BaseAdapter {
        @Override
        public long getItemId(int position) {
            return 0;
        }

        private Context mContext;
//        private ArrayList<CategoryDO> mCategoryDOs;
        public CategoriesAdapter1(Context context, ArrayList<CategoryDO> categoryDOs){


//            this.mCategoryDOs=categoryDOs;
            this.mContext=context;
        }

        @Override
        public int getCount() {
            return mCategories.size();
        }

        @Override
        public Object getItem(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();

                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.categories_list_adapter, null);
                holder.title = (TextView) convertView.findViewById(R.id.title);
                holder.imageselect = (ImageView) convertView.findViewById(R.id.imageselect);
                holder.tv = (CheckBox) convertView.findViewById(R.id.checkBox);
                convertView.setTag(holder);

            }
            else {
                holder = (ViewHolder)convertView.getTag();
            }
            holder.title.setText(mCategories.get(position).categorieName);
            holder.title.setTypeface(Utils.getHelvaticaBold(CategoriesActivity.this));
//
//}
            System.out.println("selectedPos::"+selectedPos);
            holder.tv.setTag(position); // This line is important.
            holder.tv.setChecked(mCategories.get(position).isSelect);

            if(mCategories.get(position).isSelect){
                holder.imageselect.setVisibility(View.VISIBLE);
            }else{
                holder.imageselect.setVisibility(View.GONE);
            }
            holder.tv.setEnabled(false);

//
//            if(holder.tv.isChecked()){
//                holder.tv.setVisibility(View.VISIBLE);
//            }else{
//                holder.tv.setVisibility(View.GONE);
//            }
            return convertView;
        }
        public void refresh(ArrayList<CategoryDO> data){
            mCategories=data;
        }
        public class ViewHolder
        {
            CheckBox tv;
            ImageView img;
            TextView title;
            ImageView imageselect;
        }

    }
    private void getDraftAlert(){

        AlertDialog.Builder builder=new AlertDialog.Builder(CategoriesActivity.this);
        builder.setMessage(getResources().getString(R.string.save_to_draft_request));
        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                categoriesArr=new ArrayList<CategoryDO>();

                for(int i=0;i<mCategories.size();i++) {
//                    CheckBox tv = (CheckBox) parent.findViewById(R.id.checkBox);
                    if(mCategories.get(i).isSelect){
                        System.out.println("mCategoriesNAME  "+mCategories.get(i).categorieName);
                        categoriesArr.add(mCategories.get(i));
                    }else{
                        categoriesArr.remove(mCategories.get(i));

                    }
                }

                mDBCreator.deleteCategories(getIntent().getStringExtra("draftId"));
                mDBCreator.insertCategpries(getIntent().getStringExtra("draftId"),categoriesArr);

                dialog.dismiss();
                onBackPressed();

            }
        });
        builder.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                onBackPressed();

                dialog.dismiss();
            }
        });

        builder.show();


    }

}
