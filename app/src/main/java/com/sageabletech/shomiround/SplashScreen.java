package com.sageabletech.shomiround;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.sageabletech.R;
import com.sageabletech.adapter.SplashPagerAdapter;
import com.sageabletech.newclassess.WordUtils;
import com.sageabletech.views.CirclePageIndicator;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class SplashScreen extends BaseActivity implements LocationListener {

    private ViewPager mViewPager = null;
    private SplashPagerAdapter mSplashPagerAdapter;
    Handler mHandler;
    Runnable mRunnable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        preferenceUtils = null;

        boolean isFirst = preferenceUtils.getbooleanFromPreference("isFirstTime",false);
        System.out.println("splash::::");
//        getLocation();

        Log.e("isFiirst",""+isFirst);
//        WordUtils.deleteFolder();
        if(isFirst){
            startActivity(new Intent(SplashScreen.this, Login.class));
            finish();
            return;
        }

        setContentView(R.layout.activity_splash_screen);
//        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        am.setTimeZone("America/Los_Angeles");

//        printHashKey();
        mSplashPagerAdapter = new SplashPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSplashPagerAdapter);

        CirclePageIndicator mCirclePageIndicator = (CirclePageIndicator)findViewById(R.id.circles);
        mCirclePageIndicator.setViewPager(mViewPager);

        mRunnable = new Runnable() {
            @Override
            public void run() {
//                preferenceUtils.saveBoolean("isFirstTime",true);
                startActivity(new Intent(SplashScreen.this, Login.class));
                finish();
            }
        };

        mHandler = new Handler();
        mHandler.postDelayed(mRunnable,3000);


        /*mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {

                if(position == 2){
                    startActivity(new Intent(SplashScreen.this,Login.class));
                    preferenceUtils.saveBoolean("isFirstTime",true);
                    finish();
                }

            }
        });*/

    }


    public void printHashKey(){
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.showmeround",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mHandler!=null){
            mHandler.removeCallbacks(mRunnable);
        }
    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
