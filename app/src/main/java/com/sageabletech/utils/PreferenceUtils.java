package com.sageabletech.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class PreferenceUtils 
{
	private static SharedPreferences preferences;
	private  static SharedPreferences.Editor edit;
	public static String KEY 			= 	"key";

    // All Shared Preferences Keys
    public static final String IS_LOGIN = "IsLoggedIn";
    // Email address
    public static final String KEY_EMAIL = "email";
    
    public static final String CURRENT_LOCATION_LATTITUDE = "current_lattitude";
    public static final String CURRENT_LOCATION_LONGITUDE = "current_longitude";
    
    public static final String OLD_LOCATION_LATTITUDE = "old_lattitude";
    public static final String OLD_LOCATION_LONGITUDE = "old_longitude";
    
    public static final String USER_ID       = "user_name";
    public static final String MOBILE_NUMBER = "mobile_number";
    
    public static final String PASSWORD  = "password";
    public static final String DEVICE_ID = "device_id";
    public static final String GCM_KEY   = "gcm_key";
    
    public static final String TRIP_ID   = "trip_id";
    
   public  static final String IS_TRIP_START = "is_trip_start";
    
    public static final String TOTAL_DISTANCE  = "total_distance_travelled";
    public static String IS_FIRST_TIME_LOGIN   = "is_first_time_login";
    
    public static String LAT_LNG_STRING = "lat_lng-string";
	public static String TRIP_STATUS 				= "TRIP_STATUS";


    public PreferenceUtils(Context context)
	{
		preferences		=	PreferenceManager.getDefaultSharedPreferences(context);
		edit			=	preferences.edit();
	}
	
	public static void saveString(String strKey,String strValue)
	{
		edit.putString(strKey, strValue);
		edit.commit();
	}
	public void saveProfileImageURL(String strKey,String strValue)
	{
		edit.putString(strKey, strValue);
		edit.commit();
	}
	public String getProfileImageURL(String strKey,String defaultValue )
	{
		return preferences.getString(strKey, defaultValue);
	}

	public static void saveInt(String strKey,int value)
	{
		edit.putInt(strKey, value);
		edit.commit();
	}
	
	public void saveLong(String strKey,Long value)
	{
		edit.putLong(strKey, value);
		edit.commit();
	}
	
	public void saveFloat(String strKey, float value)
	{
		edit.putFloat(strKey, value);
		edit.commit();
	}
	
	public void saveDouble(String strKey,String value)
	{
		edit.putString(strKey, value);
		edit.commit();
	}
	
	public void saveBoolean(String strKey,boolean value)
	{
		edit.putBoolean(strKey, value);
		edit.commit();
	}
	
	public void removeFromPreference(String strKey)
	{
		edit.remove(strKey);
	}
	
	public static String getStringFromPreference(String strKey,String defaultValue )
	{
		return preferences.getString(strKey, defaultValue);
	}
	
	public boolean getbooleanFromPreference(String strKey,boolean defaultValue)
	{
		return preferences.getBoolean(strKey, defaultValue);
	}
	
	public static int getIntFromPreference(String strKey,int defaultValue)
	{
		return preferences.getInt(strKey, defaultValue);
	}
	
	public long getLongFromPreference(String strKey)
	{
		return preferences.getLong(strKey, 0);
	}
	
	public float getFloatFromPreference(String strKey,float defaultValue)
	{
		return preferences.getFloat(strKey, defaultValue);
	}
	
	public double getDoubleFromPreference(String strKey,double defaultValue)
	{
		return	Double.parseDouble(preferences.getString(strKey, ""+defaultValue));
	}
	public void clearData(){
		edit.clear();
		edit.commit();
	}
}
