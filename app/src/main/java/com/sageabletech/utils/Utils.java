package com.sageabletech.utils;


import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.format.DateFormat;

import com.sageabletech.model.ImagesDO;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.SortedSet;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

public class Utils {
	public static int HEIGHT;
	public static int WIDTH;
	public static String IMAGE_FILE_PATH = "";
	public static int SDK_VERSION;

	public static final String SERVER_ERROR = "The server isn't responding. Please try again later.";


	public static ArrayList<ImagesDO> getFilePaths(Activity context) {
		boolean imageListFlag = LocalData.getInstance().isImageListFlag();
		ArrayList<ImagesDO> galleryList = LocalData.getInstance().getGalleryList();
		if(imageListFlag||galleryList.size()==0) {
			LocalData.getInstance().setImageListFlag(false);


			Uri u = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//			String[] projection = {MediaStore.Images.ImageColumns.DATA, MediaStore.Images.ImageColumns._ID};

			String[] projection = { MediaStore.MediaColumns.DATA,
					MediaStore.Images.ImageColumns.DATE_TAKEN };
//			String[] columns = {MediaStore.Images.Media._ID, MediaStore.Images.ImageColumns.DATE_TAKEN};
			String orderBy = MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC";
			Cursor c = null;
			SortedSet<String> dirList = new TreeSet<String>();
			ArrayList<ImagesDO> resultIAV = new ArrayList<ImagesDO>();

			String[] directories = null;
			if (u != null) {
				c = context.getContentResolver().query(u, projection, null, null, orderBy);
			}

			if ((c != null) && (c.moveToFirst())) {
				do {
					String tempDir = c.getString(0);
					tempDir = tempDir.substring(0, tempDir.lastIndexOf("/"));
					try {
						dirList.add(tempDir);
					} catch (Exception e) {

					}
				}
				while (c.moveToNext());
				directories = new String[dirList.size()];
				dirList.toArray(directories);

			}

			for (int i = 0; i < dirList.size(); i++) {
				File imageDir = new File(directories[i]);
				File[] imageList = imageDir.listFiles();
				if (imageList == null)
					continue;
				for (File imagePath : imageList) {
					try {

						if (imagePath.isDirectory()) {
							imageList = imagePath.listFiles();

						}
						if (imagePath.getName().contains(".jpg") || imagePath.getName().contains(".JPG")
								|| imagePath.getName().contains(".jpeg") || imagePath.getName().contains(".JPEG")
								|| imagePath.getName().contains(".png") || imagePath.getName().contains(".PNG")
								|| imagePath.getName().contains(".gif") || imagePath.getName().contains(".GIF")
								|| imagePath.getName().contains(".bmp") || imagePath.getName().contains(".BMP")
								) {

							ImagesDO imageItem = new ImagesDO();

							imageItem.imageName = "file://" + imagePath.getAbsolutePath();

//						Log.e("TAG", "" + imagePath.getAbsolutePath());
							resultIAV.add(imageItem);

						}
					}
					//  }
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			LocalData.getInstance().setGalleryList(resultIAV);

			return resultIAV;
		}else{
			return  galleryList;
		}



	}

	public static String recordTime(int count) {
		String str = "";
		if (count < 9)
			str = "00:0" + count;

		if (count > 59) {
			str = "00:" + count;
		}

		return str;
	}

	public static int progressToTimer(int progress, int totalDuration) {
		int currentDuration = 0;
		totalDuration = (int) (totalDuration / 1000);
		currentDuration = (int) ((((double) progress) / 100) * totalDuration);

		// return current duration in milliseconds
		return currentDuration * 1000;
	}

	public static int getProgressPercentage(long currentDuration, long totalDuration) {
		Double percentage = (double) 0;

		long currentSeconds = (int) (currentDuration / 1000);
		long totalSeconds = (int) (totalDuration / 1000);

		// calculating percentage
		percentage = (((double) currentSeconds) / totalSeconds) * 100;

		// return percentage
		return percentage.intValue();
	}

	public static String longToDate(long date) {
		String dateStr = "";
		long millisecond = Long.parseLong(String.valueOf(date));
		dateStr = DateFormat.format("MM/dd/yyyy", new Date(millisecond)).toString();
		return dateStr;
	}


	//	public static loadPicasco(Activity activity,String url,){
//		Picasso.with(getActivity())
//				.load(url)
//				.resize(AppConstants.DEVICE_DISPLAY_WIDTH/2, AppConstants.DEVICE_DISPLAY_WIDTH/2).into(holder.img);
//	}
	public static String getTimeAgo(String time) {
		try {


			String date = getDate(time);
			System.out.println("original date::" + time);
			System.out.println("date got::" + date);
			time = date;
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		2017-08-03 14:24:47
			Date past = format.parse(time);
			Date now = new Date();
			long seconds = TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime());
			long minutes = TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime());
			long hours = TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime());
			long days = TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime());
//
//          System.out.println(TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime()) + " milliseconds ago");
//          System.out.println(TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime()) + " minutes ago");
//          System.out.println(TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime()) + " hours ago");
//          System.out.println(TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime()) + " days ago");

			if (seconds < 60) {
				System.out.println(seconds + " seconds ago");


				return "Just Now";
			} else if (minutes < 60) {
				System.out.println(minutes + " minutes ago");

				return "" + minutes + " minutes ago";
			} else if (hours < 24) {
				System.out.println(hours + " hours ago");
				SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
				String time1 = sdf.format(past);
				if(hours==1){
					return "" + hours + " hour ago";
				}else {
					return "" + hours + " hours ago";
				}
			} else {
				System.out.println(days + " days ago");

				if (days > 7) {
					SimpleDateFormat targetFormat = new SimpleDateFormat("MMMM d");
					SimpleDateFormat original = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date parse = original.parse(time);
					String format1 = targetFormat.format(parse);
					return format1;

				} else {
					SimpleDateFormat sdf1 = new SimpleDateFormat("EEEE");
					SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
					String time1 = sdf.format(past);
					String format1 = sdf1.format(past);
					return format1 + " at " + time1;
				}

			}
		} catch (Exception j) {
			j.printStackTrace();
			System.out.println("time exception::" + j.toString());
		}
		return "";
	}

	public static String getDate(String OurDate) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date value = formatter.parse(OurDate);

			SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //this format changeable
			dateFormatter.setTimeZone(TimeZone.getDefault());
			OurDate = dateFormatter.format(value);

			//Log.d("OurDate", OurDate);
		} catch (Exception e) {
			OurDate = "00-00-0000 00:00";
		}
		return OurDate;
	}

	public float[] getLatLangFromPic(String imagePath) {
		float[] latLong = new float[2];
		try {
			final ExifInterface exifInterface = new ExifInterface(imagePath);

			if (exifInterface.getLatLong(latLong)) {
				System.out.println("lat from photo::"+latLong[0]);
				System.out.println("long from photo::"+latLong[1]);
				// Do stuff with lat / long...
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return latLong;
	}
	public static Typeface getHelvaticaLight(Context context){
		Typeface typeface = Typeface.createFromAsset(context.getAssets(), "HelveticaNeue_Light.ttf");
		return typeface;
	}
	public static Typeface getHelvaticaThin(Context context){
		Typeface typeface = Typeface.createFromAsset(context.getAssets(), "HelveticaNeue_Thin.ttf");
		return typeface;
	}
	public static Typeface getHelvaticaBold(Context context){
//		Typeface typeface = Typeface.createFromAsset(context.getAssets(), "Helvetica_Neu_Bold.ttf");
		Typeface typeface = Typeface.createFromAsset(context.getAssets(), "HelveticaNeue_Medium.ttf");

		return typeface;
	}
	public static Typeface getHelvaticaMedium(Context context){
//		Typeface typeface = Typeface.createFromAsset(context.getAssets(), "HelveticaNeue_Medium.ttf");
//		Typeface typeface = Typeface.createFromAsset(context.getAssets(), "HelveticaNeue_Light.ttf");
		Typeface typeface = Typeface.createFromAsset(context.getAssets(),  "HelveticaNeue_Light.ttf");

		return typeface;
	}





}

