package com.sageabletech.utils;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

import java.io.IOException;
import java.net.URL;

public class BitmapUtils 
{
	//Method to get resized bitmap
	public static Bitmap  resizeBitmap(Bitmap bitmapOrg,int newWidth,int newHeight)
	{
		Bitmap resizedBitmap = null;
		try
		{
        
		int width=bitmapOrg.getWidth();
        int height=bitmapOrg.getHeight();
        
        // calculate the scale - in this case = 0.4f 
        float scaleWidth = ((float) newWidth) / width; 
        float scaleHeight = ((float) newHeight) / height;
        
        // createa matrix for the manipulation 
        Matrix matrix = new Matrix(); 
        // resize the bit map 
        matrix.postScale(scaleWidth, scaleHeight); 
        // rotate the Bitmap 
        //matrix.postRotate(45); 
        resizedBitmap = Bitmap.createBitmap(bitmapOrg, 0, 0,width, height, matrix, true);
        bitmapOrg.recycle();
		
		}
		catch(Throwable e)
		{
			e.printStackTrace();
		}
        
        return resizedBitmap;
	}
	
	  //Method to getRoundedCorner Bitmap
	 public static Bitmap getRoundedCornerBitmap(Bitmap bitmap,float roundPxRadius)
		{
	        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
	            bitmap.getHeight(), Config.ARGB_8888);
	        Canvas canvas = new Canvas(output);
	     
	        final int color = 0xff424242;
	        final Paint paint = new Paint();
	        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
	        final RectF rectF = new RectF(rect);
	        final float roundPx =roundPxRadius;
	     
	        paint.setAntiAlias(true);
	        canvas.drawARGB(0, 0, 0, 0);
	        paint.setColor(color);
	        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
	     
	        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	        canvas.drawBitmap(bitmap, rect, rect, paint);
	     
	        return output;
		}

	 
	 public static Bitmap getBitmap(String path)
	 {
		 Bitmap bmp = null;
		 try
		 {
			 bmp = BitmapFactory.decodeFile(path);
		 }
		 catch(OutOfMemoryError e)
		 {
			 e.printStackTrace();
			 System.gc();
		 }
		 catch (Exception e) 
		 {
			e.printStackTrace();
		 }
		 return bmp;
	 }
	public static Bitmap getResizedBitmap(Bitmap image, int newHeight, int newWidth) {
		int width = image.getWidth();
		int height = image.getHeight();
		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;
		// create a matrix for the manipulation
		Matrix matrix = new Matrix();
		// resize the bit map
		matrix.postScale(scaleWidth, scaleHeight);
		// recreate the new Bitmap
		Bitmap resizedBitmap = Bitmap.createBitmap(image, 0, 0, width, height,
				matrix, false);
		return resizedBitmap;
	}
	public Bitmap getBitMap(String path) {
		Bitmap nextImageBitmap = null;

		System.out.println("sdsdsdsddsdsfdfsgdfgfg");
		try {
			URL url = new URL(path);
			nextImageBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
			return getResizedBitmap(nextImageBitmap, 80);
		} catch (IOException e) {
			System.out.println(e);
		}
		return getResizedBitmap(nextImageBitmap, 80);

	}

	public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
		System.out.println("resizeeee:::" + maxSize);
		int width = image.getWidth();
		int height = image.getHeight();

		float bitmapRatio = (float) width / (float) height;
		if (bitmapRatio > 1) {
			width = maxSize;
			height = (int) (width / bitmapRatio);
		} else {
			height = maxSize;
			width = (int) (height * bitmapRatio);
		}
		return Bitmap.createScaledBitmap(image, width, height, true);
	}
	
	
	
}
