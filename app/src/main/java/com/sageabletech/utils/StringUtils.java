package com.sageabletech.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.net.ParseException;

public class StringUtils 
{
	 //String to Int
	public static int getInt(String str) 
	{
		int value = 0;
		if (str == null || str.equalsIgnoreCase(""))
			return value;

		/*if(str.contains("."))
		str = str.split(".")[0];
*/
		try 
		{
			value = Integer.parseInt(str);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return value;
	}

	//String to long
	public static long getLong(String string) 
	{
		long value = 0;
		
		if (string == null || string.equalsIgnoreCase(""))
			return value;
		
		try 
		{
			value = Long.parseLong(string);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return value;
	}
	
	public static boolean isValidDate(String pDateString) throws ParseException {
        Date date = null;
		try {
			date = new SimpleDateFormat("MM-dd-yyyy").parse(pDateString);
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        return new Date().before(date);
	}
	
	
	//String to float
	public static float getFloat(String string) 
	{
		float value = 0f;
		
		if (string == null || string.equalsIgnoreCase("")
				|| string.equalsIgnoreCase("."))
			return value;
		
		try 
		{
			value = Float.parseFloat(string);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return value;
		}
		
		return value;
	}
	
	//String to double
	public static double getDouble(String str) 
	{
		double value = 0;
		
		if (str == null || str.equalsIgnoreCase(""))
			return value;
		
		try 
		{
			value = Double.parseDouble(str);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return value;
	}
	
	public static String removeLastIndex(String str) {
		if (str != null && str.length() > 0) {
		      str = str.substring(0, str.length()-1);
		    }
		    return str;
	}
	
	public static String removeLastIndexOr(String str) {
		if (str != null && str.length() > 0) {
		      str = str.substring(0, str.length()-3);
		    }
		    return str;
	}
	
	
	public static String removeFirstIndex(String str) {
		str = str.substring(1);
	    return str;
	}

	
	//String to boolean
	public static boolean getBoolean(String str) 
	{
		boolean value = false;
		if (str == null || str.equalsIgnoreCase(""))
			return value;
		try 
		{
			value = Boolean.parseBoolean(str);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return value;
	}

	//Method to check email validation
	public static boolean isValidEmail(String string) 
	{
		final Pattern EMAIL_ADDRESS_PATTERN = Pattern
				.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
						+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
						+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");
		Matcher matcher = EMAIL_ADDRESS_PATTERN.matcher(string);
		boolean value = matcher.matches();
		return value;
	}


	public static String dateFormater(String date){

		String myDateString = date;//"2009-04-22 15:51";

		SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat outFormat = new SimpleDateFormat("EEE MMM,yyyy HH:mm");
		String formatterDate = "";

		try {
			formatterDate=  outFormat.format(inFormat.parse(myDateString));
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}

		return formatterDate;
	}


	
}
