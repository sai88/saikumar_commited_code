package com.sageabletech.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.sageabletech.adapter.FeedsAdapter;
import com.sageabletech.adapter.SectionsPagerAdapter;
import com.sageabletech.model.AddTourDO;
import com.sageabletech.model.CategoryDO;
import com.sageabletech.model.FeedsDO;
import com.sageabletech.model.ImagesDO;
import com.sageabletech.model.LoginDO;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TagsDo;
import com.sageabletech.model.TourItem;
import com.sageabletech.newclassess.AddAudioFragmentNew;
import com.sageabletech.viewtour.ViewSubDetailsPageViewer;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ASMAN on 19-07-2017.
 */

public class LocalData {
    ProgressDialog viewDetailsDialog;
    public ArrayList<TagsDo> getfilterTagServiceData() {
        return filterTagServiceData;
    }

    public void setfilterTagServiceData(ArrayList<TagsDo> filterTag) {
        this.filterTagServiceData = filterTag;
    }

    ArrayList<TagsDo> filterTagServiceData=new ArrayList<>();
    ArrayList<TagsDo> filterTags=new ArrayList<>();

    public ArrayList<TagsDo> getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(ArrayList<TagsDo> filterTags) {
        this.filterTags = filterTags;
    }

    public ArrayList<TourItem> getCameraTourItem() {
        return cameraTourItem;
    }

    public void setCameraTourItem(ArrayList<TourItem> cameraTourItem) {
        this.cameraTourItem = cameraTourItem;
    }

    public ArrayList<TourItem> cameraTourItem=new ArrayList<>();
    public AddTourDO getCreateAddTourData() {
        return getCreateAddTourData;
    }

    public void setCreateAddTourData(AddTourDO getCreateAddTourData) {
        this.getCreateAddTourData = getCreateAddTourData;
    }

    AddTourDO getCreateAddTourData;
    Activity finalScreen;
    public Activity getFeedSearchActivity() {
        return FeedSearchActivity;
    }

    public void setFeedSearchActivity(Activity feedSearchActivity) {
        FeedSearchActivity = feedSearchActivity;
    }

    Activity FeedSearchActivity;
    RecyclerView searchRecycle;

    public RecyclerView getSearchRecycle() {
        return searchRecycle;
    }

    public void setSearchRecycle(RecyclerView searchRecycle) {
        this.searchRecycle = searchRecycle;
    }

    public RecyclerView getFeedFragmentRecycle() {
        return feedFragmentRecycle;
    }

    public void setFeedFragmentRecycle(RecyclerView feedFragmentRecycle) {
        this.feedFragmentRecycle = feedFragmentRecycle;
    }

    RecyclerView feedFragmentRecycle;
    ArrayList<Bitmap> mapbitmapArry=new ArrayList<>();
    public String getIsFromProfile() {
        return IsFromProfile;
    }

    public void setIsFromProfile(String isFromProfile) {
        IsFromProfile = isFromProfile;
    }

    String IsFromProfile="";
    public EditText getSignupedittext() {
        return signupedittext;
    }

    public void setSignupedittext(EditText signupedittext) {
        this.signupedittext = signupedittext;
    }

    EditText signupedittext;
    public ViewPager getLoginadapter() {
        return Loginadapter;
    }

    public void setLoginadapter(ViewPager loginadapter) {
        Loginadapter = loginadapter;
    }

    ViewPager Loginadapter;
    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    String emailId="";
    public String getInnerEditpath() {
        return innerEditpath;
    }

    public void setInnerEditpath(String innerEditpath) {
        this.innerEditpath = innerEditpath;
    }

    String innerEditpath="";
    public String getInnertitleedit() {
        return innertitleedit;
    }

    public void setInnertitleedit(String innertitleedit) {
        this.innertitleedit = innertitleedit;
    }


    String innertitleedit="";
    ArrayList<String> deletedPicturesNames=new ArrayList<>();

    public ArrayList<String> getDeletedPicturesNames() {
        return deletedPicturesNames;
    }

    public void setDeletedPicturesNames(ArrayList<String> deletedPicturesNames) {
        this.deletedPicturesNames = deletedPicturesNames;
    }

    public String getLocationhittext() {
        return locationhittext;
    }

    public void setLocationhittext(String locationhittext) {
        this.locationhittext = locationhittext;
    }

    String locationhittext="";
    public String getLocationHit() {
        return locationHit;
    }

    public void setLocationHit(String locationHit) {
        this.locationHit = locationHit;
    }

    String locationHit="";
    public int getGetDirectionRight() {
        return getDirectionRight;
    }

    public void setGetDirectionRight(int getDirectionRight) {
        this.getDirectionRight = getDirectionRight;
    }

    int getDirectionRight;
    public int getDirectionroutenumber() {
        return directionroutenumber;
    }

    public void setDirectionroutenumber(int directionroutenumber) {
        this.directionroutenumber = directionroutenumber;
    }

    int directionroutenumber;
    public String getNewToDirections() {
        return newToDirections;
    }

    public void setNewToDirections(String newToDirections) {
        this.newToDirections = newToDirections;
    }

    String newToDirections;
    public Bitmap getMapImageBitmap() {
        return mapImageBitmap;
    }

    public void setMapImageBitmap(Bitmap mapImageBitmap) {
        this.mapImageBitmap = mapImageBitmap;
    }

    Bitmap mapImageBitmap;

    public String getMapImagePath() {
        return mapImagePath;
    }

    public void setMapImagePath(String mapImagePath) {
        this.mapImagePath = mapImagePath;
    }

    String mapImagePath;
    public String getFromWhere() {
        return fromWhere;
    }

    public void setFromWhere(String fromWhere) {
        this.fromWhere = fromWhere;
    }

    String fromWhere="";
    JSONObject searchJson=new JSONObject();
    public ArrayList<CategoryDO> getmCategoryDOs() {
        return mCategoryDOs;
    }

    public void setmCategoryDOs(ArrayList<CategoryDO> mCategoryDOs) {
        this.mCategoryDOs = mCategoryDOs;
    }

    ArrayList<CategoryDO> mCategoryDOs = new ArrayList<>();
    SeekBar seekBar;

    public SeekBar getSeekBar() {
        return seekBar;
    }

    public void setSeekBar(SeekBar seekBar) {
        this.seekBar = seekBar;
    }

    public boolean isImageListFlag() {
        return imageListFlag;
    }

    public void setImageListFlag(boolean imageListFlag) {
        this.imageListFlag = imageListFlag;
    }

    boolean imageListFlag=false;
    ArrayList<String> galleryImages = new ArrayList<String>();

    public ArrayList<String> getGalleryImages() {
        return galleryImages;
    }

    public void setGalleryImages(ArrayList<String> galleryImages) {
        this.galleryImages = galleryImages;
    }

    public ArrayList<ImagesDO> getGalleryList() {
        return GalleryList;
    }

    public void setGalleryList(ArrayList<ImagesDO> galleryList) {
        GalleryList = galleryList;
    }

    ArrayList<ImagesDO> GalleryList = new ArrayList<ImagesDO>();
    public TextView getSearchError() {
        return searchError;
    }

    public void setSearchError(TextView searchError) {
        this.searchError = searchError;
    }

    TextView searchError;
    List<FeedsDO> existingFeeds=new ArrayList<>() ;

    ArrayList<HashMap<String,String>> commentsArry=new ArrayList<>();
    public HashMap<String, String> getCommenthash() {
        return commenthash;
    }

    public void setCommenthash(HashMap<String, String> commenthash) {
        this.commenthash = commenthash;
    }

    HashMap<String,String> commenthash=new HashMap<>();
    Activity mainActivity;

    public Activity getMainActivity() {
        return mainActivity;
    }

    public void setMainActivity(Activity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public boolean isCatnew() {
        return isCatnew;
    }

    public void setCatnew(boolean catnew) {
        isCatnew = catnew;
    }

    LocationManager locationManager=null;
    boolean isCatnew=false;
    public int getSelectedTourNumber() {
        return SelectedTourNumber;
    }

    public void setSelectedTourNumber(int selectedTourNumber) {
        SelectedTourNumber = selectedTourNumber;
    }

    public int SelectedTourNumber;
    public ArrayList<Bitmap> getDisplayImagesArry() {
        return displayImagesArry;
    }

    public void setDisplayImagesArry(ArrayList<Bitmap> displayImagesArry) {
        this.displayImagesArry = displayImagesArry;
    }
Bitmap mainTourImage;

    public Bitmap getMainTourImage() {
        return mainTourImage;
    }

    public void setMainTourImage(Bitmap mainTourImage) {
        this.mainTourImage = mainTourImage;
    }

    ArrayList<Bitmap> displayImagesArry=new ArrayList<>();
    public FeedsAdapter feedsAdapter;
    public AddAudioFragmentNew getAddAudioFragmentNew() {
        return addAudioFragmentNew;
    }

    public void setAddAudioFragmentNew(AddAudioFragmentNew addAudioFragmentNew) {
        this.addAudioFragmentNew = addAudioFragmentNew;
    }

    AddAudioFragmentNew addAudioFragmentNew;
    public String getNewTour() {
        return newTour;
    }

    public void setNewTour(String newTour) {
        this.newTour = newTour;
    }

    String newTour="no";
    public String getImageselection() {
        return imageselection;
    }

    public void setImageselection(String imageselection) {
        this.imageselection = imageselection;
    }

    String imageselection="no";
   public MediaPlayer MediaPlayer;

    public ArrayList<String> getcatTourArry() {
        return tourArry;
    }

    public void setcatTourArry(ArrayList<String> tourArry) {
        this.tourArry = tourArry;
    }

    public ArrayList<String> getTagtourArry() {
        return tagtourArry;
    }

    public void setTagtourArry(ArrayList<String> tagtourArry) {
        this.tagtourArry = tagtourArry;
    }

    ArrayList<String> tourArry=new ArrayList<>();
    ArrayList<String> tagtourArry=new ArrayList<>();

    public android.media.MediaPlayer getMediaPlayer() {
        return MediaPlayer;
    }

    public void setMediaPlayer(android.media.MediaPlayer mediaPlayer) {
        MediaPlayer = mediaPlayer;
    }

    public MyTourItemDO favselectedItem;
    public android.app.ProgressDialog getProgressDialog() {
        return ProgressDialog;
    }

    public void setProgressDialog(android.app.ProgressDialog progressDialog) {
        ProgressDialog = progressDialog;
    }

    public ProgressDialog ProgressDialog;
   public String settings;
    public String getProfilepicpath() {
        return profilepicpath;
    }

    public void setProfilepicpath(String profilepicpath) {
        this.profilepicpath = profilepicpath;
    }

    public String profilepicpath;
    public MyTourItemDO getSelectedMyTourItemDO() {
        return selectedMyTourItemDO;
    }

    public void setSelectedMyTourItemDO(MyTourItemDO selectedMyTourItemDO) {
        this.selectedMyTourItemDO = selectedMyTourItemDO;
    }

    MyTourItemDO selectedMyTourItemDO;
    public ArrayList<MyTourItemDO> getmFeeds() {
        return mFeeds;
    }

    public void setmFeeds(ArrayList<MyTourItemDO> mFeeds) {
        this.mFeeds = mFeeds;
    }

    public ArrayList<MyTourItemDO> getFavoritesArray() {
        return favoritesArray;
    }

    public void setFavoritesArray(ArrayList<MyTourItemDO> favoritesArray) {
        this.favoritesArray = favoritesArray;
    }

    ArrayList<MyTourItemDO> mFeeds = new ArrayList<>();
    ArrayList<MyTourItemDO> favoritesArray = new ArrayList<>();
    String editpicpath;

    public String getFrom() {
        return From;
    }

    public void setFrom(String from) {
        From = from;
    }

    String From;

    public String getFromDrafts() {
        return fromDrafts;
    }

    public void setFromDrafts(String fromDrafts) {
        this.fromDrafts = fromDrafts;
    }

    String fromDrafts="";
    public HashMap<String, Object> getTotalHash() {
        return totalHash;
    }

    public void setTotalHash(HashMap<String, Object> totalHash) {
        this.totalHash = totalHash;
    }

    TourItem Dataofsubtour;
    int subtourposition;
    HashMap<String,Object> totalHash=new HashMap<>();

    public boolean isSubtouredittitle() {
        return subtouredittitle;
    }

    public void setSubtouredittitle(boolean subtouredittitle) {
        this.subtouredittitle = subtouredittitle;
    }

    boolean subtouredittitle;

    public int getSubtourposition() {
        return subtourposition;
    }

    public void setSubtourposition(int subtourposition) {
        this.subtourposition = subtourposition;
    }

    public void setDataofsubtour(TourItem dataofsubtour) {
        Dataofsubtour = dataofsubtour;
    }

    public void setDeletedSubTour(HashMap<String, TourItem> deletedSubTour) {
        this.deletedSubTour = deletedSubTour;
    }

    HashMap<String,TourItem> deletedSubTour=new HashMap<>();

    public HashMap<String, TourItem> getDeletedSubTour() {
        return deletedSubTour;
    }

    public TourItem getDataofsubtour() {
        return Dataofsubtour;
    }

    public String getLocationText() {
        return locationText;
    }

    public void setLocationText(String locationText) {
        this.locationText = locationText;
    }

    String locationText;

    public boolean isFromedittour() {
        return fromedittour;
    }

    public void setFromedittour(boolean fromedittour) {
        this.fromedittour = fromedittour;
    }

    boolean fromedittour;

    public String getEditpicpath() {
        return editpicpath;
    }

    public void setEditpicpath(String editpicpath) {
        this.editpicpath = editpicpath;
    }

    static LocalData localdata=null;
    public static LocalData getInstance(){
        if(localdata==null){
            localdata=new LocalData();
        }
        return localdata;
    }

    public String getSettings() {
        return settings;
    }

    public void setSettings(String settings) {
        this.settings = settings;
    }

    public MyTourItemDO getFavselectedItem() {
        return favselectedItem;
    }

    public void setFavselectedItem(MyTourItemDO favselectedItem) {
        this.favselectedItem = favselectedItem;
    }

 public LoginDO loginDO;

    public LoginDO getLoginDO() {
        return loginDO;
    }

    public void setLoginDO(LoginDO loginDO) {
        this.loginDO = loginDO;
    }

    public TourItem getSelectedTourItem() {
        return selectedTourItem;
    }

    public void setSelectedTourItem(TourItem selectedTourItem) {
        this.selectedTourItem = selectedTourItem;
    }

    public TourItem selectedTourItem;

    public MyTourItemDO TourSeletedItem;

    public MyTourItemDO getTourSeletedItem() {
        return TourSeletedItem;
    }

    public void setTourSeletedItem(MyTourItemDO tourSeletedItem) {
        TourSeletedItem = tourSeletedItem;
    }

    public FeedsAdapter getFeedsAdapter() {
        return feedsAdapter;
    }

    public void setFeedsAdapter(FeedsAdapter feedsAdapter) {
        this.feedsAdapter = feedsAdapter;
    }

    public LocationManager getLocationManager() {
        return locationManager;
    }

    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    public ArrayList<HashMap<String, String>> getTourCommentsArry() {
        return commentsArry;
    }

    public void setTourCommentsArry(ArrayList<HashMap<String, String>> commentsArry) {
        this.commentsArry = commentsArry;
    }

    public List<FeedsDO> getExistingFeeds() {
        return existingFeeds;
    }

    public void setExistingFeeds(List<FeedsDO> existingFeeds) {
        this.existingFeeds = existingFeeds;
    }

    public JSONObject getSearchJson() {
        return searchJson;
    }

    public void setSearchJson(JSONObject searchJson) {
        this.searchJson = searchJson;
    }

    public ArrayList<Bitmap> getMapbitmapArry() {
        return mapbitmapArry;
    }

    public void setMapbitmapArry(ArrayList<Bitmap> mapbitmapArry) {
        this.mapbitmapArry = mapbitmapArry;
    }

    public Activity getFinalScreen() {
        return finalScreen;
    }

    public void setFinalScreen(Activity finalScreen) {
        this.finalScreen = finalScreen;
    }

    public android.app.ProgressDialog getViewDetailsDialog() {
        return viewDetailsDialog;
    }

    public void setViewDetailsDialog(android.app.ProgressDialog viewDetailsDialog) {
        this.viewDetailsDialog = viewDetailsDialog;
    }


}
