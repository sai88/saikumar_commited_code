package com.sageabletech.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DATE FORMAT:                                       | TIME FORMAT:                       | SEPARATOR:   //
// d -- 1   dd -- 01                                  | h - 9 hh - 09 H - 9&21 HH - 09&21  | .  -- dot    //
// M -- 1   MM -- 01     MMM -- jan MMMM -- january   | m - 9 mm - 09                      | -  -- hypen  //
// yy - 14  yyyy - 2014                               | s - 9 ss - 09                      | "" -- space  //
//                                                    | a - am/pm                          |  : -- colon  //
//                                                    |                                    |  / -- slash  //
// 1.Calendar calendar = Calendar.getInstance();                                                          //   
// 2.SimpleDateFormat dateFormat = new SimpleDateFormat("pattern");                                       //
// 3.String date = dateFormat.format(calendar.getTime());                                                 //
////////////////////////////////////////////////////////////////////////////////////////////////////////////


public class CalendarUtils 
{
	//dd_MMM_yyyy_hh:mm:ss_a
	private static final String PATTERN_COMPLETE 	   = "dd_MM_yyyy_HH_mm_ss";//1808201524235
	private static final String PATTERN_DATE 		   = "dd/MM/yyyy";//16/08/2015.
	private static final String PATTERN_TIME 		   = "HH:mm";//2:30.
	
	private static final String PATTERN_DATE_TIME 		   = "MM/dd/yyyy hh:mm:ss a";//03/13/2016 07:48:20 pm.
	
	public static final String DATE_STD_PATTERN2 = "dd-MM-yyyy HH:mm ";
	
	private static final String PATTERN_DATE_VIOLATIONS 		   = "dd/MM/yyyy";


	private static final String PATTERN_ORDER_DATE = "yyyy-MM-dd HH:mm:ss";//2016-07-19 09:02:00
	
	//getCurrentDate in Complete Format
	public static String getCompleteCurrentDate()
	{
		Date date            = new Date(System.currentTimeMillis());
		SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_DATE_TIME);
		String dateFormat    = sdf.format(date);
		return dateFormat;
		
	}
	
	public static String getCompleteCurrentDateTime()
	{
		Date date            = new Date(System.currentTimeMillis());
		SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_COMPLETE);
		String dateFormat    = sdf.format(date);
		return dateFormat;
		
	}
	
	//getCurrentDate in Complete Format
		public static String getCurrentDate(long seconds)
		{
			Date date            = new Date(seconds);
			SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_ORDER_DATE);
			String dateFormat    = sdf.format(date);
			return dateFormat;
			
		}
		
		//getCurrentDate in Complete Format
		public static String getCurrentDateForViolations()
		{
			Date date            = new Date(System.currentTimeMillis());
			SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_DATE_VIOLATIONS);
			String dateFormat    = sdf.format(date);
			return dateFormat;
			
		}
		
		//getCurrentDate in Complete Format
		public static String getCurrentTime(long seconds)
		{
			Date date            = new Date(seconds);
			SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_TIME);
			String dateFormat    = sdf.format(date);
			return dateFormat;
			
		}
		
		public static String getCurrentDateForLogs() 
		{
			String sdf = new SimpleDateFormat(DATE_STD_PATTERN2).format(System.currentTimeMillis());
			return sdf;
		}

		public static String getCurrentDate(){
			SimpleDateFormat pattern_order_date =  new SimpleDateFormat(PATTERN_ORDER_DATE, Locale.US);
			Calendar c = Calendar.getInstance();
			return pattern_order_date.format(c.getTime());
		}


	   public static String getOrderDate(String order_date){

		   SimpleDateFormat formatter = new SimpleDateFormat(PATTERN_ORDER_DATE);
		   Date date = null;
		   try {
			   date = formatter.parse(order_date);
		   } catch (ParseException e) {
			   e.printStackTrace();
		   }

		   Calendar calendar = Calendar.getInstance();
		   if(date !=null)
		   calendar.setTime(date);

		   return formatter.format(calendar.getTime());
	   }
		
}

