package com.sageabletech.utils;

import android.os.Build;

import com.sageabletech.BuildConfig;
import com.sageabletech.Exception.DeviceException;

import java.lang.reflect.Field;

/**
 * Created by arige on 7/8/2017.
 */

public class DeviceInfo {

    public static String getOSVersionNo() throws DeviceException {
        String OSVersionNo = (Build.VERSION.RELEASE);
        if (OSVersionNo != null && !OSVersionNo.isEmpty() ) {
            return OSVersionNo;
        } else {
            throw new DeviceException("OS Version not found");

        }
    }

    public static String getOSVersionName() throws DeviceException {
        String versionName = null;
        Field[] fields = Build.VERSION_CODES.class.getFields();
        for (Field field : fields) {
            String fieldName = field.getName();
            int fieldValue = -1;
            try {
                fieldValue = field.getInt(new Object());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (ExceptionInInitializerError e) {
                e.printStackTrace();
            }
            if (fieldValue == Build.VERSION.SDK_INT) {
                versionName = fieldName;
            }
        }
        if (versionName != null) {
            return versionName;
        } else {
            throw new DeviceException("Os version name not found.");
        }
    }

    public static String getAppVersion(){
        int versionCode = BuildConfig.VERSION_CODE;
        String versionName = BuildConfig.VERSION_NAME;

        return  versionName +":"+ versionCode;
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    public static  String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }


}