package com.sageabletech.utils;


public class Urls 
{
	
//	Main server URL
	
// 	DEV

//	public static String BASE_URL		=	"http://api.xcater.com/api/";//http://xcater.samplespace.com

//	public static String BASE_URL		=	"http://35.154.128.56:8080/";//Dev//Old



    public static String BASE_URL		=	"http://shomiround-dev.us-east-1.elasticbeanstalk.com/";//Dev New

//	public static String BASE_URL		=	"http://api.shomiround.com:8080/";//pro//

// 	LIVE
	
//	public static String BASE_URL		=	"";
	
//	Service name and urls
	
	public static String IMAGE_BASE_URL		=	BASE_URL+"uploads/";
	public static String PROFILE_IMG_URL	=	IMAGE_BASE_URL+"profileimages/";
	
	public static String FORGET_PASSWORD	=	BASE_URL+"forgotPassword?useremail=";
	public static String POST_LOGIN			=	BASE_URL+"authenticate";
	public static String POST_SIGNUP		=	BASE_URL+"signUp";
	public static String NEWS_FEED			=	BASE_URL+"tour/newsFeed/";
	public static String MY_TOUR			=	BASE_URL+"tour/tourList/";
	public static String CREATE				= 	BASE_URL+"tour/create/";
	public static String UPDATETOUR				= 	BASE_URL+"tour/updateTour/";
	public static String DELETE				=	BASE_URL+"tour/deleteTour/";
	public static String GET_TOUR = BASE_URL + "tour/getTour/";
	public static String LOGOUT = BASE_URL + "logout/";
	public static String PRIVACY_POLICY = BASE_URL + "privacyPolicy/";
	public static String CHANGE_PASSWORD = BASE_URL + "updatePassword/";


	////
	public static String EDIT_PROFILE				=	BASE_URL+"/updateUserProfile/";
	public static String CATEGORIES_LIST	=	BASE_URL+"tags/listCategories/";
	public static String Tags_LIST			=	BASE_URL+"tags/listCategoryAndTags/";
	public static String ADD_Tags			=	BASE_URL+"tags/create/";
	public static String GET_SINGLE_TOUR_DETAILS =	BASE_URL+"tour/getTour/";


	public static String DELETE_FAVOURITE =	BASE_URL+"fav/deleteFav/";
	public static String ADD_FAVOURITE =	BASE_URL+"fav/create/";
	public static String LIST_OF_FAVOURITE =	BASE_URL+"fav/listFavsByUser/";

	public static String GET_VIEW_COUNT =	BASE_URL+"feedback/getViewCount/";
	public static String SAVE_VIEW_COUNT =	BASE_URL+"feedback/updateViewCount/";
	public static String REPORT =	BASE_URL+"reportTour/";
	public static String SAVE_COMMENTS =	BASE_URL+"feedback/saveComment/";
	public static String GET_COMMENTS =	BASE_URL+"feedback/listComments/";

	public static String SEARCH_FEEDS =	BASE_URL+"search/advancedSearch/";
	public static String SEARCH_Tags =	BASE_URL+"tags/searchTags/";

	public static String UNPUBLISH =BASE_URL+"tour/unPublishTour/";
	public static String TERMS =BASE_URL+"termsConditions/";

//	http://52.87.71.226:8080/tour/unPublishTour/481ce0de285384fed32a78a6d415fa5b0277be22

//	http://35.154.128.56:8080/tags/searchTags/481ce0de285384fed32a78a6d415fa5b0277be22?tagsearch
//	http://35.154.128.56:8080/search/simpleSearch/{accessToken}

//	http:// 35.154.128.56:8080/feedback/listComments/{accessToken}
//	http://35.154.128.56:8080/feedback/saveComment/{accessToken}
//	http://35.154.128.56:8080/reportTour/{accessToken}


}
