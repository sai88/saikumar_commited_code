package com.sageabletech.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;


public class NetworkUtils 
{
	//Method to check the Internet availability
	public static boolean isNetworkConnectionAvailable(Context context) 
	{
		// checking the Internet availability
		boolean isNetworkConnectionAvailable = false;
		ConnectivityManager connectivityManager = (ConnectivityManager) context	.getSystemService("connectivity");
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

		if (activeNetworkInfo != null)
		{
			isNetworkConnectionAvailable = activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED;
		}

		return isNetworkConnectionAvailable;
	}
	
	
	//To Check isDevice Supported Camera or not.
 	public static boolean isDeviceSupportCamera(Context context) 
	{
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) 
        {
            return true;
        } 
        else
        {
            return false;
        }
    }
 	
 	public static boolean isGpsEnabled(Context context)
 	{
 		boolean isGpsEnable = false;
 		LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE );
        isGpsEnable = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        
        return isGpsEnable;
 	}
 	
 	
 	public void turnGPSOn(Context ctx)
 	{
 	     Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
 	     intent.putExtra("enabled", true);
 	     ctx.sendBroadcast(intent);

 	    String provider = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
 	    if(!provider.contains("gps")){ //if gps is disabled
 	        final Intent poke = new Intent();
 	        poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider"); 
 	        poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
 	        poke.setData(Uri.parse("3")); 
 	        ctx.sendBroadcast(poke);


 	    }
 	}
 	// automatic turn off the gps
 	public void turnGPSOff(Context ctx)
 	{
 	    String provider = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
 	    if(provider.contains("gps")){ //if gps is enabled
 	        final Intent poke = new Intent();
 	        poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
 	        poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
 	        poke.setData(Uri.parse("3")); 
 	        ctx.sendBroadcast(poke);
 	    }
 	}

 	
 	
 	
 	
}
