package com.sageabletech.utils;

import java.io.File;

import android.os.Environment;


public class AppConstants 
{
	
	
	public static String DATABASE_NAME 	= "xater.sqlite";
	public static String DATABASE_PATH 	= "";
	
	//Device height & width
	public static int DEVICE_DISPLAY_WIDTH = 0;
	public static int DEVICE_DISPLAY_HEIGHT = 0;
	
	public static final int GET  = 1;
	public static final int POST = 2;
	
	public static int ONE_MONTH_MILLISECONDS= 604800000;
	
	public static final int LOGIN_FAIL_MAX  = 5;
	
	public static String LOGIN_TYPE = "login_type"; 
	
	public static String LOGIN_ADMIN = "Admin"; 
	

	public static final String TRIP_STARTED  = "STARTED";
	public static final String TRIP_STOPED  = "STOPED";
	
	public static final String GOOGLE_PROJECT_ID        = "836492684674";//(SENDER_ID)
	
	
	public static String LOGIN_SALES_OFFICER = "Sales"; 
	
	public static String SDCARD_ROOT                      = Environment.getExternalStorageDirectory().toString() + File.separator;
	
	public static String appCacheDir = "";
	
	//GPS Constants
	public static final double DISTANCE_VALIDATION_RANGE     = 50;           //50 meters.
	public static final double EARTH_RADIUS                  = 3958.75;
	public static final int METER_CONVERSION                 = 1609;
	
	//location updates
	public static final int MAX_RESULTS                      = 1;
	public static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;        // 10 meters.
	public static final long INTERVAL                       = 1*30*1000;  //1 min.
	public static final long FASTEST_INTERVAL               = 1*30*1000;  //1 min.
	
	//timer task
	public static final int TIMER_TASK_DELAY                 = 3*1000;      //1 second.
	public static final int TIMER_TASK_PERIOD                = 10*1000;     //2 seconds.
	
	public static final String MESSAGE_KEY              = "message";


	public static final int NEW_ORDER = 0;
	public static final int COMPLETED = 1;
	public static final int ORDER_CONFIRM = 2;
	public static final int CATERING_UNDER_PROCESS = 3;
	public static final int READY = 4;
	public static final int FOR_PICK_UP = 5;
	public static final int FOR_HOME_DELIVERY = 6;
	public static final int ORDER_DENY = 8;
	public static final int ORDER_CANCEL = 9;



	public static String getStatus(String status){
		String StatusText = "";
		int StatusNumber = StringUtils.getInt(status);
		switch (StatusNumber){

			case NEW_ORDER :
				StatusText = "NEW ORDER";
				break;
			case COMPLETED :
				StatusText = "COMPLETED";
				break;
			case ORDER_CONFIRM :
				StatusText = "ORDER CONFIRM";
				break;
			case CATERING_UNDER_PROCESS :
				StatusText = "CATERING UNDER PROCESS";
				break;
			case READY :
				StatusText = "READY";
				break;
			case FOR_PICK_UP :
				StatusText = "FOR PICK UP";
				break;
			case FOR_HOME_DELIVERY :
				StatusText = "FOR HOME DELIVERY";
				break;
			case ORDER_DENY :
				StatusText = "ORDER DENY";
				break;
			case ORDER_CANCEL :
				StatusText = "ORDER CANCEL";
				break;
			default:
				StatusText = "";
				break;

		}
		return StatusText;
	}

	/*
     * You should replace these values with your own. See the README for details
     * on what to fill in.
     */
	public static final String COGNITO_POOL_ID = "us-east-1:fec7c9bf-cae9-41c7-a41e-66f7b00b4a38";//local
//    public static final String COGNITO_POOL_ID = "us-east-1:fec7c9bf-cae9-41c7-a41e-66f7b00b4a38";//prod

	/*
     * Region of your Cognito identity pool ID.
     */
	public static final String COGNITO_POOL_REGION = "us-east-1";//local &7 production same

	/*
     * Note, you must first create a bucket using the S3 console before running
     * the sample (https://console.aws.amazon.com/s3/). After creating a bucket,
     * put it's name in the field below.
     */
	public static final String BUCKET_NAME = "shomiround-dev";//local
//    public static final String BUCKET_NAME = "shomiround-production";//production


	/*
     * Region of your bucket.
     */
	public static final String BUCKET_REGION = "AWSRegionUSEast1";//local
//    public static final String BUCKET_REGION = "AWSRegionUSEast1";//production



    public static final String DRAFT_NAME = "DRAFT";//local and production same



}
