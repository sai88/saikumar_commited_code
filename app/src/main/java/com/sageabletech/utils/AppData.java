package com.sageabletech.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class AppData {
	
	public static SharedPreferences pref; 
	
	public  AppData(Context context){
		
		pref = context.getSharedPreferences("threesixtydegrees", context.MODE_PRIVATE);
		
	}
	
	
	public AppData() {
		// TODO Auto-generated constructor stub
	}

	public String getTheme(){
		
		return pref.getString("theme", "none");
		
	}
	
	
	
}