package com.sageabletech.dynamicgrid;

import android.content.Context;

import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.FinalScreen;
import com.sageabletech.utils.LocalData;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: alex askerov
 * Date: 9/7/13
 * Time: 10:49 PM
 */
public abstract class BaseDynamicGridAdapter extends AbstractDynamicGridAdapter {
    private Context mContext;

    public  ArrayList<TourItem> mItems  = new ArrayList<TourItem>();
    private int mColumnCount;

    protected BaseDynamicGridAdapter(Context context, int columnCount) {
        this.mContext = context;
        this.mColumnCount = columnCount;
    }

    public BaseDynamicGridAdapter(Context context, ArrayList<TourItem> images, int columnCount) {
        mContext = context;
        mColumnCount = columnCount;
        init(images);
    }

    private void init(ArrayList<TourItem> images) {
        addAllStableId(images);
        this.mItems.addAll(images);
        savecahnges();


    }
    public void savecahnges(){
        MyTourItemDO selectedMyTourItemDO = LocalData.getInstance().getSelectedMyTourItemDO();
        if(selectedMyTourItemDO!=null) {
            selectedMyTourItemDO.subtour = mItems;
            LocalData.getInstance().setSelectedMyTourItemDO(selectedMyTourItemDO);
        }else{
            ((FinalScreen)mContext).mAddTourDO.tourPhotos = mItems;
        }
    }


    public void set(ArrayList<TourItem> images) {
        clear();
        init(images);
        notifyDataSetChanged();
    }

    public void clear() {
        clearStableIdMap();
        mItems.clear();
        savecahnges();
        notifyDataSetChanged();
    }

    public void add(TourItem item) {
        addStableId(item);
        mItems.add(item);
        savecahnges();
        notifyDataSetChanged();
    }

    public void add(int position, TourItem item) {
        addStableId(item);
        mItems.add(position, item);
        savecahnges();
        notifyDataSetChanged();
    }

    public void add(ArrayList<TourItem>  items) {
        addAllStableId(items);
        this.mItems.addAll(items);
        savecahnges();
        notifyDataSetChanged();
    }


    public void remove(Object item) {
        mItems.remove(item);
        removeStableID(item);
        savecahnges();
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }



    @Override
    public int getColumnCount() {
        return mColumnCount;
    }

    public void setColumnCount(int columnCount) {
        this.mColumnCount = columnCount;
        notifyDataSetChanged();
    }

    @Override
    public void reorderItems(int originalPosition, int newPosition) {
        if (newPosition < getCount()) {
            DynamicGridUtils.reorder(mItems, originalPosition, newPosition);
            notifyDataSetChanged();
        }
    }

    @Override
    public boolean canReorder(int position) {
        return true;
    }

    public ArrayList<TourItem>  getItems() {
        return mItems;
    }

    protected Context getContext() {
        return mContext;
    }
}
