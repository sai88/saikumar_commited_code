package com.sageabletech.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.sageabletech.model.AddTourDO;
import com.sageabletech.model.CategoryDO;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TagsDo;
import com.sageabletech.model.TourItem;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by WIN 7 on 6/19/2017.
 */

public class DBCreator extends SQLiteOpenHelper{

    final static String DB_NAME = "ShomiRound";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_IMAGES = "Images";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "imageURL";
    private static final String KEY_PH_NO = "status";
    private static final String KEY_TOURID = "TOURID";
    private static final String TAG = DBCreator.class.getName();

    public DBCreator(Context context) {
        super(context, DB_NAME,null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_IMAGES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_PH_NO + " TEXT," + KEY_TOURID + " TEXT"+")";


        String CREATE_REQ = "CREATE TABLE IF NOT EXISTS REQ (TOURPHOTO TEXT, STATUS TEXT)";

        String DRAFT_MAIN = "CREATE TABLE IF NOT EXISTS DRAFT_MAIN (DRAFT_ID TEXT,TOURPHOTO TEXT,TOUR_TITLE TEXT,TOUR_SUMMARY TEXT,TOUR_LOCATION TEXT," +
                "TOUR_LATTITUDE TEXT,TOUR_LONGITUDE TEXT,STATUS TEXT,CREATE_DATE TEXT)";

        String DRAFT_PHOTOS = "CREATE TABLE IF NOT EXISTS DRAFT_PHOTOS (DRAFT_ID TEXT,TOURPHOTO TEXT,TOURPHOTO_AUDIO TEXT,TOURPHOTO_TEXT TEXT,TOURPHOTO_SUMMERY TEXT," +
                "TOUR_LOCATION TEXT,TOUR_LATTITUDE TEXT,TOUR_LONGITUDE TEXT,TOURPHOTO_AUDIO_DURATION TEXT,STATUS TEXT,TOURPHOTO_ID TEXT)";

        String DRAFT_CATEGOTIES = "CREATE TABLE IF NOT EXISTS DRAFT_CATEGORIES (DRAFT_ID TEXT,CATID TEXT,CAT_NAME TEXT,CATPHOTO_TEXT TEXT,STATUS TEXT)";

        String DRAFT_TAGS = "CREATE TABLE IF NOT EXISTS DRAFT_TAGS (DRAFT_ID TEXT,TAGID TEXT,TAGNAME TEXT,STATUS TEXT,SEARCH TEXT)";

        db.execSQL(CREATE_CONTACTS_TABLE);
        db.execSQL(CREATE_REQ);

        db.execSQL(DRAFT_MAIN);
        db.execSQL(DRAFT_PHOTOS);
        db.execSQL(DRAFT_CATEGOTIES);
        db.execSQL(DRAFT_TAGS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMAGES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMAGES);
        db.execSQL("DROP TABLE IF EXISTS DRAFT_MAIN");
        db.execSQL("DROP TABLE IF EXISTS DRAFT_PHOTOS");
        db.execSQL("DROP TABLE IF EXISTS DRAFT_CATEGORIES");
        db.execSQL("DROP TABLE IF EXISTS DRAFT_TAGS");

        // Create tables again
        onCreate(db);
    }


    public void insertImage(String path,String status,String TOURID){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, path); // Contact Name
        values.put(KEY_PH_NO, status); // Contact Phone
        values.put(KEY_TOURID, TOURID); // Contact Phone

        // Inserting Row
        db.insert(TABLE_IMAGES, null, values);
        db.close(); // Closing database connection
    }

    public void insertTourIcon(String path,String status){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("TOURPHOTO", path); // Contact Name
        values.put("STATUS", status); // Contact Phone

        // Inserting Row
        db.insert("REQ", null, values);
        db.close(); // Closing database connection
    }

    public int updateTourIcon(String path){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("STATUS", "yes");

        // updating row
        return db.update("REQ", values, "TOURPHOTO" + " = ?",
                new String[] { path });
    }



    public ArrayList<ImageModel> getAllImages() {
        List<ImageModel> contactList = new ArrayList<ImageModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_IMAGES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                // Adding contact to list
                ImageModel mImageModel = new ImageModel();
                mImageModel.name = cursor.getString(1);
                mImageModel.status = cursor.getString(2);
                contactList.add(mImageModel);
            } while (cursor.moveToNext());
        }

        // return contact list
        return (ArrayList<ImageModel>) contactList;


    }


    public AddTourDO getDetailsFromDraft(String draftId){
        AddTourDO mAddTourDO = new AddTourDO();

        String selectQuery = "SELECT  * FROM DRAFT_MAIN where DRAFT_ID = '"+draftId+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                // Adding contact to list
                mAddTourDO.tourIcon = cursor.getString(1);
                mAddTourDO.title = cursor.getString(2);
                mAddTourDO.tourSummary = cursor.getString(3);
                mAddTourDO.tourLocation = cursor.getString(4);
                mAddTourDO.lat = StringUtils.getDouble(cursor.getString(5));
                mAddTourDO.lang = StringUtils.getDouble(cursor.getString(6));

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

       return mAddTourDO;
    }


   public String getImage(String tourId,String pathFromURL){

       String pathFromDB = "";
       String[] tempURL = pathFromURL.split("/");
       //Select * from TABLEImages where TOURID
       String selectQuery = "SELECT  * FROM " + TABLE_IMAGES +" Where TOURID = "+tourId;

       SQLiteDatabase db = this.getWritableDatabase();
       Cursor cursor = db.rawQuery(selectQuery, null);

       // looping through all rows and adding to list
       if (cursor.moveToFirst()) {
           do {
               // Adding contact to list
                String name = cursor.getString(1);

               if(name.contains(tempURL[tempURL.length-1])){
                   pathFromDB = name;
                   break;
               }

           } while (cursor.moveToNext());
       }

       cursor.close();
       db.close();

       return pathFromDB;
   }

    public void saveDetailsToDraftTable(AddTourDO mAddTourDO, String DRAFT_ID){

        Log.e(TAG,"Saved the details of "+DRAFT_ID);

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("DRAFT_ID", DRAFT_ID); // DRAFT_ID
        values.put("TOURPHOTO", mAddTourDO.tourIcon); // DRAFT_ID
        values.put("TOUR_TITLE", mAddTourDO.title); // DRAFT_ID
        values.put("TOUR_SUMMARY", mAddTourDO.tourSummary); // DRAFT_ID
        values.put("TOUR_LOCATION", mAddTourDO.tourLocation); // DRAFT_ID
        values.put("TOUR_LATTITUDE", ""+mAddTourDO.lat); // DRAFT_ID
        values.put("TOUR_LONGITUDE", ""+mAddTourDO.lang); // DRAFT_ID
        values.put("STATUS", "no"); // DRAFT_ID
        values.put("CREATE_DATE", ""+mAddTourDO.date); // DATE

        Log.e("DRAFT_ID","Inserted : "+DRAFT_ID);

        // Inserting Row
        db.insert("DRAFT_MAIN", null, values);
        db.close(); // Closing database connection
    }


    public void updateDetailsToDraftTable(AddTourDO mAddTourDO, String DRAFT_ID){

        Log.e(TAG,"Saved the details of "+DRAFT_ID);

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("TOURPHOTO", mAddTourDO.tourIcon); // DRAFT_ID
        values.put("TOUR_TITLE", mAddTourDO.title); // DRAFT_ID
        values.put("TOUR_SUMMARY", mAddTourDO.tourSummary); // DRAFT_ID
        values.put("TOUR_LOCATION", mAddTourDO.tourLocation); // DRAFT_ID
        values.put("TOUR_LATTITUDE", ""+mAddTourDO.lat); // DRAFT_ID
        values.put("TOUR_LONGITUDE", ""+mAddTourDO.lang); // DRAFT_ID

        values.put("STATUS", "no"); // DRAFT_ID

        Log.e("DRAFT_ID","Updated : "+DRAFT_ID);

        if(mAddTourDO.tourPhotos.size()>0){
            for(int i=0;i<mAddTourDO.tourPhotos.size();i++){


                System.out.println("INSERTING_SUMMERY::"+mAddTourDO.tourPhotos.get(i).summary);
                Cursor cur = db.rawQuery("Select * from DRAFT_PHOTOS where DRAFT_ID = '"+DRAFT_ID+"' and TOURPHOTO = '"+mAddTourDO.tourPhotos.get(i).ImagePath+"'",null);
                if(cur.getCount() == 0){

                    System.out.println("AUDIO SIZE::"+mAddTourDO.tourPhotos.get(i).audio_duration);
                    ContentValues phto = new ContentValues();
                    phto.put("DRAFT_ID", DRAFT_ID); // DRAFT_ID
                    phto.put("TOURPHOTO", mAddTourDO.tourPhotos.get(i).ImagePath);
                    phto.put("TOURPHOTO_AUDIO", mAddTourDO.tourPhotos.get(i).audioPath);
                    phto.put("TOURPHOTO_TEXT", mAddTourDO.tourPhotos.get(i).text);
                    phto.put("TOURPHOTO_SUMMERY", mAddTourDO.tourPhotos.get(i).summary);
                    phto.put("TOUR_LOCATION", mAddTourDO.tourPhotos.get(i).location);
                    phto.put("TOUR_LATTITUDE", mAddTourDO.tourPhotos.get(i).latitude);
                    phto.put("TOUR_LONGITUDE", mAddTourDO.tourPhotos.get(i).longitude);
                    phto.put("TOURPHOTO_AUDIO_DURATION", mAddTourDO.tourPhotos.get(i).audio_duration);


                    phto.put("STATUS", "no");

                    db.insert("DRAFT_PHOTOS",null,phto);

                    System.out.println("PHOTOS"+"INSERTED : "+mAddTourDO.tourPhotos.get(i).audioPath+",TEXT : "+mAddTourDO.tourPhotos.get(i).text);
                }else{
                    ContentValues phto = new ContentValues();
                    phto.put("DRAFT_ID", DRAFT_ID); // DRAFT_ID
                    phto.put("TOURPHOTO", mAddTourDO.tourPhotos.get(i).ImagePath);
                    phto.put("TOURPHOTO_AUDIO", mAddTourDO.tourPhotos.get(i).audioPath);
                    phto.put("TOURPHOTO_TEXT", mAddTourDO.tourPhotos.get(i).text);
                    phto.put("TOURPHOTO_SUMMERY", mAddTourDO.tourPhotos.get(i).summary);
                    phto.put("TOUR_LOCATION", mAddTourDO.tourPhotos.get(i).location);
                    phto.put("TOUR_LATTITUDE", mAddTourDO.tourPhotos.get(i).latitude);
                    phto.put("TOUR_LONGITUDE", mAddTourDO.tourPhotos.get(i).longitude);
                    phto.put("TOURPHOTO_AUDIO_DURATION", mAddTourDO.tourPhotos.get(i).audio_duration);

                    phto.put("STATUS", "no");

                    db.update("DRAFT_PHOTOS",phto,"DRAFT_ID" + " = ? and TOURPHOTO = ?", new String[] { DRAFT_ID,mAddTourDO.tourPhotos.get(i).ImagePath});

                    System.out.println("PHOTOS"+"UPADTE : "+mAddTourDO.tourPhotos.get(i).ImagePath+" "+mAddTourDO.tourPhotos.get(i).audioPath+""+mAddTourDO.tourPhotos.get(i).text+" audio"+mAddTourDO.tourPhotos.get(i).audio_duration);

                    Log.e("Updated","Audio : "+mAddTourDO.tourPhotos.get(i).audioPath+",TEXT : "+mAddTourDO.tourPhotos.get(i).text);

                }

            }
        }

        // Inserting Row
        db.update("DRAFT_MAIN", values, "DRAFT_ID" + " = ?",
                new String[] { DRAFT_ID });
        db.close(); // Closing database connection
    }


    public void updateDetailsToDraftTableNew(MyTourItemDO mAddTourDO, String DRAFT_ID){

        Log.e(TAG,"Saved the details of "+DRAFT_ID);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mainCur = db.rawQuery("Select * from DRAFT_MAIN  where DRAFT_ID = '"+DRAFT_ID+"'",null);





        ContentValues values = new ContentValues();
        values.put("TOURPHOTO", mAddTourDO.tourPhotoPath); // DRAFT_ID
        values.put("TOUR_TITLE", mAddTourDO.title); // DRAFT_ID
        values.put("TOUR_SUMMARY", mAddTourDO.summary); // DRAFT_ID
        values.put("TOUR_LOCATION", mAddTourDO.location); // DRAFT_ID
        values.put("TOUR_LATTITUDE", ""+mAddTourDO.tourLatitude); // DRAFT_ID
        values.put("TOUR_LONGITUDE", ""+mAddTourDO.tourLangitude); // DRAFT_ID
        values.put("STATUS", "yes"); // DRAFT_ID
        values.put("DRAFT_ID", ""+DRAFT_ID);

        Log.e("DRAFT_ID","Updated : "+DRAFT_ID);
//        db.execSQL("delete from DRAFT_PHOTOS where DRAFT_ID ='"+DRAFT_ID+"'");

        if(mAddTourDO.subtour.size()>0){
            for(int i=0;i<mAddTourDO.subtour.size();i++){


                System.out.println("INSERTING_SUMMERY::"+mAddTourDO.subtour.get(i).photoId);
                Cursor cur = db.rawQuery("Select * from DRAFT_PHOTOS where DRAFT_ID = '"+DRAFT_ID+"' and TOURPHOTO = '"+mAddTourDO.subtour.get(i).ImagePath+"'",null);
                if(cur.getCount() == 0){

                    System.out.println("AUDIO SIZE::"+mAddTourDO.subtour.get(i).audio_duration);
                    ContentValues phto = new ContentValues();
                    phto.put("DRAFT_ID", DRAFT_ID); // DRAFT_ID
                    phto.put("TOURPHOTO", mAddTourDO.subtour.get(i).ImagePath);
                    phto.put("TOURPHOTO_ID",mAddTourDO.subtour.get(i).photoId);
                    phto.put("TOURPHOTO_AUDIO", mAddTourDO.subtour.get(i).audioPath);
                    phto.put("TOURPHOTO_TEXT", mAddTourDO.subtour.get(i).title);
                    phto.put("TOURPHOTO_SUMMERY", mAddTourDO.subtour.get(i).summary);
                    phto.put("TOUR_LOCATION", mAddTourDO.subtour.get(i).location);
                    phto.put("TOUR_LATTITUDE", mAddTourDO.subtour.get(i).latitude);
                    phto.put("TOUR_LONGITUDE", mAddTourDO.subtour.get(i).longitude);
                    phto.put("TOURPHOTO_AUDIO_DURATION", mAddTourDO.subtour.get(i).audio_duration);


                    phto.put("STATUS", "no");

                    db.insert("DRAFT_PHOTOS",null,phto);
                    System.out.println("PHOTOS"+"INSERTED : "+mAddTourDO.subtour.get(i).audioPath+",TEXT : "+mAddTourDO.subtour.get(i).title);
                }else{
                    ContentValues phto = new ContentValues();
                    phto.put("DRAFT_ID", DRAFT_ID); // DRAFT_ID
                    phto.put("TOURPHOTO", mAddTourDO.subtour.get(i).ImagePath);
                    phto.put("TOURPHOTO_ID",mAddTourDO.subtour.get(i).photoId);
                    phto.put(" ", mAddTourDO.subtour.get(i).audioPath);
                    phto.put("TOURPHOTO_TEXT", mAddTourDO.subtour.get(i).title);
                    phto.put("TOURPHOTO_SUMMERY", mAddTourDO.subtour.get(i).summary);
                    phto.put("TOUR_LOCATION", mAddTourDO.subtour.get(i).location);
                    phto.put("TOUR_LATTITUDE", mAddTourDO.subtour.get(i).latitude);
                    phto.put("TOUR_LONGITUDE", mAddTourDO.subtour.get(i).longitude);
                    phto.put("TOURPHOTO_AUDIO_DURATION", mAddTourDO.subtour.get(i).audio_duration);

                    phto.put("STATUS", "no");

                    db.update("DRAFT_PHOTOS",phto,"DRAFT_ID" + " = ? and TOURPHOTO = ?",
                            new String[] { DRAFT_ID,mAddTourDO.subtour.get(i).ImagePath});

                    System.out.println("PHOTOS"+"UPADTE : "+mAddTourDO.subtour.get(i).ImagePath+" "+mAddTourDO.subtour.get(i).audioPath+""+mAddTourDO.subtour.get(i).text+" audio"+mAddTourDO.subtour.get(i).audio_duration);

                    Log.e("Updated","Audio : "+mAddTourDO.subtour.get(i).audioPath+",TEXT : "+mAddTourDO.subtour.get(i).title);

                }

            }

        }

        // Inserting Row

        if(mainCur.getCount()==0){
            db.insert("DRAFT_MAIN",null, values);
        }else{

            db.update("DRAFT_MAIN", values, "DRAFT_ID" + " = ?",
                    new String[] { DRAFT_ID });



        }

        db.close(); // Closing database connection
    }

   /* public void insertOrUpdate(String drftId,String image){
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cur = db.rawQuery("Select * from DRAFT_PHOTOS where DRAFT_ID = '"+drftId+"' and TOURPHOTO = '"+image+"'",null);
        if(cur.getCount() == 0){
            ContentValues phto = new ContentValues();
            phto.put("DRAFT_ID", drftId); // DRAFT_ID
            phto.put("TOURPHOTO", image);
            phto.put("STATUS", "no");

            db.insert("DRAFT_PHOTOS",null,phto);
        }
        db.close();
    }
*/
    public void deleteCategories (String draftId){
        SQLiteDatabase db = this.getWritableDatabase();
        String deletQuery="delete from DRAFT_CATEGORIES where DRAFT_ID='"+draftId+"'";
        db.execSQL(deletQuery);
        db.close();

    }
    public void insertCategpries(String draftId, ArrayList<CategoryDO> categoryDOs){
        SQLiteDatabase db = this.getWritableDatabase();
        for(int i=0;i<categoryDOs.size();i++) {

        }
      /*  String DRAFT_CATEGOTIES = "CREATE TABLE IF NOT EXISTS DRAFT_CATEGORIES (DRAFT_ID TEXT,CATID TEXT,CAT_NAME TEXT,CATPHOTO_TEXT TEXT,STATUS TEXT)";

        String DRAFT_TAGS = "CREATE TABLE IF NOT EXISTS DRAFT_TAGS (DRAFT_ID TEXT,TAGID TEXT,TAGNAME TEXT,STATUS TEXT)";*/
       for(int i=0;i<categoryDOs.size();i++) {
           String inserQuery = "insert into DRAFT_CATEGORIES(DRAFT_ID,CATID,CAT_NAME,CATPHOTO_TEXT,STATUS) values('" + draftId + "','" + categoryDOs.get(i).categorieId + "','" + categoryDOs.get(i).categorieName + "','" +
                   categoryDOs.get(i).categorieImageLocation + "'," + "'false')";

           System.out.println("categoriesInsertQuery"+inserQuery);
           db.execSQL(inserQuery);
       }

        db.close();
    }
    public void updateCategories(String draftId,ArrayList<CategoryDO> categoryDOs){

        SQLiteDatabase db = this.getWritableDatabase();

        for(int i=0;i<categoryDOs.size();i++) {

            String updateQuery = "update DRAFT_CATEGORIES SET CATID='" +categoryDOs.get(i).categorieId+ "',"+
                    "CAT_NAME='"+categoryDOs.get(i).categorieName+"',CATPHOTO_TEXT='"+categoryDOs.get(i).categorieImageLocation+"' WHERE DRAFT_ID='"+draftId+"'";
            db.execSQL(updateQuery);


        }
         db.close();
    }
    public void deleteTags(String draftId){
        SQLiteDatabase db = this.getWritableDatabase();
        String deletQuery="delete from DRAFT_TAGS where DRAFT_ID='"+draftId+"'";

        System.out.println("DELETETAGS::"+deletQuery);
        db.execSQL(deletQuery);
        db.close();

    }
    public void deleteDrafts(String draftId){
        SQLiteDatabase db = this.getWritableDatabase();
        String deletQuery="delete from DRAFT_MAIN where DRAFT_ID='"+draftId+"'";

        System.out.println("DELETETAGS::"+deletQuery);
        db.execSQL(deletQuery);
        db.close();

    }
    public void deleteDraftsPhotos(String draftId){
        SQLiteDatabase db = this.getWritableDatabase();
        String deletQuery="delete from DRAFT_PHOTOS where DRAFT_ID='"+draftId+"'";

        System.out.println("DELETETAGS::"+deletQuery);
        db.execSQL(deletQuery);
        db.close();

    }
    public void insertTags(String draftId, ArrayList<TagsDo> tagsDos){
        SQLiteDatabase db = this.getWritableDatabase();

      /*  String DRAFT_CATEGOTIES = "CREATE TABLE IF NOT EXISTS DRAFT_CATEGORIES (DRAFT_ID TEXT,CATID TEXT,CAT_NAME TEXT,CATPHOTO_TEXT TEXT,STATUS TEXT)";

        String DRAFT_TAGS = "CREATE TABLE IF NOT EXISTS DRAFT_TAGS (DRAFT_ID TEXT,TAGID TEXT,TAGNAME TEXT,STATUS TEXT)";*/

        for(int i=0;i<tagsDos.size();i++) {
            String inserQuery = "insert into DRAFT_TAGS(DRAFT_ID,TAGID,TAGNAME,STATUS,SEARCH) values('" + draftId + "','" + tagsDos.get(i).tagId + "','" + tagsDos.get(i).tagName + "','false"+ "','" + tagsDos.get(i).tagSearch+ "')";

          System.out.println("TAGSINSERT::"+inserQuery);
            db.execSQL(inserQuery);
        }
        db.close();;

    }
    public ArrayList<CategoryDO> getCategories(String draftId){


        ArrayList<CategoryDO> categoryDOs = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery="Select * from DRAFT_CATEGORIES where DRAFT_ID='"+draftId+"'";

        System.out.println("selectQuery::"+selectQuery);


        Cursor cursor=db.rawQuery(selectQuery,null);

        if(cursor.moveToFirst()){


            do{
                CategoryDO categoryDO = new CategoryDO();

                String catId=cursor.getString(cursor.getColumnIndex("CATID"));
                String catName=cursor.getString(cursor.getColumnIndex("CAT_NAME"));
                String catImage=cursor.getString(cursor.getColumnIndex("CATPHOTO_TEXT"));

                categoryDO.categorieId=catId;
                categoryDO.categorieName=catName;
                categoryDO.categorieImageLocation=catImage;

                System.out.println("catName::"+catName);


                categoryDOs.add(categoryDO);


            }while (cursor.moveToNext());
        }

        return categoryDOs;

    }
    public ArrayList<TagsDo> getTags(String draftId){


        ArrayList<TagsDo> tagsDos = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery="Select * from DRAFT_TAGS where DRAFT_ID='"+draftId+"'";


        System.out.println("TAGSselectQuery::"+selectQuery);

        Cursor cursor=db.rawQuery(selectQuery,null);

            if(cursor.moveToFirst()){


            do{
                TagsDo tagsDo = new TagsDo();

                String tagId=cursor.getString(cursor.getColumnIndex("TAGID"));
                String tagName=cursor.getString(cursor.getColumnIndex("TAGNAME"));
                String SEARCH=cursor.getString(cursor.getColumnIndex("SEARCH"));

                tagsDo.tagId=tagId;
                tagsDo.tagName=tagName;
                tagsDo.tagSearch=SEARCH;

                System.out.println("tagName"+tagName);

                tagsDos.add(tagsDo);


            }while (cursor.moveToNext());
        }
       db.close();;
        return tagsDos;

    }

    public int getLastRecordId(){

        String selectQuery = "SELECT * FROM DRAFT_MAIN";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        int count = cursor.getCount();

        cursor.close();
        db.close();

        return count;
    }

    public void deleteTour(String tourId){
//        d.execSQL("delete from DRAFT_MAIN where DRAFT_ID='"+tourId+"'");
        SQLiteDatabase db = this.getWritableDatabase();

        String query ="delete from DRAFT_MAIN where DRAFT_ID='"+tourId+"'";
        System.out.println("delete query::"+query);
        db.execSQL(query);
    }

    public void updateTourPhotoInDraft(String draftId,String updatedIcon,String date){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("TOURPHOTO", updatedIcon); // DRAFT_ID
        values.put("CREATE_DATE", date); // DRAFT_ID

        // updating row
        db.update("DRAFT_MAIN", values, "DRAFT_ID" + " = ?",
                new String[] { draftId });

        db.close();

    }

    public ArrayList<MyTourItemDO> getAllListFromDraft() {
        List<MyTourItemDO> contactList = new ArrayList<MyTourItemDO>();
        // Select All Query
        String selectQuery = "SELECT * FROM DRAFT_MAIN order by CREATE_DATE desc";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                // Adding contact to list
                MyTourItemDO mImageModel = new MyTourItemDO();
                mImageModel.tourId = cursor.getString(0);
                mImageModel.tourPhotoPath = cursor.getString(1);
                mImageModel.title = cursor.getString(2);
                mImageModel.summary = cursor.getString(3);
                mImageModel.area = cursor.getString(4);
                mImageModel.tourLatitude = cursor.getString(5);
                mImageModel.tourLangitude = cursor.getString(6);
                mImageModel.status = cursor.getString(7);
                System.out.println("tourLatitude:lat:"+cursor.getString(5));
                System.out.println("tourLangitude:long:"+ cursor.getString(6));

                Cursor subTourCursor = db.rawQuery("Select * from DRAFT_PHOTOS where DRAFT_ID = '"+cursor.getString(0)+"'",null);
                System.out.println("sql:Select * from DRAFT_PHOTOS where DRAFT_ID = "+cursor.getString(0));
                if (subTourCursor.moveToFirst()) {
                    do {
                        // Adding contact to list
                        TourItem mTourItem = new TourItem();
                        mTourItem.ImagePath = subTourCursor.getString(1);
                        mTourItem.audioPath = subTourCursor.getString(2);
                        mTourItem.text = subTourCursor.getString(3);
                        mTourItem.title = subTourCursor.getString(3);

                        mTourItem.tourId = subTourCursor.getString(0);
                        mTourItem.summary = subTourCursor.getString(4);
                        mTourItem.photoCaption=subTourCursor.getString(4);
                        mTourItem.location = subTourCursor.getString(5);
                        mTourItem.latitude = subTourCursor.getString(6);
                        mTourItem.longitude = subTourCursor.getString(7);
                        mTourItem.audio_duration=subTourCursor.getString(8);
                        mTourItem.photoId=subTourCursor.getString(10);

                        System.out.println("photoId::"+subTourCursor.getString(10));

                        Log.e("Got","Audio : "+mTourItem.audioPath+",TEXT : "+mTourItem.text);
                        mImageModel.subtour.add(mTourItem);
                    } while (subTourCursor.moveToNext());
                }
                subTourCursor.close();

                contactList.add(mImageModel);
            } while (cursor.moveToNext());
        }
        Log.e(TAG,"contact list : "+contactList.size());
        // return contact list
        return (ArrayList<MyTourItemDO>) contactList;

    }
    public void backupDatabase(Context context) throws IOException {
        //Open your local db as the input stream
        String inFileName ="/data/data/com.showmeround/databases"+"/"+DB_NAME;
        //OR use- context.getFilesDir().getPath()+"/databases/yourDBName.db";//
        File dbFile = new File(inFileName);
        FileInputStream fis = new FileInputStream(dbFile);

        String outFileName = Environment.getExternalStorageDirectory()+"/"+DB_NAME	;
        //Open the empty db as the output stream
        OutputStream output = new FileOutputStream(outFileName);
        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = fis.read(buffer))>0){
            output.write(buffer, 0, length);
        }
        //Close the streams
        output.flush();
        output.close();
        fis.close();

        Toast.makeText(context, "Copied your database file into sd card", Toast.LENGTH_LONG).show();
    }
    public ArrayList<TourItem> getSubTourItems(String draftId){
        ArrayList<TourItem> subItem= new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor subTourCursor = db.rawQuery("Select * from DRAFT_PHOTOS where DRAFT_ID = '"+draftId+"'",null);

        if (subTourCursor.moveToFirst()) {
            do {
                // Adding contact to list
                TourItem mTourItem = new TourItem();
                mTourItem.ImagePath = subTourCursor.getString(1);
                mTourItem.audioPath = subTourCursor.getString(2);
                mTourItem.text = subTourCursor.getString(3);
                mTourItem.title = subTourCursor.getString(3);

                mTourItem.tourId = subTourCursor.getString(0);
                mTourItem.summary = subTourCursor.getString(4);
                mTourItem.photoCaption=subTourCursor.getString(4);
                mTourItem.location = subTourCursor.getString(5);
                mTourItem.latitude = subTourCursor.getString(6);
                mTourItem.longitude = subTourCursor.getString(7);
                mTourItem.audio_duration=subTourCursor.getString(8);
                mTourItem.photoId=subTourCursor.getString(10);



                Log.e("Got","Audio : "+mTourItem.audioPath+",TEXT : "+mTourItem.text);
                subItem.add(mTourItem);
            } while (subTourCursor.moveToNext());
        }
        subTourCursor.close();

        return subItem;
    }

}
