package com.sageabletech.viewtour;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.sageabletech.adapter.FeedsAdapter;
import com.sageabletech.model.FeedsDO;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;
import com.sageabletech.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by ASMAN on 24-07-2017.
 */

public class SearchFeeds extends AsyncTask<Void, Void, String> {
    ProgressDialog dialog;
    Activity activity;
    PreferenceUtils preferenceUtils;
    String searchstr;
    FeedsAdapter mOfferAdapter;
    boolean flag;

    public SearchFeeds(Activity activity, String searchstr, FeedsAdapter mOfferAdapter,boolean flag) {
        this.activity = activity;
        preferenceUtils = new PreferenceUtils(activity);
        this.searchstr = searchstr;
        this.mOfferAdapter = mOfferAdapter;
        this.flag=flag;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(activity);
        dialog.setMessage("please wait..");
        dialog.show();
    }

    @Override
    protected String doInBackground(Void... params) {
        HttpClient httpClient = new DefaultHttpClient();

        HttpContext localContext = new BasicHttpContext();
//        String url = Urls.DELETE+accessToken+"?tourid="+myTourItemDO.tourId;
        HttpPost httppost = new HttpPost(Urls.SEARCH_FEEDS + preferenceUtils.getStringFromPreference("accessToken", ""));

        httppost.setHeader("Content-Type", "application/json");

        try {
            URL url = httppost.getURI().toURL();
            System.out.println("urlddd::" + url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        JSONObject jsonObject;
        JSONObject searchJson = LocalData.getInstance().getSearchJson();
        int length = searchJson.length();
        System.out.println("json Lenght:::" + length);
        if (length > 5) {
            jsonObject = LocalData.getInstance().getSearchJson();
            try {
                jsonObject.put("searchStr", searchstr);
                jsonObject.put("searchStr", searchstr);
                httppost.setEntity((new StringEntity(jsonObject.toString())));
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            System.out.println("local json data::" + jsonObject.toString());
        } else {
            jsonObject = new JSONObject();
            try {

                jsonObject.put("searchStr", searchstr);
                jsonObject.put("tagids", new JSONArray());
                jsonObject.put("categoryIds", new JSONArray());
                jsonObject.put("author", "");
                jsonObject.put("location", "");
                jsonObject.put("minradius", 0);
                jsonObject.put("maxradius", 0);
                jsonObject.put("userLatitude", 0);
                jsonObject.put("userLangitude", 0);
                jsonObject.put("pageLastRecord", 0);

                httppost.setEntity((new StringEntity(jsonObject.toString())));
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            System.out.println("normal json data::" + jsonObject.toString());
        }


        System.out.println("json object::" + jsonObject.toString());
        String text = "";
        try {
            HttpResponse response = httpClient.execute(httppost, localContext);
            HttpEntity entity = response.getEntity();
            text = getASCIIContentFromEntity(entity);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return e.getLocalizedMessage();
        }
        return text;

    }

    HashMap<String, Object> totalHash = new HashMap<>();

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        dialog.dismiss();
        if(!flag){
            activity.finish();
        }



        System.out.println("datat got from search List :::" + s);

        try {
            JSONObject jsonObject = new JSONObject(s);
            String message = jsonObject.getString("message");
            if (jsonObject.has("message") && message.equalsIgnoreCase("success")) {
                System.out.println("news dataaa:::" + jsonObject);
                JSONArray jsondata = jsonObject.optJSONArray("data");

                ArrayList<FeedsDO> mFeeds = new ArrayList<>();
                for (int i = 0; i < jsondata.length(); i++) {
                    JSONObject jo = jsondata.getJSONObject(i);
                    FeedsDO mfeeds = new FeedsDO();
                    mfeeds.tourPhotoPath = jo.optString("tourPhotoPath");
                    mfeeds.tourTitle = jo.optString("tourTitle");
                    mfeeds.tourSummary = jo.optString("tourSummary");
                    mfeeds.tourLatitude = jo.optString("tourLatitude");
                    mfeeds.tourLangitude = jo.optString("tourLangitude");
                    mfeeds.viewCount = jo.optString("viewCount");
                    mfeeds.commentsCount = jo.optString("commentsCount");
                    mfeeds.tourOrderNumber = jo.optString("tourOrderNumber");
                    mfeeds.tourId = jo.optString("tourId");
                    mfeeds.userPicPath = jo.optString("userPicPath");
                    mfeeds.userId = jo.optString("userId");
//                    long createdOn = jo.getLong("createdOn");
//                    String s1 = convertDate(createdOn);
                    mfeeds.favcount = jo.optString("favCount");

                    mfeeds.tourTime = jo.optString("tourTime");
                    String timeAgo = Utils.getTimeAgo(mfeeds.tourTime);
                    System.out.println("timeAgo::"+timeAgo);
                    mfeeds.tourTime=timeAgo;


//                    String timeAgo = Utils.getTimeAgo(mfeeds.tourTime);
//                    System.out.println("timeAgo::"+timeAgo);
                    mfeeds.userName = jo.optString("userName");
                    mfeeds.isfavorite = jo.optString("isfavourite");

                    JSONArray categoriesList = jo.getJSONArray("categoriesList");
                    ArrayList<String> catArry = new ArrayList<>();
                    for (int cat = 0; cat < categoriesList.length(); cat++) {
                        JSONObject catObject = categoriesList.getJSONObject(cat);
                        String categorieName = catObject.getString("categorieName");
                        catArry.add("#" + categorieName);

                    }
                    mfeeds.catList = catArry;

                    JSONArray tagList = jo.getJSONArray("tagsList");
                    ArrayList<String> tagArry = new ArrayList<>();
                    for (int tag = 0; tag < tagList.length(); tag++) {
                        JSONObject tagObject = tagList.getJSONObject(tag);
                        String tagName = tagObject.getString("tagName");
                        tagArry.add("#" + tagName);

                    }
                    mfeeds.tagList = tagArry;


                    mFeeds.add(mfeeds);
                }

//                LocalData.getInstance().setExistingFeeds(mFeeds);
                System.out.println("mFeeds size::"+mFeeds);
                mOfferAdapter.mOffers = mFeeds;
                mOfferAdapter.notifyDataSetChanged();
                mOfferAdapter.refresh();
            } else {
                Toast.makeText(activity, "No data found", Toast.LENGTH_SHORT).show();
            }


        } catch (Exception e) {
            System.out.println("eroor:::" + e.toString());
        }
    }

    public String convertDate(long data) {


        long val = data;
        Date date = new Date(val);
        SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateText = df2.format(date);
        System.out.println(dateText);
        return dateText;

    }

    protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
        InputStream in = entity.getContent();
        StringBuffer out = new StringBuffer();
        int n = 1;
        while (n > 0) {
            byte[] b = new byte[4096];
            n = in.read(b);
            if (n > 0) out.append(new String(b, 0, n));
        }
        return out.toString();
    }

    public String getString(String object, JSONObject jsonobject) throws JSONException {
        String data = "";
        if (jsonobject.has(object)) {
            data = jsonobject.getString(object);
            return data;
        }
        return data;
    }
}
