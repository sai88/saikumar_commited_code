package com.sageabletech.viewtour;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.utils.BitmapUtils;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;
import com.sageabletech.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ASMAN on 24-07-2017.
 */

public class GetTourDetails extends AsyncTask <Void, Void, String>{
    ProgressDialog dialog;
    Activity activity;
    PreferenceUtils preferenceUtils;
    String tourId,flag;
    public GetTourDetails(Activity activity, String tourId,String flag) {
        this.activity=activity;
        this.tourId=tourId;
        preferenceUtils = new PreferenceUtils(activity);
        this.flag=flag;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog=new ProgressDialog(activity);
        dialog.setMessage("please wait..");
        dialog.show();
    }

    @Override
    protected String doInBackground(Void... params) {
        HttpClient httpClient = new DefaultHttpClient();

        HttpContext localContext = new BasicHttpContext();
        HttpPost httppost = new HttpPost(Urls.GET_SINGLE_TOUR_DETAILS+preferenceUtils.getStringFromPreference("accessToken","")+"?tourid="+tourId);

        httppost.setHeader("Content-Type", "application/json");
//

        String text = "";
            try {
                HttpResponse response = httpClient.execute(httppost,localContext);
                HttpEntity entity = response.getEntity();
                text = getASCIIContentFromEntity(entity);
            } catch (Exception e) {
                System.out.println(e.getLocalizedMessage());
                return e.getLocalizedMessage();
            }
        return text;

    }
HashMap<String,Object> totalHash=new HashMap<>();
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        System.out.println("datat got from single tour:::"+s);
        Log.e("SDfdsf","response:::"+s);
        try {
            JSONObject jsonObject=new JSONObject(s);
            if (jsonObject.getString("message").equalsIgnoreCase("success")) {

//                        JSONArray jsondataTour = jsonObjectMain.optJSONArray("data");
                JSONObject jo = jsonObject.getJSONObject("data");
//						ArrayList<MyTourItemDO> favoritesArray = LocalData.getInstance().getFavoritesArray();
//                ArrayList<MyTourItemDO> mToursArry = new ArrayList<>();
                MyTourItemDO mtours = new MyTourItemDO();
                mtours.tourId = jo.getString("tourId");
                mtours.userId = jo.getString("userId");
                mtours.title = jo.getString("title");
                mtours.summary = jo.getString("summary");
                mtours.address = jo.getString("address");
                mtours.location = jo.getString("address");
                mtours.area = jo.getString("area");
                mtours.city = jo.getString("city");
//                mtours.areacode = jo.getString("areacode");
                mtours.tourLatitude = jo.getString("tourLatitude");
                mtours.tourLangitude = jo.getString("tourLangitude");
                mtours.tourPhotoPath = jo.getString("tourPhotoPath");
                JSONArray categoriesList = jo.getJSONArray("categoriesList");
                ArrayList<String> catArry=new ArrayList<>();
                for (int cat=0;cat<categoriesList.length();cat++){
                    JSONObject catObject = categoriesList.getJSONObject(cat);
                    String categorieName = catObject.getString("categorieName");
                    catArry.add("#"+categorieName);

                }
                mtours.catArry=catArry;

                JSONArray tagList = jo.getJSONArray("tagsList");
                ArrayList<String> tagArry=new ArrayList<>();
                for (int tag=0;tag<tagList.length();tag++){
                    JSONObject tagObject = tagList.getJSONObject(tag);
                    String tagName = tagObject.getString("tagName");
                    tagArry.add("#"+tagName);

                }
                mtours.tagArry=tagArry;
//						mtours.tour = jo.getString("tourPhotoThumb");
//						mtours.tourPhotoThumbExt = jo.getString("tourPhotoThumbExt");
                mtours.createdOn = Utils.longToDate(jo.getLong("createdOn"));
                mtours.tourPhotoExt = jo.getString("tourPhotoExt");
//                mtours.comments = jo.getString("comments");
//                mtours.viewCount = jo.getString("viewCount");
//						mtours.draftTourFlag = jo.getString("draftTourFlag");
                JSONArray subJsonObject = jo.getJSONArray("photos");
                ArrayList<TourItem> subList = new ArrayList<>();
                TourItem[] tourItemsArry=new TourItem[subJsonObject.length()];

                for (int i = 0; i < subJsonObject.length(); i++) {
                    JSONObject c = subJsonObject.getJSONObject(i);
                    TourItem subItem = new TourItem();
                    subItem.photoId = c.getString("photoId");
                    subItem.title = c.getString("photoTitle");
                    subItem.photoCaption = c.getString("photoCaption");
                    subItem.location = c.getString("photoLocation");
                    subItem.ImagePath = c.getString("photoPath");
//							subItem.photoThumbPath = c.getString("photoThumbPath");
                    subItem.audioPath = c.getString("photoAudioPath");
                    subItem.audioTime = c.getString("audioTime");
//							subItem.imageExtension = c.getString("imageExtension");
//							subItem.audioExtension = c.getString("audioExtension");
                    subItem.tourLatitude = c.getString("photolatitude");
                    subItem.tourLangitude = c.getString("photolangitude");
                    subItem.latitude = c.getString("photolatitude");
                    subItem.longitude = c.getString("photolangitude");
                    addDataWithTourOrder(c,subList,subItem,tourItemsArry);
//
                }
                if(tourOrderNumberflag) {
                    Collections.addAll(subList, tourItemsArry);
                }

//                subList.removeAll(Collections.singleton(null));
//                System.out.println("sublist size::"+subList.size());

                System.out.println("subList Array::"+subList);


                mtours.subtour = subList;
//                mToursArry.add(mtours);
                Log.e("dfdsf","data saved::"+mtours.title);
                System.out.println("data saved::"+mtours.title);

                LocalData.getInstance().setFavselectedItem(mtours);
                LocalData.getInstance().setTourSeletedItem(mtours);
                if(flag.equalsIgnoreCase("location")){

                    dialog.dismiss();
                    activity.startActivity(new Intent(activity, MultiplePoints.class).putExtra("title", mtours.title));


                }else{
                    dialog.dismiss();

                    activity.startActivity(new Intent(activity, FavMainTour.class).putExtra("From", "MyTour"));
                }



//
//                        }
//						mDataListener.onData(mTours, null, tag);
            }else {
                dialog.dismiss();
                Toast.makeText(activity, "Could not get details... ", Toast.LENGTH_SHORT).show();
//						mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
            }
        }catch(Exception e){
            dialog.dismiss();
            System.out.println("exception::::"+e.toString());

        }
    }
    boolean tourOrderNumberflag=false;

    public void addDataWithTourOrder(JSONObject joj,ArrayList<TourItem> subtour,TourItem ti ,TourItem[] tourItemsArry){
        try {
            if (joj.has("tourOrderNumber")) {
                Object tourOrderNumber1 = joj.get("tourOrderNumber");
                boolean nullObject = joj.isNull("tourOrderNumber");
                System.out.println("nullObject::" + nullObject);
                if (tourOrderNumber1 != null && !nullObject) {
                    int tourOrderNumber = joj.getInt("tourOrderNumber");
                    tourOrderNumber= tourOrderNumber-1;
//                    while (subtour.size() < tourOrderNumber) {
//                        subtour.add(null);
//                    }
//                    if(tourOrderNumber>(subtour.size()-1)){
//                        subtour.ensureCapacity(tourOrderNumber+10);
//                        System.out.println("arry size:::"+subtour.size());
//                    }
//                    subtour.add(tourOrderNumber, ti);
                    tourItemsArry[tourOrderNumber]=ti;
                    tourOrderNumberflag=true;
                } else {
                    subtour.add(ti);
                }
            } else {
                subtour.add(ti);
            }

        }catch (Exception e) {
            System.out.println("exception in subbb::"+e.toString());
        }



    }
    protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
        InputStream in = entity.getContent();
        StringBuffer out = new StringBuffer();
        int n = 1;
        while (n>0) {
            byte[] b = new byte[4096];
            n =  in.read(b);
            if (n>0) out.append(new String(b, 0, n));
        }
        return out.toString();
    }
    public String getString(String object,JSONObject jsonobject) throws JSONException {
        String data="";
        if(jsonobject.has(object)){
            data= jsonobject.getString(object);
            return data;
        }
        return data;
    }
}
