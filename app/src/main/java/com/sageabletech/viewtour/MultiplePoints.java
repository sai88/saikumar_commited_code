package com.sageabletech.viewtour;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sageabletech.R;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.BaseActivity;
import com.sageabletech.utils.BitmapUtils;
import com.sageabletech.utils.LocalData;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class MultiplePoints extends BaseActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    String tourLangitude, tourLatitude, title, imagePath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        TextView  tvTitle=(TextView)findViewById(R.id.header_text);

        tvTitle.setText("NEW TOUR");
      ImageView  right = (ImageView) findViewById(R.id.submit);
right.setVisibility(View.GONE);



      ImageView  back =(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                backAction();
            }
        });

//        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);


        String title = getIntent().getStringExtra("title");
        if (title.equalsIgnoreCase("No Text") || title.equalsIgnoreCase("No Title")) {
            title = "";
        }

        title = title.toUpperCase();
        this.title = title;
        tvTitle.setText(this.title);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void backAction() {
        onBackPressed();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    ArrayList<Marker> mMarkerArray = new ArrayList<Marker>();
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        MyTourItemDO tourSeletedItem = LocalData.getInstance().getTourSeletedItem();
        String tourLangitude = tourSeletedItem.tourLangitude;
        String tourLatitude = tourSeletedItem.tourLatitude;
        addmarker(tourLatitude,tourLangitude,0,tourSeletedItem.tourPhotoPath);
        ArrayList<TourItem> subtour = tourSeletedItem.subtour;
        for (int i=0;i<subtour.size();i++){
            TourItem tourItem = subtour.get(i);
            String longitude = tourItem.longitude;
            String latitude = tourItem.latitude;
            if(latitude==null||latitude.equalsIgnoreCase("")||latitude.equalsIgnoreCase("0")){
                addmarker(tourLatitude,tourLangitude,(i+1),tourItem.ImagePath);
            }else{
                addmarker(latitude,longitude,(i+1),tourItem.ImagePath);
            }

        }

zoomDynamically();

//        mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
//        LatLngBounds latLngBounds;
    }
    public void zoomDynamically(){
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : mMarkerArray) {
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.20);
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        mMap.animateCamera(cu);
    }
    public void addmarker(String latitude,String longitude,int position,String text){
        LatLng latLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
//        LatLngBounds latLngBounds = new LatLngBounds(latLng, latLng);

        Bitmap bitMap = null;
        MarkerOptions options = new MarkerOptions();
        options.position(latLng);


        Bitmap bitmap1 = BitmapUtils.getBitmap(text);


        if (bitmap1 != null) {
            BitmapDescriptorFactory.fromBitmap(bitMap);
//            Bitmap smallMarker = Bitmap.createScaledBitmap(mainTourImage, 60, 60, false);
            BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitMap);
            options.icon(bitmapDescriptor);
        } else {

            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }




        Marker marker = mMap.addMarker(options);
        mMarkerArray.add(marker);
//Change the padding as per needed



    }



}
