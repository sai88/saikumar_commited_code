package com.sageabletech.viewtour;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by ASMAN on 24-07-2017.
 */

public class DeleteTour extends AsyncTask <Void, Void, String>{
    ProgressDialog dialog;
    Activity activity;
    PreferenceUtils preferenceUtils;
    String tourId;

    public DeleteTour(Activity activity,String tourId) {
        this.activity=activity;
        preferenceUtils = new PreferenceUtils(activity);
        this.tourId=tourId;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog=new ProgressDialog(activity);
        dialog.setMessage("please wait..");
        dialog.show();
    }

    @Override
    protected String doInBackground(Void... params) {
        HttpClient httpClient = new DefaultHttpClient();

        HttpContext localContext = new BasicHttpContext();
//        String url = Urls.DELETE+accessToken+"?tourid="+myTourItemDO.tourId;
        HttpPost httppost = new HttpPost(Urls.DELETE+preferenceUtils.getStringFromPreference("accessToken","")+"?tourid="+tourId);

        httppost.setHeader("Content-Type", "application/json");

        try {
            URL url = httppost.getURI().toURL();
            System.out.println("urlddd::"+url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        String text = "";
        try {
            HttpResponse response = httpClient.execute(httppost,localContext);
            HttpEntity entity = response.getEntity();
            text = getASCIIContentFromEntity(entity);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return e.getLocalizedMessage();
        }
        return text;

    }
    HashMap<String,Object> totalHash=new HashMap<>();
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        dialog.dismiss();
        LocalData.getInstance().setSelectedMyTourItemDO(null);

        System.out.println("datat got from delete List :::"+s);

        try {
            JSONObject jsonObject = new JSONObject(s);
            String message = jsonObject.getString("message");
            if(message.equalsIgnoreCase("The Tour is deleted successfully")) {
                new GetMyToursList(activity).execute();
                    }else{
                Toast.makeText(activity,"Could not delete tour",Toast.LENGTH_SHORT);
            }


        }catch(Exception e){

        }
    }
    protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
        InputStream in = entity.getContent();
        StringBuffer out = new StringBuffer();
        int n = 1;
        while (n>0) {
            byte[] b = new byte[4096];
            n =  in.read(b);
            if (n>0) out.append(new String(b, 0, n));
        }
        return out.toString();
    }
    public String getString(String object,JSONObject jsonobject) throws JSONException {
        String data="";
        if(jsonobject.has(object)){
            data= jsonobject.getString(object);
            return data;
        }
        return data;
    }
}
