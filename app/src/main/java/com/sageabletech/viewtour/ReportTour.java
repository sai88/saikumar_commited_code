package com.sageabletech.viewtour;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.sageabletech.adapter.FeedsAdapter;
import com.sageabletech.model.FeedsDO;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ASMAN on 24-07-2017.
 */

public class ReportTour extends AsyncTask <Void, Void, String>{
    ProgressDialog dialog;
    Activity activity;
    PreferenceUtils preferenceUtils;
    String tourId;
    String tourname;
    String reportedTxt;

    public ReportTour(Activity activity, String tourId,String tourName,String reportedTxt) {
        this.activity=activity;
        this.tourId=tourId;
        this.tourname=tourName;
        this.reportedTxt=reportedTxt;
        preferenceUtils = new PreferenceUtils(activity);

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog=new ProgressDialog(activity);
        dialog.setMessage("please wait..");
        dialog.show();
    }

    @Override
    protected String doInBackground(Void... params) {
        HttpClient httpClient = new DefaultHttpClient();

        HttpContext localContext = new BasicHttpContext();
        HttpPost httppost = new HttpPost(Urls.REPORT+preferenceUtils.getStringFromPreference("accessToken",""));

        httppost.setHeader("Content-Type", "application/json");

        JSONObject tourJsonObject=new JSONObject();

        try {
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("tourId",tourId);

            tourJsonObject.put("tourReported",jsonObject);
            tourJsonObject.put("reportedSubTour",new JSONObject());
            tourJsonObject.put("tourName",tourname);
            tourJsonObject.put("reportDescription",reportedTxt);
            tourJsonObject.put("reportAudioPath","");
            tourJsonObject.put("audioExtension","");
            Date date=new Date();
            long time = date.getTime();
            tourJsonObject.put("reportedDate",time);
            URL url = httppost.getURI().toURL();
            System.out.println("url of reporttour ::"+url.toString());


            httppost.setEntity((new StringEntity(tourJsonObject.toString())));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
//


        System.out.println("customer dtata::"+tourJsonObject);
        String text = "";
        try {
            HttpResponse response = httpClient.execute(httppost,localContext);
            HttpEntity entity = response.getEntity();
            text = getASCIIContentFromEntity(entity);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return e.getLocalizedMessage();
        }
        return text;

    }
    HashMap<String,Object> totalHash=new HashMap<>();
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        dialog.dismiss();
        System.out.println("datat got from tour save view status:::"+s);
       try{
           JSONObject jsonObject=new JSONObject(s);
           String message = jsonObject.getString("message");
           if(message.equalsIgnoreCase("Tour reported successfully")){
               Toast.makeText(activity,message,Toast.LENGTH_LONG).show();
               activity.finish();
           }else{
               Toast.makeText(activity,message,Toast.LENGTH_LONG).show();
               activity.finish();
           }
       }catch (Exception e){

       }
//        {"message":"Tour reported successfully","data":{"reportId":80,"tourReported":{"tourId":679,"userOfTour":{"userId":192,"userLoginId":null,"firstName":"Reshma","lastName":"B","location":null,"email":"reshma@sageabletech.com","userPicPath":"192/10.jpeg","userPicExt":null,"password":"1052733a84880e1a0010f71240f24a0f49e6791c6daa1d5cbc5fa6cb6353e984","createdBy":null,"createdOn":1498721693466,"updatedBy":null,"updatedOn":1501770471885,"latitude":null,"longitude":null,"loginUsing":null,"status":true,"resetPwd":false},"title":"Owl Image","summary":"Summary","address":"Jaggayyapet, Andhra Pradesh, India Jaggayyapet, Andhra Pradesh, India","area":"Jaggayyapet, Andhra Pradesh, India Jaggayyapet, Andhra Pradesh, India","city":"Jaggayyapet, Andhra Pradesh, India Jaggayyapet, Andhra Pradesh, India","areacode":null,"tourLatitude":16.9021166,"tourLangitude":80.1037721,"tourPhotoPath":"192/7.jpeg","tourPhotoExt":"jpeg","tourPhotoThumb":"192/7_thumb.jpeg","tourPhotoThumbExt":"jpeg","createdOn":1502800399844,"draftTourFlag":null},"reportedUser":{"userId":199,"userLoginId":null,"firstName":"sai","lastName":"kumar","location":null,"email":"kumari@asmansoft.com","userPicPath":"199/IMG_Feb242017_121924.jpg","userPicExt":null,"password":"e554bb1966342affc73936a75c98681e27f33d93eb01b1baf9ab2b3d250e58ac","createdBy":null,"createdOn":1500357027801,"updatedBy":null,"updatedOn":1502089692529,"latitude":null,"longitude":null,"loginUsing":null,"status":true,"resetPwd":false},"reportedSubTour":null,"tourName":"Owl Image","reportDescription":"super ","reportAudioPath":"","audioExtension":"","reportedDate":1502873626724}}

    }
    protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
        InputStream in = entity.getContent();
        StringBuffer out = new StringBuffer();
        int n = 1;
        while (n>0) {
            byte[] b = new byte[4096];
            n =  in.read(b);
            if (n>0) out.append(new String(b, 0, n));
        }
        return out.toString();
    }
    public String getString(String object,JSONObject jsonobject) throws JSONException {
        String data="";
        if(jsonobject.has(object)){
            data= jsonobject.getString(object);
            return data;
        }
        return data;
    }
}
