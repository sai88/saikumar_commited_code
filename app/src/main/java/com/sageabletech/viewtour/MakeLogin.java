package com.sageabletech.viewtour;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by ASMAN on 24-07-2017.
 */

public class MakeLogin extends AsyncTask <Void, Void, String>{
    ProgressDialog dialog;
    Activity activity;
    PreferenceUtils preferenceUtils;
    String email,password;

    public MakeLogin(Activity activity,String email ,String password) {
        this.activity=activity;
        preferenceUtils = new PreferenceUtils(activity);
        this.email=email;
        this.password=password;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog=new ProgressDialog(activity);
        dialog.setMessage("please wait..");
        dialog.show();
    }

    @Override
    protected String doInBackground(Void... params) {
        HttpClient httpClient = new DefaultHttpClient();

        HttpContext localContext = new BasicHttpContext();
        HttpPost httppost = new HttpPost(Urls.POST_LOGIN+preferenceUtils.getStringFromPreference("accessToken",""));

        httppost.setHeader("Content-Type", "application/json");
        JSONObject loginRequest = new JSONObject();
        try {
            loginRequest.put("deviceType","mobile");
            loginRequest.put("userLoginId","");
            loginRequest.put("emailId",""+email);
            loginRequest.put("password",""+password);
            loginRequest.put("loginUsing","FACEBOOK");
            loginRequest.put("deviceToken","3BBB848D-79D6-4880-BBF6-DF645756493B");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            URL url = httppost.getURI().toURL();
            System.out.println("urlddd::"+url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        String text = "";
        try {
            HttpResponse response = httpClient.execute(httppost,localContext);
            int statusCode = response.getStatusLine().getStatusCode();
            System.out.println("statusCode::"+statusCode);
            HttpEntity entity = response.getEntity();
            text = getASCIIContentFromEntity(entity);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return e.getLocalizedMessage();
        }
        return text;

    }
    HashMap<String,Object> totalHash=new HashMap<>();
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        dialog.dismiss();
        System.out.println("datat got from login List :::"+s);
//        s.getStatusLine().getStatusCode()
//        try {
//            JSONObject jsonObject = new JSONObject(s);
//
//            String message = jsonObject.getString("message");
//            if(message.equalsIgnoreCase("success")) {
//                JSONArray jsondata = jsonObject.optJSONArray("data");
//
//                ArrayList<MyTourItemDO> mFeeds = new ArrayList<>();
//                for(int i=0;i<jsondata.length();i++){
//                    JSONObject jo = jsondata.getJSONObject(i);
//                    MyTourItemDO mfeeds = new MyTourItemDO();
//                    mfeeds.tourPhotoPath = jo.optString("tourPhotoPath");
//                    mfeeds.title = jo.optString("title");
//                    mfeeds.summary = jo.optString("summary");
//                    mfeeds.tourLatitude = jo.optString("tourLatitude");
//                    mfeeds.tourLangitude = jo.optString("tourLangitude");
//                    mfeeds.viewCount = jo.optString("viewCount");
//                    mfeeds.comments = jo.optString("comments");
//                    mfeeds.area = jo.optString("area");
//                    mfeeds.city = jo.optString("city");
//                    mfeeds.tourId = jo.optString("tourId");
//                    mfeeds.tourRating = jo.optString("tourRating");
//                    mfeeds.userId = jo.optString("userId");
//                    mfeeds.createdOn = jo.optString("createdOn");
//
//                    JSONArray photsArray = jo.optJSONArray("photos");
//
//                    for(int j=0;j<photsArray.length();j++){
//
//                        JSONObject joj = photsArray.getJSONObject(j);
//                        TourItem ti = new TourItem();
//                        System.out.println("photo pathsss:::"+joj.optString("photoPath"));
//                        ti.ImagePath = joj.optString("photoPath");
//                        ti.text = joj.optString("photoTitle");
//                        ti.audioPath = joj.optString("photoAudioPath");
//                        ti.summary = joj.optString("photoCaption");
//                        ti.photoId = joj.optString("photoId");
//                        ti.location = joj.optString("photoLocation");
//                        ti.latitude = joj.optString("photolatitude");
//                        ti.longitude = joj.optString("photolangitude");
//                        ti.tourId =	jo.optString("tourId");
//
//                        mfeeds.subtour.add(ti);
//                    }


//                    mFeeds.add(mfeeds);
//
//
//                }
//                LocalData.getInstance().setmFeeds(mFeeds);
////                activity.startActivity(new Intent(activity, NewGragment.class));
//                activity.startActivity(new Intent(activity, MyTourList.class).putExtra("From","MyTour"));
//            }else{
//                Toast.makeText(activity,"Could Load mytours ",Toast.LENGTH_SHORT).show();
//            }

//                JSONObject data = jsonObject.getJSONObject("data");

//                String title = getString("title",data);
//                String summary = getString("summary",data);
//                String address = getString("address",data);
//                String area = getString("area",data);
//                String city = getString("city",data);
//                String tourLatitude = getString("tourLatitude",data);
//                String tourLangitude = getString("tourLangitude",data);
//                String tourPhotoPath = getString("tourPhotoPath",data);
//
//                HashMap<String,String> hash=new HashMap<>();
//                hash.put("title",title);
//                hash.put("summary",summary);
//                hash.put("address",address);
//                hash.put("area",area);
//                hash.put("city",city);
//                hash.put("tourLatitude",tourLatitude);
//                hash.put("tourLangitude",tourLangitude);
//                hash.put("tourPhotoPath",tourPhotoPath);
//                totalHash.put("main",hash);
//
//                LocalData.getInstance().setTotalHash(totalHash);
//
//            }else{
//                Toast.makeText(activity,"Try atfer some time.",Toast.LENGTH_LONG).show();
//            }
//        }catch(Exception e){
//
//        }
    }
    protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
        InputStream in = entity.getContent();
        StringBuffer out = new StringBuffer();
        int n = 1;
        while (n>0) {
            byte[] b = new byte[4096];
            n =  in.read(b);
            if (n>0) out.append(new String(b, 0, n));
        }
        return out.toString();
    }
    public String getString(String object,JSONObject jsonobject) throws JSONException {
        String data="";
        if(jsonobject.has(object)){
            data= jsonobject.getString(object);
            return data;
        }
        return data;
    }
}
