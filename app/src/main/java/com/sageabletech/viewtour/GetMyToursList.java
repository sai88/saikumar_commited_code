package com.sageabletech.viewtour;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;


import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.MyTourList;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by ASMAN on 24-07-2017.
 */

public class GetMyToursList extends AsyncTask<Void, Void, String> {
    ProgressDialog dialog;
    Activity activity;
    PreferenceUtils preferenceUtils;

    public GetMyToursList(Activity activity) {
        this.activity = activity;
        preferenceUtils = new PreferenceUtils(activity);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(activity);
        dialog.setMessage("please wait..");
        dialog.show();
    }

    @Override
    protected String doInBackground(Void... params) {
        HttpClient httpClient = new DefaultHttpClient();

        HttpContext localContext = new BasicHttpContext();
        HttpPost httppost = new HttpPost(Urls.MY_TOUR + preferenceUtils.getStringFromPreference("accessToken", ""));

        httppost.setHeader("Content-Type", "application/json");

        try {
            URL url = httppost.getURI().toURL();
            System.out.println("urlddd::" + url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        String text = "";
        try {
            HttpResponse response = httpClient.execute(httppost, localContext);
            HttpEntity entity = response.getEntity();
            text = getASCIIContentFromEntity(entity);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return e.getLocalizedMessage();
        }
        return text;

    }

    HashMap<String, Object> totalHash = new HashMap<>();

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        dialog.dismiss();
        LocalData.getInstance().setSelectedMyTourItemDO(null);
        System.out.println("datat got from favorite List :::" + s);
        try {
            JSONObject jsonObject = new JSONObject(s);
            String message = jsonObject.getString("message");
            if (message.equalsIgnoreCase("success")) {
                JSONArray jsondata = jsonObject.optJSONArray("data");

                ArrayList<MyTourItemDO> mFeeds = new ArrayList<>();

                for (int i = 0; i < jsondata.length(); i++) {
                    JSONObject jo = jsondata.getJSONObject(i);
                    MyTourItemDO mfeeds = new MyTourItemDO();
                    mfeeds.tourPhotoPath = jo.optString("tourPhotoPath");
                    mfeeds.title = jo.optString("title");
                    mfeeds.summary = jo.optString("summary");
                    mfeeds.tourLatitude = jo.optString("tourLatitude");
                    mfeeds.tourLangitude = jo.optString("tourLangitude");
                    mfeeds.viewCount = jo.optString("viewCount");
                    mfeeds.comments = jo.optString("comments");
                    mfeeds.area = jo.optString("area");
                    mfeeds.city = jo.optString("city");
                    mfeeds.tourId = jo.optString("tourId");
                    mfeeds.tourRating = jo.optString("tourRating");
                    mfeeds.userId = jo.optString("userId");
                    mfeeds.createdOn = jo.optString("createdOn");
                    mfeeds.unPublishFlag = jo.optString("unPublishFlag");


                    JSONArray photsArray = jo.optJSONArray("photos");
                    ArrayList<TourItem> subtour = new ArrayList<>();
                    TourItem[] tourItemsArry=new TourItem[photsArray.length()];
                    for (int j = 0; j < photsArray.length(); j++) {

                        JSONObject joj = photsArray.getJSONObject(j);
                        TourItem ti = new TourItem();
                        System.out.println("photo pathsss:::" + joj.optString("photoPath"));
                        ti.ImagePath = joj.optString("photoPath");
                        ti.text = joj.optString("photoTitle");
                        ti.title = joj.optString("photoTitle");
                        ti.audioPath = joj.optString("photoAudioPath");
                        ti.summary = joj.optString("photoCaption");
                        ti.photoCaption = joj.optString("photoCaption");
                        ti.photoId = joj.optString("photoId");
                        ti.location = joj.optString("photoLocation");
                        ti.latitude = joj.optString("photolatitude");
                        ti.longitude = joj.optString("photolangitude");
                        ti.tourLatitude = joj.getString("photolatitude");
                        ti.tourLangitude = joj.getString("photolangitude");
                        ti.tourId = jo.optString("tourId");
                        addDataWithTourOrder(joj,subtour,ti,tourItemsArry);
//
                    }
                    if(tourOrderNumberflag) {
                        Collections.addAll(subtour, tourItemsArry);
                    }
                    mfeeds.subtour = subtour;

                    JSONArray categoriesListArray = jo.optJSONArray("categoriesList");
                    ArrayList<String> caterorieArry = new ArrayList<String>();
                    ArrayList<String> caterorieArryId = new ArrayList<String>();
                    for (int cat = 0; cat < categoriesListArray.length(); cat++) {
                        JSONObject data = categoriesListArray.getJSONObject(cat);
                        String categorieId = data.getString("categorieId");
                        String categorieName = data.getString("categorieName");
                        caterorieArry.add("#" + categorieName);
                        caterorieArryId.add(categorieId);

                    }
                    mfeeds.catArry = caterorieArry;
                    mfeeds.catArryId = caterorieArryId;

                    JSONArray tagsListArry = jo.optJSONArray("tagsList");
                    ArrayList<String> tagsArry = new ArrayList<String>();
                    ArrayList<String> tagsArryId = new ArrayList<String>();
                    for (int ta = 0; ta < tagsListArry.length(); ta++) {
                        JSONObject data = tagsListArry.getJSONObject(ta);
                        String tagId = data.getString("tagId");
                        String tagName = data.getString("tagName");
                        tagsArry.add("#" + tagName);
                        tagsArryId.add(tagId);

                    }
                    mfeeds.tagArry = tagsArry;
                    mfeeds.tagArryId = tagsArryId;

                    mFeeds.add(mfeeds);


                }
                LocalData.getInstance().setmFeeds(mFeeds);
//                activity.startActivity(new Intent(activity, NewGragment.class));
                activity.startActivity(new Intent(activity, MyTourList.class).putExtra("From", "MyTour"));
            } else {
                Toast.makeText(activity, "Could Load mytours ", Toast.LENGTH_SHORT).show();
            }

//                JSONObject data = jsonObject.getJSONObject("data");

//                String title = getString("title",data);
//                String summary = getString("summary",data);
//                String address = getString("address",data);
//                String area = getString("area",data);
//                String city = getString("city",data);
//                String tourLatitude = getString("tourLatitude",data);
//                String tourLangitude = getString("tourLangitude",data);
//                String tourPhotoPath = getString("tourPhotoPath",data);
//
//                HashMap<String,String> hash=new HashMap<>();
//                hash.put("title",title);
//                hash.put("summary",summary);
//                hash.put("address",address);
//                hash.put("area",area);
//                hash.put("city",city);
//                hash.put("tourLatitude",tourLatitude);
//                hash.put("tourLangitude",tourLangitude);
//                hash.put("tourPhotoPath",tourPhotoPath);
//                totalHash.put("main",hash);
//
//                LocalData.getInstance().setTotalHash(totalHash);
//
//            }else{
//                Toast.makeText(activity,"Try atfer some time.",Toast.LENGTH_LONG).show();
//            }
        } catch (Exception e) {
            System.out.println("exception::::" + e.toString());
        }
    }

    boolean tourOrderNumberflag=false;

    public void addDataWithTourOrder(JSONObject joj,ArrayList<TourItem> subtour,TourItem ti ,TourItem[] tourItemsArry){
        try {
            if (joj.has("tourOrderNumber")) {
                Object tourOrderNumber1 = joj.get("tourOrderNumber");
                boolean nullObject = joj.isNull("tourOrderNumber");
                System.out.println("nullObject::" + nullObject);
                if (tourOrderNumber1 != null && !nullObject) {
                    int tourOrderNumber = joj.getInt("tourOrderNumber");
                    tourOrderNumber= tourOrderNumber-1;
//
                    tourItemsArry[tourOrderNumber]=ti;
                    tourOrderNumberflag=true;
                } else {
                    subtour.add(ti);
                }
            } else {
                subtour.add(ti);
            }

        }catch (Exception e) {
            System.out.println("exception in subbb::"+e.toString());
        }



    }

    protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
        InputStream in = entity.getContent();
        StringBuffer out = new StringBuffer();
        int n = 1;
        while (n > 0) {
            byte[] b = new byte[4096];
            n = in.read(b);
            if (n > 0) out.append(new String(b, 0, n));
        }
        return out.toString();
    }

    public String getString(String object, JSONObject jsonobject) throws JSONException {
        String data = "";
        if (jsonobject.has(object)) {
            data = jsonobject.getString(object);
            return data;
        }
        return data;
    }
}
