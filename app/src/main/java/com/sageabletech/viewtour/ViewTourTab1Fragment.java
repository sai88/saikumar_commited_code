package com.sageabletech.viewtour;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import com.sageabletech.R;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.MapsActivity;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;

import java.util.ArrayList;

/**
 * Created by ASMAN on 26-07-2017.
 */

public class ViewTourTab1Fragment extends Fragment{

    RecyclerView recyclerView;
    MyTourAdapter MyTourAdapter;
    MyTourItemDO tourSeletedItem;
    ArrayList<TourItem> subtourMain;
    public static ViewTourTab1Fragment newInstance() {

        ViewTourTab1Fragment fragment = new ViewTourTab1Fragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.edittab1, container, false);
        recyclerView = (RecyclerView) inflate.findViewById(R.id.respodedList);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        tourSeletedItem = LocalData.getInstance().getTourSeletedItem();


        MyTourItemDO favselectedItem = LocalData.getInstance().getFavselectedItem();
        subtourMain = favselectedItem.subtour;
        Log.e("dasfsdf","ArrayList got::"+subtourMain.toString());
        for (int i=0;i<subtourMain.size();i++){
            TourItem tourItem = subtourMain.get(i);
            if(tourItem!=null) {
                String tourLangitude = tourItem.tourLangitude;
                String tourLatitude = tourItem.tourLatitude;
                System.out.println("tourLangitude::" + tourLangitude);
                System.out.println("tourLangitude::%%%" + tourLatitude);
            }
        }



        Log.e("dsfdsf","subtour::"+ this.subtourMain.toString());


        MyTourAdapter = new MyTourAdapter(getActivity(), this.subtourMain);
        recyclerView.setAdapter(MyTourAdapter);
        return inflate;
    }

    private class MyTourAdapter extends RecyclerView.Adapter<MyTourAdapter.MyViewHolder> {

        private ArrayList<TourItem> mOffers;
        private Context mContext;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title,tvItemName,tvItemDesc;
            public ImageView ivImage,ivEdit,ivLocation,ivDelete,ivEye;
           public RelativeLayout listaction;


            public MyViewHolder(View view) {
                super(view);
                tvItemName = (TextView) view.findViewById(R.id.tvItemName);
                tvItemDesc = (TextView) view.findViewById(R.id.tvDesc);

                ivImage = (ImageView)view.findViewById(R.id.ivImage);
                ivEdit = (ImageView)view.findViewById(R.id.ivEdit);
                ivLocation = (ImageView)view.findViewById(R.id.ivLocation);
                ivEye = (ImageView)view.findViewById(R.id.ivEye);
                ivDelete = (ImageView)view.findViewById(R.id.ivDelete);
                listaction=(RelativeLayout)view.findViewById(R.id.listaction);
            }
        }

        public void refresh(ArrayList<TourItem> mOffers){

            this.mOffers = mOffers;
            this.notifyDataSetChanged();
        }


        public MyTourAdapter(Context mContext, ArrayList<TourItem> mOffers) {
            this.mOffers = mOffers;
            this.mContext = mContext;
            System.out.println("moffers.size::"+mOffers.size());
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.my_grid_itemnew, parent, false);

            RelativeLayout.LayoutParams lm = new RelativeLayout.LayoutParams(AppConstants.DEVICE_DISPLAY_WIDTH,
                    AppConstants.DEVICE_DISPLAY_HEIGHT/5);
            lm.setMargins(5,7,5,7);
            itemView.setLayoutParams(lm);

            return new MyViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            System.out.println(" data title::"+mOffers.get(position).title);
            System.out.println(" data::"+mOffers.get(position).summary);
            System.out.println(" data caption::"+mOffers.get(position).photoCaption);
holder.listaction.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        listAction(position);
    }
});


            holder.setIsRecyclable(false);
                holder.ivEdit.setVisibility(View.GONE);
                holder.ivDelete .setVisibility(View.GONE);
            if(mOffers.get(position).title.length()>0&&!mOffers.get(position).title.equalsIgnoreCase("No Text")){
                holder.tvItemName.setText(mOffers.get(position).title);
            }else{
                holder.tvItemName.setVisibility(View.INVISIBLE);
            }


            if(mOffers.get(position).photoCaption.length()>0&&!mOffers.get(position).photoCaption.equalsIgnoreCase("No Text")){
                holder.tvItemDesc.setText(mOffers.get(position).photoCaption);
            }else{
                holder.tvItemDesc.setVisibility(View.INVISIBLE);
            }

//            String loc = mOffers.get(position).tourLatitude+","+ mOffers.get(position).tourLangitude;

            System.out.println("mOffers.get(position)::"+mOffers.get(position).ImagePath);
            Glide.with(mContext).load(mOffers.get(position).ImagePath)
                    .placeholder(R.mipmap.app_icon)
                    .centerCrop()
                    .thumbnail(0.5f)
                    .crossFade()
//                    .override(AppConstants.DEVICE_DISPLAY_WIDTH/3, AppConstants.DEVICE_DISPLAY_HEIGHT/5)
                    .into(holder.ivImage);

            holder.ivImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    TourItem tourItem = mOffers.get(position);
                    LocalData.getInstance().setSelectedTourItem(tourItem);
                    LocalData.getInstance().setSelectedTourNumber(position);

//                        mContext.startActivity(new Intent(mContext, FavMainTour.class)
//                                .putExtra("Details",mOffers.get(position)));
//                    }else{
                    if(LocalData.getInstance().getFromWhere().equalsIgnoreCase("mapLeft")){
                        ProgressDialog dialog1=new ProgressDialog(mContext);
                        dialog1.setMessage("Loading...");
                        dialog1.show();
                        LocalData.getInstance().setDirectionroutenumber(position);
                        LocalData.getInstance().setNewToDirections("no");
                        LocalData.getInstance().setFromWhere("");
                        ((Activity) mContext).finish();
                        mContext.startActivity(new Intent(mContext, com.sageabletech.direction.MapsActivity.class));
                    }
                    else if(LocalData.getInstance().getFromWhere().equalsIgnoreCase("map")){
                        ProgressDialog dialog1=new ProgressDialog(mContext);
                        dialog1.setMessage("Loading...");
                        dialog1.show();
                        LocalData.getInstance().setNewToDirections("no");
                        LocalData.getInstance().setGetDirectionRight(position);
                        LocalData.getInstance().setFromWhere("");
                        ((Activity) mContext).finish();
                        mContext.startActivity(new Intent(mContext, com.sageabletech.direction.MapsActivity.class));
                    }else {
                        ProgressDialog dialog1=new ProgressDialog(mContext);
                        dialog1.setMessage("Loading...");
                        dialog1.show();
                        LocalData.getInstance().setViewDetailsDialog(dialog1);
                        mContext.startActivity(new Intent(mContext, ViewSubDetailsPageViewer.class));


                    }
                }
            });

            holder.ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    LocalData.getInstance().setFromedittour(true);
//                    MyTourItemDO myTourItemDO = mOffers.get(position);
//                    ArrayList<TourItem> subtour = myTourItemDO.subtour;
//                    System.out.println("subtour123:::"+subtour);
//
//                    LocalData.getInstance().setSelectedMyTourItemDO(myTourItemDO);
//                    ProgressDialog  dialog=new ProgressDialog(mContext);
//                    dialog.setMessage("please wait..");
//                    dialog.show();
//                    LocalData.getInstance().setProgressDialog(dialog);
//
//                    mContext.startActivity(new Intent(mContext, AddTourScreenNew.class)
//                            .putExtra("Details",myTourItemDO)
//                            .putExtra("TourPhotoPath",""+mOffers.get(position).tourPhotoPath)
//                            .putExtra("From","MyTourListNew"));
                }
            });
            holder.ivLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(holder.ivLocation.isPressed()) {
                        System.out.println("location::" + mOffers.get(position).tourLangitude);
                        System.out.println("location::" + mOffers.get(position).tourLatitude);
                        String tourLangitude = mOffers.get(position).tourLangitude;
                        String tourLatitude = mOffers.get(position).tourLatitude;
//                    System.out.println("location::"+mOffers.get(position).lang);
                        if (tourLangitude != null && !tourLangitude.equalsIgnoreCase("") && !tourLangitude.equalsIgnoreCase("0")) {

                            Intent i = new Intent(getActivity(), MapsActivity.class);
                            i.putExtra("tourLangitude", mOffers.get(position).tourLangitude);
                            i.putExtra("tourLatitude", mOffers.get(position).tourLatitude);
                            i.putExtra("title", mOffers.get(position).title);
                            startActivity(i);


                        } else if (tourSeletedItem.tourLangitude != null && !tourSeletedItem.tourLangitude.equalsIgnoreCase("")) {
                            Intent i = new Intent(getActivity(), MapsActivity.class);
                            i.putExtra("tourLangitude", tourSeletedItem.tourLangitude);
                            i.putExtra("tourLatitude", tourSeletedItem.tourLatitude);
                            if (mOffers.get(position).title == null) {
                                i.putExtra("title", "No Title");
                            } else if (mOffers.get(position).title.equalsIgnoreCase("")) {
                                i.putExtra("title", "No Title");

                            } else {
                                i.putExtra("title", mOffers.get(position).title);
                            }
                            getActivity().startActivity(i);

                        } else {
                            Toast.makeText(getActivity(), "No Location Found", Toast.LENGTH_SHORT).show();
                        }
                    }
//                    mContext.startActivity(new Intent(mContext, MapsActivity.class)
//                            .putExtra("tourLatitude",mOffers.get(position).tourLatitude)
//                            .putExtra("tourLangitude",mOffers.get(position).tourLangitude)
//                            .putExtra("title",mOffers.get(position).title));
                }
            });

            holder.ivEye.setVisibility(View.INVISIBLE);
            holder.ivEye.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if(LocalData.getInstance().getFromDrafts().equalsIgnoreCase("fav")){
                    TourItem tourItem = mOffers.get(position);
                        LocalData.getInstance().setSelectedTourItem(tourItem);
                    LocalData.getInstance().setSelectedTourNumber(position);

//                        mContext.startActivity(new Intent(mContext, FavMainTour.class)
//                                .putExtra("Details",mOffers.get(position)));
//                    }else{
                        mContext.startActivity(new Intent(mContext, ViewSubDetailsPageViewer.class));
//                    }

                }
            });



        }

        @Override
        public int getItemCount() {
            //mOffers.size();
            return mOffers.size();
        }
        public void listAction(int position){
            TourItem tourItem = mOffers.get(position);
            LocalData.getInstance().setSelectedTourItem(tourItem);
            LocalData.getInstance().setSelectedTourNumber(position);

//                        mContext.startActivity(new Intent(mContext, FavMainTour.class)
//                                .putExtra("Details",mOffers.get(position)));
//                    }else{
            if(LocalData.getInstance().getFromWhere().equalsIgnoreCase("mapLeft")){
                ProgressDialog dialog1=new ProgressDialog(mContext);
                dialog1.setMessage("Loading...");
                dialog1.show();
                LocalData.getInstance().setDirectionroutenumber(position);
                LocalData.getInstance().setNewToDirections("no");
                LocalData.getInstance().setFromWhere("");
                ((Activity) mContext).finish();
                mContext.startActivity(new Intent(mContext, com.sageabletech.direction.MapsActivity.class));
            }
            else if(LocalData.getInstance().getFromWhere().equalsIgnoreCase("map")){
                ProgressDialog dialog1=new ProgressDialog(mContext);
                dialog1.setMessage("Loading...");
                dialog1.show();

                LocalData.getInstance().setNewToDirections("no");
                LocalData.getInstance().setGetDirectionRight(position);
                LocalData.getInstance().setFromWhere("");
                ((Activity) mContext).finish();
                mContext.startActivity(new Intent(mContext, com.sageabletech.direction.MapsActivity.class));
            }else {
                ProgressDialog dialog1=new ProgressDialog(mContext);
                dialog1.setMessage("Loading...");
                dialog1.show();
                LocalData.getInstance().setViewDetailsDialog(dialog1);
                mContext.startActivity(new Intent(mContext, ViewSubDetailsPageViewer.class));
            }
        }
    }





}
