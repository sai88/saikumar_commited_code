package com.sageabletech.viewtour;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sageabletech.model.TourItem;


import java.util.ArrayList;

/**
 * Created by arige on 7/18/2017.
 */

public class ViewSubDetailsAdapter extends FragmentPagerAdapter {

    private ArrayList<TourItem>  list;

    public ViewSubDetailsAdapter(FragmentManager fm, ArrayList<TourItem>  list) {
        super(fm);
        this.list = list;
    }


    @Override
    public Fragment getItem(int position) {
        list.get(position);
        return ViewSubTourDetails.newInstance(list.get(position));
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
