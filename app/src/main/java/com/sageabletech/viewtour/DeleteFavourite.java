package com.sageabletech.viewtour;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.sageabletech.adapter.FeedsAdapter;
import com.sageabletech.model.FeedsDO;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ASMAN on 24-07-2017.
 */

public class DeleteFavourite extends AsyncTask <Void, Void, String>{
    ProgressDialog dialog;
    Activity activity;
    PreferenceUtils preferenceUtils;
    String tourId;
    String userId;
    List<FeedsDO> tours;
    int position;
    String isfrom;
    public DeleteFavourite(Activity activity, String tourId, String userId, int position, List<FeedsDO> tours,String isfrom) {
        this.activity=activity;
        this.tourId=tourId;
        preferenceUtils = new PreferenceUtils(activity);
        this.userId=userId;
        this.tours=tours;
        this.position=position;
        this.isfrom=isfrom;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog=new ProgressDialog(activity);
        dialog.setMessage("please wait..");
        dialog.show();
    }

    @Override
    protected String doInBackground(Void... params) {
        HttpClient httpClient = new DefaultHttpClient();

        HttpContext localContext = new BasicHttpContext();
        HttpPost httppost = new HttpPost(Urls.DELETE_FAVOURITE+preferenceUtils.getStringFromPreference("accessToken",""));

        httppost.setHeader("Content-Type", "application/json");
        JSONObject jsonObject=new JSONObject();
        JSONObject userJsonObject=new JSONObject();
        JSONObject tourJsonObject=new JSONObject();

        try {
            tourJsonObject.put("tourId",tourId);
            userJsonObject.put("userId",userId);
            jsonObject.put("favUser",userJsonObject);
            jsonObject.put("favTour",tourJsonObject);
            httppost.setEntity((new StringEntity(jsonObject.toString())));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
//

        System.out.println("customer dtata::"+jsonObject);
        String text = "";
        try {
            HttpResponse response = httpClient.execute(httppost,localContext);
            HttpEntity entity = response.getEntity();
            text = getASCIIContentFromEntity(entity);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return e.getLocalizedMessage();
        }
        return text;

    }
    HashMap<String,Object> totalHash=new HashMap<>();
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        dialog.dismiss();
        System.out.println("datat got from  delete favorite tour:::"+s);
        try {
            JSONObject jsonObject = new JSONObject(s);
            String message = jsonObject.getString("message");
            if(message.equalsIgnoreCase("The favourite is deleted successfully")) {
                Toast.makeText(activity,"Tour deleted from favorites",Toast.LENGTH_SHORT).show();
                tours.get(position).isfavorite="false";
                String favcount = tours.get(position).favcount;

                int i = Integer.parseInt(favcount);
                i--;

                tours.get(position).favcount=""+i;
//                FeedsAdapter feedsAdapter = LocalData.getInstance().getFeedsAdapter();
//                feedsAdapter.refreshAdd(tours,feedsAdapter);
                if(isfrom.equalsIgnoreCase("search")){
                    RecyclerView recycle = LocalData.getInstance().getSearchRecycle();
                    FeedsAdapter feedsAdapter=new FeedsAdapter(activity,tours,"search");
                    recycle.setAdapter(feedsAdapter);
                    recycle.getLayoutManager().scrollToPosition(position);

                }else {
                    RecyclerView recycle = LocalData.getInstance().getFeedFragmentRecycle();
                    FeedsAdapter feedsAdapter=new FeedsAdapter(activity,tours,"normal");
                    recycle.setAdapter(feedsAdapter);
                    recycle.getLayoutManager().scrollToPosition(position);
                }



            }else{
                Toast.makeText(activity,message,Toast.LENGTH_SHORT).show();
            }

//
        }catch(Exception e){

        }
    }
    protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
        InputStream in = entity.getContent();
        StringBuffer out = new StringBuffer();
        int n = 1;
        while (n>0) {
            byte[] b = new byte[4096];
            n =  in.read(b);
            if (n>0) out.append(new String(b, 0, n));
        }
        return out.toString();
    }
    public String getString(String object,JSONObject jsonobject) throws JSONException {
        String data="";
        if(jsonobject.has(object)){
            data= jsonobject.getString(object);
            return data;
        }
        return data;
    }
}
