package com.sageabletech.viewtour;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.sageabletech.R;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.newclassess.WordUtils;
import com.sageabletech.utils.LocalData;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Handler;

/**
 * Created by arige on 7/18/2017.
 */

public class ViewSubDetailsPageViewer extends FragmentActivity {

    private ViewPager viewPager;
    private ViewSubDetailsAdapter pagerAdapter;


    private LinearLayout dotLayout;
    private int mDotsCount, currentPage = 0;
    private ImageView[] image;

    ArrayList<TourItem> subtour;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subtour_details_viewpager);
        dataTobeCalled();
//        ViewSubDetailsPageViewer viewSubDetailsPageViewer = ;



    }
    public void dataTobeCalled(){
        MyTourItemDO favselectedItem = LocalData.getInstance().getFavselectedItem();
        subtour = favselectedItem.subtour;


        viewPager = (ViewPager) findViewById(R.id.viewPager);
//        back = (TextView) findViewById(R.id.back);

        pagerAdapter = new ViewSubDetailsAdapter(getSupportFragmentManager(), subtour);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(LocalData.getInstance().getSelectedTourNumber());


        dotLayout = (LinearLayout) findViewById(R.id.dotLayout);
        mDotsCount = subtour.size();
        image = new ImageView[mDotsCount];
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        // here we set the dots
        for (int i = 0; i < mDotsCount; i++) {
            image[i] = new ImageView(this);
            image[i].setBackgroundResource(R.mipmap.dot_active);
            params.setMargins(5, 0, 5, 0);
            image[i].setLayoutParams(params);
            dotLayout.addView(image[i]);
        }
        getHeader();


        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                for (int i = 0; i < mDotsCount; i++) {
                    image[i].setBackgroundResource(R.mipmap.dot);
                }
                image[position].setBackgroundResource(R.mipmap.dot_active);
                currentPage = position;
            }

            @Override
            public void onPageSelected(int position) {

                TourItem tourItem = subtour.get(position);
                String title = tourItem.title;
                System.out.println("title sai:::"+title);
                header.setText(title);
                MediaPlayer mediaPlayer = LocalData.getInstance().getMediaPlayer();
                SeekBar seekBar = LocalData.getInstance().getSeekBar();
                if(seekBar!=null)
                    seekBar.setProgress(0);
                if (mediaPlayer != null) {
                    boolean playing = mediaPlayer.isPlaying();
                    if (playing) {
                        mediaPlayer.pause();
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    TextView header;
    ImageView back;
    ImageView submit;

    public void getHeader() {
        header = (TextView) findViewById(R.id.header_text);
        header.setText(WordUtils.capitalize(subtour.get(LocalData.getInstance().getSelectedTourNumber()).title));
        back = (ImageView) findViewById(R.id.back);
        back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_back_white));
        submit = (ImageView) findViewById(R.id.submit);
        submit.setVisibility(View.GONE);

        back.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        MediaPlayer mediaPlayer = LocalData.getInstance().getMediaPlayer();
                                        SeekBar seekBar = LocalData.getInstance().getSeekBar();
                                        if (seekBar != null)
                                            seekBar.setProgress(0);


                                        if (mediaPlayer != null) {
                                            boolean playing = mediaPlayer.isPlaying();
                                            if (playing) {
                                                mediaPlayer.pause();

                                            }
                                        }
                                        if (LocalData.getInstance().getViewDetailsDialog() != null){
                                            ProgressDialog viewDetailsDialog = LocalData.getInstance().getViewDetailsDialog();
                                            if(viewDetailsDialog.isShowing()){
                                                viewDetailsDialog.dismiss();
                                            }

                                        }


               finish();

            }
        });


    }

    @Override
    public void onBackPressed() {
        MediaPlayer mediaPlayer = LocalData.getInstance().getMediaPlayer();
        SeekBar seekBar = LocalData.getInstance().getSeekBar();
        if (seekBar != null)
            seekBar.setProgress(0);


        if (mediaPlayer != null) {
            boolean playing = mediaPlayer.isPlaying();
            if (playing) {
                mediaPlayer.pause();

            }
        }
        if (LocalData.getInstance().getViewDetailsDialog() != null){
            ProgressDialog viewDetailsDialog = LocalData.getInstance().getViewDetailsDialog();
            if(viewDetailsDialog.isShowing()){
                viewDetailsDialog.dismiss();
            }

        }


      finish();
    }
}
