package com.sageabletech.viewtour;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;


import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TagsDo;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.MyTourList;
import com.sageabletech.shomiround.TagsActivity;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by ASMAN on 24-07-2017.
 */

public class SaveTag extends AsyncTask<Void, Void, String> {
    ProgressDialog dialog;
    Activity activity;
    PreferenceUtils preferenceUtils;
    String text;
    TagsActivity.CategoriesAdapter1 categoriesAdapter1;

    public SaveTag(Activity activity, String tagName, TagsActivity.CategoriesAdapter1 categoriesAdapter1) {
        this.activity = activity;
        preferenceUtils = new PreferenceUtils(activity);
        this.text = tagName.replaceAll(" ", "%20");
        this.categoriesAdapter1 = categoriesAdapter1;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(activity);
        dialog.setMessage("please wait..");
        dialog.show();
    }
    @Override
    protected String doInBackground(Void... params) {
        HttpClient httpClient = new DefaultHttpClient();

        HttpContext localContext = new BasicHttpContext();
        HttpPost httppost = new HttpPost(Urls.ADD_Tags+preferenceUtils.getStringFromPreference("accessToken",""));

        httppost.setHeader("Content-Type", "application/json");

        JSONObject tourJsonObject=new JSONObject();

        try {
            tourJsonObject.put("tagName",text);
            URL url = httppost.getURI().toURL();
            System.out.println("url of save comment::"+url.toString());


            httppost.setEntity((new StringEntity(tourJsonObject.toString())));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
//


        System.out.println("customer dtata::"+tourJsonObject);
        String text = "";
        try {
            HttpResponse response = httpClient.execute(httppost,localContext);
            HttpEntity entity = response.getEntity();
            text = getASCIIContentFromEntity(entity);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return e.getLocalizedMessage();
        }
        return text;

    }

    HashMap<String, Object> totalHash = new HashMap<>();
    boolean flag = false;

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        dialog.dismiss();
        System.out.println("datat saved  tags:::" + s);
        try {
            JSONObject jsonObject = new JSONObject(s);
            String message = jsonObject.getString("message");
            if(message.equalsIgnoreCase("Success")){
                categoriesAdapter1.disableCont();
                new GetsTagsDynamic(activity,text,categoriesAdapter1).execute();
            }else{
                Toast.makeText(activity,message,Toast.LENGTH_SHORT).show();
            }


//            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        } catch (Exception e) {

        }
    }


    protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
        InputStream in = entity.getContent();
        StringBuffer out = new StringBuffer();
        int n = 1;
        while (n > 0) {
            byte[] b = new byte[4096];
            n = in.read(b);
            if (n > 0) out.append(new String(b, 0, n));
        }
        return out.toString();
    }

    public String getString(String object, JSONObject jsonobject) throws JSONException {
        String data = "";
        if (jsonobject.has(object)) {
            data = jsonobject.getString(object);
            return data;
        }
        return data;
    }

}
