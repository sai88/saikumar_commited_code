package com.sageabletech.viewtour;

/**
 * Created by ASMAN on 08-09-2017.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.sageabletech.adapter.FeedsAdapter;
import com.sageabletech.model.FeedsDO;
import com.sageabletech.newclassess.CommentsPage;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;
import com.sageabletech.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PublishTour extends AsyncTask<Void, Void, String> {
    ProgressDialog dialog;
    Activity activity;
    PreferenceUtils preferenceUtils;
    String tourId;
    String comments;
    int publish;
    List<FeedsDO> tours;
    int position;
    

    public PublishTour(Activity activity, String tourId,int publish,List<FeedsDO> tours,int position) {
        this.activity=activity;
        this.tourId=tourId;
        this.publish=publish;
        this.tours=tours;
        this.position=position;
        preferenceUtils = new PreferenceUtils(activity);

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog=new ProgressDialog(activity);
        dialog.setMessage("please wait..");
        dialog.show();
    }

    @Override
    protected String doInBackground(Void... params) {
        HttpClient httpClient = new DefaultHttpClient();

        HttpContext localContext = new BasicHttpContext();
        HttpPost httppost = new HttpPost(Urls.UNPUBLISH+preferenceUtils.getStringFromPreference("accessToken",""));

        httppost.setHeader("Content-Type", "application/json");

        JSONObject tourJsonObject=new JSONObject();

        try {
            tourJsonObject.put("tourId",tourId);
            tourJsonObject.put("unPublishFlag",publish);

            URL url = httppost.getURI().toURL();
            System.out.println("url of get comment::"+url.toString());


            httppost.setEntity((new StringEntity(tourJsonObject.toString())));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
//


        System.out.println("customer dtata::"+tourJsonObject);
        String text = "";
        try {
            HttpResponse response = httpClient.execute(httppost,localContext);
            HttpEntity entity = response.getEntity();
            text = getASCIIContentFromEntity(entity);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return e.getLocalizedMessage();
        }
        return text;

    }
    HashMap<String,Object> totalHash=new HashMap<>();
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        dialog.dismiss();
        System.out.println("datat got from publish:::"+s);
        try{
            JSONObject jsonObject=new JSONObject(s);
            String message = jsonObject.getString("message");
            if(tours!=null) {
                tours.remove(position);

                FeedsAdapter feedsAdapter = LocalData.getInstance().getFeedsAdapter();
                feedsAdapter.refresh(tours);
            }

            Toast.makeText(activity,message,Toast.LENGTH_SHORT).show();

        }catch (JSONException e){

        }



    }
    protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
        InputStream in = entity.getContent();
        StringBuffer out = new StringBuffer();
        int n = 1;
        while (n>0) {
            byte[] b = new byte[4096];
            n =  in.read(b);
            if (n>0) out.append(new String(b, 0, n));
        }
        return out.toString();
    }
    public String getString(String object,JSONObject jsonobject) throws JSONException {
        String data="";
        if(jsonobject.has(object)){
            data= jsonobject.getString(object);
            return data;
        }
        return data;
    }
}
