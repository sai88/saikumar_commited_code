package com.sageabletech.viewtour;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.sageabletech.adapter.FeedsAdapter;
import com.sageabletech.model.CategoryDO;
import com.sageabletech.model.FeedsDO;
import com.sageabletech.newclassess.CommentsPage;
import com.sageabletech.newclassess.FeedSearch;
import com.sageabletech.newclassess.FilterSearch;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;
import com.sageabletech.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.R.attr.tag;
import static android.R.id.message;

/**
 * Created by ASMAN on 24-07-2017.
 */

public class GetCategories extends AsyncTask <Void, Void, String>{
    ProgressDialog dialog;
    Activity activity;
    PreferenceUtils preferenceUtils;
    String tourId;
    String comments;
    String type;

    public GetCategories(Activity activity,String type) {
        this.activity=activity;
        preferenceUtils = new PreferenceUtils(activity);
        this.type=type;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog=new ProgressDialog(activity);
        dialog.setMessage("please wait..");
        dialog.show();
    }

    @Override
    protected String doInBackground(Void... params) {
        HttpClient httpClient = new DefaultHttpClient();

        HttpContext localContext = new BasicHttpContext();
        HttpPost httppost = new HttpPost(Urls.CATEGORIES_LIST + preferenceUtils.getStringFromPreference("accessToken", ""));

        httppost.setHeader("Content-Type", "application/json");




//



        String text = "";
        try {
            HttpResponse response = httpClient.execute(httppost,localContext);
            HttpEntity entity = response.getEntity();
            text = getASCIIContentFromEntity(entity);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return e.getLocalizedMessage();
        }
        return text;

    }
    HashMap<String,Object> totalHash=new HashMap<>();
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        dialog.dismiss();
        System.out.println("datat got get comment:::"+s);
        try{
            JSONObject jsonObjectMain=new JSONObject(s);
            if (jsonObjectMain.toString().contains("Success")) {

                ArrayList<CategoryDO> mCategoryDOs = new ArrayList<>();

                System.out.println("jsonObjectMain::" + jsonObjectMain);

                JSONArray jsondata = jsonObjectMain.optJSONArray("data");

                for (int i = 0; i < jsondata.length(); i++) {

                    CategoryDO mCategoryDO = new CategoryDO();
                    JSONObject jo = jsondata.getJSONObject(i);
                    mCategoryDO.categorieId = jo.optString("categorieId");
                    mCategoryDO.categorieName = jo.optString("categorieName");
                    mCategoryDO.categorieImageLocation = jo.optString("categorieImageLocation");

                    mCategoryDOs.add(mCategoryDO);
                }
                LocalData.getInstance().setmCategoryDOs(mCategoryDOs);
                if(type.equalsIgnoreCase("feed")){
                    Intent intent = new Intent(activity, FeedSearch.class);
                    activity.startActivity(intent);
                }else {
                    Intent intent = new Intent(activity, FilterSearch.class);
                    activity.startActivity(intent);
                }

                System.out.println("listCategories::" + jsondata);
            } else {
                String message = jsonObjectMain.getString("message");
                Toast.makeText(activity, message,Toast.LENGTH_SHORT).show();
            }


        }catch (JSONException e){

        }



    }
    protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
        InputStream in = entity.getContent();
        StringBuffer out = new StringBuffer();
        int n = 1;
        while (n>0) {
            byte[] b = new byte[4096];
            n =  in.read(b);
            if (n>0) out.append(new String(b, 0, n));
        }
        return out.toString();
    }
    public String getString(String object,JSONObject jsonobject) throws JSONException {
        String data="";
        if(jsonobject.has(object)){
            data= jsonobject.getString(object);
            return data;
        }
        return data;
    }
}
