package com.sageabletech.viewtour;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sageabletech.R;
import com.sageabletech.direction.MapsActivity;
import com.sageabletech.fragment.ViewTourGrid;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.newclassess.WordUtils;
import com.sageabletech.shomiround.BaseActivity;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.Utils;

import java.util.ArrayList;

public class ViewTourGridList extends BaseActivity {
    RecyclerView recyclerView;
    ViewTourListAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    MyTourItemDO favselectedItem;
    ImageView back, right;
    String from_where = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_tour_list);

        favselectedItem = LocalData.getInstance().getFavselectedItem();
        TextView tvTitle = (TextView) findViewById(R.id.header_text);
        tvTitle.setText(WordUtils.capitalize(favselectedItem.title));
        tvTitle.setTypeface(Utils.getHelvaticaBold(ViewTourGridList.this));
        back = (ImageView) findViewById(R.id.back);
        back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_back_white));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
//                }

            }
        });
        from_where = LocalData.getInstance().getFromWhere();
        right = (ImageView) findViewById(R.id.submit);


        right.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_route_outline_white));

        ArrayList<TourItem> subtour = favselectedItem.subtour;
        if (subtour.size() != 0) {
            right.setVisibility(View.VISIBLE);
        } else {
            right.setVisibility(View.INVISIBLE);
        }
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("my conditon::");
//                if(LocalData.getInstance().getFromDrafts().equalsIgnoreCase("no")){
                ProgressDialog  dialog1=new ProgressDialog(ViewTourGridList.this);
                dialog1.setMessage("please wait..");
                dialog1.show();
//                }
                ArrayList<TourItem> subtour = favselectedItem.subtour;


                LocalData.getInstance().setGetDirectionRight(1);
                LocalData.getInstance().setDirectionroutenumber(0);
                LocalData.getInstance().setNewToDirections("yes");
                LocalData.getInstance().setFromWhere("");

                Intent intent = new Intent(ViewTourGridList.this, MapsActivity.class);
                startActivity(intent);

            }
        });


        System.out.println("sdfsdfdsfdsfdsf");

        mSectionsPagerAdapter = new ViewTourListAdapter(getSupportFragmentManager(), this);

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        final ImageView iv1 = new ImageView(this);
        iv1.setImageResource(R.mipmap.ic_list_solid_blue);
        iv1.setPadding(15, 15, 15, 15);

        final ImageView iv2 = new ImageView(this);
        iv2.setImageResource(R.mipmap.ic_grid_solid_blue);
        iv2.setPadding(15, 15, 15, 15);

        tabLayout.getTabAt(0).setCustomView(iv1);
        tabLayout.getTabAt(1).setCustomView(iv2);

        if (from_where.equalsIgnoreCase("mapleft")) {
            right.setVisibility(View.GONE);
            tvTitle.setText("Select Origin");
            tvTitle.setTypeface(Utils.getHelvaticaBold(ViewTourGridList.this));

        }
        if (from_where.equalsIgnoreCase("map")) {
            right.setVisibility(View.GONE);
            tvTitle.setText("Select Destination");
            tvTitle.setTypeface(Utils.getHelvaticaBold(ViewTourGridList.this));

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if(from_where.equalsIgnoreCase("mapleft")||from_where.equalsIgnoreCase("map")) {
            finish();
        }else{
            Intent  intent=new Intent(ViewTourGridList.this,FavMainTour.class);
            startActivity(intent);
        }


        }

}
