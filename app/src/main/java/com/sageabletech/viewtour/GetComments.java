package com.sageabletech.viewtour;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.sageabletech.adapter.FeedsAdapter;
import com.sageabletech.model.FeedsDO;
import com.sageabletech.newclassess.CommentsPage;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;
import com.sageabletech.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ASMAN on 24-07-2017.
 */

public class GetComments extends AsyncTask <Void, Void, String>{
    ProgressDialog dialog;
    Activity activity;
    PreferenceUtils preferenceUtils;
    String tourId;
    String comments;

    public GetComments(Activity activity, String tourId) {
        this.activity=activity;
        this.tourId=tourId;
        preferenceUtils = new PreferenceUtils(activity);

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog=new ProgressDialog(activity);
        dialog.setMessage("please wait..");
        dialog.show();
    }

    @Override
    protected String doInBackground(Void... params) {
        HttpClient httpClient = new DefaultHttpClient();

        HttpContext localContext = new BasicHttpContext();
        HttpPost httppost = new HttpPost(Urls.GET_COMMENTS+preferenceUtils.getStringFromPreference("accessToken",""));

        httppost.setHeader("Content-Type", "application/json");

        JSONObject tourJsonObject=new JSONObject();

        try {
            tourJsonObject.put("tourid",tourId);
            URL url = httppost.getURI().toURL();
            System.out.println("url of get comment::"+url.toString());


            httppost.setEntity((new StringEntity(tourJsonObject.toString())));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
//


        System.out.println("customer dtata::"+tourJsonObject);
        String text = "";
        try {
            HttpResponse response = httpClient.execute(httppost,localContext);
            HttpEntity entity = response.getEntity();
            text = getASCIIContentFromEntity(entity);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return e.getLocalizedMessage();
        }
        return text;

    }
    HashMap<String,Object> totalHash=new HashMap<>();
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        dialog.dismiss();
        System.out.println("datat got get comment:::"+s);
        try{
            JSONObject jsonObject=new JSONObject(s);
            ArrayList<HashMap<String,String>> commentsArry=new ArrayList<>();
            String message = jsonObject.getString("message");
            if(message.equalsIgnoreCase("success")){
                JSONObject data = jsonObject.getJSONObject("data");
                JSONArray clist = data.getJSONArray("clist");
                for (int i=0;i<clist.length();i++){
                    HashMap<String,String> hash=new HashMap<>();
                    JSONObject dataObj = clist.getJSONObject(i);
                    String tourid = dataObj.getString("tourid");
                    String comment = dataObj.getString("comment");
                    String userid = dataObj.getString("userid");
                    String userFullName = dataObj.getString("userFullName");
                    String commentDate = dataObj.getString("commentDate");
                    String timeAgo = Utils.getTimeAgo(commentDate);
                    commentDate=timeAgo;
                    String profilePicPath = dataObj.getString("profilePicPath");
                    hash.put("tourid",tourid);
                    hash.put("comment",comment);
                    hash.put("userid",userid);
                    hash.put("userFullName",userFullName);
                    hash.put("commentDate",commentDate);
                    hash.put("profilePicPath",profilePicPath);
                    commentsArry.add(hash);




                }
                LocalData.getInstance().setTourCommentsArry(commentsArry);
                Intent intent=new Intent(activity, CommentsPage.class);
                activity.startActivity(intent);

            }else{
                Toast.makeText(activity,message,Toast.LENGTH_SHORT).show();
            }
        }catch (JSONException e){

        }



    }
    protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
        InputStream in = entity.getContent();
        StringBuffer out = new StringBuffer();
        int n = 1;
        while (n>0) {
            byte[] b = new byte[4096];
            n =  in.read(b);
            if (n>0) out.append(new String(b, 0, n));
        }
        return out.toString();
    }
    public String getString(String object,JSONObject jsonobject) throws JSONException {
        String data="";
        if(jsonobject.has(object)){
            data= jsonobject.getString(object);
            return data;
        }
        return data;
    }
}
