package com.sageabletech.viewtour;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sageabletech.*;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.MapsActivity;
import com.sageabletech.utils.AppConstants;
import com.sageabletech.utils.LocalData;

import java.util.ArrayList;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class ViewTourTab2Fragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    ArrayList<TourItem> subtour;


    public ViewTourTab2Fragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ViewTourTab2Fragment newInstance(int sectionNumber, Context ctx,String draftId) {

        ViewTourTab2Fragment fragment = new ViewTourTab2Fragment();

        return fragment;
    }
    ImageAdapter mImageAdapterGridView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.my_grid_fragment, container, false);

        GridView gallery = (GridView) rootView.findViewById(R.id.galleryGridView);

        subtour = LocalData.getInstance().getFavselectedItem().subtour;


        System.out.println("tourListINGrids::"+subtour.size());

        mImageAdapterGridView = new ImageAdapter(getActivity(),subtour);
        gallery.setAdapter(mImageAdapterGridView);

        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


            }
        });


        return rootView;
    }

    private class ImageAdapter extends BaseAdapter {

        /** The context. */
        private Activity context;
        ArrayList<TourItem> mFeeds1;

        /**
         * Instantiates a new image adapter.
         *
         * @param localContext
         *            the local context
         */
        public ImageAdapter(Activity localContext,ArrayList<TourItem> mFeeds) {
            context = localContext;
            this.mFeeds1 = mFeeds;
        }

        public int getCount() {
            if(this.mFeeds1 == null){
                return 0;
            }
            return this.mFeeds1.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            final ViewHolder holder;


                convertView = LayoutInflater.from(context).inflate(R.layout.mytour_grid_itemnew,null,false);
                holder = new ViewHolder();
                holder.img = (ImageView) convertView.findViewById(R.id.imageView1);
                holder.ivEdit = (ImageView) convertView.findViewById(R.id.ivEdit);
                holder.ivEdit.setImageDrawable(getResources().getDrawable(R.drawable.ic_eye_outline_blue));
//                android:src="@mipmap/ic_delete_outline_white"
                holder.ivLocation = (ImageView) convertView.findViewById(R.id.ivLocation);
                holder.ivDelete = (ImageView) convertView.findViewById(R.id.ivDelete);
                holder.totalCont = (FrameLayout) convertView.findViewById(R.id.totalCont);
                holder.ivLocation.setImageDrawable(getResources().getDrawable(R.mipmap.ic_map_outline_white));
                holder.ivEdit.setImageDrawable(getResources().getDrawable(R.mipmap.ic_eye_outline_white));




                holder.ivDelete.setVisibility(View.GONE);

            holder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);
            convertView.setLayoutParams(new GridView.LayoutParams((AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2, (AppConstants.DEVICE_DISPLAY_WIDTH / 3) - 2));

            //Check availablity in Local Db

            Glide.with(context)
                    .load(this.mFeeds1.get(position).ImagePath)
                    .centerCrop()
                    .placeholder(R.color.black)
                    .thumbnail(0.5f)
                    .crossFade()
                    .into(holder.img);
            holder.totalCont.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TourItem tourItem = mFeeds1.get(position);
                    LocalData.getInstance().setSelectedTourItem(tourItem);
                    LocalData.getInstance().setSelectedTourNumber(position);
//                        mContext.startActivity(new Intent(mContext, FavMainTour.class)
//                                .putExtra("Details",mOffers.get(position)));
//                    }else{
                    startActivity(new Intent(getActivity(), ViewSubDetailsPageViewer.class));
                }
            });
            holder.ivEdit.setVisibility(View.GONE);

            holder.ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("cleckedddddddd:::");
                    TourItem tourItem = mFeeds1.get(position);
                    LocalData.getInstance().setSelectedTourItem(tourItem);
                    LocalData.getInstance().setSelectedTourNumber(position);
//                        mContext.startActivity(new Intent(mContext, FavMainTour.class)
//                                .putExtra("Details",mOffers.get(position)));
//                    }else{
                    startActivity(new Intent(getActivity(), ViewSubDetailsPageViewer.class));

                }
            });

            holder.ivLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("cleckedddddddd:::");
                    MyTourItemDO tourSeletedItem = LocalData.getInstance().getTourSeletedItem();
                    String tourLangitude = mFeeds1.get(position).tourLangitude;

                    if(tourLangitude!=null&&!tourLangitude.equalsIgnoreCase("")&&!tourLangitude.equalsIgnoreCase("0")) {
                        Intent i = new Intent(getActivity(), MapsActivity.class);
                        i.putExtra("tourLangitude", mFeeds1.get(position).tourLangitude);
                        i.putExtra("tourLatitude", mFeeds1.get(position).tourLatitude);
                        i.putExtra("title", mFeeds1.get(position).title);
                        startActivity(i);
                    }else if(tourSeletedItem.tourLangitude!=null&&!tourSeletedItem.tourLangitude.equalsIgnoreCase("")) {
                        Intent i =new Intent(getActivity(), MapsActivity.class);
                        i.putExtra("tourLangitude",tourSeletedItem.tourLangitude);
                        i.putExtra("tourLatitude",tourSeletedItem.tourLatitude);
                        if(mFeeds1.get(position).title==null) {
                            i.putExtra("title","");
                        }else if(mFeeds1.get(position).title.equalsIgnoreCase("")){
                            i.putExtra("title","");

                        }else{
                            i.putExtra("title", mFeeds1.get(position).title);
                        }
                        getActivity().startActivity(i);

                    }else{
                        Toast.makeText(getActivity(),"No Location Found",Toast.LENGTH_SHORT).show();
                    }
//                    startActivity(new Intent(getActivity(), MyTourDetails.class)
//                            .putExtra("Details",ImageAdapter.this.mFeeds1.get(position)));
                }
            });


            return convertView;
        }



        public class ViewHolder{
            ImageView img,ivEdit,ivLocation,ivDelete;
            FrameLayout totalCont;

        }

    }





}