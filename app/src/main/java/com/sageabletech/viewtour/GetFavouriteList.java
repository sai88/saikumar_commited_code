package com.sageabletech.viewtour;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;


import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.MyTourList;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by ASMAN on 24-07-2017.
 */

public class GetFavouriteList extends AsyncTask<Void, Void, String> {
    ProgressDialog dialog;
    Activity activity;
    PreferenceUtils preferenceUtils;

    public GetFavouriteList(Activity activity) {
        this.activity = activity;
        preferenceUtils = new PreferenceUtils(activity);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(activity);
        dialog.setMessage("please wait..");
        dialog.show();
    }

    @Override
    protected String doInBackground(Void... params) {
        HttpClient httpClient = new DefaultHttpClient();

        HttpContext localContext = new BasicHttpContext();
        HttpPost httppost = new HttpPost(Urls.LIST_OF_FAVOURITE + preferenceUtils.getStringFromPreference("accessToken", ""));

        httppost.setHeader("Content-Type", "application/json");

        try {
            URL url = httppost.getURI().toURL();
            System.out.println("urlddd::" + url.getPath().toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        String text = "";
        try {
            HttpResponse response = httpClient.execute(httppost, localContext);
            HttpEntity entity = response.getEntity();
            text = getASCIIContentFromEntity(entity);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return e.getLocalizedMessage();
        }
        return text;

    }

    HashMap<String, Object> totalHash = new HashMap<>();

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        dialog.dismiss();
        System.out.println("datat got from favorite List :::" + s);
        try {
            JSONObject jsonObject = new JSONObject(s);

            String message = jsonObject.getString("message");
            if (message.equalsIgnoreCase("success")) {
                JSONArray jsondata = jsonObject.optJSONArray("data");

                ArrayList<MyTourItemDO> mFeeds = new ArrayList<>();
                for (int i = 0; i < jsondata.length(); i++) {
                    JSONObject jo = jsondata.getJSONObject(i);
                    MyTourItemDO mfeeds = new MyTourItemDO();
                    mfeeds.tourId = jo.optString("tourId");
                    mfeeds.userId = jo.optString("userId");
                    mfeeds.title = jo.optString("title");
                    mfeeds.summary = jo.optString("summary");
                    mfeeds.address = jo.optString("address");
                    mfeeds.area = jo.optString("area");
                    mfeeds.city = jo.optString("city");
                    mfeeds.tourLatitude = jo.optString("tourLatitude");
                    mfeeds.tourLangitude = jo.optString("tourLangitude");
                    mfeeds.tourPhotoPath = jo.optString("tourPhotoPath");
                    JSONArray categoriesList = jo.getJSONArray("categoriesList");
                    ArrayList<String> catArry=new ArrayList<>();
                    for (int cat=0;cat<categoriesList.length();cat++){
                        JSONObject catObject = categoriesList.getJSONObject(cat);
                        String categorieName = catObject.getString("categorieName");
                        catArry.add("#"+categorieName);

                    }
                    mfeeds.catArry=catArry;

                    JSONArray tagList = jo.getJSONArray("tagsList");
                    ArrayList<String> tagArry=new ArrayList<>();
                    for (int tag=0;tag<tagList.length();tag++){
                        JSONObject tagObject = tagList.getJSONObject(tag);
                        String tagName = tagObject.getString("tagName");
                        tagArry.add("#"+tagName);

                    }
                    mfeeds.tagArry=tagArry;
                    mfeeds.createdOn = jo.optString("createdOn");

                    JSONArray photsArray = jo.optJSONArray("photos");
                    ArrayList<TourItem> subtour = new ArrayList<>();
                    TourItem[] tourItemsArry = new TourItem[photsArray.length()];
                    for (int j = 0; j < photsArray.length(); j++) {

                        JSONObject joj = photsArray.getJSONObject(j);
                        TourItem ti = new TourItem();
                        System.out.println("photo pathsss:::" + joj.optString("photoPath"));
                        ti.photoId = joj.optString("photoId");
                        ti.text = joj.optString("photoTitle");
                        ti.title = joj.optString("photoTitle");
                        ti.summary = joj.optString("photoCaption");
                        ti.photoCaption = joj.optString("photoCaption");
                        ti.location = joj.optString("photoLocation");
                        ti.ImagePath = joj.optString("photoPath");
                        ti.audioPath = joj.optString("photoAudioPath");
                        ti.audioTime = joj.optString("audioTime");

                        ti.latitude = joj.optString("photolatitude");
                        ti.longitude = joj.optString("photolangitude");

                        addDataWithTourOrder(joj, subtour, ti, tourItemsArry);

                    }

                    if (tourOrderNumberflag) {
                        Collections.addAll(subtour, tourItemsArry);
                    }
                    mfeeds.subtour = subtour;


                    mFeeds.add(mfeeds);


                }
                ArrayList<MyTourItemDO> mFeedsNew = new ArrayList<>();
                for (int j = mFeeds.size() - 1; j >= 0; j--) {
                    mFeedsNew.add(mFeeds.get(j));
                }
                LocalData.getInstance().setFavoritesArray(mFeedsNew);
                LocalData.getInstance().setFromDrafts("Fav");
                activity.startActivity(new Intent(activity, MyTourList.class).putExtra("From", "MyTour"));
            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        } catch (Exception e) {

        }
    }

    boolean tourOrderNumberflag = false;

    public void addDataWithTourOrder(JSONObject joj, ArrayList<TourItem> subtour, TourItem ti, TourItem[] tourItemsArry) {
        try {
            if (joj.has("tourOrderNumber")) {
                Object tourOrderNumber1 = joj.get("tourOrderNumber");
                boolean nullObject = joj.isNull("tourOrderNumber");
                System.out.println("nullObject::" + nullObject);
                if (tourOrderNumber1 != null && !nullObject) {
                    int tourOrderNumber = joj.getInt("tourOrderNumber");
                    tourOrderNumber = tourOrderNumber - 1;
//
                    tourItemsArry[tourOrderNumber] = ti;
                    tourOrderNumberflag = true;
                } else {
                    subtour.add(ti);
                }
            } else {
                subtour.add(ti);
            }

        } catch (Exception e) {
            System.out.println("exception in subbb::" + e.toString());
        }


    }

    protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
        InputStream in = entity.getContent();
        StringBuffer out = new StringBuffer();
        int n = 1;
        while (n > 0) {
            byte[] b = new byte[4096];
            n = in.read(b);
            if (n > 0) out.append(new String(b, 0, n));
        }
        return out.toString();
    }

    public String getString(String object, JSONObject jsonobject) throws JSONException {
        String data = "";
        if (jsonobject.has(object)) {
            data = jsonobject.getString(object);
            return data;
        }
        return data;
    }

}
