package com.sageabletech.viewtour;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

/**
 * Created by WIN 7 on 4/11/2017.
 */
public class ViewTourListAdapter extends FragmentStatePagerAdapter {
    Context ctx;
    String draftId;
    ViewPager mViewPager;
    public ViewTourListAdapter(FragmentManager fm, Context ctx) {
        super(fm);
        this.ctx = ctx;
        this.draftId = draftId;
    }

    ViewTourTab1Fragment mMyTourList;
    ViewTourTab2Fragment myTourGridNew;
    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).

        if(position == 0){
               mMyTourList = ViewTourTab1Fragment.newInstance();
                return mMyTourList;

        }else{
                myTourGridNew = ViewTourTab2Fragment.newInstance(position + 1,ctx,draftId);
                return myTourGridNew;

        }




    }


    @Override
    public int getCount() {
        // Show 3 total pages.
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Grid";
            case 1:
                return "List";

        }
        return null;
    }



}
