package com.sageabletech.viewtour;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sageabletech.R;
import com.sageabletech.fragment.ExpandableTextView;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.newclassess.FullScreenViewActivity;
import com.sageabletech.newclassess.WordUtils;
import com.sageabletech.shomiround.MainScreen;
import com.sageabletech.shomiround.MapsActivity;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by ASMAN on 27-07-2017.
 */


public class FavMainTour extends FragmentActivity implements OnMapReadyCallback {
    private ImageView ivSelectedImage, ivMapTourDetails;

    private ExpandableTextView tvmainSummary;

    TextView catData, tagData;

    private ProgressBar progressBar;
    MyTourItemDO favselectedItem;
    Button buyTour;
    ArrayList<TourItem> subtour;
    @Override
    public void onBackPressed() {
//        LocalData.getInstance().setFromWhere("");
        startActivity(new Intent(FavMainTour.this, MainScreen.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favmaintour);

        favselectedItem = LocalData.getInstance().getTourSeletedItem();
       subtour = favselectedItem.subtour;


        Log.e("sdfdsf", "dfdsfdsf" + favselectedItem);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapTourDetails);
        mapFragment.getMapAsync(FavMainTour.this);

        progressBar = (ProgressBar) findViewById(R.id.imageprogressbar);
        ivSelectedImage = (ImageView) findViewById(R.id.ivSelectedImage);
        ivMapTourDetails = (ImageView) findViewById(R.id.ivMapTourDetails);
        buyTour = (Button) findViewById(R.id.buyTour);

        buyTour.setTypeface(Utils.getHelvaticaBold(FavMainTour.this));
        buyTour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMainTourBitMap();
                startActivity(new Intent(FavMainTour.this, ViewTourGridList.class));
            }
        });

        ivMapTourDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(FavMainTour.this, MapsActivity.class);
                i.putExtra("tourLangitude", favselectedItem.tourLangitude);
                i.putExtra("tourLatitude", favselectedItem.tourLatitude);
                i.putExtra("title", favselectedItem.title);
                startActivity(i);
            }
        });


        tvmainSummary = (ExpandableTextView) findViewById(R.id.tvmainSummary);
        catData = (TextView) findViewById(R.id.catData);
        tagData = (TextView) findViewById(R.id.tagData);
        if (favselectedItem != null) {

            ArrayList<String> catList = favselectedItem.catArry;
            ArrayList<String> tagList = favselectedItem.tagArry;
            if (catList.size() > 0) {
                String data = "";
                for (int i = 0; i < catList.size(); i++) {
                    String s = catList.get(i);
                    data = data + s;

                }
                catData.setText(data);
                catData.setTypeface(Utils.getHelvaticaMedium(FavMainTour.this));

            } else {
                catData.setVisibility(View.GONE);
            }
            if (tagList.size() > 0) {
                String data = "";
                for (int i = 0; i < tagList.size(); i++) {
                    String s = tagList.get(i);
                    data = data + s;

                }
                tagData.setText(data);
                tagData.setTypeface(Utils.getHelvaticaMedium(FavMainTour.this));

            } else {
                tagData.setVisibility(View.GONE);
            }


            String summary = favselectedItem.summary;

            if (!summary.equalsIgnoreCase("")) {
                tvmainSummary.setText(summary);
                tvmainSummary.setTypeface(Utils.getHelvaticaMedium(FavMainTour.this));
            } else {
                tvmainSummary.setVisibility(View.GONE);
            }
            ivSelectedImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                Bitmap bitmap;
//                BitmapDrawable drawable = (BitmapDrawable) ivSelectedImage.getDrawable();
                    Bitmap bitmap = getBitMap();
//                getBitMap
                    if (bitmap != null) {
//                    bitmap = ((BitmapDrawable) ivSelectedImage.getDrawable()).getBitmap();
                        System.out.println("bitmap:::" + bitmap);
//                tourItemsDO.bitmap=bitmap;
                        if (bitmap != null) {
                            System.out.println("data existsssssss");
                            LocalData.getInstance().setMainTourImage(bitmap);
                            LocalData.getInstance().setMapImageBitmap(bitmap);
                            ArrayList<Bitmap> arry = new ArrayList<Bitmap>();
                            arry.add(bitmap);
                            LocalData.getInstance().setDisplayImagesArry(arry);
                            Intent i = new Intent(FavMainTour.this, FullScreenViewActivity.class);
                            i.putExtra("position", 0);

                            startActivity(i);
                        } else {
                            Toast.makeText(FavMainTour.this, "Please wait until image loads", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(FavMainTour.this, "Please wait until image loads", Toast.LENGTH_SHORT).show();
                    }

                }
            });
            addData();
            getHeader();
            setTheMap();
            if(subtour.size()==0){
                buyTour.setVisibility(View.GONE);

            }
        }


    }

    Bitmap nextImageBitmap = null;

    public Bitmap getBitMap() {
        if (nextImageBitmap == null) {
            System.out.println("sdsdsdsddsdsfdfsgdfgfg");
            try {
                URL url = new URL(favselectedItem.tourPhotoPath);
                nextImageBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                return nextImageBitmap;
            } catch (IOException e) {
                System.out.println(e);
            }
            return nextImageBitmap;
        } else {
            return nextImageBitmap;
        }
    }

    public void getMainTourBitMap() {
        Bitmap bitmap;
        BitmapDrawable drawable = (BitmapDrawable) ivSelectedImage.getDrawable();
        if (drawable != null) {
            bitmap = ((BitmapDrawable) ivSelectedImage.getDrawable()).getBitmap();
            System.out.println("bitmap:::" + bitmap);
//                tourItemsDO.bitmap=bitmap;
            if (bitmap != null) {
                System.out.println("data existsssssss");
                LocalData.getInstance().setMainTourImage(bitmap);
                LocalData.getInstance().setMapImageBitmap(bitmap);
            }
        }
        LocalData.getInstance().setMapImagePath(favselectedItem.tourPhotoPath);
    }

    protected LocationManager locationManager;

    private void setTheMap() {

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapTourDetails);
        mapFragment.getMapAsync(FavMainTour.this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(this, "Not", Toast.LENGTH_LONG).show();
            return;
        }


//		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, this);

    }

    TextView header;
    ImageView back;
    ImageView submit;

    public void getHeader() {
        header = (TextView) findViewById(R.id.header_text);
        header.setText(WordUtils.capitalize(favselectedItem.title));
        header.setTypeface(Utils.getHelvaticaBold(FavMainTour.this));
        back = (ImageView) findViewById(R.id.back);
        back.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_back_white));
        submit = (ImageView) findViewById(R.id.submit);

        submit.setImageDrawable(getResources().getDrawable(R.mipmap.ic_ic_forward_circle));

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              startActivity(new Intent(FavMainTour.this, MainScreen.class));

            }
        });
//        submit.setBackgroundDrawable(getResources().getDrawable(R.drawable.rightarrow));
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMainTourBitMap();
                startActivity(new Intent(FavMainTour.this, ViewTourGridList.class));


            }
        });
        if(subtour.size()==0){
            submit.setVisibility(View.GONE);

        }


    }

    private void addData() {
        String tourPhotoPath = favselectedItem.tourPhotoPath;
        if (tourPhotoPath != null) {
//            Glide.with(this).load(tourPhotoPath)
//                    .listener(new RequestListener<String, GlideDrawable>() {
//                        @Override
//                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                            progressBar.setVisibility(View.GONE);
//                            return false;
//                        }
//
//                        @Override
//                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                            progressBar.setVisibility(View.GONE);
//                            return false;
//                        }
//                    })
////                            .placeholder(R.color.black)
//                    .centerCrop()
////                            .override((AppConstants.DEVICE_DISPLAY_WIDTH), (AppConstants.DEVICE_DISPLAY_HEIGHT / 3))
//                    .into(ivSelectedImage);

            Picasso.with(this)
                    .load(tourPhotoPath)
                    .fit().centerCrop()
                    .into(ivSelectedImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            progressBar.setVisibility(View.GONE);
                        }
                    });

        }
    }

    private GoogleMap googleMap;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        ArrayList<Marker> mMarkerArray = new ArrayList<Marker>();
        System.out.println("favselectedItem.tourLatitude::" + favselectedItem.tourLatitude + "::" + favselectedItem.tourLangitude);
        if (!favselectedItem.tourLatitude.isEmpty() && !favselectedItem.tourLangitude.isEmpty()) {

            LatLng latLng1 = new LatLng(Double.parseDouble(favselectedItem.tourLatitude),
                    Double.parseDouble(favselectedItem.tourLangitude));
            if (latLng1 != null) {
                Marker markers = googleMap.addMarker(new MarkerOptions().position(latLng1).title("Markers"));
                mMarkerArray.add(markers);
//                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng1));


            }
        }
        ArrayList<TourItem> subtour = favselectedItem.subtour;
        System.out.println("subtour array size:::" + subtour.size());
        for (int i = 0; i < subtour.size(); i++) {
            TourItem tourItem = subtour.get(i);
            if (tourItem != null) {
                String tourLatitude = tourItem.tourLatitude;
                String tourLangitude = tourItem.tourLangitude;
                System.out.println("tourLatitude %%%::" + tourLatitude);
                if (tourLangitude != null && !tourLangitude.equalsIgnoreCase("") && !tourLangitude.equalsIgnoreCase("0")) {
                    LatLng latLng1 = new LatLng(Double.parseDouble(tourLatitude),
                            Double.parseDouble(tourLangitude));
                    if (latLng1 != null) {
                        Marker markers = googleMap.addMarker(new MarkerOptions().position(latLng1).title("Markers"));
//                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng1));
                        mMarkerArray.add(markers);


                    }
                }
            }
        }

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : mMarkerArray) {
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.40);
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        googleMap.animateCamera(cu);


//        googleMap.animateCamera(CameraUpdateFactory.zoomTo(10));


    }



}
