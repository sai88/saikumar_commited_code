package com.sageabletech.viewtour;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;


import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TagsDo;
import com.sageabletech.model.TourItem;
import com.sageabletech.shomiround.MyTourList;
import com.sageabletech.shomiround.TagsActivity;
import com.sageabletech.utils.LocalData;
import com.sageabletech.utils.PreferenceUtils;
import com.sageabletech.utils.Urls;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by ASMAN on 24-07-2017.
 */

public class GetsTagsDynamic extends AsyncTask<Void, Void, String> {
    ProgressDialog dialog;
    Activity activity;
    PreferenceUtils preferenceUtils;
    String text;
    String directText;
    TagsActivity.CategoriesAdapter1 categoriesAdapter1;

    public GetsTagsDynamic(Activity activity, String text, TagsActivity.CategoriesAdapter1 categoriesAdapter1) {
        this.activity = activity;
        preferenceUtils = new PreferenceUtils(activity);
        directText=text;
        this.text = text;
        if(!text.trim().equalsIgnoreCase("")) {
            this.text = this.text.replaceAll(" ", "%20");
        }
        this.categoriesAdapter1 = categoriesAdapter1;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(activity);
        dialog.setMessage("please wait..");
        dialog.show();
    }

    @Override
    protected String doInBackground(Void... params) {
        HttpClient httpClient = new DefaultHttpClient();

        HttpContext localContext = new BasicHttpContext();
        HttpPost httppost = new HttpPost(Urls.SEARCH_Tags + preferenceUtils.getStringFromPreference("accessToken", "") + "?tagsearch=" + text);

        httppost.setHeader("Content-Type", "application/json");

        try {
            URL url = httppost.getURI().toURL();
            System.out.println("urlddd::" + url.getPath().toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        String text = "";
        try {
            HttpResponse response = httpClient.execute(httppost, localContext);
            HttpEntity entity = response.getEntity();
            text = getASCIIContentFromEntity(entity);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return e.getLocalizedMessage();
        }
        return text;

    }

    HashMap<String, Object> totalHash = new HashMap<>();
    boolean flag = false;

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        dialog.dismiss();
        System.out.println("datat got search tags:::" + s);
        try {
            JSONObject jsonObject = new JSONObject(s);

            if (jsonObject.toString().contains("Success")) {

                ArrayList<TagsDo> mTagDos = new ArrayList<>();

                System.out.println("jsonObjectMain::" + jsonObject);
                JSONObject datajsondata = jsonObject.optJSONObject("data");

                JSONArray jsondata = datajsondata.optJSONArray("tags");

                for (int i = 0; i < jsondata.length(); i++) {

                    TagsDo mTagsDo = new TagsDo();
                    JSONObject jo = jsondata.getJSONObject(i);
                    mTagsDo.tagId = jo.optString("tagId");
                    mTagsDo.tagName = jo.optString("tagName");
                    mTagsDo.tagSearch=directText;
                    if (text.equalsIgnoreCase(mTagsDo.tagName)||text.equalsIgnoreCase("")) {
                        flag = true;
                    }

                    mTagDos.add(mTagsDo);
                }
//                mDataListener.onData(mTagDos, null, tag);

                if (!flag) {
                 categoriesAdapter1.checkAddCont(directText);
                }
                System.out.println("mTagDos::"+mTagDos.size());
                categoriesAdapter1.doAction(mTagDos);
                categoriesAdapter1.mtagsnew = mTagDos;

                categoriesAdapter1.notifyDataSetChanged();


                System.out.println("listCategories::" + jsondata);
            } else {
//                mDataListener.onData(null, jsonObjectMain.optString("message"), tag);
            }
//                activity.startActivity(new Intent(activity, MyTourList.class).putExtra("From", "MyTour"));
//            }
        } catch (JSONException e1) {
            e1.printStackTrace();
            System.out.println("errororo::"+e1.toString());
        } catch (Exception e) {
            System.out.println("errororo::"+e.toString());
        }
    }

    boolean tourOrderNumberflag = false;

    public void addDataWithTourOrder(JSONObject joj, ArrayList<TourItem> subtour, TourItem ti, TourItem[] tourItemsArry) {
        try {
            if (joj.has("tourOrderNumber")) {
                Object tourOrderNumber1 = joj.get("tourOrderNumber");
                boolean nullObject = joj.isNull("tourOrderNumber");
                System.out.println("nullObject::" + nullObject);
                if (tourOrderNumber1 != null && !nullObject) {
                    int tourOrderNumber = joj.getInt("tourOrderNumber");
                    tourOrderNumber = tourOrderNumber - 1;
//
                    tourItemsArry[tourOrderNumber] = ti;
                    tourOrderNumberflag = true;
                } else {
                    subtour.add(ti);
                }
            } else {
                subtour.add(ti);
            }

        } catch (Exception e) {
            System.out.println("exception in subbb::" + e.toString());
        }


    }

    protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
        InputStream in = entity.getContent();
        StringBuffer out = new StringBuffer();
        int n = 1;
        while (n > 0) {
            byte[] b = new byte[4096];
            n = in.read(b);
            if (n > 0) out.append(new String(b, 0, n));
        }
        return out.toString();
    }

    public String getString(String object, JSONObject jsonobject) throws JSONException {
        String data = "";
        if (jsonobject.has(object)) {
            data = jsonobject.getString(object);
            return data;
        }
        return data;
    }

}
