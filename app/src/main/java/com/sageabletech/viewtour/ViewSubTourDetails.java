package com.sageabletech.viewtour;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sageabletech.R;
import com.sageabletech.model.MyTourItemDO;
import com.sageabletech.model.TourItem;
import com.sageabletech.newclassess.FullScreenViewActivity;
import com.sageabletech.shomiround.MapsActivity;
import com.sageabletech.utils.LocalData;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by Uday on 09-07-2017.
 */

public class ViewSubTourDetails extends Fragment {
    private ImageView ivSelectedImageSub;
    private ImageView ivMapTourDetailsSub;

    private TextView tv_title, tv_summary;
    ProgressBar progressDialog ;

    private LinearLayout llTitleCaption, contAudio;
    private TourItem tourItemsDO;
    private MediaPlayer mp;
    private ProgressBar progressBar;
    MyTourItemDO tourSeletedItem;


    public static ViewSubTourDetails newInstance(TourItem tourItemsDO) {
        ViewSubTourDetails fragment = new ViewSubTourDetails();
        Bundle args = new Bundle();
        args.putSerializable("tourItemsDO", tourItemsDO);
        fragment.setArguments(args);
        return fragment;
    }

    View rootView;
    LinearLayout buttonCont;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.sub_tour_selected_details, container, false);
        progressDialog=(ProgressBar) rootView.findViewById(R.id.progressss);
        buttonCont=(LinearLayout) rootView.findViewById(R.id.buttonCont);
        dataTobecalled();


        return rootView;
    }
    Bitmap nextImageBitmap=null;
    public Bitmap getBitMap(){
        if(nextImageBitmap==null) {
            System.out.println("sdsdsdsddsdsfdfsgdfgfg");
            try {
                URL url = new URL(tourItemsDO.ImagePath);
                nextImageBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                return nextImageBitmap;
            } catch (IOException e) {
                System.out.println(e);
            }
            return nextImageBitmap;
        }else {
            return nextImageBitmap;
        }
    }


    public void dataTobecalled(){
        tourItemsDO = (TourItem) getArguments().getSerializable("tourItemsDO");
        tourSeletedItem = LocalData.getInstance().getTourSeletedItem();

        //init views
        progressBar = (ProgressBar) rootView.findViewById(R.id.imageprogressbar);
        ivSelectedImageSub = (ImageView) rootView.findViewById(R.id.ivSelectedImageSub);
        ivSelectedImageSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap=getBitMap();
//                BitmapDrawable drawable = (BitmapDrawable) ivSelectedImageSub.getDrawable();
                if (bitmap != null) {
//                    bitmap = ((BitmapDrawable) ivSelectedImageSub.getDrawable()).getBitmap();
                    System.out.println("bitmap:::" + bitmap);
//                tourItemsDO.bitmap=bitmap;
                    if (bitmap != null) {
                        System.out.println("data existsssssss");
                        ArrayList<Bitmap> arry = new ArrayList<Bitmap>();
                        arry.add(bitmap);
                        LocalData.getInstance().setDisplayImagesArry(arry);
                        Intent i = new Intent(getActivity(), FullScreenViewActivity.class);
                        i.putExtra("position", 0);
                        getActivity().startActivity(i);
                    } else {
                        Toast.makeText(getActivity(), "Please wait until image loads..", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Please wait until image loads..", Toast.LENGTH_SHORT).show();
                }

            }
        });

        tv_title = (TextView) rootView.findViewById(R.id.tv_title);
        tv_summary = (TextView) rootView.findViewById(R.id.tv_summary);
        ivMapTourDetailsSub = (ImageView) rootView.findViewById(R.id.ivMapTourDetailsSub);

        llTitleCaption = (LinearLayout) rootView.findViewById(R.id.ll_title_caption1);
        contAudio = (LinearLayout) rootView.findViewById(R.id.ll_audio_play);



        System.out.println("tourItemsDO.ImagePath::" + tourItemsDO.ImagePath);

        if (tourItemsDO.ImagePath != null && !tourItemsDO.ImagePath.isEmpty()) {

            Picasso.with(getActivity())
                    .load(tourItemsDO.ImagePath)

                    .fit().centerCrop()


                    .into(ivSelectedImageSub, new Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            progressBar.setVisibility(View.GONE);
                        }
                    });


        }
        boolean flag = false;
        System.out.println("titleeeee:::"+tourItemsDO.title);
        if (tourItemsDO.title != null && !tourItemsDO.title.isEmpty() && !tourItemsDO.title.equalsIgnoreCase("") && !tourItemsDO.title.equalsIgnoreCase("No Text")) {
            llTitleCaption.setVisibility(View.VISIBLE);
            tv_title.setText(tourItemsDO.title);

            flag = true;

        } else {
            tv_title.setVisibility(View.GONE);

        }
        if (tourItemsDO.photoCaption != null && !tourItemsDO.photoCaption.isEmpty() && !tourItemsDO.photoCaption.equalsIgnoreCase("") && !tourItemsDO.summary.equalsIgnoreCase("No Text")) {
            tv_summary.setText("" + tourItemsDO.photoCaption);
            flag = true;
        } else {
            tv_summary.setVisibility(View.GONE);
        }
        if (!flag) {
            llTitleCaption.setVisibility(View.GONE);
        }

        ivMapTourDetailsSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("tourItemsDO.longitude::::"+tourItemsDO.tourLatitude);
                if (tourItemsDO.latitude != null && !tourItemsDO.latitude.equalsIgnoreCase("") && !tourItemsDO.latitude.equalsIgnoreCase("0")) {
                    ivMapTourDetailsSub.setVisibility(View.VISIBLE);
                    ivMapTourDetailsSub.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(getActivity(), MapsActivity.class);
                            i.putExtra("tourLangitude", tourItemsDO.longitude);
                            i.putExtra("tourLatitude", tourItemsDO.latitude);
                            if (tourItemsDO.title == null) {
                                i.putExtra("title", "No Title");
                            } else if (tourItemsDO.title.equalsIgnoreCase("")) {
                                i.putExtra("title", "No Title");

                            } else {
                                i.putExtra("title", tourItemsDO.title);
                            }
                            getActivity().startActivity(i);
                        }
                    });

                }
                else if (tourSeletedItem.tourLangitude != null && !tourSeletedItem.tourLangitude.equalsIgnoreCase("") && !tourSeletedItem.tourLangitude.equalsIgnoreCase("0")) {
                    Intent i = new Intent(getActivity(), MapsActivity.class);
                    i.putExtra("tourLangitude", tourSeletedItem.tourLangitude);
                    i.putExtra("tourLatitude", tourSeletedItem.tourLatitude);
                    if (tourItemsDO.title == null) {
                        i.putExtra("title", "No Title");
                    } else if (tourItemsDO.title.equalsIgnoreCase("")) {
                        i.putExtra("title", "No Title");

                    } else {
                        i.putExtra("title", tourItemsDO.title);
                    }
                    getActivity().startActivity(i);
                }
            }
        });
        if (tourItemsDO.audioPath != null && !tourItemsDO.audioPath.isEmpty()) {


            contAudio.setVisibility(View.VISIBLE);

                    getMuusicCont(tourItemsDO.audioPath);
//
        } else {
            contAudio.setVisibility(View.GONE);
        }

        System.out.println("tourItemsDO.photoAudioPath::" + tourItemsDO.audioPath);


    }

    ProgressBar dialog;





    private Button b2,b3;
    private MediaPlayer mediaPlayer;

    private double startTime = 0;
    private double finalTime = 0;

    private Handler myHandler = new Handler();;

    private SeekBar seekbar;
    private TextView tx1,tx2;
    ProgressDialog progressdialog;

    public static int oneTimeOnly = 0;
    public void getMuusicCont(final String path){
        b2 = (Button)rootView. findViewById(R.id.button2);
        b3 = (Button)rootView.findViewById(R.id.button3);
        b2.setEnabled(true);
        b3.setEnabled(true);
//
        tx1 = (TextView)rootView.findViewById(R.id.textView2);
        tx2 = (TextView)rootView.findViewById(R.id.textView3);


        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b3.setEnabled(false);
                b3.setVisibility(View.GONE);


//                Toast.makeText(getActivity(), "Playingsound",Toast.LENGTH_SHORT).show();
                if(mediaPlayer==null) {
                    playaudio(path);
                }
                mediaPlayer.start();
                b2.setVisibility(View.VISIBLE);
                b3.setVisibility(View.GONE);

                finalTime = mediaPlayer.getDuration();
                startTime = mediaPlayer.getCurrentPosition();

//                if (oneTimeOnly == 0) {
                    seekbar.setMax((int) finalTime);
                    oneTimeOnly = 1;
//                }

                tx2.setText(getTimeTotal());



                tx1.setText(getRunningTime());

                seekbar.setProgress((int)startTime);
                myHandler.postDelayed(UpdateSongTime,100);
                b2.setEnabled(true);

            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(), "Pausing sound",Toast.LENGTH_SHORT).show();
                System.out.println("caleddddd::::button 222");
                mediaPlayer.pause();
                b2.setEnabled(false);
                b2.setVisibility(View.GONE);
                b3.setVisibility(View.VISIBLE);
                b3.setEnabled(true);
            }
        });
        progressDialog.setVisibility(View.GONE);
        buttonCont.setVisibility(View.VISIBLE);

    }
public void playaudio(String path){
    System.out.println("path:::::activatedddd:::"+path);
    try {
        Uri uri = Uri.parse(path);
//
        mediaPlayer = MediaPlayer.create(getActivity(),uri);
//    mediaPlayer = new MediaPlayer();
//    mediaPlayer.setDataSource(path);

        LocalData.getInstance().setMediaPlayer(mediaPlayer);
    }catch (Exception e){
        System.out.println("exception eee::"+e.getMessage());

    }
    seekbar = (SeekBar)rootView.findViewById(R.id.seekBar);
    seekbar.setClickable(false);
    LocalData.getInstance().setSeekBar(seekbar);
    b2.setEnabled(false);

    finalTime = mediaPlayer.getDuration();
    tx2.setText(getTimeTotal());
}
    public String getTimeTotal(){
        long minutes = TimeUnit.MILLISECONDS.toMinutes((long) finalTime);
        long seconds = TimeUnit.MILLISECONDS.toSeconds((long) finalTime)-
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) finalTime));
        String sec = String.valueOf(seconds);
        String min = String.valueOf(minutes);

        if (sec.length() == 1) {
            sec = "0" + sec;
        }
        if (min.length() == 1) {
            min = "0" + min;
        }
        return "-"+min+":"+sec;

    }
    public String getTotalTimeDecrease(){
        long minutesTotal = TimeUnit.MILLISECONDS.toMinutes((long) finalTime);
        long secondstotal = TimeUnit.MILLISECONDS.toSeconds((long) finalTime)-
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) finalTime));

        long minutes = TimeUnit.MILLISECONDS.toMinutes((long) startTime);
        long seconds= TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) startTime));

        long presentMin=minutesTotal-minutes;
        long presentsec=secondstotal-seconds;

        String sec = String.valueOf(presentsec);
        String min = String.valueOf(presentMin);

        if (sec.length() == 1) {
            sec = "0" + sec;
        }
        if (min.length() == 1) {
            min = "0" + min;
        }
        return "-"+min+":"+sec;
    }
    public String getRunningTime(){
        long minutes = TimeUnit.MILLISECONDS.toMinutes((long) startTime);
        long seconds= TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) startTime));
        String sec = String.valueOf(seconds);
        String min = String.valueOf(minutes);

        if (sec.length() == 1) {
            sec = "0" + sec;
        }
        if (min.length() == 1) {
            min = "0" + min;
        }
        return min+":"+sec;

    }

    private Runnable UpdateSongTime = new Runnable() {
        public void run() {
            startTime = mediaPlayer.getCurrentPosition();
            tx1.setText(getRunningTime());
            tx2.setText(getTotalTimeDecrease());
            seekbar.setProgress((int)startTime);
            myHandler.postDelayed(this, 100);
        }
    };


}
