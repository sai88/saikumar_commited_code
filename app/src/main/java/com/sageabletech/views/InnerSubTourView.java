package com.sageabletech.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sageabletech.R;
import com.sageabletech.adapter.SubTourDetailsViewPagerAdapter;
import com.sageabletech.model.SubTourItemsDO;

import java.util.ArrayList;

/**
 * Created by arige on 7/18/2017.
 */

public class InnerSubTourView extends FragmentActivity{

    private ViewPager viewPager;
    private SubTourDetailsViewPagerAdapter pagerAdapter;


    private ArrayList<SubTourItemsDO> list;
    private LinearLayout dotLayout;
    private int mDotsCount, currentPage = 0;
    private ImageView[] image;
    private ImageView back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subtour_details_viewpager);



//        list = (ArrayList<SubTourItemsDO>) getIntent().getSerializableExtra("subtourList");
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        back = (ImageView) findViewById(R.id.back);

        pagerAdapter = new SubTourDetailsViewPagerAdapter(getSupportFragmentManager(),list);
        viewPager.setAdapter(pagerAdapter);



        dotLayout = (LinearLayout) findViewById(R.id.dotLayout);
        mDotsCount = list.size();
        image = new ImageView[mDotsCount];
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        // here we set the dots
        for (int i = 0; i < mDotsCount; i++) {
            image[i] = new ImageView(this);
            image[i].setBackgroundResource(R.mipmap.dot_active);
            params.setMargins(5, 0, 5, 0);
            image[i].setLayoutParams(params);
            dotLayout.addView(image[i]);
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                for (int i = 0; i < mDotsCount; i++) {
                    image[i].setBackgroundResource(R.mipmap.dot);
                }
                image[position].setBackgroundResource(R.mipmap.dot_active);
                currentPage = position;
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }
}
