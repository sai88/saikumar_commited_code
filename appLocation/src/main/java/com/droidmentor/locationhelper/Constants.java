package com.droidmentor.locationhelper;

/**
 * Created by ASMAN on 14-09-2017.
 */

public class Constants {
    static Constants constants=null;
    String locationText="";
    double latitude=0;
    double longitude=0;

    public String getLocationText() {
        return locationText;
    }

    public void setLocationText(String locationText) {
        this.locationText = locationText;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public static Constants getInstance(){
        if(constants==null){
            constants=new Constants();
            return constants;
        }
        return constants;
    }

}
