package com.droidmentor.locationhelper;

import android.app.Activity;
import android.location.Address;
import android.location.Location;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.droidmentor.locationhelper.LocationUtil.LocationHelper;

/**
 * Created by ASMAN on 14-09-2017.
 */

public class GetLocation {
    Activity activity;
    LocationHelper locationHelper;
    public GetLocation(Activity activity){
        this.activity=activity;
        locationHelper=new LocationHelper(activity);
        locationHelper.checkpermission();


    }
    private Location mLastLocation;
    double latitude;
    double longitude;


    public void getLocation(){
        mLastLocation=locationHelper.getLocation();

        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();
            Constants.getInstance().setLatitude(latitude);
            Constants.getInstance().setLongitude(longitude);

            getAddress();

        } else {


            showToast("Couldn't get the location. Make sure location is enabled on the device");
        }
    }

    public void showToast(String message)
    {
        Toast.makeText(activity,message,Toast.LENGTH_SHORT).show();
    }


    public void getAddress() {
        Address locationAddress;

        locationAddress = locationHelper.getAddress(latitude, longitude);

        if (locationAddress != null) {

            String address = locationAddress.getAddressLine(0);
            String address1 = locationAddress.getAddressLine(1);
            String city = locationAddress.getLocality();
            String state = locationAddress.getAdminArea();
            String country = locationAddress.getCountryName();
            String postalCode = locationAddress.getPostalCode();


            String currentLocation;

            if (!TextUtils.isEmpty(address)) {
                currentLocation = address;

                if (!TextUtils.isEmpty(address1))
                    currentLocation += "\n" + address1;

                if (!TextUtils.isEmpty(city)) {
                    currentLocation += "\n" + city;

                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += " - " + postalCode;
                } else {
                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += "\n" + postalCode;
                }

                if (!TextUtils.isEmpty(state))
                    currentLocation += "\n" + state;

                if (!TextUtils.isEmpty(country))
                    currentLocation += "\n" + country;


                Constants.getInstance().setLocationText(currentLocation);


            }

        }
    }

}
